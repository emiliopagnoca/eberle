﻿//Impalce
//Chgs 1.0.1

if (!document.registerElement)
{
    (function (e, t, n, r)
    {
        "use strict";
        function q(e, t) { for (var n = 0, r = e.length; n < r; n++) J(e[n], t) } function R(e) { for (var t = 0, n = e.length, r; t < n; t++) r = e[t], $(r, c[z(r)]) } function U(e) { return function (t) { g.call(L, t) && (J(t, e), q(t.querySelectorAll(h), e)) } } function z(e) { var t = e.getAttribute("is"); return d.call(l, t ? t.toUpperCase() : e.nodeName) } function W(e) { var t = e.currentTarget, n = e.attrChange, r = e.prevValue, i = e.newValue; t.attributeChangedCallback && e.attrName !== "style" && t.attributeChangedCallback(e.attrName, n === e.ADDITION ? null : r, n === e.REMOVAL ? null : i) } function X(e) { var t = U(e); return function (e) { t(e.target) } } function V(e, t) { var n = this; O.call(n, e, t), B.call(n, { target: n }) } function $(e, t) { N(e, t), I ? I.observe(e, _) : (H && (e.setAttribute = V, e[i] = F(e), e.addEventListener(u, B)), e.addEventListener(o, W)), e.createdCallback && (e.created = !0, e.createdCallback(), e.created = !1) } function J(e, t) { var n, r = z(e), i = "attached", s = "detached"; -1 < r && (C(e, c[r]), r = 0, t === i && !e[i] ? (e[s] = !1, e[i] = !0, r = 1) : t === s && !e[s] && (e[i] = !1, e[s] = !0, r = 1), r && (n = e[t + "Callback"]) && n.call(e)) } if (r in t) return; var i = "__" + r + (Math.random() * 1e5 >> 0), s = "extends", o = "DOMAttrModified", u = "DOMSubtreeModified", a = /^[A-Z][A-Z0-9]*(?:-[A-Z0-9]+)+$/, f = ["ANNOTATION-XML", "COLOR-PROFILE", "FONT-FACE", "FONT-FACE-SRC", "FONT-FACE-URI", "FONT-FACE-FORMAT", "FONT-FACE-NAME", "MISSING-GLYPH"], l = [], c = [], h = "", p = t.documentElement, d = l.indexOf || function (e) { for (var t = this.length; t-- && this[t] !== e;); return t }, v = n.prototype, m = v.hasOwnProperty, g = v.isPrototypeOf, y = n.defineProperty, b = n.getOwnPropertyDescriptor, w = n.getOwnPropertyNames, E = n.getPrototypeOf, S = n.setPrototypeOf, x = !!n.__proto__, T = n.create || function K(e) { return e ? (K.prototype = e, new K) : this }, N = S || (x ? function (e, t) { return e.__proto__ = t, e } : w && b ? function () { function e(e, t) { for (var n, r = w(t), i = 0, s = r.length; i < s; i++) n = r[i], m.call(e, n) || y(e, n, b(t, n)) } return function (t, n) { do e(t, n); while (n = E(n)); return t } }() : function (e, t) { for (var n in t) e[n] = t[n]; return e }), C = S || x ? function (e, t) { g.call(t, e) || $(e, t) } : function (e, t) { e[i] || (e[i] = n(!0), $(e, t)) }, k = e.MutationObserver || e.WebKitMutationObserver, L = (e.HTMLElement || e.Element || e.Node).prototype, A = L.cloneNode, O = L.setAttribute, M = t.createElement, _ = k && { attributes: !0, characterData: !0, attributeOldValue: !0 }, D = k || function (e) { H = !1, p.removeEventListener(o, D) }, P = !1, H = !0, B, j, F, I; k || (p.addEventListener(o, D), p.setAttribute(i, 1), p.removeAttribute(i), H && (B = function (e) { var t = this, n, r, s; if (t === e.target) { n = t[i], t[i] = r = F(t); for (s in r) { if (!(s in n)) return j(0, t, s, n[s], r[s], "ADDITION"); if (r[s] !== n[s]) return j(1, t, s, n[s], r[s], "MODIFICATION") } for (s in n) if (!(s in r)) return j(2, t, s, n[s], r[s], "REMOVAL") } }, j = function (e, t, n, r, i, s) { var o = { attrChange: e, currentTarget: t, attrName: n, prevValue: r, newValue: i }; o[s] = e, W(o) }, F = function (e) { for (var t, n, r = {}, i = e.attributes, s = 0, o = i.length; s < o; s++) t = i[s], n = t.name, n !== "setAttribute" && (r[n] = t.value); return r })), t[r] = function (n, r) { y = n.toUpperCase(), P || (P = !0, k ? (I = function (e, t) { function n(e, t) { for (var n = 0, r = e.length; n < r; t(e[n++])); } return new k(function (r) { for (var i, s, o = 0, u = r.length; o < u; o++) i = r[o], i.type === "childList" ? (n(i.addedNodes, e), n(i.removedNodes, t)) : (s = i.target, s.attributeChangedCallback && i.attributeName !== "style" && s.attributeChangedCallback(i.attributeName, i.oldValue, s.getAttribute(i.attributeName))) }) }(U("attached"), U("detached")), I.observe(t, { childList: !0, subtree: !0 })) : (t.addEventListener("DOMNodeInserted", X("attached")), t.addEventListener("DOMNodeRemoved", X("detached"))), t.addEventListener("readystatechange", function (e) { q(t.querySelectorAll(h), "attached") }), t.createElement = function (e, n) { var r, i = M.apply(t, arguments); return n && i.setAttribute("is", e = n.toLowerCase()), r = d.call(l, e.toUpperCase()), -1 < r && $(i, c[r]), i }, L.cloneNode = function (e) { var t = A.call(this, !!e), n = z(t); return -1 < n && $(t, c[n]), e && R(t.querySelectorAll(h)), t }); if (-1 < d.call(l, y)) throw new Error("A " + n + " type is already registered"); if (!a.test(y) || -1 < d.call(f, y)) throw new Error("The type " + n + " is invalid"); var i = function () { return t.createElement(p, u && y) }, o = r || v, u = m.call(o, s), p = u ? r[s] : y, g = l.push(y) - 1, y; return h = h.concat(h.length ? "," : "", u ? p + '[is="' + n.toLowerCase() + '"]' : p), i.prototype = c[g] = m.call(o, "prototype") ? o.prototype : T(L), q(t.querySelectorAll(h), "attached"), i }
    })(window, document, Object, "registerElement");
}

var corOver = "#bdcee0";
var corOver2 = "#bdcee0";
var corToolTip = "rgb(142, 196, 208)";
var corBorder = "#e1e1e1";
var corLinha = "rgb(142, 196, 208)";
var corFundoGrade = "#fafafa";
var fontFamily = "open_sansregular";

// CHAGAS
(function ()
{
    var _Chagas = Object.create(HTMLElement.prototype);
    _Chagas.novoButton = Object.create(HTMLDivElement.prototype);
    _Chagas.excluirButton = Object.create(HTMLDivElement.prototype);
    _Chagas.salvarButton = Object.create(HTMLDivElement.prototype);
    _Chagas.pesquisarButton = Object.create(HTMLDivElement.prototype);
    _Chagas.desfazerButton = Object.create(HTMLDivElement.prototype);
    _Chagas.primeiroButton = Object.create(HTMLDivElement.prototype);
    _Chagas.anteriorButton = Object.create(HTMLDivElement.prototype);
    _Chagas.proximoButton = Object.create(HTMLDivElement.prototype);
    _Chagas.ultimoButton = Object.create(HTMLDivElement.prototype);
    _Chagas.imprimirButton = Object.create(HTMLDivElement.prototype);
    _Chagas.digitalizarButton = Object.create(HTMLDivElement.prototype);
    _Chagas.divConteudo;
    _Chagas.mode = "";
    _Chagas.persistencia;
    _Chagas.semOnLoad = false;
    _Chagas.urlInicio = "";
    document.Chagas = _Chagas;

    _Chagas.createdCallback = function ()
    {
        var links = document.getElementsByTagName("link");
        var linkok = false;
        for (var x = 0 ; x < links.length; x++)
        {
            if (links[x].rel == "shortcut icon")
            {
                linkok = true;
                break;
            }
        }
        if (!linkok)
        {
            var icone = document.createElement("link");
            icone.rel = "shortcut icon";
            icone.href = iconeImpalce;
            document.head.appendChild(icone);
        }

        document.Chagas = this;
        this.className = "chagas"

        document.documentElement.ondragover = function () { return false; };
        document.documentElement.ondragend = function () { return false; };
        document.documentElement.ondrop = function (event) { return false; };

        var mt = document.createElement("meta");
        mt.name = "viewport";
        mt.content = "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no";
        document.head.appendChild(mt);

        //<link href="css/bootstrap.min.css" rel="stylesheet">

        //var bpcss = document.createElement("link");
        //bpcss.href = 'Bootstrap/css/bootstrap.min.css';
        //bpcss.rel = 'stylesheet';
        //document.head.appendChild(bpcss);

        //var bpjs = document.createElement("script");
        //bpjs.src = 'Bootstrap/js/bootstrap.min.js';
        //bpjs.rel = 'stylesheet';
        //document.body.appendChild(bpjs);

        var that = this;
        this.divCab = document.createElement("div");
        this.divCab.id = 'cabecalhoChagas';
        this.divCab.className = "chagas-noselect";
        this.divCab.style.cssText = "height:45px; width:100%; text-align:center; border-bottom:1px " + corBorder + " solid; display:none;";

        var divInnerCab = document.createElement("div");

        divInnerCab.style.display = "inline-block";
        divInnerCab.style.height = "100%";
        document.body.style.cssText = document.body.style.cssText + "-webkit-overflow-scrolling: touch;";

        this.linkHome2 = document.createElement("a");
        this.divLogo = document.createElement("div");
        this.divLogo.className = "chagas-logoemp";
        this.linkHome2.appendChild(this.divLogo);
        this.divCab.appendChild(this.linkHome2);

        function criarButton(titulo, backgroundImage)
        {
            var btn = document.createElement("button");
            btn.type = "button";
            btn.style.cssText = "height:100%; width:48px; background-position:center 4px; float:left; background-repeat:no-repeat; background-size:60%; position:relative;";
            btn.className = "chagas-button header";
            btn.style.backgroundImage = backgroundImage;
            btn.title = titulo;
            var divSeta = document.createElement("div");
            divSeta.style.cssText = "border-top:5px solid white; border-right:6px solid transparent; border-left:6px solid transparent; width:0; height:0; position:absolute; bottom:3px; left:calc(50% - 6px); display:none;";
            btn.appendChild(divSeta);
            btn.divSeta = divSeta;
            btn.chagas = that;
            btn.addEventListener("click", function (e)
            {
                if (that.toolTipAberto)
                {
                    that.closeToolTipBtn();
                }
                if (this.toolTip && this.toolTip.childElementCount > 0)
                {
                    that.openMenuStripBtn(this);
                    e.stopPropagation();
                }
                else
                {
                    that.setMenuStripBtn(null, this);
                    if (this.onclickEvent)
                    {
                        this.onclickEvent();
                    }
                }
            });

            return btn;
        };
        this.novoButton = criarButton("Novo", "url(" + novoButtomImage + ")");
        this.excluirButton = criarButton("Excluir", "url(" + excluirButtonImage + ")");
        this.salvarButton = criarButton("Salvar", "url(" + salvarButtonImage + ")");
        this.pesquisarButton = criarButton("Buscar", "url(" + pesquisarButtonImage + ")");
        this.desfazerButton = criarButton("Desfazer", "url(" + desfazerButtonImage + ")");
        this.primeiroButton = criarButton("Primeiro", "url(" + primeiroButtonImage + ")");
        this.anteriorButton = criarButton("Anterior", "url(" + anteriorButtonImage + ")");
        this.proximoButton = criarButton("Próximo", "url(" + proximoButtonImage + ")");
        this.ultimoButton = criarButton("Último", "url(" + ultimoButtonImage + ")");
        this.imprimirButton = criarButton("Imprimir", "url(" + imprimirButtonImage + ")");
        this.digitalizarButton = criarButton("Digitalizar", "url(" + digitalizarButtonImage + ")");

        this.novoButton.onclickEvent = function () { that.novo(); };
        this.excluirButton.onclickEvent = function () { that.excluir(); };
        this.salvarButton.onclickEvent = function () { that.gravar(); };
        this.pesquisarButton.onclickEvent = function () { that.pesquisar(); };
        this.desfazerButton.onclickEvent = function () { that.desfazer(); };
        this.primeiroButton.onclickEvent = function () { that.primeiro(); };
        this.anteriorButton.onclickEvent = function () { that.anterior(); };
        this.proximoButton.onclickEvent = function () { that.proximo(); };
        this.ultimoButton.onclickEvent = function () { that.ultimo(); };
        this.imprimirButton.onclickEvent = function () { that.imprimir(); };
        this.digitalizarButton.onclickEvent = function () { that.digitalizar(); };


        var listabtns = new Array();
        listabtns.push(this.novoButton);
        listabtns.push(this.excluirButton);
        listabtns.push(this.salvarButton);
        listabtns.push(this.pesquisarButton);
        listabtns.push(this.desfazerButton);
        listabtns.push(this.primeiroButton);
        listabtns.push(this.anteriorButton);
        listabtns.push(this.proximoButton);
        listabtns.push(this.ultimoButton);
        listabtns.push(this.imprimirButton);
        listabtns.push(this.digitalizarButton);

        this.divEnfeite = document.createElement("div");
        //this.divEnfeite.style.cssText = "border-top:45px solid #34383B; border-left:11px solid transparent; height:0px; width:0px; float:left;";
        //this.divEnfeite.className = "chagas-borderbox";
        divInnerCab.appendChild(this.divEnfeite);

        for (var x = 0; x < listabtns.length; x++)
        {
            divInnerCab.appendChild(listabtns[x]);
        }
        this.divCab.appendChild(divInnerCab);
        this.logoutButton = document.createElement("button");
        this.logoutButton.className = "chagas-button chagas-homebutton";
        this.logoutButton.type = "button";
        this.logoutButton.style.backgroundSize = "85%";
        this.logoutButton.style.backgroundImage = "url(" + logoutButtonImage + ")";
        this.logoutButton.title = "Sair";
        this.logoutButton.onclick = function ()
        {
            if (that.sairClick)
            {
                alert("Deseja sair?", "Não", ["Sim"], function (index)
                {
                    if (index == 0)
                    {
                        that.sairClick();
                    }
                });
            }
        };
        this.divCab.appendChild(this.logoutButton);

        this.linkHome = document.createElement("a");
        var homeButton = document.createElement("div");
        homeButton.className = "chagas-button chagas-homebutton";
        homeButton.style.top = "22px";
        homeButton.style.backgroundSize = "96%";
        homeButton.style.backgroundImage = "url(" + homeButtonImage + ")";
        homeButton.title = "Inicio";
        this.linkHome.appendChild(homeButton);
        this.divCab.appendChild(this.linkHome);

        for (var x = 0; x < this.childElementCount; x++)
        {
            if (this.children[x].getAttribute("is") == "chagas-conteudo")
            {
                this.divConteudo = this.children[x];
                break;
            }
        }
        if (this.divConteudo)
        {
            this.divConteudo.className = "chagas-borderbox " + this.divConteudo.className;
            this.divConteudo.style.width = "100%";
            this.divConteudo.style.overflow = "auto";
            this.divConteudo.style.float = "left";
            this.insertBefore(this.divCab, this.divConteudo);
        }
        else
        {
            this.appendChild(this.divCab);
        }

        var tooltips = this.getElementsByTagName("ul");
        for (var i = 0; i < tooltips.length; i++)
        {
            if (tooltips[i].attributes["for"] &&
                tooltips[i].attributes["for"].value.length > 0)
            {
                that.setMenuStrip(tooltips[i], tooltips[i].attributes["for"].value);
            }
        }
        this.mensagemDiv = document.createElement("div");
        this.mensagemDiv.className = "chagas-font";
        this.mensagemDiv.style.cssText = "width:100%; height:0; background-color:red; position:absolute; text-align:center; color:white; font-size:14px; font-weight:bold; overflow:hidden;";
        this.mensagemDivText = document.createElement("div");
        this.mensagemDivText.style.cssText = "height:20px; line-height:20px; padding-left:23px; left:0; bottom:0; position:absolute; background-position:0 center; background-repeat:no-repeat; background-size:18px;";
        this.mensagemDiv.appendChild(this.mensagemDivText);
        this.mensagemDiv.onclick = function ()
        {
            if (that.timeMsg)
            {
                clearTimeout(that.timeMsg);
                that.timeMsg = null;
            }
            this.style.height = "0";
        }
        this.appendChild(this.mensagemDiv);

        window.addEventListener("resize", function ()
        {
            that.resized();
        });
        window.addEventListener("click", function (e)
        {
            if (that.toolTipAberto)
            {
                that.closeToolTipBtn();
            }
        });

        this.resized();

        Object.defineProperty(this, 'mode', {
            get: function ()
            {
                return that.attributes["mode"] ? that.attributes["mode"].value : "";
            },
            set: function (value)
            {
                that.setAttribute("mode", value);
                if (value == Chagas.mode.consulta)
                {
                    that.novoButton.style.display = "";
                    that.excluirButton.style.display = "none";
                    that.salvarButton.style.display = "none";
                    that.pesquisarButton.style.display = "";
                    that.desfazerButton.style.display = "none";
                    that.primeiroButton.style.display = "none";
                    that.anteriorButton.style.display = "none";
                    that.proximoButton.style.display = "none";
                    that.ultimoButton.style.display = "none";
                    that.imprimirButton.style.display = "";
                    that.digitalizarButton.style.display = "none";
                    that.divEnfeite.style.display = "";
                }
                else if (value == Chagas.mode.hidden)
                {
                    that.novoButton.style.display = "none";
                    that.excluirButton.style.display = "none";
                    that.salvarButton.style.display = "none";
                    that.pesquisarButton.style.display = "none";
                    that.desfazerButton.style.display = "none";
                    that.primeiroButton.style.display = "none";
                    that.anteriorButton.style.display = "none";
                    that.proximoButton.style.display = "none";
                    that.ultimoButton.style.display = "none";
                    that.imprimirButton.style.display = "none";
                    that.digitalizarButton.style.display = "none";
                    that.divEnfeite.style.display = "none";
                }
                else
                {
                    that.novoButton.style.display = "";
                    that.excluirButton.style.display = "";
                    that.salvarButton.style.display = "";
                    that.pesquisarButton.style.display = "";
                    that.desfazerButton.style.display = "";
                    that.primeiroButton.style.display = "";
                    that.anteriorButton.style.display = "";
                    that.proximoButton.style.display = "";
                    that.ultimoButton.style.display = "";
                    that.imprimirButton.style.display = "";
                    that.digitalizarButton.style.display = "none";
                    that.divEnfeite.style.display = "";
                }
            }
        });
        Object.defineProperty(this, 'persistencia', {
            get: function ()
            {
                return that._persistencia;
            },
            set: function (value)
            {
                that._persistencia = value;
                PersistenciaJS.create(value);
            }
        });
        Object.defineProperty(this, 'urlInicio', {
            get: function ()
            {
                return that.linkHome.href;
            },
            set: function (value)
            {
                that.linkHome.href = value;
                that.linkHome2.href = value;
            }
        });
        window.addEventListener("load", function ()
        {
            if (!that.semOnLoad)
            {
                that.onLoad();
            }
        });
        this.mode = this.mode;

        PageMethods.GetCredenciais(function (result)
        {
            that.credenciais = result;
            that.atualizarCredenciais();
        });
    };

    _Chagas.onLoad = function ()
    {
        var _id = queryString.value("id");
        if (_id && _id.length > 0)
        {
            this.consultar(_id);
        }
        else
        {
            this.novo();
        }
    };

    _Chagas.resized = function ()
    {
        this.style.height = (window.innerHeight - this.offsetTop - 60) + "px";
        this.style.maxHeight = (window.innerHeight - this.offsetTop - 60) + "px";
        if (this.divConteudo && this.divConteudo.style)
        {
            this.divConteudo.style.height = (this.clientHeight - this.divCab.clientHeight - 1) + "px";
        }
        this.reposicionarToolTip();
        this.mensagemDivText.style.left = (this.mensagemDiv.offsetWidth - this.mensagemDivText.offsetWidth) / 2 + "px";
    };

    _Chagas.getBotao = function (nome)
    {
        switch (nome)
        {
            case "novoButton": return this.novoButton;
            case "excluirButton": return this.excluirButton;
            case "salvarButton": return this.salvarButton;
            case "pesquisarButton": return this.pesquisarButton;
            case "desfazerButton": return this.desfazerButton;
            case "primeiroButton": return this.primeiroButton;
            case "anteriorButton": return this.anteriorButton;
            case "proximoButton": return this.proximoButton;
            case "ultimoButton": return this.ultimoButton;
            case "imprimirButton": return this.imprimirButton;
            case "digitalizarButton": return this.digitalizarButton;
            default: alert("'" + nome + "' não existe!"); return null;
        }
    }

    _Chagas.setMenuStripBtn = function (lista, botao)
    {
        if (lista)
        {
            lista.style.display = "none";
        }
        if (botao)
        {
            if (lista && lista.childElementCount > 0)
            {
                botao.divSeta.style.display = "";
                botao.toolTip = lista;
            }
            else
            {
                botao.divSeta.style.display = "none";
                botao.toolTip = null;
            }
        }
    }

    _Chagas.setMenuStrip = function (lista, nomeBotao)
    {
        this.setMenuStripBtn(lista, this.getBotao(nomeBotao));
    };

    _Chagas.openMenuStrip = function (nomeBotao)
    {
        this.openMenuStripBtn(this.getBotao(nomeBotao));
    };

    _Chagas.openMenuStripBtn = function (botao)
    {
        if (botao && botao.toolTip)
        {
            var that = this;
            var tooltip = document.createElement("div");
            var lista = botao.toolTip.cloneNode(true);
            lista.className = "chagas-font " + lista.className;
            lista.style.cssText = "padding:0; margin:0; min-width:100px; list-style-type:none; font-size:11.5px; " + lista.style.cssText;
            lista.style.display = "";
            for (var x = 0; x < lista.childElementCount; x++)
            {
                lista.children[x].style.cssText = "height:22px; background-color:" + corToolTip + "; line-height:22px; cursor:pointer; border-bottom:1px " + corOver + " solid; padding-left:7px; padding-right:7px;";
                lista.children[x].onmouseenter = function ()
                {
                    this.style.backgroundColor = corOver2;
                };
                lista.children[x].onmouseleave = function ()
                {
                    this.style.backgroundColor = corToolTip;
                };
            }
            tooltip.appendChild(lista);
            tooltip.style.cssText = "position:absolute; border:1px solid " + corBorder + "; border-top:0; box-shadow:0px 3px 15px 0px rgba(50, 50, 50, 0.5); overflow-y:auto; overflow-x:hidden; ";
            tooltip.botao = botao;
            this.toolTipAberto = tooltip;
            this.appendChild(tooltip);
            this.reposicionarToolTip();
        }
    };

    _Chagas.closeToolTipBtn = function ()
    {
        if (this.toolTipAberto)
        {
            this.toolTipAberto.parentElement.removeChild(this.toolTipAberto);
            this.toolTipAberto = null;
        }
    };

    _Chagas.reposicionarToolTip = function ()
    {
        if (this.toolTipAberto)
        {
            var that = this;
            var rect = that.toolTipAberto.botao.getBoundingClientRect();
            that.toolTipAberto.style.top = (rect.top + rect.height + 1) + "px";
            if (that.toolTipAberto.botao.offsetLeft + that.toolTipAberto.offsetWidth > window.innerWidth)
            {
                that.toolTipAberto.style.left = (window.innerWidth - that.toolTipAberto.offsetWidth) + "px";
            }
            else
            {
                that.toolTipAberto.style.left = that.toolTipAberto.botao.offsetLeft + "px";
            }
            that.toolTipAberto.style.height = "";
            if (that.toolTipAberto.offsetTop + that.toolTipAberto.offsetHeight > window.innerHeight)
            {
                that.toolTipAberto.style.height = (window.innerHeight - that.toolTipAberto.offsetTop - 8) + "px";
            }
        }
    };

    _Chagas.sairClick = function ()
    {
    };

    _Chagas.novo = function ()
    {
        var that = this;
        document.showLoading(true);
        that.novoAntes(function ()
        {
            PageMethods.GetPersistenciaJS(function (result)
            {
                that.persistencia = result;
                that.limparCampos();
                that.novoDepois(function ()
                {
                    document.showLoading(false);
                });
            });
        });
    };

    _Chagas.novoAntes = function (retorno)
    {
        if (retorno)
        {
            retorno();
        }
    };

    _Chagas.novoDepois = function (retorno)
    {
        if (retorno)
        {
            retorno();
        }
    };

    _Chagas.excluir = function ()
    {
        var that = this;
        alert("Deseja excluir?", "Não", ["Sim"], function (index)
        {
            if (index == 0)
            {
                document.showLoading(true);
                PageMethods.Excluir(that.persistencia, function (result)
                {
                    if (result.erro)
                    {
                        document.showLoading(false);
                        that.mensagem(result.mensagem, true);
                    }
                    else
                    {
                        that.persistencia.limparIds(true);
                        that.divConteudo.popularGrades(that.persistencia);
                        that.divConteudo.limparArquivos();
                        queryString.set("id", "");
                        that.atualizarCredenciais();
                        document.showLoading(false);
                        that.mensagem(result.mensagem);
                    }
                });
            }
        })
    };

    _Chagas.validar = function (retorno)
    {
        if (retorno)
        {
            retorno(true);
        }
    };

    _Chagas.validarCampos = function ()
    {
        var valid = this.divConteudo.validarCampos();
        if (!valid.ok)
        {
            this.mensagem(valid.mensagem, true);
        }
        return valid.ok;
    };

    _Chagas.gravar = function ()
    {
        var that = this;
        if (that.validarCampos())
        {
            that.validar(function (ok)
            {
                if (ok)
                {
                    document.showLoading(true);
                    that.carregarCampos();
                    document.divBloqueio.divProgressoOut.style.display = "none";
                    that.gravarAntes(function ()
                    {
                        var todosarquivos = that.persistencia.todosArquivos(true);
                        if (todosarquivos.length > 0)
                        {
                            document.divBloqueio.divProgressoOut.style.display = "";
                            that.setarProgressoUploads(0, "");
                        }
                        ChagasFile.upload(location.pathname + "/UploadArquivo", todosarquivos, function (ret, msg)
                        {
                            document.divBloqueio.divProgressoOut.style.display = "none";
                            if (ret)
                            {
                                PageMethods.Gravar(JSON.stringify(that.persistencia), function (result)
                                {
                                    if (result.erro)
                                    {
                                        document.showLoading(false);
                                        that.mensagem(result.mensagem, true);
                                    }
                                    else
                                    {
                                        that.persistencia = result.persistencia;
                                        that.gravarDepois(function ()
                                        {
                                            that.popular(function ()
                                            {
                                                document.showLoading(false);
                                                that.mensagem(result.mensagem);
                                            });
                                        });
                                    }
                                });
                            }
                            else
                            {
                                document.showLoading(false);
                                that.mensagem(msg, true);
                            }
                        },
                        function (evt)
                        {
                            var cada = 100 / todosarquivos.length;
                            var pers = cada * evt.fileIndex;
                            pers += cada / 100 * evt.percentual;
                            var msg = "";
                            if (todosarquivos.length == 1)
                            {
                                msg = "Enviando 1 arquivo";
                            }
                            else
                            {
                                msg = "Enviando " + (evt.fileIndex + 1) + " de " + todosarquivos.length + " arquivos";
                            }
                            that.setarProgressoUploads(pers, msg);
                        });
                    });
                }
            });
        }
    };

    _Chagas.gravarAntes = function (retorno)
    {
        if (retorno)
        {
            retorno();
        }
    };

    _Chagas.gravarDepois = function (retorno)
    {
        if (retorno)
        {
            retorno();
        }
    };

    _Chagas.pesquisar = function ()
    {
        var that = this;
        var cr = new ChagasConsultaRapida();
        cr.open(function (pers)
        {
            that.consultar(pers.Identidade.valor);
        });
    };

    _Chagas.desfazer = function ()
    {
        if (this.persistencia && this.persistencia.Identidade.valor.length > 0)
        {
            this.consultar(this.persistencia.Identidade.valor);
        }
        else
        {
            this.novo();
        }
    };

    _Chagas.consultar = function (id, retorno)
    {
        var that = this;
        document.showLoading(true);
        PageMethods.Consultar(id,
            function (result)
            {
                that.persistencia = result;
                that.popular(function ()
                {
                    document.showLoading(false);
                    if (retorno)
                    {
                        retorno();
                    }
                });
            });
    };

    _Chagas.primeiro = function ()
    {
        var that = this;
        document.showLoading(true);
        PageMethods.Primeiro(function (result)
        {
            that.persistencia = result;
            that.popular(function ()
            {
                document.showLoading(false);
            });
        });
    };

    _Chagas.anterior = function ()
    {
        if (this.persistencia && this.persistencia.Identidade)
        {
            var that = this;
            document.showLoading(true);
            PageMethods.Anterior(that.persistencia.Identidade.valor,
                function (result)
                {
                    PersistenciaJS.create(result);
                    if (result.Identidade.valor.length > 0)
                    {
                        that.persistencia = result;
                        that.popular(function ()
                        {
                            document.showLoading(false);
                        });
                    }
                    else
                    {
                        document.showLoading(false);
                    }
                });
        }
        else
        {
            this.primeiro();
        }
    };

    _Chagas.proximo = function ()
    {
        if (this.persistencia && this.persistencia.Identidade)
        {
            var that = this;
            document.showLoading(true);
            PageMethods.Proximo(that.persistencia.Identidade.valor,
                function (result)
                {
                    PersistenciaJS.create(result);
                    if (result.Identidade.valor.length > 0)
                    {
                        that.persistencia = result;
                        that.popular(function ()
                        {
                            document.showLoading(false);
                        });
                    }
                    else
                    {
                        document.showLoading(false);
                    }
                });
        }
        else
        {
            this.primeiro();
        }
    };

    _Chagas.ultimo = function ()
    {
        var that = this;
        document.showLoading(true);
        PageMethods.Ultimo(function (result)
        {
            that.persistencia = result;
            that.popular(function ()
            {
                document.showLoading(false);
            });
        });
    };

    _Chagas.imprimir = function ()
    {

    };

    _Chagas.digitalizar = function ()
    {

    };

    _Chagas.popular = function (retorno)
    {
        var that = this;
        queryString.set("id", this.persistencia.Identidade.valor);
        this.atualizarTitulo();
        this.atualizarCredenciais();
        this.popularAntes(function ()
        {
            that.divConteudo.popular(that.persistencia, function ()
            {
                that.popularDepois(function ()
                {
                    if (retorno)
                    {
                        retorno();
                    }
                });
            });
        });
    };

    _Chagas.popularAntes = function (retorno)
    {
        if (retorno)
        {
            retorno();
        }
    };

    _Chagas.popularDepois = function (retorno)
    {
        if (retorno)
        {
            retorno();
        }
    };

    _Chagas.atualizarTitulo = function ()
    {
        var tit = this.attributes["titulo"] ? this.attributes["titulo"].value : "";
        var data = this.attributes["dataPropertyName"] ? this.attributes["dataPropertyName"].value : "";
        var val = "";
        if (data && this.persistencia && this.persistencia[data])
        {
            val = this.persistencia[data].valor;
        }
        if (tit && tit.length > 0 && val.length > 0)
        {
            document.title = tit + ": " + val;
        }
        else if (tit && tit.length > 0)
        {
            document.title = tit;
        }
        else if (val.length > 0)
        {
            document.title = val;
        }
    };

    _Chagas.carregarCampos = function ()
    {
        return this.divConteudo.carregarCampos(this.persistencia);
    };

    _Chagas.limparCampos = function ()
    {
        queryString.set("id", "");
        this.atualizarTitulo();
        this.atualizarCredenciais();
        this.divConteudo.limparCampos(this.persistencia);
    };

    _Chagas.getAllCampos = function ()
    {
        return this.divConteudo.getAllCampos();
    };

    _Chagas.mensagem = function (mensagem, alerta, erro)
    {
        if (erro)
        {
            this.mensagemDivText.style.backgroundImage = "url(" + errorImage + ")";
            this.mensagemDiv.style.backgroundColor = "#d9534f";
        }
        else if (alerta)
        {
            this.mensagemDivText.style.backgroundImage = "url(" + alertImage + ")";
            this.mensagemDiv.style.backgroundColor = "#f0ad4e";
        }
        else
        {
            this.mensagemDivText.style.backgroundImage = "url(" + successImage + ")";
            this.mensagemDiv.style.backgroundColor = "rgba(92, 184, 92, 0.5)";
        }
        this.mensagemDivText.innerHTML = mensagem;
        var that = this;
        that.mensagemDiv.style.top = that.divConteudo.offsetTop + "px";
        that.mensagemDivText.style.left = "0";//é necessário para zerar.
        that.mensagemDivText.style.left = (that.mensagemDiv.offsetWidth - that.mensagemDivText.offsetWidth) / 2 + "px";

        that.mensagemDiv.style.transitionDuration = "0.4s";
        that.mensagemDiv.style.transitionProperty = "height";
        that.mensagemDiv.style.transitionTimingFunction = "linear";
        that.mensagemDiv.style.height = "20px";

        if (this.timeMsg)
        {
            clearTimeout(this.timeMsg);
            this.timeMsg = null;
        }
        this.timeMsg = setTimeout(function ()
        {
            this.timeMsg = null;
            that.mensagemDiv.style.height = "0px";
        }, 3000);

    };

    _Chagas.setarProgressoUploads = function (percentual, mensagem)
    {
        document.divBloqueio.divProgressoOut.divPercentual.innerHTML = parseInt(percentual) + "%";
        document.divBloqueio.divProgressoOut.divProgressInner.style.width = percentual + "%";
        if (mensagem)
        {
            document.divBloqueio.divProgressoOut.text.innerHTML = mensagem;
        }
        else
        {
            document.divBloqueio.divProgressoOut.text.innerHTML = "";
        }
    };

    _Chagas.atualizarCredenciais = function ()
    {
        if (this.credenciais)
        {
            if (!this.credenciais.permiteExcluir)
            {
                this.excluirButton.disabled = true;
            }
            else
            {
                this.excluirButton.disabled = false;
            }
            if (!this.credenciais.permiteIncluir &&
                !this.credenciais.permiteAlterar)
            {
                this.salvarButton.disabled = true;
            }
            if ((!this.credenciais.permiteAlterar &&
                 this.persistencia &&
                 this.persistencia.Identidade &&
                 this.persistencia.Identidade.valor.length > 0) ||
                (!this.credenciais.permiteIncluir &&
                 (!this.persistencia ||
                  !this.persistencia.Identidade ||
                  this.persistencia.Identidade.valor.length == 0)))
            {
                this.salvarButton.disabled = true;
            }
            else
            {
                this.salvarButton.disabled = false;
            }
        }
    };

    var _ChagasConteudo = Object.create(HTMLElement.prototype);

    _ChagasConteudo.limparCampos = function (persistencia)
    {
        var campos = this.getAllCampos();
        for (var i = 0; i < campos.length; i++)
        {
            if (campos[i].chagastext)
            {
                removeClass(campos[i].chagastext.divBorder, "invalido");
            }
            if (campos[i].type &&
                campos[i].type.toLowerCase() == "checkbox")
            {
                removeClass(campos[i], "invalido");
                campos[i].checked = false;
            }
            else if (campos[i].type &&
                     campos[i].type.toLowerCase() == "radio")
            {
                if (campos[i].name.length > 0)
                {
                    var index = 0;
                    for (var r = 0; r < campos.length; r++)
                    {
                        if (campos[r].type &&
                            campos[r].type.toLowerCase() == "radio" &&
                            campos[r].name == campos[i].name)
                        {
                            campos[r].checked = index == 0;
                            index++;
                        }
                    }
                }
                else
                {
                    campos[i].checked = false;
                }
            }
            else if ((campos[i].type &&
                      campos[i].type.toLowerCase() == "file") ||
                     (campos[i].attributes["is"] &&
                      campos[i].attributes["is"].value.toLowerCase() == "chagas-file"))
            {
                if (campos[i].carregarFiles)
                {
                    campos[i].limparFiles();
                }
            }
            else
            {
                campos[i].value = "";
            }
        }
        this.popularGrades(persistencia);
    };

    _ChagasConteudo.campoInner = function (campo)
    {
        var parent = campo.parentElement;
        while (parent && parent != this && parent.getAttribute("is") != "chagas-conteudo")
        {
            parent = parent.parentElement;
        }
        return parent == this;
    };

    _ChagasConteudo.getAllCampos = function ()
    {
        var ret = new Array();
        var campos = this.querySelectorAll("input, select, textarea, .chagas-uploadinput, .chagas-texte, .chagas-datepicker");
        for (var i = 0; i < campos.length; i++)
        {
            var t = campos[i].type ? campos[i].type.toLowerCase() : "";
            if (campos[i].tagName.toLowerCase() == "input" &&
                campos[i].attributes["chagasfield"] &&
                campos[i].attributes["chagasfield"].value.length > 0 &&
                !campos[i].textE && campos[i].type &&
                (t == "text" ||
                 t == "email" ||
                 t == "password" ||
                 t == "checkbox" ||
                 t == "radio" ||
                 t == "file" ||
                 t == "tel" ||
                 t == "number" ||
                 t == "date" ||
                 t == "datetime" ||
                 t == "datetime-local" ||
                 t == "time" ||
                 t == "search" ||
                 t == "range" ||
                 t == "url" ||
                 t == "month" ||
                 t == "week" ||
                 t == "range" ||
                 t == "hidden") &&
                this.campoInner(campos[i]))
            {
                ret.push(campos[i]);
            }
            else if ((campos[i].tagName.toLowerCase() == "select" ||
                      campos[i].tagName.toLowerCase() == "textarea") &&
                     campos[i].attributes["chagasfield"] &&
                     campos[i].attributes["chagasfield"].value.length > 0 &&
                     this.campoInner(campos[i]))
            {
                ret.push(campos[i]);
            }
            else if (containsClass(campos[i], "chagas-uploadinput") &&
                     campos[i].attributes["chagasfield"] &&
                     campos[i].attributes["chagasfield"].value.length > 0 &&
                     this.campoInner(campos[i]))
            {
                ret.push(campos[i]);
            }
            else if (containsClass(campos[i], "chagas-texte") &&
                     campos[i].attributes["chagasfield"] &&
                     campos[i].attributes["chagasfield"].value.length > 0 &&
                     this.campoInner(campos[i]))
            {
                ret.push(campos[i]);
            }
            else if (containsClass(campos[i], "chagas-datepicker") &&
                    campos[i].attributes["chagasfield"] &&
                    campos[i].attributes["chagasfield"].value.length > 0 &&
                    this.campoInner(campos[i]))
            {
                ret.push(campos[i]);
            }
        }
        return ret;
    };

    _ChagasConteudo.popular = function (persistencia, retorno)
    {
        var campos = this.getAllCampos();
        var textEs = new Array();
        for (var i = 0; i < campos.length; i++)
        {
            if (campos[i].chagastext)
            {
                removeClass(campos[i].chagastext.divBorder, "invalido");
            }
            if (campos[i].type &&
                campos[i].type.toLowerCase() == "checkbox")
            {
                removeClass(campos[i], "invalido");
            }
            var ncampo = campos[i].attributes["chagasfield"].value;
            var valor = '';
            for (var c = 0; c < persistencia.Campos.length; c++)
            {
                if (persistencia.Campos[c].nome == ncampo)
                {
                    valor = persistencia.Campos[c].valor;
                    break;
                }
            }
            if (campos[i].type &&
                campos[i].type.toLowerCase() == "checkbox")
            {
                campos[i].checked = valor.toLowerCase() == 'true' || valor == '1';
            }
            else if (campos[i].type &&
                     campos[i].type.toLowerCase() == "radio")
            {
                campos[i].checked = valor.toLowerCase() == 'true' || valor == '1';
            }
            else if ((campos[i].type &&
                      campos[i].type.toLowerCase() == "file") ||
                     (campos[i].attributes["is"] &&
                      campos[i].attributes["is"].value.toLowerCase() == "chagas-file"))
            {
                if (campos[i].carregarFiles)
                {
                    campos[i].limparFiles();
                    for (var f = 0; f < persistencia.Arquivos.length; f++)
                    {
                        if (persistencia.Arquivos[f].GCampo == ncampo)
                        {
                            var ar = new Array();
                            ar.push(persistencia.Arquivos[f]);
                            campos[i].carregarFiles(ar);
                        }
                    }
                }
            }
            else
            {
                if (containsClass(campos[i], "chagas-datepicker"))
                {
                    campos[i].value = toDate(valor);
                }
                else
                {
                    campos[i].value = valor;
                }
                if (campos[i].atualizar)
                {
                    campos[i].atualizar();
                }
                if (valor.length > 0 && containsClass(campos[i], "chagas-texte"))
                {
                    textEs.push(campos[i]);
                }
            }
        }
        this.popularGrades(persistencia);
        if (textEs.length > 0)
        {
            var textEsConsulta = new Array();
            for (var i = 0; i < textEs.length; i++)
            {
                textEsConsulta.push({
                    id: textEs[i].value,
                    nomeConsulta: textEs[i].nomeConsulta
                });
            }
            var hidebloqueio = false;
            if (document.divBloqueio &&
                !containsClass(document.divBloqueio, "show"))
            {
                document.showLoading(true);
                hidebloqueio = true;
            }
            var that = this;
            if (this.requestTexteE)
            {
                var executor = this.requestTexteE.get_executor();
                if (executor.get_started())
                {
                    try
                    {
                        executor.abort();
                    }
                    catch (e) { }
                }
                this.requestTexteE = null;
            }

            this.requestTexteE = PageMethods._staticInstance.ConsultarVariosTextE(textEsConsulta,
            function (result)
            {
                this.requestTexteE = null;
                if (result)
                {
                    for (var i = 0; i < result.length; i++)
                    {
                        PersistenciaJS.create(result[i]);
                        textEs[i].setarPersistencia(result[i]);
                    }
                }
                else
                {
                    for (var i = 0; i < textEs.length; i++)
                    {
                        textEs[i].setarPersistencia(null);
                    }
                }
                if (hidebloqueio)
                {
                    document.showLoading(false);
                }
                if (retorno)
                {
                    retorno();
                }
            });
        }
        else if (retorno)
        {
            retorno();
        }
    };

    _ChagasConteudo.popularGrades = function (persistencia)
    {
        if (persistencia &&
            persistencia.Grades &&
            persistencia.Grades.length > 0)
        {
            var divs = this.getElementsByClassName("chagas-table");
            for (var x = 0; x < persistencia.Grades.length; x++)
            {
                for (var d = 0; d < divs.length; d++)
                {
                    if (divs[d].id == persistencia.Grades[x].grade)
                    {
                        divs[d].persistenciaGrade = persistencia.Grades[x];
                        divs[d].persistenciaDefault = persistencia.Grades[x].persistencia;
                        divs[d].setDataSourcePersistencia(persistencia.Grades[x].valores);
                        break;
                    }
                }
            }
        }
    };

    _ChagasConteudo.carregarCampos = function (persistencia)
    {
        persistencia.Arquivos = new Array();
        var campos = this.getAllCampos();
        for (var i = 0; i < campos.length; i++)
        {
            var ncampo = campos[i].attributes["chagasfield"].value;
            var campo = null;
            for (var c = 0; c < persistencia.Campos.length; c++)
            {
                if (persistencia.Campos[c].nome == ncampo)
                {
                    campo = persistencia.Campos[c];
                    break;
                }
            }
            if (!campo)
            {
                campo = {
                    nome: ncampo,
                    valor: "",
                    identidade: false,
                    cabecalho: false
                };
                persistencia.Campos.push(campo);
                PersistenciaJS.create(persistencia);
            }
            if (campos[i].type &&
                campos[i].type.toLowerCase() == "checkbox")
            {
                campo.valor = campos[i].checked ? "true" : "false";
            }
            else if (campos[i].type &&
                     campos[i].type.toLowerCase() == "radio")
            {
                campo.valor = campos[i].checked ? "true" : "false";
            }
            else if ((campos[i].type &&
                      campos[i].type.toLowerCase() == "file") ||
                      (campos[i].attributes["is"] &&
                      campos[i].attributes["is"].value.toLowerCase() == "chagas-file"))
            {
                if (campos[i].files && campos[i].files.length > 0)
                {
                    if (!campos[i].files[0].FileTemp)
                    {
                        campos[i].files[0].FileTemp = "";
                    }
                    if (!campos[i].files[0].Nome)
                    {
                        campos[i].files[0].Nome = "";
                    }
                    campos[i].files[0].GCampo = ncampo;
                    var ix = persistencia.Arquivos.indexOf(campos[i].files[0]);
                    if (ix > -1)
                    {
                        persistencia.Arquivos[ix] = campos[i].files[0];
                    }
                    else
                    {
                        persistencia.Arquivos.push(campos[i].files[0]);
                    }
                }
            }
            else
            {
                if (containsClass(campos[i], "chagas-datepicker"))
                {
                    campo.valor = formatarData(campos[i].value, 'dd/MM/yyyy HH:mm:ss');
                }
                else
                {
                    campo.valor = campos[i].value;
                }
                if (containsClass(campos[i], "chagas-texte") &&
                    campos[i].chagasfieldTexto &&
                    campos[i].chagasfieldTexto.length > 0)
                {
                    if (persistencia[campos[i].chagasfieldTexto])
                    {
                        persistencia[campos[i].chagasfieldTexto].valor = campos[i].input.value;
                    }
                    else
                    {
                        persistencia.Campos.push({
                            nome: campos[i].chagasfieldTexto,
                            valor: campos[i].input.value,
                        });
                        PersistenciaJS.create(persistencia);
                    }
                }
            }
        }
    };

    _ChagasConteudo.limparArquivos = function ()
    {
        var campos = this.getAllCampos();
        for (var i = 0; i < campos.length; i++)
        {
            if ((campos[i].type &&
                 campos[i].type.toLowerCase() == "file") ||
                (campos[i].attributes["is"] &&
                 campos[i].attributes["is"].value.toLowerCase() == "chagas-file"))
            {
                if (campos[i].carregarFiles)
                {
                    campos[i].limparFiles();
                }
            }
        }
    };

    _ChagasConteudo.validarCampos = function ()
    {
        var campos = this.getAllCampos();
        for (var x = 0; x < campos.length; x++)
        {
            if (campos[x].chagastext &&
                campos[x].chagastext.obrigatorio)
            {
                removeClass(campos[x].chagastext.divBorder, "invalido");
            }
            if (campos[x].type &&
                campos[x].type.toLowerCase() == "checkbox")
            {
                removeClass(campos[x], "invalido");
            }
        }
        for (var x = 0; x < campos.length; x++)
        {
            if (campos[x].chagastext && campos[x].chagastext.obrigatorio)
            {
                if (campos[x].type &&
                    campos[x].type.toLowerCase() == "checkbox")
                {
                    if (!campos[x].checked)
                    {
                        addClass(campos[x].chagastext.divBorder, "invalido");
                        campos[x].focus();
                        return { ok: false, mensagem: "Marque o campo \"" + campos[x].innerHTML + "\"", campo: campos[x] };
                    }
                }
                else if (campos[x].type &&
                         campos[x].type.toLowerCase() == "radio")
                {

                }
                else if (campos[x].type &&
                         campos[x].type.toLowerCase() == "file")
                {

                }
                else
                {
                    if ((containsClass(campos[x], "chagas-datepicker") && !campos[x].value) ||
                        (!containsClass(campos[x], "chagas-datepicker") && campos[x].value.trim().length == 0))
                    {
                        addClass(campos[x].chagastext.divBorder, "invalido");
                        campos[x].focus();
                        return { ok: false, mensagem: "Preencha o campo \"" + campos[x].chagastext.titulo + "\"", campo: campos[x] };
                    }
                }
            }
            else
            {
                if (campos[x].attributes["obrigatorio"] && campos[x].attributes["obrigatorio"].value == "true")
                {
                    if (campos[x].type &&
                        campos[x].type.toLowerCase() == "checkbox")
                    {
                        if (!campos[x].checked)
                        {
                            addClass(campos[x], "invalido");
                            campos[x].focus();
                            return { ok: false, mensagem: "Marque o campo \"" + campos[x].innerHTML + "\"", campo: campos[x] };
                        }
                    }
                    else
                    {
                        if ((containsClass(campos[x], "chagas-datepicker") && !campos[x].value) ||
                            (!containsClass(campos[x], "chagas-datepicker") && campos[x].value && campos[x].value.trim().length == 0))
                        {
                            campos[x].focus();
                            return { ok: false, mensagem: "Preencha o campo", campo: campos[x] };
                        }
                    }

                }
            }
        }
        return { ok: true };
    };

    Chagas = document.registerElement('impalce-chagas',
    {
        prototype: _Chagas,
        extends: 'div'
    });

    ChagasConteudo = document.registerElement('chagas-conteudo',
    {
        prototype: _ChagasConteudo,
        extends: 'div'
    });

    Chagas.openModal = function (modal)
    {
        modal.bloqueio = document.createElement("div");
        modal.bloqueio.className = "chagas-bloqueio";
        addClass(modal, "chagas-modal");
        modal.onkeydown = function (evt)
        {
            evt = evt || window.event;
            if (evt.keyCode == 9)
            {
                var ccs = this.querySelectorAll("input, button, select, textarea");
                if (!evt.shiftKey)
                {
                    if (document.activeElement == ccs[ccs.length - 1])
                    {
                        ccs[0].focus();
                        evt.preventDefault();
                    }
                }
                else
                {
                    if (document.activeElement == ccs[0])
                    {
                        ccs[ccs.length - 1].focus();
                        evt.preventDefault();
                    }
                }
            }
        };
        modal.onkeyup = function (evt)
        {
            evt = evt || window.event;
            if (evt.keyCode == 27)
            {
                Chagas.closeModal(this);
            }
        };
        modal.bloqueio.onkeyup = function (evt)
        {
            modal.onkeyup(evt);
        }
        if (!modal.onModalOpen)
        {
            modal.onModalOpen = function (row)
            {
            };
        }
        if (!modal.onModalClose)
        {
            modal.onModalClose = function (row, retorno)
            {
                if (retorno)
                {
                    retorno(false);
                }
            };
        }

        document.body.appendChild(modal.bloqueio);
        document.body.appendChild(modal);
        modal.focus();
        var ccs = modal.querySelectorAll("input, button, select, textarea");
        if (ccs.length > 0)
        {
            ccs[0].focus();
        }

        //modal.style.top = -modal.clientHeight - 25 + "px";
        modal.style.top = "45px";
        modal.style.left = (window.innerWidth - modal.clientWidth) / 2 + "px";
        modal.style.maxHeight = "100%";

        window.addEventListener("resize", function ()
        {
            //modal.style.top = (window.innerHeight - modal.clientHeight) / 2 + "px";
            modal.style.top = "45px";
            modal.style.left = (window.innerWidth - modal.clientWidth) / 2 + "px";
        });

        addClass(modal, "animated");
        addClass(modal, "faster");
        addClass(modal, "fadeInDown");

        modal.onModalOpen();
        setTimeout(function ()
        {
            addClass(modal.bloqueio, "show");
            //modal.style.top = (window.innerHeight - modal.clientHeight) / 2 + "px";
            modal.style.top = "45px";
            var tables = modal.getElementsByClassName("chagas-table");
            for (var x = 0; x < tables.length; x++)
            {
                if (tables[x].atualizarHeaders)
                {
                    tables[x].atualizarHeaders();
                }
            }
            setTimeout(function ()
            {
                removeClass(modal, "anim");
            }, 300);
        }, 1);
    };

    Chagas.closeModal = function (modal, ok)
    {
        function close()
        {
            addClass(modal, "anim");
            setTimeout(function ()
            {
                removeClass(modal, "show");
                //modal.style.top = -modal.clientHeight - 25 + "px";
                modal.style.top = "45px";
                removeClass(modal.bloqueio, "show");
                setTimeout(function ()
                {
                    modal.bloqueio.parentElement.removeChild(modal.bloqueio);
                    modal.parentElement.removeChild(modal);
                    removeClass(modal, "chagas-modal");
                }, 200);
            }, 1);
        }
        if (modal.onModalClose)
        {
            modal.onModalClose(ok, function (cancel)
            {
                if (!cancel)
                {
                    close();
                }
            });
        }
        else
        {
            close();
        }
    };

    Chagas.carregarCombo = function (combo, dados, semVazio)
    {
        combo.innerHTML = "";
        var op = document.createElement('option');
        if (!semVazio)
        {
            op.value = "";
            op.innerHTML = "Selecione...";
            combo.appendChild(op);
        }
        for (var x = 0; x < dados.length; x++)
        {
            op = document.createElement('option');
            op.value = dados[x].id;
            op.innerHTML = dados[x].texto;
            op.cabecalho = dados[x].cabecalho;
            combo.appendChild(op);
        }
    };

    Chagas.mode =
    {
        consulta: 'consulta',
        hidden: 'hidden'
    };
})();
// CHAGAS //

// CONSULTA RÁPIDA
(function ()
{
    var _ChagasConsultaRapida = Object.create(HTMLElement.prototype);
    _ChagasConsultaRapida.eventoRetorno = null;
    _ChagasConsultaRapida.nomeConsulta = "";

    _ChagasConsultaRapida.createdCallback = function ()
    {
        var that = this;

        this.limpar = true;
        this.linhaInicial = 0;
        this.linhaFinal = 30;
        this.className = "chagas-consultarapida chagas-noselect " + this.className;
        this.style.paddingLeft = "5px";
        this.style.paddingRight = "5px";

        this.buscaText = document.createElement("input");
        this.buscaText.type = "text";
        this.buscaText.style.cssText = "width:100%; margin-top:10px;";
        this.buscaText.className = "chagas-font chagas-borderbox " + this.buscaText.className;
        this.buscaText.valorAnterior = this.buscaText.value;
        this.buscaText.onkeydown = function (evt)
        {
            that.onkeydown(evt);
        };

        this.buscaText.onkeyup = function ()
        {
            if (this.valorAnterior != this.value)
            {
                this.valorAnterior = this.value;
                that.limpar = true;
                that.linhaInicial = 0;
                that.linhaFinal = 30;
                that.consultar();
            }
        };
        this.buscaText.onmouseup = function ()
        {
            setTimeout(function ()
            {
                if (that.buscaText.valorAnterior != that.buscaText.value)
                {
                    that.buscaText.onkeyup();
                }
            }, 1);
        };
        this.appendChild(this.buscaText);

        this.divTable = new ChagasTable();
        this.divTable.style.marginTop = "5px";
        this.divTable.style.width = "100%";
        this.divTable.style.height = "calc(100% - 45px)";
        this.divTable.scrollTopAnterior = 0;

        function carregarMais()
        {
            if (!that.consultando)
            {
                that.limpar = false;
                that.linhaInicial = that.linhaFinal + 1;
                that.linhaFinal = that.linhaInicial + 30;
                that.consultar();
            }
        }

        var onscrollTable = this.divTable.divTable.onscroll;
        this.divTable.divTable.onscroll = function (e)
        {
            onscrollTable();
            if (this.scrollTopAnterior != this.scrollTop)
            {
                this.scrollTopAnterior = this.scrollTop;
                if (this.clientHeight + this.scrollTop > that.divTable.grade.clientHeight - 30)
                {
                    carregarMais();
                }
            }
        };

        var divMais = document.createElement("div");
        divMais.className = "chagas-font";
        divMais.style.cssText = "width:100px; height:20px; line-height:20px; cursor:pointer; margin:0 auto; text-align:center; font-size:12.5px; font-weight:bold;";
        divMais.innerHTML = "Carregar mais...";
        divMais.onclick = function ()
        {
            carregarMais();
        };
        this.divTable.divTable.appendChild(divMais);

        this.lock = false;
        this.divTable.onCellClick = function (cell)
        {
            if (!that.lock)
            {
                that.lock = true;
                if (that.eventoRetorno)
                {
                    that.eventoRetorno(cell.row.persistencia);
                }
                that.close();
            }
        };
        this.onkeydown = function (evt)
        {
            evt = evt || window.event;
            if (evt.keyCode == 40 || evt.keyCode == 38 || evt.keyCode == 39 || evt.keyCode == 37)
            {
                that.divTable.grade.onkeydown(evt);
                setTimeout(function ()
                {
                    that.buscaText.focus();
                }, 1);
            }
        };
        this.onkeyup = function (evt)
        {
            evt = evt || window.event;
            if (evt.keyCode == 13)//Enter
            {
                that.divTable.grade.onkeyup(evt);
            }
            else if (evt.keyCode == 27)//Esc
            {
                that.close();
            }
        };

        this.loadingDiv = document.createElement("div");
        this.loadingDiv.style.cssText = "position:absolute; bottom:0; margin:0 auto; height:20px; width:calc(100% - 12px);";
        this.loadingDiv.style.display = "none";

        this.loadingDivImg = document.createElement("div");
        this.loadingDivImg.className = "chagas-font";
        this.loadingDivImg.style.cssText = "margin:0 auto; font-weight:bold; height:20px; background-color:" + corOver + "; width:130px; background-image:url(" + loadingGifImage + "); background-position:-5px -9px; background-repeat:no-repeat; background-size:auto 26px; line-height:18px; padding-left:30px; color:white; font-size:14px; border:1px solid " + corBorder + "; ";
        this.loadingDivImg.className = "chagas-borderbox " + this.loadingDivImg.className;
        this.loadingDivImg.innerHTML = "Carregando...";
        this.loadingDiv.appendChild(this.loadingDivImg);
        this.divTable.appendChild(this.loadingDiv);

        this.divBloqueio = document.createElement("div");
        this.divBloqueio.style.cssText = "position:fixed; left:0; top:0; width:100%; height:100%; background-color:rgba(0, 0, 0, 0.2); z-index:0;";
        this.divBloqueio.onclick = function ()
        {
            that.close();
        };

        this.appendChild(this.divTable);
        this.divTable.selectionMode = ChagasTable.selectionMode.fullRow;

        function resized()
        {
            //that.style.width = (window.innerWidth - 16) + "px";
            that.style.left = ((window.innerWidth - that.clientWidth) / 2) + "px";
            that.divTable.style.height = that.clientHeight - that.divTable.offsetTop - 5 + "px";
            that.divTable.atualizarHeaders();
        }
        window.addEventListener("resize", function ()
        {
            resized();
        });
    };

    _ChagasConsultaRapida.close = function ()
    {
        if (this.opened)
        {
            this.opened = false;
            var that = this;
            this.style.top = -(this.clientHeight + 20) + "px";
            this.divBloqueio.style.opacity = "0";
            setTimeout(function ()
            {
                that.style.transitionDuration = "";
                that.style.transitionProperty = "";
                that.style.transitionTimingFunction = "";
                that.divBloqueio.style.transitionDuration = "";
                that.divBloqueio.style.transitionProperty = "";
                that.divBloqueio.style.transitionTimingFunction = "";
                if (that.divBloqueio.parentElement)
                {
                    that.divBloqueio.parentElement.removeChild(that.divBloqueio);
                }
                if (that.parentElement)
                {
                    that.parentElement.removeChild(that);
                }
            }, 500);
        }
    };

    _ChagasConsultaRapida.consultar = function ()
    {
        var that = this;
        that.consultando = true;
        that.loadingDiv.style.display = "";
        if (this.request)
        {
            var executor = this.request.get_executor();
            if (executor.get_started())
            {
                try
                {
                    executor.abort();
                }
                catch (e) { }
            }
            this.request = null;
        }
        this.request = PageMethods._staticInstance.Pesquisar({
            textoConsulta: this.buscaText.value,
            linhaInicio: this.linhaInicial,
            linhaFim: this.linhaFinal,
            nomeConsulta: this.nomeConsulta
        },
        function (result)
        {
            that.request = null;
            if (that.limpar)
            {
                that.divTable.divTable.scrollTop = 0;
                while (that.divTable.columns.length > 0)
                {
                    that.divTable.removeColumn(that.divTable.columns[0]);
                }

                for (k in result.table)
                {
                    var s = k.replace("_", " ");
                    if (s.length > 0)
                    {
                        s = s.substring(0, 1).toUpperCase() + s.substring(1, s.length);
                    }
                    var col = new ChagasTableColumn();
                    col.header = s;
                    col.dataPropertyName = k;
                    that.divTable.addColumn(col);
                }
            }
            that.divTable.setDataSource(result, !that.limpar);
            that.consultando = false;
            that.loadingDiv.style.display = "none";
        });
    };

    _ChagasConsultaRapida.open = function (retorno)
    {
        this.opened = true;
        this.divBloqueio.style.opacity = "0";
        this.eventoRetorno = retorno;
        document.body.appendChild(this.divBloqueio);
        document.body.appendChild(this);
        this.style.left = "calc(50% - " + (this.clientWidth / 2) + "px)";
        this.style.top = -(this.clientHeight + 20) + "px";
        var that = this;
        setTimeout(function ()
        {
            that.style.transitionDuration = "0.4s";
            that.style.transitionProperty = "top";
            that.style.transitionTimingFunction = "ease-out";
            that.divBloqueio.style.transitionDuration = "0.5s";
            that.divBloqueio.style.transitionProperty = "opacity";
            that.divBloqueio.style.transitionTimingFunction = "linear";
            that.style.top = "0px";
            that.divBloqueio.style.opacity = "1";
            that.buscaText.focus();
        }, 1);
        this.consultar();
    };

    ChagasConsultaRapida = document.registerElement('chagas-consultarapida',
    {
        prototype: _ChagasConsultaRapida,
        extends: 'div'
    });
})();
// CONSULTA RÁPIDA //

// CHAGAS TEXT
(function ()
{
    var _ChagasText = Object.create(HTMLElement.prototype);
    _ChagasText.disable = false;
    _ChagasText.chagasfield = '';
    _ChagasText.obrigatorio = false;
    _ChagasText.readonly = false;
    _ChagasText.titulo = '';
    _ChagasText.type = 'text';
    _ChagasText.value = '';

    _ChagasText.createdCallback = function ()
    {
        var that = this;
        that.classList.add('form-group');
        this.label = document.createElement("label");
        if (this.attributes["titulo"])
        {
            this.label.innerHTML = this.attributes["titulo"].value;
        }

        this.label.className = "chagas-noselect";
        //this.label.style.cssText = " margin-bottom:2px; margin-top:3px; margin-left:3px; font-size:12px; color:black; ";
        this.label.style.display = this.label.innerHTML.length > 0 ? "" : "none";
        this.label.onclick = function ()
        {
            that.input.focus();
        };

        this.divBorder = document.createElement("div");
        this.divBorder.className = "chagas-campoborda";

        //this.className = "chagas-font " + this.className;
        //this.style.cssText = "height:48px; " + this.style.cssText;

        function createInput()
        {
            if (that.input && that.input.parentElement)
            {
                that.input.parentElement.removeChild(that.input);
            }
            that.input = document.createElement("input");

            var isTextE = false;
            var isTextArea = false;
            var isNumber = false;
            var isDatePicker = false;
            if (that.attributes["type"] && that.attributes["type"].value == ChagasText.type.select)
            {
                that.input = document.createElement("select");
            }
            else if (that.attributes["type"] && that.attributes["type"].value == ChagasText.type.textarea)
            {
                isTextArea = true;
                that.input = document.createElement("textarea");
                if (that.style.height && that.style.height != 'auto' && that.style.height.length > 0)
                {
                    var heighttextarea = that.style.height.split('px', -4);
                    if (heighttextarea[0])
                    {
                        that.input.style.height = (heighttextarea[0] - 20) + 'px';
                    }
                }
            }
            else if (that.attributes["type"] && that.attributes["type"].value == ChagasText.type.datepicker)
            {
                isDatePicker = true;
                that.input = new ChagasDatePicker();
            }
            else if (that.attributes["type"] && that.attributes["type"].value == ChagasText.type.texte)
            {
                isTextE = true;
                that.input = new ChagasTextE();
                if (that.attributes["nomeconsulta"])
                {
                    that.input.nomeConsulta = that.attributes["nomeConsulta"].value;
                }
                if (that.attributes["chagasfieldtexto"])
                {
                    that.input.chagasfieldTexto = that.attributes["chagasfieldtexto"].value;
                }
            }
            else if (that.attributes["type"] && that.attributes["type"].value == ChagasText.type.inteiro)
            {
                ChagasText.createDecimal(that.input, 0);
                isNumber = true;
            }
            else if (that.attributes["type"] && that.attributes["type"].value.indexOf(ChagasText.type.decimal) > -1)
            {
                var d = that.attributes["type"].value.replace(ChagasText.type.decimal, '');
                d = apenasNumeros(d);
                d = parseInt(d);
                ChagasText.createDecimal(that.input, !isNaN(d) && d > 0 ? d : 2);
                isNumber = true;
            }
            else if (that.attributes["type"])
            {
                that.input.type = that.attributes["type"].value;
            }
            if (that.attributes["value"])
            {
                that.input.value = that.attributes["value"].value;
                if (isNumber)
                {
                    that.input.atualizar();
                }
            }
            if (that.attributes["chagasfield"])
            {
                that.input.setAttribute("chagasfield", that.attributes["chagasfield"].value);
                that.input.chagasfield = that.attributes["chagasfield"].value;
            }
            if (that.attributes["readonly"])
            {
                that.input.readOnly = that.attributes["readonly"].value.toLowerCase() == "true";
            }
            if (that.attributes["disable"])
            {
                that.input.disabled = that.attributes["disable"].value.toLowerCase() == "true";
            }
            that.divBorder.style.height = isTextArea ? "calc(100% - 20px)" : "";
            that.input.className = (isTextE || isDatePicker ? "" : "chagas-input ") + "chagas-borderbox " + that.input.className;
            if (isNumber)
            {
                that.input.style.textAlign = "right";
            }
            if (that.attributes["type"].value != 'texte')
            {
                that.input.classList.add('form-control');
            }


            that.input.chagastext = that;
            that.divBorder.appendChild(that.input);
        }

        this.appendChild(this.label);
        createInput();
        this.appendChild(this.divBorder);

        Object.defineProperty(this, 'value', {
            get: function ()
            {
                return that.input.value;
            },
            set: function (value)
            {
                that.input.value = value;
            }
        });
        Object.defineProperty(this, 'titulo', {
            get: function ()
            {
                return that.label.innerHTML;
            },
            set: function (value)
            {
                that.label.innerHTML = value;
                that.label.style.display = value.length > 0 ? "" : "none";
            }
        });
        Object.defineProperty(this, 'chagasfield', {
            get: function ()
            {
                return that.input.attributes["chagasfield"] ? that.input.attributes["chagasfield"].value : "";
            },
            set: function (value)
            {
                that.input.setAttribute("chagasfield", value);
            }
        });
        Object.defineProperty(this, 'type', {
            get: function ()
            {
                return that.attributes["type"] ? that.attributes["type"].value : "text";
            },
            set: function (value)
            {
                if (that.getAttribute("type") != value)
                {
                    that.setAttribute("type", value);
                    createInput();
                }
            }
        });
        Object.defineProperty(this, 'obrigatorio', {
            get: function ()
            {
                return that.attributes["obrigatorio"] && that.attributes["obrigatorio"].value == 'true' ? true : false;
            },
            set: function (value)
            {
                that.setAttribute("obrigatorio", value ? 'true' : 'false');
                that.input.obrigatorio = value;
            }
        });
        this.obrigatorio = this.obrigatorio;
        Object.defineProperty(this, 'onchange', {
            get: function ()
            {
                return this.input.onchange;
            },
            set: function (value)
            {
                this.input.onchange = value;
            }
        });
        Object.defineProperty(this, 'readonly', {
            get: function ()
            {
                return that.input.readOnly;
            },
            set: function (value)
            {
                that.setAttribute("readonly", value ? 'true' : 'false');
                that.input.readOnly = value;
            }
        });
        this.readonly = this.readonly;
        Object.defineProperty(this, 'disable', {
            get: function ()
            {
                return that.input.disabled;
            },
            set: function (value)
            {
                that.setAttribute("disable", value ? 'true' : 'false');
                that.input.disabled = value;
            }
        });
        this.disable = this.disable;
    };

    _ChagasText.focus = function ()
    {
        this.input.focus();
    };

    ChagasText = document.registerElement('chagas-text',
    {
        prototype: _ChagasText,
        extends: 'div'
    });

    ChagasText.createDecimal = function (input, decimais)
    {
        input.atualizar = function ()
        {
            this.value = formatarDecimal(this.value, this.decimais);
        };
        input.decimais = decimais;
        input.addEventListener("focus", function (evt)
        {
            var that = this;
            setTimeout(function ()
            {
                var s = that.selectionStart;
                var e = that.selectionEnd;
                var val = that.value;
                var i = val.indexOf('.');
                while (i > -1)
                {
                    val = val.replace('.', '');
                    if (i < s)
                    {
                        s--;
                        e--;
                    }
                    i = val.indexOf('.');
                }
                that.value = val;
                try
                {
                    that.setSelectionRange(s, e);
                }
                catch (e) { }
            }, 1);
        });
        input.addEventListener("blur", function (evt)
        {
            this.atualizar();
        });
        input.addEventListener("keypress", function (evt)
        {
            evt = evt || window.event;
            var char = String.fromCharCode(evt.keyCode);
            if ((parseInt(char) >= 0 && parseInt(char) <= 9) ||
                char == ',')
            {
            }
            else
            {
                evt.preventDefault();
                return false;
            }
        });
        input.addEventListener("change", function (evt)
        {
            this.atualizar();
        });
        input.atualizar();
    };

    ChagasText.type =
    {
        text: "text",
        select: "select",
        textarea: "textarea",
        texte: "texte",
        inteiro: "inteiro",
        decimal: "decimal",
        datepicker: "datepicker"
    };
})();
// CHAGAS TEXT //

// CHAGAS TABCONTROL
(function ()
{
    var _ChagasTabControl = Object.create(HTMLElement.prototype);
    _ChagasTabControl.tabActive = -1;

    _ChagasTabControl.createdCallback = function ()
    {
        var that = this;
        this.style.cssText = "overflow:hidden;" + this.style.cssText;
        this.header = document.createElement("div");
        this.header.style.cssText = "width:100%; height:21px; font-size:14px; float:left; overflow-x:auto;";
        this.header.className = "chagas-hidescroll chagas-noselect chagas-font";
        this.header.style.msOverflowStyle = "none";

        this.headerScroll = document.createElement("div");
        this.headerScroll.style.cssText = "min-width:100%; height:21px; border-bottom:1px solid " + corBorder + ";";
        this.headerScroll.className = "chagas-borderbox " + this.headerScroll.className;
        this.header.appendChild(this.headerScroll);

        var cssSeta = "display:none; width:20px; height:20px; line-height:20px; margin-top:1px; float:left; text-align:center; cursor:pointer; border:1px solid " + corBorder + ";";
        var cssSeta2 = "height:0; width:0; margin-top:4px; margin-left:6px; border-top:solid 5px transparent; border-bottom:solid 5px transparent; ";
        this.divLeft = document.createElement("div");
        this.divLeft.className = "chagas-noselect chagas-borderbox";
        this.divLeft.style.cssText = cssSeta;
        var seta = document.createElement("div");
        seta.style.cssText = cssSeta2 + " border-right:solid 6px black;";
        this.divLeft.appendChild(seta);
        this.divRight = document.createElement("div");
        this.divRight.className = "chagas-noselect chagas-borderbox";
        this.divRight.style.cssText = cssSeta + "border-left:0;";
        var seta = document.createElement("div");
        seta.style.cssText = cssSeta2 + " border-left:solid 6px black;";
        this.divRight.appendChild(seta);

        this.divLeft.onclick = function ()
        {
            that.header.scrollLeft -= 50;
        };
        this.divLeft.onmouseenter = function ()
        {
            this.style.backgroundColor = corOver;
        };
        this.divLeft.onmouseleave = function ()
        {
            this.style.backgroundColor = "white";
        };

        this.divRight.onclick = function ()
        {
            that.header.scrollLeft += 50;
        };
        this.divRight.onmouseenter = function ()
        {
            this.style.backgroundColor = corOver;
        };
        this.divRight.onmouseleave = function ()
        {
            this.style.backgroundColor = "white";
        };

        this.body = document.createElement("div");
        this.body.style.cssText = "width:100%; float:left; height:calc(100% - 21px); font-size:14px; border:1px solid " + corBorder + "; border-top:0; ";
        this.body.className = "chagas-font chagas-borderbox";
        this.tabs = new Array();
        that.create = function ()
        {
            for (var x = 0; x < that.childElementCount; x++)
            {
                if (that.children[x].getAttribute("is") == "chagas-tab")
                {
                    that.addTab(this.children[x], true);
                    x--;
                }
            }
        };
        this.appendChild(this.header);
        this.appendChild(this.divLeft);
        this.appendChild(this.divRight);
        this.appendChild(this.body);

        window.addEventListener("resize", function ()
        {
            that.atualizarScroll();
        });
        window.addEventListener("load", function ()
        {
            that.create();
            that.atualizarScroll();
            if (that.tabs.length > 0)
            {
                that.activeTab(that.tabs[0]);
            }
        });
    };

    _ChagasTabControl.activeTab = function (tab)
    {
        var indexActive = this.tabs.indexOf(tab);
        this.tabActive = tab;
        for (var x = 0; x < this.tabs.length; x++)
        {
            removeClass(this.tabs[x].head, "active");
            this.tabs[x].style.display = "none";

            removeClass(this.tabs[x].head, "ritem");
            removeClass(this.tabs[x].head, "litem");
            if (x <= 0 || x != indexActive + 1)
            {
                addClass(this.tabs[x].head, "litem");
            }
            if (x >= this.tabs.length - 1)
            {
                addClass(this.tabs[x].head, "ritem");
            }
            this.tabs[x].head.style.borderBottom = "0";
        }
        tab.style.display = "";
        addClass(tab.head, "active");
        var tables = tab.getElementsByClassName("chagas-table");
        for (var x = 0; x < tables.length; x++)
        {
            tables[x].atualizarHeaders();
        }
    };

    _ChagasTabControl.addTab = function (div, noload)
    {
        var that = this;
        this.tabs.push(div);

        div.style.cssText = "overflow:auto; height:100%; " + div.style.cssText;
        div.className = "chagas-borderbox " + div.className;

        var head = document.createElement("button");
        head.type = "button";
        head.className = "chagas-font chagas-tabhead";
        div.head = head;
        div.head.tab = div;
        div.tabControl = that;
        if (div.titulo)
        {
            div.head.innerHTML = div.titulo;
        }
        else if (div.attributes["titulo"])
        {
            div.head.innerHTML = div.attributes["titulo"].value;
        }
        head.onclick = function ()
        {
            that.activeTab(this.tab);
        };

        this.headerScroll.appendChild(head);
        this.body.appendChild(div);
        if (!noload)
        {
            if (that.tabActive)
            {
                that.activeTab(that.tabActive);
            }
            else if (that.tabs.length > 0)
            {
                that.activeTab(that.tabs[0]);
            }
            that.atualizarScroll();
        }
    };

    _ChagasTabControl.atualizarScroll = function ()
    {
        var wTot = 0;
        for (var x = 0; x < this.tabs.length; x++)
        {
            var rect = this.tabs[x].head.getBoundingClientRect();
            wTot += rect.width;
        }
        this.headerScroll.style.width = (wTot + 0.5) + "px";

        this.header.style.width = "100%";
        this.divLeft.style.display = "none";
        this.divRight.style.display = "none";
        if (this.header.scrollWidth - 1 > this.header.offsetWidth)
        {
            this.header.style.width = "calc(100% - 40px)";
            this.divLeft.style.display = "";
            this.divRight.style.display = "";
        }
    };

    _ChagasTabControl.removeTab = function (tab)
    {
        var index = this.tabs.indexOf(tab);
        var indexA = -1;
        if (this.tabActive && this.tabActive == tab)
        {
            indexA = this.tabs.indexOf(this.tabActive);
            if (indexA > 0)
            {
                indexA--;
            }
        }
        this.tabs.splice(index, 1);
        tab.head.parentElement.removeChild(tab.head);
        this.body.removeChild(tab);

        if (indexA > -1)
        {
            if (indexA < this.tabs.length)
            {
                this.activeTab(this.tabs[indexA]);
            }
            else
            {
                this.tabActive = null;
            }
        }
        this.atualizarScroll();
    };

    var _ChagasTab = Object.create(HTMLElement.prototype);
    _ChagasTab.titulo = "";

    _ChagasTab.createdCallback = function ()
    {
        var that = this;
        this.titulo = that.attributes["titulo"] ? that.attributes["titulo"].value : "";
        Object.defineProperty(this, 'titulo', {
            get: function ()
            {
                return that.attributes["titulo"].value;
            },
            set: function (value)
            {
                that.setAttribute("titulo", value);
                if (that.head)
                {
                    that.head.innerHTML = value;
                }
                if (that.tabControl)
                {
                    that.tabControl.atualizarScroll();
                }
            }
        });
    };

    ChagasTabControl = document.registerElement('chagas-tabcontrol',
    {
        prototype: _ChagasTabControl,
        extends: 'div'
    });

    ChagasTab = document.registerElement('chagas-tab',
    {
        prototype: _ChagasTab,
        extends: 'div'
    });
})();
// CHAGAS TABCONTROL //

// CHAGAS FILE
(function ()
{
    var _ChagasFile = Object.create(HTMLElement.prototype);
    _ChagasFile.disableAdd = false;
    _ChagasFile.disableDelete = false;
    _ChagasFile.divsFiles = new Array();
    _ChagasFile.extensoes = "";
    _ChagasFile.files = new Array();
    _ChagasFile.itemClass = "";
    _ChagasFile.limite = 1;
    _ChagasFile.selectedFile;
    _ChagasFile.titulo = "";
    _ChagasFile.uploadAtual;

    _ChagasFile.createdCallback = function ()
    {
        var that = this;
        this.style.cssText = "overflow:hidden; " + this.style.cssText;
        this.className = "chagas-uploadinput chagas-borderbox " + this.className;
        document.documentElement.ondragover = function () { return false; };
        document.documentElement.ondragend = function () { return false; };
        document.documentElement.ondrop = function (event) { return false; };
        function reset()
        {
            that.input = document.createElement("input");
            that.input.type = "file";
            that.input.style.display = "none";
            that.appendChild(that.input);
            that.input.onchange = function ()
            {
                var files = validarFiles(that.input.files);
                if (that.limite == 1 && that.files.length > 0 && files.length > 0)
                {
                    that.removeFile(that.files[0]);
                }
                that.carregarFiles(files);
                reset();
            };
        };
        function validarFiles(files)
        {
            if (!that.extensoes || that.extensoes.length == 0)
            {
                return files;
            }
            var ret = new Array();
            var ext = that.extensoes.replace(" ", "").toLowerCase().split(",");
            for (var x = 0 ; x < files.length; x++)
            {
                var s = ChagasFile.fileExtension(files[x]);
                alert(!that.extensoes);
                if (ext.indexOf(s) > -1 &&
                    (that.limite <= 1 || that.limite > that.files.length + ret.length))
                {
                    ret.push(files[x]);
                }
            }
            return ret;
        };
        reset();

        this.ondragover = function ()
        {
            if (!that.disableAdd)
            {
                that.divLista.style.border = '4px solid #85ce73';
            }
        };

        this.ondragleave = function ()
        {
            that.divLista.style.border = '';
        }

        this.ondrop = function ()
        {
            if (!that.disableAdd)
            {
                that.divLista.style.border = '';
                var files = validarFiles(event.dataTransfer.files);
                if (that.limite == 1 && that.files.length > 0 && files.length > 0)
                {
                    that.removeFile(that.files[0]);
                }
                that.carregarFiles(files);
            }
        };
        this.onkeydown = function (evt)
        {
            evt = evt || window.event;
            if (evt.keyCode == 46)//Delete
            {
                if (that.deleteButton)
                {
                    that.deleteButton.click();
                }
            }
        };
        this.style.position = "relative";

        this.divTitulo = document.createElement("div");
        this.divTitulo.style.cssText = "width:100%; height:20px; cursor:default; ";
        this.divTituloText = document.createElement("div");
        this.divTituloText.className = "chagas-noselect chagas-font";
        this.divTituloText.innerHTML = "Teste";
        this.divTituloText.style.cssText = "height:19px; font-size:13px; border-bottom:1px solid white; padding-left:5px; padding-right:8px; float:left; cursor:pointer; background-color:transparent;";

        this.divTitulo.appendChild(this.divTituloText);
        this.divTitulo.onclick = function ()
        {
            that.addButton.click();
        };
        this.appendChild(this.divTitulo);

        this.divLista = document.createElement("div");
        this.divLista.onclick = function ()
        {
            if (that.limite == 1)
            {
                that.addButton.click();
            }
            else if (that.selectedFile)
            {
                that.selectedFile.style.backgroundColor = "";
                that.selectedFile = null;
                that.atualizarBotoes();
            }
        };

        this.appendChild(this.divLista);

        var divFooter = document.createElement("div");
        divFooter.style.cssText = "width:100%; height:20px; ";
        divFooter.className = "chagas-borderbox";

        this.addButton = document.createElement("button");
        this.addButton.type = "button";
        this.addButton.title = "Adicionar";
        this.addButton.className = "chagas-button chagas-tablebutton add";
        this.addButton.style.backgroundImage = "url(" + addTableImage + ")";
        divFooter.appendChild(this.addButton);
        this.addButton.onclick = function ()
        {
            if (!that.disableAdd)
            {
                that.input.multiple = that.limite == -1 || that.limite > 1 ? "multiple" : "";
                that.input.accept = that.extensoes;
                that.input.click();
            }
        };
        this.deleteButton = document.createElement("button");
        this.deleteButton.type = "button";
        this.deleteButton.title = "Remover";
        this.deleteButton.className = "chagas-button chagas-tablebutton remove";
        this.deleteButton.style.backgroundImage = "url(" + deleteTableImage + ")";
        divFooter.appendChild(this.deleteButton);
        this.deleteButton.onclick = function ()
        {
            if (that.disableDelete)
            {
                return;
            }
            var rs = that.getCheckedFiles();
            if (rs.length > 0)
            {
                alert("Excluir o" + (rs.length == 1 ? " item" : "s " + rs.length + " itens") + " marcado" + (rs.length == 1 ? "" : "s") + "?", "Não", ["Sim"], function (index)
                {
                    if (index == 0)
                    {
                        var i = that.files.indexOf(rs[0]);
                        for (var x = 0; x < rs.length; x++)
                        {
                            that.removeFile(rs[x]);
                        }
                        that.selectedFile = null;
                        if (i < that.divsFiles.length)
                        {
                            that.divsFiles[i].focus();
                        }
                        else if (i > 0)
                        {
                            that.divsFiles[i - 1].focus();
                        }
                        if (that.onFilesDeleted)
                        {
                            that.onFilesDeleted(rs);
                        }
                    }
                });
                return;
            }
            if (that.selectedFile || (that.limite == 1 && that.files.length > 0))
            {
                alert((that.limite == 1 ? "Excluir o arquivo?" : "Excluir o arquivo selecionado?"), "Não", ["Sim"], function (index)
                {
                    if (index == 0)
                    {
                        if (that.limite == 1)
                        {
                            that.removeFile(that.files[0]);
                        }
                        else
                        {
                            var i = that.divsFiles.indexOf(that.selectedFile);
                            var f = that.selectedFile.file;
                            that.removeFile(f);
                            that.selectedFile = null;
                            if (i < that.divsFiles.length)
                            {
                                that.divsFiles[i].focus();
                            }
                            else if (i > 0)
                            {
                                that.divsFiles[i - 1].focus();
                            }
                            if (that.onFilesDeleted)
                            {
                                that.onFilesDeleted([f]);
                            }
                        }
                    }
                });
            }
        };

        this.downloadButton = document.createElement("button");
        this.downloadButton.type = "button";
        this.downloadButton.title = "Baixar arquivo";
        this.downloadButton.className = "chagas-button chagas-tablebutton add";
        this.downloadButton.style.backgroundImage = "url(" + downloadFileImage + ")";
        this.downloadButton.style.display = "none";
        divFooter.appendChild(this.downloadButton);
        this.downloadButton.onclick = function ()
        {
            if (that.selectedFile || (that.limite == 1 && that.files.length > 0))
            {
                if (that.limite == 1)
                {
                    that.downloadFile(that.files[0]);
                }
                else
                {
                    that.downloadFile(that.selectedFile.file);
                }
            }
        };

        this.checkAll = document.createElement("input");
        this.checkAll.type = "checkbox";
        this.checkAll.style.cssText = "margin-left:5px; margin-top:3px;";
        this.checkAll.onmousedown = function ()
        {
            if (this.indeterminate)
            {
                this.indeterminate = false;
            }
        };
        this.checkAll.onclick = function ()
        {
            if (that.divsFiles.length > 0)
            {
                that.checkAllFiles(this.checked);
            }
            else
            {
                this.checked = false;
            }
        };
        this.divCheckAll = document.createElement("div");
        this.divCheckAll.className = "chagas-tablebutton add";
        this.divCheckAll.title = "Marcar todos";
        this.divCheckAll.appendChild(this.checkAll);

        divFooter.appendChild(this.divCheckAll);

        this.contadorDiv = document.createElement("div");
        this.contadorDiv.className = "chagas-font";
        this.contadorDiv.style.cssText = "padding-right:5px; height:20px; line-height:20px; float:right; font-size:12px;";
        this.contadorDiv.innerHTML = "0 arquivos";
        divFooter.appendChild(this.contadorDiv);
        that.appendChild(divFooter);
        that.files = new Array();
        that.divsFiles = new Array();
        Object.defineProperty(this, 'limite', {
            get: function ()
            {
                var l = 1;
                if (that.attributes["limite"] && parseInt(that.attributes["limite"].value) != 0)
                {
                    l = parseInt(that.attributes["limite"].value);
                }
                return l;
            },
            set: function (value)
            {
                if (that.attributes["chagasfield"] && that.attributes["chagasfield"].value.length > 0)
                {
                    value = 1;
                }
                that.setAttribute("limite", value);
                that.divLista.className = value == 1 ? "chagas-listafilesunico" : "chagas-listafiles";

                that.divCheckAll.style.display = value == 1 ? "none" : "";
                if (value == 1)
                {
                    that.divLista.style.backgroundImage = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAgCAYAAAB6kdqOAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjlEQTRDNUI5NDFDNjExRTVBMzhDRkU4NjA1NzU4RkY3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjlEQTRDNUJBNDFDNjExRTVBMzhDRkU4NjA1NzU4RkY3Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OURBNEM1Qjc0MUM2MTFFNUEzOENGRTg2MDU3NThGRjciIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OURBNEM1Qjg0MUM2MTFFNUEzOENGRTg2MDU3NThGRjciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6JW5NNAAABZ0lEQVR42uSX0Q2CMBCGC/FdnEDcgA10A91AZAHZQDbQBQA3cAQcwQ3UDXQCvEtq0jRHaRXbJl7yP9AU+nP92l6DsiwXjLEaFDO7cQNtsiw7i42hIzOMj1nLjSPBTGDZUEslImSexYhoQ6ZOoMiGgaqqWpEpKkPWzFBMURmKbDIFq0zMVOwdQzYMYcb3oCvXzhTqoaMAbaXnCSh3laGUaFu6nDIqgl8YWnT8vRwHou34C4bwo1PQhUvFEIK9Et4rhjZUcDMYDWgGeij6510QDzFlkbRqIm7K2T50II6WhColNKf9K0MI8lqxvFODLJ+ob4UfsKOKmmerz0wjQP6xIfz7uUa/RlEtJPz4SL5l6H0e6falIF/1mDUylBvWSDLkqW6dpbMPxX0n9ACQG2Xo6Fs9NPfNkPWK8Snck1ofDOFyvPt0LztLN8hWt5j6C4aofQiZGtviCe5j4uM97NjiXTCFY6YvAQYAFgZCKFUGrTIAAAAASUVORK5CYII=')";
                    that.contadorDiv.innerHTML = "";
                }

                if (value > 1)
                {
                    that.divLista.style.backgroundImage = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABACAYAAACa5WD/AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NTgyOTBCRTg0MURFMTFFNTlBMjVDNDI1MkY1RTg1QUMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NTgyOTBCRTc0MURFMTFFNTlBMjVDNDI1MkY1RTg1QUMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MEY1NjZBOUM0MUNCMTFFNTkxOEFCNENBMjE0OEY3NzQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MEY1NjZBOUQ0MUNCMTFFNTkxOEFCNENBMjE0OEY3NzQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6n+6LxAAAE6ElEQVR42uxbvVLbQBCWmPRBfgGI/ALM2D0p7Do0oo1oTBk604XSqkJrN3ZtFzyANQMPgGfyAijhBVDyBMpe8t3McnOSLSJkG+/O7MjWre7nu9Xe7odxsyxzRF5f9gQCAfpNybs6B5tMJh/pMiY9rHmdP0nPwjC82xWPXgfIDsYc74xHM5DdmsfN1rTBEqPfukebomL2Del+TWdEtq6YvW6Prg3kdcfsdXv0fp0xmzyYe3atMVtitAD94jfkG+kP6Fc5DF9Hrki/GN890gvx6IrDsOXeJwkd9YgrQJfLt8MV7K5t6bPE6BK1BukB6XdoUYxWB+IJe+5KgF79gDvA51vSD6S/CuwvNuHw27bQsW9kEfsAW/LoiuXaUqIfvbB8ngjQ+Qfg54I0LizxVtwU9LXzQC87xMbw7mUg37LDUYC2eOzxCna3Tj77d4Qy/GgTFrSJQGu+YlVb2+F4smQTBGikZmUAMg/H0Fkfz701efSh8zLGrczhKB69KeXyLgB9LECLbBXQv3HNoAL0K4lKux530aPrzjrunOd/fTa92hWPFtnqPFrF7Pd1xevJ5Fn2+LhLHr2umP1Yd4Hjyr9WSIwWoEUEaAFa0rvqUyj1W7cH0igMw6hkyrVM+qQp6aiq+eqf8lrm1aHLkNqbm5pHBwBDgRJV3Lda/KwODySAY7o0N9ajSXoAOCCv6NGER9o7SH3SGIAlzr9fenZJp2hTG3QKmznsHHhwgu/62QBj6fZzyzyG+LxAvw7GauGeuro0PzXWDHPtoe+oSo/eqzhsKBB8NWF4XsCaFZBNauviuwotDdw/B88xAkA+ANX3z7HwGJ89tDegLbYpXNqMPwnwliW4V8ub8VqHoVqMh39dUN7Uoc8ttCUEbMJsY1xTPPcEIDyAEaEP1dc97vNN8/HME4BuGXPxsGkZa/PZuNsJNA7BHrzWVco8tEimALZhLP4SntewAJng1W/AxjXOA2U7wPgN2OsQEjCnMDfGyXkzNsqjFaCx4bU6fHgFz80ASsYW7MOLM3hsDE3g5SlAe2Ieb9uIe6g+DyJcMwNodX+AN9F7k1xHyfSuSlGb+YA3TwqWtyLC3kkJLkCLCNA7Qiohg/BRPndxbda1mDyCaKOBJtCGyI0T8ATq8wJ8QMbTIma7sHAOHnJZn1V8fWxAn1WJc9heGsUH5yy66KfPqjwPOXQb4/+V8Xg8ZxXkCG0D2LfZeJonMdcwNeboM8IsZXn7KYoc1ZacnZ21Vw4d4Cw6rFrrF2xIi9mmlsqqx6q4POkD4AbsedEwwILb+KzLdl0x6o2xVXQx7HTRpIBw0U+EfgPGleStwWHkVZNxKBEDfJQHclGM9lHlpcxr8qQFHiOFx81sfWEBSUEfQ1R6vsFNaH6bV38L5lkJrjZZ0OJTZr9g/c6oLWEsnu6nW8CDJGzz7xk34xSsrRDoBISQZsn0QjxGEjls0T5shxYOQdOburTW4cQzgDtnvEXMnk3hRa5T/pdMPoUQvXGeMaeAtS3Ypg7ZG+BZyKoA2mbzfFnWAdI7hodpblZzElPDdsFsO5bBR4xp8xiwT2wREeM7TKbuEh6dsVhdhn95QP+p0WcfbTPMh69hZJkjdyzNl6SrciOVVoYr8BZzLKyyP0XlZR04DFV4KByL7P5rTOpf8mjhOqQyFBGgt1z+CDAAJlyPtN+jNpUAAAAASUVORK5CYII=')";
                    that.divTituloText.style.color = '#428bca';
                    that.divTituloText.classList.add('colortitleedit');
                    that.divTituloText.style.textDecoration = 'underline';
                }

            }
        });
        this.limite = this.limite;
        Object.defineProperty(this, 'titulo', {
            get: function ()
            {
                return that.attributes["titulo"] ? that.attributes["titulo"].value : "";
            },
            set: function (value)
            {
                that.setAttribute("titulo", value);
                if (value && value.length > 0)
                {
                    that.divTituloText.innerHTML = value;
                    that.divTitulo.style.display = "";
                    that.divLista.style.height = "calc(100% - 40px)";
                }
                else
                {
                    that.divTituloText.innerHTML = "";
                    that.divTitulo.style.display = "none";
                    that.divLista.style.height = "calc(100% - 20px)";
                }
            }
        });
        this.titulo = this.titulo;
        Object.defineProperty(this, 'extensoes', {
            get: function ()
            {
                return that.attributes["extensoes"] ? that.attributes["extensoes"].value : "";
            },
            set: function (value)
            {
                that.setAttribute("extensoes", value);
            }
        });
        this.extensoes = this.extensoes;
        Object.defineProperty(this, 'itemClass', {
            get: function ()
            {
                return that.attributes["itemClass"] ? that.attributes["itemClass"].value : "";
            },
            set: function (value)
            {
                that.setAttribute("itemClass", value);
                for (var x = 0; x < that.divsFiles.length; x++)
                {
                    that.divsFiles[x].className = "file " + that.itemClass;
                }
            }
        });
        this.itemClass = this.itemClass;
        Object.defineProperty(this, 'disableAdd', {
            get: function ()
            {
                if (!that.attributes["disableAdd"])
                {
                    return false;
                }
                return that.attributes["disableAdd"].value.toLowerCase() == "true" ? true : false;
            },
            set: function (value)
            {
                that.setAttribute("disableAdd", value ? "true" : "false");
                if (that.addButton)
                {
                    that.addButton.style.display = value ? "none" : "";
                }
            }
        });
        Object.defineProperty(this, 'disableDelete', {
            get: function ()
            {
                if (!that.attributes["disableDelete"])
                {
                    return false;
                }
                return that.attributes["disableDelete"].value.toLowerCase() == "true" ? true : false;
            },
            set: function (value)
            {
                that.setAttribute("disableDelete", value ? "true" : "false");
                if (that.deleteButton)
                {
                    that.deleteButton.style.display = value ? "none" : "";
                }
            }
        });
        this.disableAdd = this.disableAdd;
        this.disableDelete = this.disableDelete;
    };

    _ChagasFile.atualizarFiles = function ()
    {
        if (this.limite == 1)
        {
            this.contadorDiv.innerHTML = "";
        }
        else
        {
            this.contadorDiv.innerHTML = this.files.length + " arquivo" + (this.files.length == 1 ? "" : "s");
        }
    };

    _ChagasFile.atualizarBotoes = function ()
    {
        this.downloadButton.style.display = "none";
        if (this.selectedFile || (this.limite == 1 && this.files.length > 0))
        {
            this.downloadButton.style.display = "";
        }
        var checkeds = 0;
        var uncheckeds = 0;
        for (var x = 0; x < this.divsFiles.length; x++)
        {
            if (this.divsFiles[x].checkbox.checked)
            {
                checkeds++;
            }
            else
            {
                uncheckeds++;
            }
            if (checkeds > 0 && uncheckeds > 0)
            {
                break;
            }
        }
        if (checkeds > 0 && uncheckeds > 0)
        {
            this.checkAll.indeterminate = true;
            this.checkAll.checked = true;
        }
        else if (checkeds > 0)
        {
            this.checkAll.indeterminate = false;
            this.checkAll.checked = true;
        }
        else
        {
            this.checkAll.indeterminate = false;
            this.checkAll.checked = false;
        }
    };

    _ChagasFile.cancelarUpload = function ()
    {
        if (this.uploadAtual)
        {
            this.uploadAtual.cancelarUpload = true;
        }
    };

    _ChagasFile.carregarFiles = function (files)
    {
        var that = this;
        var listaACarregar = new Array();
        if (files.length > 0)
        {
            that.divLista.style.backgroundImage = 'none';
        }
        for (var x = 0; x < files.length; x++)
        {
            var f = document.createElement("div");
            f.className = "file " + that.itemClass;
            f.tabIndex = "0";
            f.onclick = function (evt)
            {
                if (that.limite != 1)
                {
                    evt = evt || window.event;
                    this.onfocus();
                    evt.stopPropagation();
                }
            };
            f.onfocus = function ()
            {
                if (that.limite != 1)
                {
                    if (that.selectedFile)
                    {
                        that.selectedFile.style.backgroundColor = "";
                    }
                    that.selectedFile = this;
                    that.atualizarBotoes();
                    this.style.backgroundColor = corOver;
                }
            };
            f.ondblclick = function ()
            {
                ChagasFile.showPreview(this.file);
            };
            var thumb = document.createElement("div");
            thumb.className = "thumb";
            f.thumb = thumb;
            f.appendChild(thumb);
            var titulo = document.createElement("div");
            titulo.className = "title";
            titulo.innerHTML = (files[x].Caminho && files[x].Caminho.length > 0 ? files[x].Nome : files[x].name);
            titulo.file = f;
            titulo.onclick = function (evt)
            {
                if (that.limite == 1 || that.selectedFile == this.file)
                {
                    this.file.editText.style.display = "";
                    this.file.editText.value = this.innerHTML;
                    this.file.editText.focus();
                    var i = this.innerHTML.lastIndexOf(".");
                    if (i > -1)
                    {
                        this.file.editText.setSelectionRange(i, i);
                    }
                    else
                    {
                        this.file.editText.setSelectionRange(this.innerHTML.length, this.innerHTML.length);
                    }
                    evt = evt || window.event;
                    evt.stopPropagation();
                }
            };
            f.title = titulo.innerHTML;

            var editText = document.createElement("input");
            editText.type = "text";
            editText.className = "edittext";
            editText.style.display = "none";
            editText.file = f;
            editText.onblur = function ()
            {
                this.file.titulo.innerHTML = this.value;
                this.file.file.name = this.value;
                if (this.file.file.Nome)
                {
                    this.file.file.Nome = this.value;
                }
                this.style.display = "none";
            };
            editText.onclick = function (evt)
            {
                evt = evt || window.event;
                evt.stopPropagation();
            };
            editText.onkeydown = function (evt)
            {
                evt = evt || window.event;
                if (evt.keyCode == 27)//Esc
                {
                    this.value = this.file.titulo.innerHTML;
                    this.file.focus();
                }
            };
            f.editText = editText;

            var checkbox = document.createElement("input");
            checkbox.type = "checkbox";
            checkbox.className = "checkbox";
            checkbox.onclick = function ()
            {
                that.atualizarBotoes();
            };
            f.appendChild(checkbox);
            f.checkbox = checkbox;
            f.titulo = titulo;
            f.appendChild(editText);
            f.appendChild(titulo);

            f.file = files[x];
            this.files.push(f.file);
            this.divsFiles.push(f);
            var ext = ChagasFile.fileExtension(f.file);
            if (ext == ".jpg" ||
                ext == ".jpeg" ||
                ext == ".png")
            {
                listaACarregar.push(f);
                thumb.className = "thumb image";
            }
            else
            {
                thumb.style.backgroundImage = "url(" + ChagasFile.thumbExtension(ext) + ")";
            }
            this.divLista.appendChild(f);
        }
        var indexCarregar = 0;
        if (indexCarregar < listaACarregar.length)
        {
            function carrega()
            {
                if (listaACarregar[indexCarregar].file.CaminhoThumb &&
                    listaACarregar[indexCarregar].file.CaminhoThumb.length > 0)
                {
                    listaACarregar[indexCarregar].thumb.style.backgroundImage = "url(" + listaACarregar[indexCarregar].file.CaminhoThumb + ")";
                    indexCarregar++;
                    if (indexCarregar < listaACarregar.length)
                    {
                        carrega();
                    }
                }
                else
                {
                    ChagasFile.montarThumb(listaACarregar[indexCarregar].file, 190, 190, null, function (base64)
                    {
                        listaACarregar[indexCarregar].thumb.style.backgroundImage = "url(" + base64 + ")";
                        indexCarregar++;
                        if (indexCarregar < listaACarregar.length)
                        {
                            carrega();
                        }
                    });
                }
            }
            carrega();
        }
        this.atualizarFiles();
        this.atualizarBotoes();
    };

    _ChagasFile.checkAllFiles = function (checked)
    {
        for (var x = 0; x < this.divsFiles.length; x++)
        {
            this.divsFiles[x].checkbox.checked = checked;
        }
        this.atualizarBotoes();
    };

    _ChagasFile.checkFiles = function (files, checked)
    {
        for (var x = 0; x < files.length; x++)
        {
            var i = this.files.indexOf(files[x]);
            this.divsFiles[i].checkbox.checked = checked;
        }
        this.atualizarBotoes();
    };

    _ChagasFile.downloadFile = function (file)
    {
        ChagasFile.download(file);
    };

    _ChagasFile.getCheckedFiles = function ()
    {
        var ret = new Array();
        for (var x = 0; x < this.divsFiles.length; x++)
        {
            if (this.divsFiles[x].checkbox.checked)
            {
                ret.push(this.divsFiles[x].file);
            }
        }
        return ret;
    };

    _ChagasFile.limparFiles = function ()
    {
        this.selectedFile = null;
        while (this.files.length > 0)
        {
            this.removeFile(this.files[0]);
        }
    };

    _ChagasFile.onFilesDeleted = function (files)
    {

    };

    _ChagasFile.removeFile = function (file)
    {
        var i = this.files.indexOf(file);
        this.files.splice(i, 1);
        this.divLista.removeChild(this.divsFiles[i]);
        if (this.divLista.children.length == 0)
        {
            if (this.limite > 1)
            {
                this.divLista.style.backgroundImage = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABACAYAAACa5WD/AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NTgyOTBCRTg0MURFMTFFNTlBMjVDNDI1MkY1RTg1QUMiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NTgyOTBCRTc0MURFMTFFNTlBMjVDNDI1MkY1RTg1QUMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MEY1NjZBOUM0MUNCMTFFNTkxOEFCNENBMjE0OEY3NzQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MEY1NjZBOUQ0MUNCMTFFNTkxOEFCNENBMjE0OEY3NzQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6n+6LxAAAE6ElEQVR42uxbvVLbQBCWmPRBfgGI/ALM2D0p7Do0oo1oTBk604XSqkJrN3ZtFzyANQMPgGfyAijhBVDyBMpe8t3McnOSLSJkG+/O7MjWre7nu9Xe7odxsyxzRF5f9gQCAfpNybs6B5tMJh/pMiY9rHmdP0nPwjC82xWPXgfIDsYc74xHM5DdmsfN1rTBEqPfukebomL2Del+TWdEtq6YvW6Prg3kdcfsdXv0fp0xmzyYe3atMVtitAD94jfkG+kP6Fc5DF9Hrki/GN890gvx6IrDsOXeJwkd9YgrQJfLt8MV7K5t6bPE6BK1BukB6XdoUYxWB+IJe+5KgF79gDvA51vSD6S/CuwvNuHw27bQsW9kEfsAW/LoiuXaUqIfvbB8ngjQ+Qfg54I0LizxVtwU9LXzQC87xMbw7mUg37LDUYC2eOzxCna3Tj77d4Qy/GgTFrSJQGu+YlVb2+F4smQTBGikZmUAMg/H0Fkfz701efSh8zLGrczhKB69KeXyLgB9LECLbBXQv3HNoAL0K4lKux530aPrzjrunOd/fTa92hWPFtnqPFrF7Pd1xevJ5Fn2+LhLHr2umP1Yd4Hjyr9WSIwWoEUEaAFa0rvqUyj1W7cH0igMw6hkyrVM+qQp6aiq+eqf8lrm1aHLkNqbm5pHBwBDgRJV3Lda/KwODySAY7o0N9ajSXoAOCCv6NGER9o7SH3SGIAlzr9fenZJp2hTG3QKmznsHHhwgu/62QBj6fZzyzyG+LxAvw7GauGeuro0PzXWDHPtoe+oSo/eqzhsKBB8NWF4XsCaFZBNauviuwotDdw/B88xAkA+ANX3z7HwGJ89tDegLbYpXNqMPwnwliW4V8ub8VqHoVqMh39dUN7Uoc8ttCUEbMJsY1xTPPcEIDyAEaEP1dc97vNN8/HME4BuGXPxsGkZa/PZuNsJNA7BHrzWVco8tEimALZhLP4SntewAJng1W/AxjXOA2U7wPgN2OsQEjCnMDfGyXkzNsqjFaCx4bU6fHgFz80ASsYW7MOLM3hsDE3g5SlAe2Ieb9uIe6g+DyJcMwNodX+AN9F7k1xHyfSuSlGb+YA3TwqWtyLC3kkJLkCLCNA7Qiohg/BRPndxbda1mDyCaKOBJtCGyI0T8ATq8wJ8QMbTIma7sHAOHnJZn1V8fWxAn1WJc9heGsUH5yy66KfPqjwPOXQb4/+V8Xg8ZxXkCG0D2LfZeJonMdcwNeboM8IsZXn7KYoc1ZacnZ21Vw4d4Cw6rFrrF2xIi9mmlsqqx6q4POkD4AbsedEwwILb+KzLdl0x6o2xVXQx7HTRpIBw0U+EfgPGleStwWHkVZNxKBEDfJQHclGM9lHlpcxr8qQFHiOFx81sfWEBSUEfQ1R6vsFNaH6bV38L5lkJrjZZ0OJTZr9g/c6oLWEsnu6nW8CDJGzz7xk34xSsrRDoBISQZsn0QjxGEjls0T5shxYOQdOburTW4cQzgDtnvEXMnk3hRa5T/pdMPoUQvXGeMaeAtS3Ypg7ZG+BZyKoA2mbzfFnWAdI7hodpblZzElPDdsFsO5bBR4xp8xiwT2wREeM7TKbuEh6dsVhdhn95QP+p0WcfbTPMh69hZJkjdyzNl6SrciOVVoYr8BZzLKyyP0XlZR04DFV4KByL7P5rTOpf8mjhOqQyFBGgt1z+CDAAJlyPtN+jNpUAAAAASUVORK5CYII=')";
            }
            else
            {
                this.divLista.style.backgroundImage = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAgCAYAAAB6kdqOAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjlEQTRDNUI5NDFDNjExRTVBMzhDRkU4NjA1NzU4RkY3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjlEQTRDNUJBNDFDNjExRTVBMzhDRkU4NjA1NzU4RkY3Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OURBNEM1Qjc0MUM2MTFFNUEzOENGRTg2MDU3NThGRjciIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OURBNEM1Qjg0MUM2MTFFNUEzOENGRTg2MDU3NThGRjciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6JW5NNAAABZ0lEQVR42uSX0Q2CMBCGC/FdnEDcgA10A91AZAHZQDbQBQA3cAQcwQ3UDXQCvEtq0jRHaRXbJl7yP9AU+nP92l6DsiwXjLEaFDO7cQNtsiw7i42hIzOMj1nLjSPBTGDZUEslImSexYhoQ6ZOoMiGgaqqWpEpKkPWzFBMURmKbDIFq0zMVOwdQzYMYcb3oCvXzhTqoaMAbaXnCSh3laGUaFu6nDIqgl8YWnT8vRwHou34C4bwo1PQhUvFEIK9Et4rhjZUcDMYDWgGeij6510QDzFlkbRqIm7K2T50II6WhColNKf9K0MI8lqxvFODLJ+ob4UfsKOKmmerz0wjQP6xIfz7uUa/RlEtJPz4SL5l6H0e6falIF/1mDUylBvWSDLkqW6dpbMPxX0n9ACQG2Xo6Fs9NPfNkPWK8Snck1ofDOFyvPt0LztLN8hWt5j6C4aofQiZGtviCe5j4uM97NjiXTCFY6YvAQYAFgZCKFUGrTIAAAAASUVORK5CYII=')";
            }
        }
        this.divsFiles[i].thumb.style.backgroundImage = "";
        this.divsFiles.splice(i, 1);
        this.atualizarFiles();
        this.atualizarBotoes();
    };

    _ChagasFile.upload = function (retorno, progressChange, url)
    {
        if (!url)
        {
            url = location.pathname + "/UploadArquivo";
        }
        var that = this;
        this.uploadAtual = ChagasFile.upload(url, this.files, function (evt, msg)
        {
            that.uploadAtual = null;
            if (retorno)
            {
                retorno(evt, msg);
            }
        }, progressChange);
    };

    ChagasFile = document.registerElement('chagas-file',
    {
        prototype: _ChagasFile,
        extends: 'div'
    });

    ChagasFile.upload = function (pageMethod, files, retorno, progressChange)
    {
        var ret = {
            cancelarUpload: false
        }
        var index = -1;
        var request;
        function next()
        {
            index++;
            if (index < files.length)
            {
                if (!files[index].name && (!files[index].base64 || files[index].base64.length == 0))
                {
                    next();
                    return;
                }

                function up(base64)
                {
                    request = $.ajax({
                        type: 'POST',
                        url: pageMethod,
                        data: '{ "file" : { "base64" : "' + base64 +
                              '" , "name" : "' + files[index].name + '" } }',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        xhr: function ()
                        {
                            xhrUpload = $.ajaxSettings.xhr();
                            if (xhrUpload.upload)
                            {
                                if (progressChange)
                                {
                                    xhrUpload.upload.addEventListener('progress', function (evt)
                                    {
                                        if (evt.lengthComputable)
                                        {
                                            evt.percentual = evt.loaded / evt.total * 100;
                                            evt.fileIndex = index;
                                            progressChange(evt);
                                        }
                                        else
                                        {
                                            evt.percentual = 100;
                                            evt.fileIndex = index;
                                            progressChange(evt);
                                        }
                                    }, false);
                                }
                            }
                            return xhrUpload;
                        },
                        success: function (msg)
                        {
                            if (msg.d.ok)
                            {
                                if (msg.d.file)
                                {
                                    files[index].FileTemp = msg.d.file.FileTemp;
                                    files[index].Nome = msg.d.file.Nome;
                                }
                                if (ret.cancelarUpload)
                                {
                                    fim(false, "Upload cancelado");
                                }
                                else
                                {
                                    next();
                                }
                            }
                            else if (!ret.cancelarUpload)
                            {
                                if (request && (request.readyState == 1 || request.statusText == 'error'))
                                {
                                    request.abort();
                                    request = null;
                                }
                                if (!ret.cancelarUpload)
                                {
                                    fim(false, msg.d.mensagem);
                                }
                            }
                            else
                            {
                                fim(false, "Upload cancelado");
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown)
                        {
                            if (request && (request.readyState == 1 || request.statusText == 'error'))
                            {
                                request.abort();
                                request = null;
                            }
                            if (ret.cancelarUpload)
                            {
                                fim(false);
                            }
                            else
                            {
                                index--;
                                next();
                            }
                        }
                    });
                }
                if (files[index].base64 && files[index].base64.length > 0)
                {
                    if (files[index].type.toLowerCase() == "image/jpeg" ||
                        files[index].type.toLowerCase() == "image/jpg" ||
                        files[index].type.toLowerCase() == "image/png")
                    {
                        var urlAPI = (window.createObjectURL && window) ||
                         (window.URL && URL.revokeObjectURL && URL) ||
                         (window.webkitURL && webkitURL);
                        var blob = urlAPI.createObjectURL(files[index]);
                        RedimensionarImagem(blob, 0.9, 900, 700, function (ret)
                        {
                            up(ret);
                        }, false);
                    }
                    else
                    {
                        up(files[index].base64);
                    }
                }
                else
                {
                    var fr = new FileReader();
                    fr.onloadend = function (evt)
                    {
                        var base64 = evt.target.result;
                        this.abort();
                        base64 = base64.substring(base64.indexOf(",") + 1, base64.length);
                        up(base64);
                    };
                    fr.onerror = function (evt)
                    {
                        fim(false, "Falha no upload dos Arquivos");
                    };

                    if (files[index].type.toLowerCase() == "image/jpeg" ||
                        files[index].type.toLowerCase() == "image/jpg" ||
                        files[index].type.toLowerCase() == "image/png")
                    {
                        var urlAPI = (window.createObjectURL && window) ||
                         (window.URL && URL.revokeObjectURL && URL) ||
                         (window.webkitURL && webkitURL);
                        var blob = urlAPI.createObjectURL(files[index]);
                        RedimensionarImagem(blob, 0.9, 900, 700, function (ret)
                        {
                            var base64 = ret;
                            base64 = base64.substring(base64.indexOf(",") + 1, base64.length);
                            up(base64);
                        }, false);
                    }
                    else
                    {
                        fr.readAsDataURL(files[index]);
                    }
                }
            }
            else
            {
                fim(true, "OK");
            }
        }
        function fim(ok, msg)
        {
            if (retorno)
            {
                retorno(ok, msg);
            }
        }
        next();
        return ret;
    };

    ChagasFile.download = function (file)
    {
        var isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        var isSafari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;
        var nome = "";
        function down(base64)
        {
            if (isChrome || isSafari)
            {
                if (base64.indexOf(",") < 0)
                {
                    base64 = "data:image/png;base64," + base64;
                }
                var a = document.createElement("a");
                a.href = base64;
                a.download = nome;
                a.click();
            }
            else
            {
                base64 = base64.substring(base64.indexOf(",") + 1, base64.length);
                var binary = atob(base64.replace(/\s/g, ''));
                var len = binary.length;
                var buffer = new ArrayBuffer(len);
                var view = new Uint8Array(buffer);
                for (var i = 0; i < len; i++)
                {
                    view[i] = binary.charCodeAt(i);
                }
                var blob = new Blob([view]);
                window.navigator.msSaveOrOpenBlob(blob, nome);
            }
        }
        if (file.base64 && file.base64.length > 0)
        {
            nome = file.Nome;
            var base64 = file.base64;
            down(base64);
        }
        else if (file.Caminho)
        {
            nome = file.Nome;
            var sUrl = file.Caminho;
            if (isChrome || isSafari)
            {
                var link = document.createElement('a');
                link.href = sUrl;
                link.download = file.Nome;
                if (document.createEvent)
                {
                    var e = document.createEvent('MouseEvents');
                    e.initEvent('click', true, true);
                    link.dispatchEvent(e);
                    return true;
                }
            }
            var query = '?download';
            window.open(sUrl + query);
        }
        else if (file.name && file.type)
        {
            nome = file.name;
            var fr = new FileReader();
            fr.onload = function (result)
            {
                down(this.result);
            };
            fr.readAsDataURL(file);
        }
    };

    ChagasFile.getURL = function (file, retorno)
    {
        if (file.base64 && file.base64.length > 0)
        {
            var base64 = file.base64;
            if (base64.indexOf(",") < 0)
            {
                var ext = ChagasFile.fileExtension(file).replace(".", "");
                base64 = "data:image/" + ext + ";base64," + base64;
            }
            if (retorno)
            {
                retorno(base64);
            }
        }
        else if (file.Caminho)
        {
            if (retorno)
            {
                retorno(file.Caminho);
            }
        }
        else if (file.name && file.type)
        {
            var urlAPI = (window.createObjectURL && window) ||
                     (window.URL && URL.revokeObjectURL && URL) ||
                     (window.webkitURL && webkitURL);
            var blob = urlAPI.createObjectURL(file);
            if (retorno)
            {
                retorno(blob);
            }
        }
    };

    ChagasFile.fileExtension = function (file, nome)
    {
        var n = "";
        if (file && file.Nome)
        {
            n = file.Nome;
        }
        else if (file && file.name)
        {
            n = file.name;
        }
        else if (nome)
        {
            n = nome;
        }
        if (n.length > 0)
        {
            var i = n.lastIndexOf(".");
            if (i == -1)
            {
                return "";
            }
            return n.substring(i).toLowerCase();
        }
        return "";
    };

    ChagasFile.thumbExtension = function (ext)
    {
        if (ext == ".txt")
        {
            return txtFileImage;
        }
        else if (ext == ".pdf")
        {
            return pdfFileImage;
        }
        else if (ext == ".doc")
        {
            return docFileImage;
        }
        else if (ext == ".docx")
        {
            return docxFileImage;
        }
        else if (ext == ".zip")
        {
            return zipFileImage;
        }
        else if (ext == ".rar")
        {
            return rarFileImage;
        }
        else if (ext.length > 0)
        {
            return fileFileImage;
        }
        return "";
    };

    ChagasFile.montarThumb = function (file, w, h, quali, retorno)
    {
        var urlAPI = (window.createObjectURL && window) ||
                     (window.URL && URL.revokeObjectURL && URL) ||
                     (window.webkitURL && webkitURL);
        var canvas = document.createElement("canvas");
        var img = new Image();
        img.setAttribute("style", "height:100px;");
        img.addEventListener('load', function ()
        {
            var MAX_WIDTH = w;
            var MAX_HEIGHT = h;
            var width = this.width;
            var height = this.height;
            if (width > height)
            {
                if (width > MAX_WIDTH)
                {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            }
            else
            {
                if (height > MAX_HEIGHT)
                {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            ctx.clearRect(0, 0, width, height);
            ctx.drawImage(this, 0, 0, width, height);
            var ret = canvas.toDataURL(file.type, (quali ? quali : 0.6));
            this.src = "";
            if (retorno)
            {
                retorno(ret);
            }
        });
        if (file.base64 && file.base64.length > 0)
        {
            img.src = (file.base64.indexOf(",") == -1 ? "data:image/jpg;base64," : "") + file.base64;
        }
        else
        {
            var blob = urlAPI.createObjectURL(file);
            img.src = blob;
        }
    };

    ChagasFile.showPreview = function (file)
    {
        var isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        var isSafari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;
        if (!isChrome && !isSafari)
        {
            return;
        }

        var m = document.createElement("div");
        m.style.cssText = "width:85%; height:95%; background-color:white;";
        var iframe = document.createElement("iframe");
        m.appendChild(iframe);
        iframe.style.cssText = "width:100%; height:100%; border:0; margin:0; ";
        Chagas.openModal(m);
        m.bloqueio.onclick = function ()
        {
            Chagas.closeModal(m);
        };
        iframe.onload = function ()
        {
            if (this.contentDocument.body.firstChild &&
                this.contentDocument.body.firstChild.tagName &&
                this.contentDocument.body.firstChild.tagName.toLowerCase() == "img")
            {
                this.contentDocument.body.firstChild.style.maxWidth = "100%";
                this.contentDocument.body.firstChild.style.maxHeight = "100%";
                this.contentDocument.body.style.textAlign = "center";
            }
        };
        ChagasFile.getURL(file, function (url)
        {
            iframe.src = url;
        });
    };
})();
// CHAGAS FILE //

// CHAGAS TABLE
(function ()
{
    var _ChagasTable = Object.create(HTMLElement.prototype);
    _ChagasTable.carregando = false;
    _ChagasTable.columns = new Array();
    _ChagasTable.disableAdd = false;
    _ChagasTable.disableDelete = false;
    _ChagasTable.disableEdit = false;
    _ChagasTable.dontShowArrows = false;
    _ChagasTable.modalDataPropertyName = "";
    _ChagasTable.modalTitulo = "";
    _ChagasTable.modalContent;
    _ChagasTable.onCellClick = null;
    _ChagasTable.orderDataPropertyName = "";
    _ChagasTable.rows;
    _ChagasTable.selectedCells = new Array();
    _ChagasTable.selectionCheckColumn = true;
    _ChagasTable.selectionMode = "cell"; //"fullrow", "cell"
    _ChagasTable.titulo = "";
    _ChagasTable.draggingRow;

    _ChagasTable.createdCallback = function ()
    {
        var that = this;
        that.columns = new Array();
        that.selectedCells = new Array();
        this.className = "chagas-borderbox chagas-table " + this.className;
        this.style.position = "relative";

        this.divext = document.createElement("div");
        this.divext.style.cssText = "width:100%; height:100%; position:relative;";

        this.header = document.createElement("div");
        this.header.style.cssText = "top:0; height:40px; width:auto; position:absolute; background-color:#5682b1; color:#fff; overflow:hidden; cursor:default; ";
        this.header.className = "chagas-noselect headercolor";
        this.headerInner = document.createElement("table");
        this.headerInner.style.cssText = "min-width:100%; height:100%; border-spacing:0; border-collapse:separate;";
        var trH = document.createElement("tr");
        trH.className = "chagas-tablerowheader";
        this.headerInner.appendChild(trH);
        this.header.appendChild(this.headerInner);

        this.grade = document.createElement("table");
        this.grade.className = "chagas-textselect";
        this.grade.style.cssText = "min-width:100%; padding-top:40px; border-collapse:separate;";
        this.grade.cellSpacing = 0;
        this.grade.columns = new Array();
        this.grade.tabIndex = "0";
        this.grade.onkeydown = function (evt)
        {
            if (that.editando)
            {
                return;
            }
            evt = evt || window.event;
            if (evt.keyCode == 40 ||
               (that.selectionMode.toLowerCase() == ChagasTable.selectionMode.fullRow && !evt.shiftKey && evt.keyCode == 9))//down
            {
                if (that.selectedCells.length > 0)
                {
                    var linhas = that.grade.getElementsByTagName("tr");
                    for (var x = 0; x < linhas.length; x++)
                    {
                        if (linhas[x] == that.selectedCells[0].row && x < linhas.length - 1)
                        {
                            var cols = that.selectedCells[0].row.getElementsByTagName("td");
                            for (var c = 0; c < cols.length; c++)
                            {
                                if (cols[c] == that.selectedCells[0])
                                {
                                    var cell = linhas[x + 1].getElementsByTagName("td")[c];
                                    that.selectCell(cell);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    evt.stopPropagation();
                    evt.preventDefault();
                }
                else if (that.grade.rows.length > 0)
                {
                    that.selectRow(that.grade.rows[0]);
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }
            else if (evt.keyCode == 38 ||
                     (that.selectionMode.toLowerCase() == ChagasTable.selectionMode.fullRow && evt.shiftKey && evt.keyCode == 9))//up
            {
                if (that.selectedCells.length > 0)
                {
                    var linhas = that.grade.getElementsByTagName("tr");
                    for (var x = 0; x < linhas.length; x++)
                    {
                        if (linhas[x] == that.selectedCells[0].row && x > 0)
                        {
                            var cols = that.selectedCells[0].row.getElementsByTagName("td");
                            for (var c = 0; c < cols.length; c++)
                            {
                                if (cols[c] == that.selectedCells[0])
                                {
                                    var cell = linhas[x - 1].getElementsByTagName("td")[c];
                                    that.selectCell(cell);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    evt.stopPropagation();
                    evt.preventDefault();
                }
                else if (that.grade.rows.length > 0)
                {
                    that.selectRow(that.grade.rows[0]);
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }
            else if (evt.keyCode == 39 && that.selectionMode.toLowerCase() != ChagasTable.selectionMode.fullRow)//right
            {
                if (that.selectedCells.length > 0)
                {
                    var linhas = that.grade.getElementsByTagName("tr");
                    for (var x = 0; x < linhas.length; x++)
                    {
                        if (linhas[x] == that.selectedCells[0].row)
                        {
                            var cols = that.selectedCells[0].row.getElementsByTagName("td");
                            for (var c = 0; c < cols.length; c++)
                            {
                                if (cols[c] == that.selectedCells[0] && c < cols.length - 1)
                                {
                                    var cell = linhas[x].getElementsByTagName("td")[c + 1];
                                    that.selectCell(cell);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }
            else if (evt.keyCode == 37 && that.selectionMode.toLowerCase() != ChagasTable.selectionMode.fullRow)//left
            {
                if (that.selectedCells.length > 0)
                {
                    var linhas = that.grade.getElementsByTagName("tr");
                    for (var x = 0; x < linhas.length; x++)
                    {
                        if (linhas[x] == that.selectedCells[0].row)
                        {
                            var cols = that.selectedCells[0].row.getElementsByTagName("td");
                            for (var c = 0; c < cols.length; c++)
                            {
                                if (cols[c] == that.selectedCells[0] && c > 0)
                                {
                                    var cell = linhas[x].getElementsByTagName("td")[c - 1];
                                    that.selectCell(cell);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }
        };
        this.grade.onkeyup = function (evt)
        {
            if (that.editando)
            {
                return;
            }
            evt = evt || window.event;
            if (evt.keyCode == 13)//Enter
            {
                if (that.selectedCells.length > 0)
                {
                    that.selectedCells[0].onclick();
                }
            }
        };

        this.divTable = document.createElement("div");
        this.divTable.style.cssText = "width:100%; height:100%; overflow-y:auto; overflow-x:hidden !important; background-color:" + corFundoGrade + "; ";
        this.divTable.className = "chagas-borderbox";
        this.divTable.onscroll = function (e)
        {
            that.headerInner.style.marginLeft = -that.divTable.scrollLeft + "px";
        };

        this.divTitulo = document.createElement("div");
        this.divTitulo.style.cssText = "width:100%; height:40px; display:none; cursor:default; ";
        this.divTituloText = document.createElement("div");
        this.divTituloText.className = "chagas-noselect chagas-font colortitleedit";
        this.divTituloText.style.cssText = "color:#159ddb; padding-left:8px; height:40px; line-height:40px; float:left; width:100%; font-size:20px;";

        this.divTitulo.appendChild(this.divTituloText);
        this.appendChild(this.divTitulo);

        this.divTable.appendChild(this.grade);
        this.divTable.appendChild(this.header);
        this.divext.appendChild(this.divTable);
        this.appendChild(this.divext);

        Object.defineProperty(this, 'orderDataPropertyName', {
            get: function ()
            {
                if (!that.attributes["orderDataPropertyName"])
                {
                    return "";
                }
                return that.attributes["orderDataPropertyName"].value;
            },
            set: function (value)
            {
                that.setAttribute("orderDataPropertyName", value);
            }
        });

        this.divFooter = null;
        function createdivFooter()
        {
            that.divTable.style.height = "calc(100% - 60px)";
            that.divFooter = document.createElement("div");
            that.divFooter.style.cssText = "width:100%; height:60px; ";
            that.divFooter.className = "chagas-borderbox";
            that.divext.appendChild(that.divFooter);
        }
        for (var x = 0; x < this.childElementCount; x++)
        {
            var c = this.children[x];
            if (c.tagName.toLowerCase() == "edit-fields")
            {
                createdivFooter();
                this.deleteButton = document.createElement("button");
                this.deleteButton.type = "button";
                this.deleteButton.title = "Remover";
                this.deleteButton.className = "btn btn-danger pull-right";
                this.deleteButton.style.cssText = "margin-top:15px; margin-left:5px;";
                this.deleteButton.innerHTML = 'Excluir';
                this.divFooter.appendChild(this.deleteButton);
                this.deleteButton.onclick = function ()
                {
                    if (that.disableDelete)
                    {
                        return;
                    }
                    if (that.selectionCheckColumn)
                    {
                        var rs = that.getCheckedRows();
                        if (rs.length > 0)
                        {
                            alert("Excluir o" + (rs.length == 1 ? " item" : "s " + rs.length + " itens") + " marcado" + (rs.length == 1 ? "" : "s") + "?", "Não", ["Sim"], function (index)
                            {
                                if (index == 0)
                                {
                                    var row = rs[0];
                                    for (var x = 0; x < rs.length; x++)
                                    {
                                        that.removeRow(rs[x]);
                                    }

                                    that.onRowDeleted(row);
                                }
                                else
                                {
                                    that.selectCells(that.selectedCells);
                                }
                            });
                            return;
                        }
                    }
                    that.deleteRowSelecionada();
                };
                this.addButton = document.createElement("button");
                this.addButton.type = "button";
                this.addButton.title = "Adicionar";
                this.addButton.className = "btn btn-success pull-right";
                this.addButton.style.cssText = "margin-top:15px;";
                this.addButton.innerHTML = 'Adicionar';
                this.divFooter.appendChild(this.addButton);
                this.addButton.onclick = function ()
                {
                    if (!that.disableAdd)
                    {
                        that.openModal();
                    }
                };
                this.editarButton = document.createElement("button");
                this.editarButton.type = "button";
                this.editarButton.title = "Editar";
                this.editarButton.className = "chagas-button chagas-tablebutton edit";
                //this.editarButton.style.backgroundImage = "url(" + editTableImage + ")";
                this.editarButton.hidden = true;
                this.divFooter.appendChild(this.editarButton);
                this.editarButton.onclick = function ()
                {
                    if (that.selectedCells.length > 0 && !that.disableEdit)
                    {
                        that.openModal(that.selectedCells[0].row);
                    }
                };

                this.contadorDiv = document.createElement("div");
                this.contadorDiv.className = "chagas-font";
                this.contadorDiv.style.cssText = "padding-left:15px; height:20px; line-height:20px; float:left; font-size:12px;";
                this.contadorDiv.innerHTML = "0 registros";
                this.divFooter.appendChild(this.contadorDiv);
                break;
            }
        }
        if (this.orderDataPropertyName.length > 0)
        {
            this.moveUpButton = document.createElement("button");
            this.moveUpButton.type = "button";
            this.moveUpButton.title = "Mover para cima";
            //this.moveUpButton.className = "chagas-button chagas-tablebutton add";
            this.moveUpButton.className = "btn btn-primary";
            this.moveUpButton.innerHTML = '<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>';
            this.moveUpButton.style.cssText = "margin-top:5px; margin-left:5px;";
            //this.moveUpButton.style.backgroundImage = "url(" + moveUpTableImage + ")";
            this.moveUpButton.onclick = function ()
            {
                if (that.selectedCells.length > 0)
                {
                    var i = that.selectedCells[0].row.sectionRowIndex == -1 ? that.selectedCells[0].row.rowIndex : that.selectedCells[0].row.sectionRowIndex;
                    if (i > 0)
                    {
                        var trFrom = that.grade.rows[i];
                        that.grade.removeChild(trFrom);
                        var trTo = that.grade.rows[i - 1];
                        that.grade.insertBefore(trFrom, trTo);
                        that.atualizarOrdemRows();
                        that.selectedCells[0].focus();
                        if (that.onOrderChanged)
                        {
                            that.onOrderChanged();
                        }

                    }
                }
            };
            if (this.divFooter)
            {
                this.moveUpButton.style.cssText += "margin-left:6px;";
            }
            else
            {
                createdivFooter();
            }
            this.divFooter.appendChild(this.moveUpButton);
            this.moveDownButton = document.createElement("button");
            this.moveDownButton.type = "button";
            this.moveDownButton.title = "Mover para baixo";
            //this.moveDownButton.className = "chagas-button chagas-tablebutton add";
            //this.moveDownButton.style.backgroundImage = "url(" + moveDownTableImage + ")";
            this.moveDownButton.className = "btn btn-primary";
            this.moveDownButton.style.cssText = "margin-top:5px; margin-left:5px;";
            this.moveDownButton.innerHTML = '<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>';
            this.moveDownButton.onclick = function ()
            {
                if (that.selectedCells.length > 0)
                {
                    var i = that.selectedCells[0].row.sectionRowIndex == -1 ? that.selectedCells[0].row.rowIndex : that.selectedCells[0].row.sectionRowIndex;;
                    if (i < that.grade.rows.length - 1)
                    {
                        var trFrom = that.grade.rows[i];
                        that.grade.removeChild(trFrom);
                        var trTo = that.grade.rows[i + 1];
                        that.grade.insertBefore(trFrom, trTo);
                        that.atualizarOrdemRows();
                        that.selectedCells[0].focus();
                        if (that.onOrderChanged)
                        {
                            that.onOrderChanged();
                        }
                    }
                }
            };
            this.divFooter.appendChild(this.moveDownButton);
        }

        Object.defineProperty(this, 'selectionMode', {
            get: function ()
            {
                return that.attributes["selectionMode"] ? that.attributes["selectionMode"].value : "Cell";
            },
            set: function (value)
            {
                that.setAttribute("selectionMode", value);
                that.selectCells(that.selectedCells);
            }
        });
        Object.defineProperty(this, 'selectionCheckColumn', {
            get: function ()
            {
                if (!that.attributes["selectionCheckColumn"])
                {
                    return true;
                }
                return that.attributes["selectionCheckColumn"].value == "true" ? true : false;
            },
            set: function (value)
            {
                that.setAttribute("selectionCheckColumn", value ? "true" : "false");
            }
        });
        Object.defineProperty(this, 'disableAdd', {
            get: function ()
            {
                if (!that.attributes["disableAdd"])
                {
                    return false;
                }
                return that.attributes["disableAdd"].value.toLowerCase() == "true" ? true : false;
            },
            set: function (value)
            {
                that.setAttribute("disableAdd", value ? "true" : "false");
                if (that.addButton)
                {
                    that.addButton.style.display = value ? "none" : "";
                }
            }
        });
        Object.defineProperty(this, 'disableDelete', {
            get: function ()
            {
                if (!that.attributes["disableDelete"])
                {
                    return false;
                }
                return that.attributes["disableDelete"].value.toLowerCase() == "true" ? true : false;
            },
            set: function (value)
            {
                that.setAttribute("disableDelete", value ? "true" : "false");
                if (that.deleteButton)
                {
                    that.deleteButton.style.display = value ? "none" : "";
                }
            }
        });
        Object.defineProperty(this, 'disableEdit', {
            get: function ()
            {
                if (!that.attributes["disableEdit"])
                {
                    return false;
                }
                return that.attributes["disableEdit"].value.toLowerCase() == "true" ? true : false;
            },
            set: function (value)
            {
                that.setAttribute("disableEdit", value ? "true" : "false");
                if (that.editarButton)
                {
                    that.editarButton.style.display = value ? "none" : "";
                }
            }
        });
        Object.defineProperty(this, 'dontShowArrows', {
            get: function ()
            {
                if (!that.attributes["dontShowArrows"])
                {
                    return false;
                }
                return that.attributes["dontShowArrows"].value.toLowerCase() == "true" ? true : false;
            },
            set: function (value)
            {
                that.setAttribute("dontShowArrows", value ? "true" : "false");
                //if (that.editarButton)
                //{
                //    that.editarButton.style.display = value ? "none" : "";
                //}
            }
        });

        Object.defineProperty(this, 'titulo', {
            get: function ()
            {
                return that.attributes["titulo"] ? that.attributes["titulo"].value : "";
            },
            set: function (value)
            {
                that.setAttribute("titulo", value);
                if (value && value.length > 0)
                {
                    that.divTituloText.innerHTML = value;
                    that.divTitulo.style.display = "";
                    that.divext.style.height = "calc(100% - 40px)";
                }
                else
                {
                    that.divTituloText.innerHTML = "";
                    that.divTitulo.style.display = "none";
                    that.divext.style.height = "100%";
                }
            }
        });

        this.disableAdd = this.disableAdd;
        this.disableDelete = this.disableDelete;
        this.disableEdit = this.disableEdit;
        this.dontShowArrows = this.dontShowArrows;
        this.titulo = this.titulo;

        if (this.selectionCheckColumn)
        {
            var colCheck = new ChagasTableColumn();
            colCheck.type = "checkbox";
            colCheck.style.width = "20px";
            colCheck.checkHeader = true;
            this.addColumn(colCheck);
        }



        that.create = function ()
        {
            that.criarModalContent();
            for (var x = 0; x < that.childElementCount; x++)
            {
                var c = that.children[x];
                if (c.tagName.toLowerCase() == "columns")
                {
                    for (var y = 0; y < c.childElementCount; y++)
                    {
                        if (c.children[y].getAttribute("is") == "chagas-tablecolumn")
                        {
                            if (c.children[y].attributes["header"])
                            {
                                c.children[y].header = c.children[y].attributes["header"].value;
                            }
                            if (c.children[y].attributes["id"])
                            {
                                c.children[y].id = c.children[y].attributes["id"].value;
                            }
                            if (c.children[y].attributes["dataPropertyName"])
                            {
                                c.children[y].dataPropertyName = c.children[y].attributes["dataPropertyName"].value;
                            }
                            if (c.children[y].attributes["image"])
                            {
                                c.children[y].image = c.children[y].attributes["image"].value;
                            }
                            if (c.children[y].attributes["type"])
                            {
                                c.children[y].type = c.children[y].attributes["type"].value;
                            }
                            if (c.children[y].attributes["format"])
                            {
                                c.children[y].format = c.children[y].attributes["format"].value;
                            }
                            this.addColumn(c.children[y].cloneNode());
                            c.children[y].style.display = "none";
                            //c.removeChild(c.children[y]);
                            //y--;
                        }
                    }
                    that.removeChild(c);
                    x--;
                }
            }
            if (this.dontShowArrows)
            {
                this.moveUpButton.style.display = 'none';
                this.moveDownButton.style.display = 'none';
                if (this.orderDataPropertyName.length > 0)
                {
                    var colDrag = new ChagasTableColumn();
                    colDrag.type = "img";
                    colDrag.style.width = "50px";
                    this.colDrag = colDrag;
                    this.addColumn(colDrag);
                }
            }
            if (!this.disableEdit)
            {
                var colEdit = new ChagasTableColumn();
                colEdit.type = "button";
                colEdit.style.width = "80px";
                this.colEdit = colEdit;
                this.addColumn(colEdit);
            }

            if (!this.disableDelete)
            {
                var colDelete = new ChagasTableColumn();
                colDelete.type = "button";
                colDelete.style.width = "80px";
                this.colDelete = colDelete;
                this.addColumn(colDelete);
            }


        }


        function resized()
        {
            that.atualizarHeaders();
        }
        window.addEventListener("resize", function ()
        {
            resized();
        });
        window.addEventListener("load", function ()
        {
            that.create();
            that.atualizarHeaders();
        });
        Object.defineProperty(this, 'rows', {
            get: function ()
            {
                return that.grade.rows;
            }
        });
    };

    _ChagasTable.addColumn = function (column)
    {
        var that = this;
        var tit = document.createElement("td");
        tit.className = "chagas-font chagas-borderbox" + (column.type != ChagasTableColumn.type.checkbox || !column.checkHeader ? "" : " checkbox");
        tit.innerHTML = column.header;
        tit.id = column.id;
        that.headerInner.rows[0].appendChild(tit);
        column.divHeader = tit;
        if (column.type && column.type.toLowerCase() == ChagasTableColumn.type.checkbox && column.checkHeader)
        {
            var check = document.createElement("input");
            check.type = "checkbox";
            check.style.marginLeft = "4px";
            tit.appendChild(check);
            tit.checkbox = check;
            check.onclick = function ()
            {
                if (that.grade.rows.length == 0)
                {
                    this.checked = false;
                }
                that.checkAllColumn(column, this.checked);
            };
            check.onmousedown = function ()
            {
                if (this.indeterminate)
                {
                    this.indeterminate = false;
                }
            };
        }
        this.columns.push(column);
        this.atualizarHeaders();
    };

    _ChagasTable.addRow = function (setPersistencia)
    {
        var that = this;
        var linha = document.createElement("tr");
        linha.className = "chagas-font chagas-tablerow";
        linha.style.cssText = "height:35px; cursor:default; font-size:12px; outline:none; border:1px solid #1d1d1d;";
        linha.height = "35px";
        linha.tabIndex = 0;
        linha.onkeydown = function (evt)
        {
            evt = evt || window.event;
            if (evt.keyCode == 13)//Enter
            {
                if (that.editando)
                {
                }
                else if (that.editarButton)
                {
                    that.editarButton.click();
                }
            }
            else if (evt.keyCode == 46)//Delete
            {
                if (that.disableDelete || that.editando)
                {
                    return;
                }
                that.deleteRowSelecionada();
            }
            else if (evt.keyCode == 113)//F2
            {
                if (!that.editando && that.selectedCells.length > 0)
                {
                    that.editCell(that.selectedCells[0]);
                }
            }
            else if (evt.keyCode == 32)//Space
            {
                if (that.selectedCells.length == 1 &&
                    that.selectedCells[0].column.type &&
                    that.selectedCells[0].column.type.toLowerCase() == ChagasTableColumn.type.checkbox &&
                    that.selectedCells[0].checkbox)
                {
                    that.selectedCells[0].checkbox.checked = !that.selectedCells[0].checkbox.checked;
                    that.selectedCells[0].checkbox.onchange();
                    if (that.selectedCells[0].checkbox.onclick)
                    {
                        that.selectedCells[0].checkbox.onclick();
                    }
                    evt.preventDefault();
                }
            }
        };
        linha.ondblclick = function (e)
        {
            if ((e.target.tagName.toLowerCase() == "td" &&
                 e.target.column.checkHeader) ||
                (e.target.type && e.target.type.toLowerCase() == "checkbox" &&
                 e.target.cell && e.target.cell.column && e.target.cell.column.checkHeader))
            {
                return;
            }
            if (that.editarButton)
            {
                that.editarButton.click();
            }
        };
        if (setPersistencia && that.persistenciaDefault)
        {
            linha.persistencia = that.persistenciaDefault.clone();
            if (that.persistenciaGrade)
            {
                that.persistenciaGrade.valores.push(linha.persistencia);
            }
        }

        if (that.orderDataPropertyName.length > 0)
        {
            //linha.style.cursor = 'move';
            //linha.style = "-webkit-touch-callout: none;-webkit-user-select: none;            -khtml-user-select: none;            -moz-user-select: none;            -ms-user-select: none;            user-select: none;"
            ////linha.unselectable="on"
            ////linha.onselectstart="return false;" 
            ////linha.onmousedown="return false;">
            //linha.ondragstart = function (event)
            //{
            //    that.draggingRow = this;
            //    //event.dataTransfer.setData("Text", String(this.persistencia.ordem.valor));
            //};

            ////linha.addEventListener("dragstart", function(event) {
            ////});

            ////linha.ondragover = function ()
            ////{
            ////    //event.dataTransfer.setData("Text", event.target.id);
            ////    return false;
            ////};

            ////linha.ondragend = function ()
            ////{
            ////    return false;
            ////};

            //linha.addEventListener("drop", function (event)
            //{
            //    event.preventDefault();
            //    var draggedRow = that.draggingRow;
            //    that.draggingRow = null;

            //    var targetRow;
            //    if (event.target.parentNode.persistencia)
            //    {
            //        targetRow = event.target.parentNode;
            //    }
            //    else
            //    {
            //        targetRow = event.target.parentNode.parentNode;
            //    }

            //    if (draggedRow && targetRow && draggedRow != targetRow)
            //    {
            //        //insere dragged row no lugar na targetrow
            //        if (targetRow.persistencia.ordem.valor != draggedRow.persistencia.ordem.valor + 1)
            //        {
            //            that.grade.insertBefore(draggedRow, targetRow);
            //            that.atualizarOrdemRows();
            //            that.selectedCells[0].focus();
            //            if (that.onOrderChanged)
            //            {
            //                that.onOrderChanged();
            //            }
            //        }
            //        //for (var i = 0; i < that.rows.length; i++)
            //        //{
            //        //    if (that.rows[i].persistencia.ordem.valor > targetRow.persistencia.ordem.valor && that.rows[i] != draggedRow)
            //        //    {
            //        //        that.rows[i].persistencia.ordem.valor = that.rows[i].persistencia.ordem.valor + 1;
            //        //    }
            //        //}
            //        //draggedRow.persistencia.ordem.valor = targetRow.persistencia.ordem.valor;
            //        //targetRow.persistencia.ordem.valor = targetRow.persistencia.ordem.valor + 1;
            //        //that.atualizarOrdemRows();
            //        //that.atualizarOrdem();

            //    }
            //    else
            //    {
            //        return false;
            //    }
            //});
        }

        for (var c = 0; c < this.columns.length; c++)
        {
            var col = document.createElement("td");
            col.row = linha;
            col.column = this.columns[c];
            if (this.columns[c].image && this.columns[c].image.length > 0)
            {
                col.style.cssText = "min-width:20px; padding:0; overflow:hidden; " +
                                    (this.columns[c].style.width.length > 0 ? " max-width:" + this.columns[c].style.width + ";" : "") +
                                    this.columns[c].style.cssText;
                col.className = "chagas-borderbox";
                col.title = this.columns[c].title;
                col.style.textAlign = this.columns[c].style.textAlign.length > 0 ? this.columns[c].style.textAlign : "center";
                var img = document.createElement("img");
                img.style.cssText = "width:100%; min-width:20px;";
                img.align = "middle";
                img.src = this.columns[c].image;
                col.image = img;
                col.appendChild(img);
                col.onclick = function ()
                {
                    var list = new Array();
                    list.push(this);
                    that.selectCells(list);
                    if (that.onCellClick)
                    {
                        that.onCellClick(this);
                    }
                    if (this.column.onclick)
                    {
                        this.column.onclick(this);
                    }
                };
                linha.appendChild(col);
            }
            else
            {
                var t = this.columns[c].type ? this.columns[c].type.toLowerCase() : "";
                var isNumber = this.columns[c].type &&
                               (t == ChagasTableColumn.type.inteiro ||
                                t.indexOf(ChagasTableColumn.type.decimal) > -1);
                col.style.cssText = (this.columns[c].style.width && this.columns[c].style.width.length > 0 ? "word-break:break-word;" : "white-space:nowrap;") +
                                    (t == ChagasTableColumn.type.checkbox ? "min-width:20px; padding:0;" : (t == ChagasTableColumn.type.textbox ? "padding-left:1px; padding-right:1px;" : "padding-left:5px; padding-right:5px; min-width:30px; ")) +
                                    this.columns[c].style.cssText;

                col.className = "chagas-cell chagas-borderbox";
                col.tabIndex = "0";
                col.style.textAlign = this.columns[c].style.textAlign.length > 0 ? this.columns[c].style.textAlign : (isNumber ? "right" : "left");
                col.onfocus = function (e)
                {
                    if (this.input)
                    {
                        this.input.focus();
                    }
                    else if (this.editInput)
                    {
                        this.editInput.focus();
                    }
                    else
                    {
                        that.selectCell(e.target);
                    }
                };
                if (t == ChagasTableColumn.type.checkbox)
                {
                    var check = document.createElement("input");
                    check.type = "checkbox";
                    check.cell = col;
                    col.checkbox = check;
                    col.appendChild(check);
                    col.style.textAlign = this.columns[c].style.textAlign.length > 0 ? this.columns[c].style.textAlign : "center";
                    check.onchange = function ()
                    {
                        that.atualizarCheckColumn(this.cell.column);
                    };
                    if (!this.columns[c].checkHeader)
                    {
                        check.onclick = function ()
                        {
                            if (this.cell.column.editavel)
                            {
                                if (this.cell.column.dataPropertyName &&
                                    this.cell.column.dataPropertyName.length > 0 &&
                                    this.cell.row.persistencia &&
                                    this.cell.row.persistencia[this.cell.column.dataPropertyName])
                                {
                                    this.cell.row.persistencia[this.cell.column.dataPropertyName].valor = this.checked ? "true" : "false";
                                    that.atualizarRow(this.cell.row);
                                }
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }
                else if (t == ChagasTableColumn.type.textbox ||
                         t == ChagasTableColumn.type.combobox)
                {
                    var input;
                    if (t == ChagasTableColumn.type.combobox)
                    {
                        input = document.createElement("select");
                    }
                    else
                    {
                        input = document.createElement("input");
                        input.type = "text";

                        if (this.columns[c].attributes['format'].value == 'inteiro')
                        {
                            ChagasText.createDecimal(input, 0);
                        }
                    }
                    input.cell = col;
                    input.className = "form-control";
                    input.readOnly = !this.columns[c].editavel;
                    input.style.cssText = " width:100%; border: 1px solid gray; height:100%;";
                    input.onchange = function ()
                    {
                        if (this.cell.column.dataPropertyName &&
                            this.cell.column.dataPropertyName.length > 0 &&
                            this.cell.row.persistencia &&
                            this.cell.row.persistencia[this.cell.column.dataPropertyName])
                        {
                            this.cell.row.persistencia[this.cell.column.dataPropertyName].valor = this.value;
                            that.atualizarRow(this.cell.row);
                        }
                    };
                    col.input = input;
                    col.appendChild(input);
                }
                if (this.columns[c] == this.colDrag)
                {
                    var col = document.createElement("td");
                    col.row = linha;
                    col.column = this.colDrag;
                    col.className = "chagas-borderbox";
                    col.style.textAlign = "center";

                    var imgDragtb = document.createElement('img');
                    //btnDeletetb.className = 'btn btn-default';
                    imgDragtb.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAATJJREFUeNrs3cENgkAARUGoVEqxE6ASS0OMtGAE3kyy9w3/eZLoMLTNxyE6/nYcEYTHF4HxRWB8ERhfBMYXgfFFYHwRGF8ExheB8UVgfBEYXwTGF4HxRWB8ERhfBMa/VQTjBe887edx0rut+1l8PgEAAAAAAID/+3wd/PIY2gFsHoMAEAACQAAIAAEgAARAI4CnxwAAAAAAAABwN94HEIAABIAAEAACQAAIAAEgAAAAAAAAAACuy8/ECcD7AAJAAAgAASAABIAAEACVAPx1LAAAAAAAAMDtjBe887Sfx0nvtu5nkdXvzcP3RZYzndks3QiMH47A+OEIjB+OwPjhCIwfjsD44QiMH47A+OEIjB+OwPjhCIwfjsD44QiMH47A+OEIjB+OwPjhCIwfjiA9/luAAQCkGfsrlv6muAAAAABJRU5ErkJggg==';
                    imgDragtb.style.width = '25px';
                    imgDragtb.row = col.row;

                    imgDragtb.style.cursor = 'move';
                    //linha.unselectable="on"
                    //linha.onselectstart="return false;" 
                    //linha.onmousedown="return false;">
                    imgDragtb.ondragstart = function (event)
                    {
                        that.draggingRow = this.row;
                        //event.dataTransfer.setData("Text", String(this.persistencia.ordem.valor));
                    };

                    //linha.addEventListener("dragstart", function(event) {
                    //});

                    //linha.ondragover = function ()
                    //{
                    //    //event.dataTransfer.setData("Text", event.target.id);
                    //    return false;
                    //};

                    //linha.ondragend = function ()
                    //{
                    //    return false;
                    //};

                    imgDragtb.addEventListener("drop", function (event)
                    {
                        event.preventDefault();
                        var draggedRow = that.draggingRow;
                        that.draggingRow = null;

                        var targetRow;
                        if (event.target.parentNode.persistencia)
                        {
                            targetRow = event.target.parentNode;
                        }
                        else
                        {
                            targetRow = event.target.parentNode.parentNode;
                        }

                        if (draggedRow && targetRow && draggedRow != targetRow)
                        {
                            //insere dragged row no lugar na targetrow
                            if (targetRow.persistencia.ordem.valor != draggedRow.persistencia.ordem.valor + 1)
                            {
                                that.grade.insertBefore(draggedRow, targetRow);
                                that.atualizarOrdemRows();
                                //that.selectedCells[0].focus();
                                if (that.onOrderChanged)
                                {
                                    that.onOrderChanged();
                                }
                            }
                            //for (var i = 0; i < that.rows.length; i++)
                            //{
                            //    if (that.rows[i].persistencia.ordem.valor > targetRow.persistencia.ordem.valor && that.rows[i] != draggedRow)
                            //    {
                            //        that.rows[i].persistencia.ordem.valor = that.rows[i].persistencia.ordem.valor + 1;
                            //    }
                            //}
                            //draggedRow.persistencia.ordem.valor = targetRow.persistencia.ordem.valor;
                            //targetRow.persistencia.ordem.valor = targetRow.persistencia.ordem.valor + 1;
                            //that.atualizarOrdemRows();
                            //that.atualizarOrdem();

                        }
                        else
                        {
                            return false;
                        }
                    });

                    col.appendChild(imgDragtb);
                    linha.appendChild(col);
                }
                if (this.columns[c] == this.colEdit)
                {
                    var col = document.createElement("td");
                    col.row = linha;
                    col.column = this.colEdit;
                    col.className = "chagas-borderbox";
                    col.style.textAlign = "center";

                    var btnEdittb = document.createElement('button');
                    btnEdittb.className = 'btn btn-default';
                    btnEdittb.innerHTML = 'Editar';
                    btnEdittb.row = col.row;
                    btnEdittb.onclick = function (e)
                    {
                        e.preventDefault();
                        that.openModal(this.row);
                        return false;
                    };
                    col.appendChild(btnEdittb);
                    linha.appendChild(col);
                }
                if (this.columns[c] == this.colDelete)
                {
                    var col = document.createElement("td");
                    col.row = linha;
                    col.column = this.colDelete;
                    col.className = "chagas-borderbox";
                    col.style.textAlign = "center";

                    var btnDeletetb = document.createElement('button');
                    btnDeletetb.className = 'btn btn-default';
                    btnDeletetb.innerHTML = 'Excluir';
                    btnDeletetb.row = col.row;
                    btnDeletetb.onclick = function (e)
                    {
                        e.preventDefault();
                        that.deleteRowSelecionada();
                        return false;
                    };
                    col.appendChild(btnDeletetb);
                    linha.appendChild(col);
                }

                col.onmousedown = function ()
                {
                    this.editar = containsClass(this, "focus");
                };
                col.onclick = function ()
                {
                    if (this.input)
                    {
                        this.input.focus();
                    }
                    else if (this.editInput)
                    {
                        this.editInput.focus();
                    }
                    else
                    {
                        if (this.column.editavel && this.column.type &&
                            (this.column.type.toLowerCase() == ChagasTableColumn.type.text ||
                             this.column.type.toLowerCase() == ChagasTableColumn.type.inteiro ||
                             this.column.type.toLowerCase().indexOf(ChagasTableColumn.type.decimal) > -1) &&
                            this.editar &&
                            !this.editInput)
                        {
                            that.editCell(this);
                        }
                        else
                        {
                            that.selectCells([this]);
                        }
                        if (that.onCellClick)
                        {
                            that.onCellClick(this);
                        }
                    }
                };
                linha.appendChild(col);
            }
            if (this.grade.rows.length == 0)
            {
                this.columns[c].firstCell = linha.cells[c];
            }
        }



        this.grade.appendChild(linha);
        if (!this.carregando)
        {
            this.atualizarTotalizador();
            this.atualizarAllCheckColumn();
        }
        return linha;
    };

    _ChagasTable.atualizarAllCheckColumn = function ()
    {
        for (var c = 0; c < this.columns.length; c++)
        {
            if (this.columns[c].type &&
                this.columns[c].type.toLowerCase() == ChagasTableColumn.type.checkbox)
            {
                this.atualizarCheckColumn(this.columns[c]);
            }
        }
    };

    _ChagasTable.atualizarCheckColumn = function (column)
    {
        for (var c = 0; c < this.columns.length; c++)
        {
            if (this.columns[c] == column && this.columns[c].divHeader.checkbox)
            {
                var checkeds = 0;
                var uncheckeds = 0;
                for (var r = 0; r < this.grade.rows.length; r++)
                {
                    if (this.grade.rows[r].cells[c].checkbox.checked)
                    {
                        checkeds++;
                    }
                    else
                    {
                        uncheckeds++;
                    }
                    if (checkeds > 0 && uncheckeds > 0)
                    {
                        break;
                    }
                }
                if (checkeds > 0 && uncheckeds > 0)
                {
                    this.columns[c].divHeader.checkbox.indeterminate = true;
                    this.columns[c].divHeader.checkbox.checked = true;
                }
                else if (checkeds > 0)
                {
                    this.columns[c].divHeader.checkbox.indeterminate = false;
                    this.columns[c].divHeader.checkbox.checked = true;
                }
                else
                {
                    this.columns[c].divHeader.checkbox.indeterminate = false;
                    this.columns[c].divHeader.checkbox.checked = false;
                }
                break;
            }
        }
    };

    _ChagasTable.atualizarHeaders = function ()
    {
        if (this.divTable.clientWidth == 0)
        {
            return;
        }
        this.header.style.width = this.divTable.clientWidth + "px";
        var wtotFix = 0;
        var countFix = 0;
        for (var c = 0; c < this.columns.length; c++)
        {
            if (this.columns[c].style.width && this.columns[c].style.width.length > 0)
            {
                this.columns[c].divHeader.style.width = this.columns[c].style.width;
                this.columns[c].divHeader.style.minWidth = this.columns[c].style.minWidth.length > 0 ? this.columns[c].style.minWidth : this.columns[c].style.width;
                this.columns[c].divHeader.style.maxWidth = this.columns[c].style.maxWidth.length > 0 ? this.columns[c].style.maxWidth : "";
                var rc = this.columns[c].divHeader.getBoundingClientRect();
                for (var r = 0; r < this.grade.rows.length; r++)
                {
                    var cell = this.grade.rows[r].cells[c];
                    cell.style.minWidth = "";
                    cell.style.maxWidth = "";
                    cell.style.width = rc.width + "px";
                    cell.style.minWidth = rc.width + "px";
                    cell.style.maxWidth = rc.width + "px";
                }
                wtotFix += rc.width;
                countFix++;
            }
        }
        var divs = (this.header.clientWidth - wtotFix) / (this.columns.length - countFix);
        var wtot = 0;
        for (var c = 0; c < this.columns.length; c++)
        {
            if (!this.columns[c].style.width || this.columns[c].style.width.length == 0)
            {
                this.columns[c].divHeader.style.width = divs + "px";
                this.columns[c].divHeader.style.minWidth = this.columns[c].style.minWidth.length > 0 ? this.columns[c].style.minWidth : (divs + "px");
                this.columns[c].divHeader.style.maxWidth = this.columns[c].style.maxWidth.length > 0 ? this.columns[c].style.maxWidth : "";
                var rc = this.columns[c].divHeader.getBoundingClientRect();
                wtot += rc.width;
                for (var r = 0; r < this.grade.rows.length; r++)
                {
                    var cell = this.grade.rows[r].cells[c];
                    cell.style.minWidth = "";
                    cell.style.maxWidth = "";
                    cell.style.width = rc.width + "px";
                    cell.style.minWidth = rc.width + "px";
                    cell.style.maxWidth = rc.width + "px";
                }
            }
        }
    };

    _ChagasTable.atualizarOrdem = function (lista)
    {
        var that = this;
        if (this.orderDataPropertyName.length > 0)
        {
            lista.sort(function (a, b)
            {
                return a[that.orderDataPropertyName].valor - b[that.orderDataPropertyName].valor;
            });
            for (var x = 0; x < lista.length; x++)
            {
                lista[x][that.orderDataPropertyName].valor = x;
            }
        }
    };

    _ChagasTable.atualizarOrdemRows = function ()
    {
        if (this.orderDataPropertyName.length > 0)
        {
            while (this.persistenciaGrade.valores.length > 0)
            {
                this.persistenciaGrade.valores.pop();
            }
            for (var x = 0; x < this.grade.rows.length; x++)
            {
                this.grade.rows[x].persistencia[this.orderDataPropertyName].valor = x;
                this.grade.rows[x].persistencia.Alterado = true;
                this.persistenciaGrade.valores.push(this.grade.rows[x].persistencia);
            }
        }
    };

    _ChagasTable.atualizarRow = function (row, naoAtualizaHeader)
    {
        var cps = null;
        for (var c = 0; c < this.columns.length; c++)
        {
            if (this.columns[c].dataPropertyName && this.columns[c].dataPropertyName.length > 0 && row.persistencia[this.columns[c].dataPropertyName])
            {
                for (var cell = 0; cell < row.cells.length; cell++)
                {
                    if (row.cells[cell].column == this.columns[c])
                    {
                        if (row.cells[cell].checkbox)
                        {
                            row.cells[cell].checkbox.checked = toBool(row.persistencia[this.columns[c].dataPropertyName].valor);
                        }
                        else if (row.cells[cell].input)
                        {
                            row.cells[cell].input.value = row.persistencia[this.columns[c].dataPropertyName].valor;
                        }
                        else
                        {
                            function createImageCell(cell, extension, thumbImg, file)
                            {
                                cell.innerHTML = "";
                                if ((thumbImg && thumbImg.length > 0 && extension && extension.length > 0) ||
                                    file)
                                {
                                    var img = document.createElement("img");
                                    img.style.cssText = "max-width:100%; min-width:20px;" +
                                                        (cell.column.style.maxHeight.length > 0 ? "max-height:" + cell.column.style.maxHeight + ";" : "max-height:80px;") +
                                                        (cell.column.style.height.length > 0 ? "height:" + cell.column.style.height + ";" : "");
                                    img.align = "middle";
                                    cell.appendChild(img);
                                    if (extension &&
                                        (extension.toLowerCase() == ".jpg" ||
                                         extension.toLowerCase() == ".jpeg" ||
                                         extension.toLowerCase() == ".png"))
                                    {
                                        if (thumbImg && thumbImg.length > 0)
                                        {
                                            img.src = thumbImg;
                                        }
                                        else if (file)
                                        {
                                            ChagasFile.montarThumb(file, 190, 190, null, function (base64)
                                            {
                                                img.src = base64;
                                            });
                                        }
                                    }
                                    else
                                    {
                                        img.src = ChagasFile.thumbExtension(extension);
                                    }
                                }
                            }

                            var ok = false;
                            if (row.persistencia.Arquivos)
                            {
                                for (var i = 0; i < row.persistencia.Arquivos.length; i++)
                                {
                                    if (row.persistencia.Arquivos[i].GCampo == this.columns[c].dataPropertyName)
                                    {
                                        if (row.cells[cell].column.type &&
                                            row.cells[cell].column.type.toLowerCase() == ChagasTableColumn.type.image)
                                        {
                                            var ext = ChagasFile.fileExtension(row.persistencia.Arquivos[i]);
                                            createImageCell(row.cells[cell], ext, row.persistencia.Arquivos[i].CaminhoThumb, row.persistencia.Arquivos[i]);
                                        }
                                        else
                                        {
                                            row.cells[cell].innerHTML = row.persistencia.Arquivos[i].name ? row.persistencia.Arquivos[i].name : row.persistencia.Arquivos[i].Nome;
                                        }
                                        ok = true;
                                        break;
                                    }
                                }
                            }
                            if (!ok && this.modalContent)
                            {
                                if (!cps)
                                {
                                    cps = this.modalContent.getAllCampos();
                                }
                                for (var i = 0; i < cps.length; i++)
                                {
                                    if (cps[i].tagName.toLowerCase() == "select" && cps[i].attributes["chagasfield"] && cps[i].attributes["chagasfield"].value == this.columns[c].dataPropertyName)
                                    {
                                        for (var o = 0; o < cps[i].options.length; o++)
                                        {
                                            if (cps[i].options[o].value == row.persistencia[this.columns[c].dataPropertyName].valor)
                                            {
                                                if (row.persistencia[this.columns[c].dataPropertyName].valor.length > 0)
                                                {
                                                    row.cells[cell].innerHTML = cps[i].options[o].text;
                                                }
                                                else
                                                {
                                                    row.cells[cell].innerHTML = "";
                                                }
                                                ok = true;
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            if (!ok)
                            {
                                var t = row.cells[cell].column.type ? row.cells[cell].column.type.toLowerCase() : "";
                                if (t == ChagasTableColumn.type.image)
                                {
                                    var ext = ChagasFile.fileExtension(null, row.persistencia[this.columns[c].dataPropertyName].valor);
                                    createImageCell(row.cells[cell], ext, row.persistencia[this.columns[c].dataPropertyName].valor);
                                }
                                else if (t == ChagasTableColumn.type.inteiro ||
                                         t.indexOf(ChagasTableColumn.type.decimal) > -1)
                                {
                                    if (t == ChagasTableColumn.type.inteiro)
                                    {
                                        row.cells[cell].innerHTML = formatarDecimal(row.persistencia[this.columns[c].dataPropertyName].valor, 0);
                                    }
                                    else if (t.indexOf(ChagasTableColumn.type.decimal) > -1)
                                    {
                                        var d = t.replace(ChagasTableColumn.type.decimal, '');
                                        d = apenasNumeros(d);
                                        d = parseInt(d);
                                        row.cells[cell].innerHTML = formatarDecimal(row.persistencia[this.columns[c].dataPropertyName].valor, !isNaN(d) && d > 0 ? d : 2);
                                    }
                                }
                                else
                                {
                                    row.cells[cell].innerHTML = row.persistencia[this.columns[c].dataPropertyName].valor;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
        if (!naoAtualizaHeader)
        {
            this.atualizarHeaders();
        }
    };

    _ChagasTable.atualizarTotalizador = function ()
    {
        if (this.contadorDiv)
        {
            this.contadorDiv.innerHTML = this.grade.rows.length + " registro" + (this.grade.rows.length == 1 ? "" : "s");
        }
    };

    _ChagasTable.clearRows = function ()
    {
        while (this.grade.rows.length > 0)
        {
            this.grade.removeChild(this.grade.rows[0]);
        }
        if (this.persistenciaGrade)
        {
            this.persistenciaGrade.valores = new Array();
        }
        for (var c = 0; c < this.columns.length; c++)
        {
            this.columns[c].firstCell = null;
        }
        this.selectCells(new Array());
        this.atualizarAllCheckColumn();
        this.atualizarHeaders();
        this.atualizarTotalizador();
    };

    _ChagasTable.closeModal = function (ok)
    {
        if (this.modalEdit && this.modalEdit.parentElement)
        {
            var that = this;
            function close()
            {
                Chagas.closeModal(that.modalEdit, ok);
                setTimeout(function ()
                {
                    that.modalEdit.divConteudo.style.display = "none";
                    that.appendChild(that.modalEdit.divConteudo);
                }, 200);
                that.selectCells(that.selectedCells);
            }
            if (this.onModalClose)
            {
                this.onModalClose(this.modalEdit.row, ok, function (cancel)
                {
                    if (!cancel)
                    {
                        close();
                    }
                });
            }
            else
            {
                close();
            }
        }
    };

    _ChagasTable.checkAllColumn = function (column, checked)
    {
        for (var c = 0; c < this.columns.length; c++)
        {
            if (this.columns[c] == column)
            {
                for (var r = 0; r < this.grade.rows.length; r++)
                {
                    if (this.grade.rows[r].cells[c].checkbox.disabled != true)
                    {
                        this.grade.rows[r].cells[c].checkbox.checked = checked;
                    }
                }
                break;
            }
        }
    };

    _ChagasTable.checkRows = function (rows, checked)
    {
        if (this.selectionCheckColumn)
        {
            for (var c = 0; c < this.columns.length; c++)
            {
                if (this.columns[c].type &&
                    this.columns[c].type.toLowerCase() == ChagasTableColumn.type.checkbox)
                {
                    for (var r = 0; r < rows.length; r++)
                    {
                        rows[r].cells[c].checkbox.checked = checked;
                    }
                    break;
                }
            }
        }
    };

    _ChagasTable.criarModalContent = function ()
    {
        for (var x = 0; x < this.childElementCount; x++)
        {
            var c = this.children[x];
            if (c.tagName.toLowerCase() == "edit-fields")
            {
                this.modalContent = new ChagasConteudo();
                this.modalContent.className = "conteudo chagas-noselect " + c.className;
                this.modalContent.style.cssText = c.style.cssText;
                this.modalContent.style.display = "none";
                this.modalDataPropertyName = c.attributes["dataPropertyName"] ? c.attributes["dataPropertyName"].value : "";
                this.modalTitulo = c.attributes["titulo"] ? c.attributes["titulo"].value : "";;
                for (var y = 0; y < c.childElementCount; y++)
                {
                    c.children[y].style.cssText = "float:left;" + c.children[y].style.cssText;
                    this.modalContent.appendChild(c.children[y]);
                    y--;
                }
                //this.removeChild(c);
                //x--;
                c.style.display = "none";
                this.appendChild(this.modalContent);
            }
        }
    };

    _ChagasTable.deleteRowSelecionada = function ()
    {
        if (this.selectedCells.length > 0)
        {
            var that = this;
            alert("Excluir o item selecionado?", "Não", ["Sim"], function (index)
            {
                if (index == 0)
                {
                    var row = that.selectedCells[0].row;
                    that.removeRow(that.selectedCells[0].row);
                    that.onRowDeleted(row);
                }
                else
                {
                    that.selectCells(that.selectedCells);
                }
            });
        }
    };

    _ChagasTable.editCell = function (cell)
    {
        var that = this;
        if (cell.column.editavel && cell.column.type &&
            (cell.column.type.toLowerCase() == ChagasTableColumn.type.text ||
             cell.column.type.toLowerCase() == ChagasTableColumn.type.inteiro ||
             cell.column.type.toLowerCase().indexOf(ChagasTableColumn.type.decimal) > -1) &&
            !cell.editInput)
        {
            that.editando = true;
            cell.editar = false;
            var input = document.createElement("textarea");

            var isNumb = false;
            if (cell.column.type.toLowerCase() == ChagasTableColumn.type.inteiro)
            {
                ChagasText.createDecimal(input, 0);
                isNumb = true;
            }
            else if (cell.column.type.toLowerCase().indexOf(ChagasTableColumn.type.decimal) > -1)
            {
                var d = cell.column.type.toLowerCase().replace(ChagasTableColumn.type.decimal, '');
                d = apenasNumeros(d);
                d = parseInt(d);
                ChagasText.createDecimal(input, !isNaN(d) && d > 0 ? d : 2);
                isNumb = true;
            }
            input.cell = cell;
            input.className = "chagas-borderbox chagas-font";
            input.style.cssText = "float:left; width:100%; border:0; padding-left:5px; height:" + cell.clientHeight + "px; background-color:" + corFundoGrade + "; overflow:hidden; outline:none; resize:none; font-size:12px; ";
            input.style.textAlign = cell.style.textAlign;

            input.value = cell.innerHTML;
            input.valueOrig = cell.innerHTML;
            cell.innerHTML = "";
            cell.editInput = input;
            input.onchange = function ()
            {
                this.onblur();
                if (this.cell.column.dataPropertyName &&
                   this.cell.column.dataPropertyName.length > 0 &&
                   this.cell.row.persistencia &&
                   this.cell.row.persistencia[this.cell.column.dataPropertyName])
                {
                    this.cell.row.persistencia[this.cell.column.dataPropertyName].valor = this.value;
                    that.atualizarRow(this.cell.row);
                }
            };
            input.onblur = function ()
            {
                this.cell.removeChild(this);
                this.cell.innerHTML = this.value;
                this.cell.editInput = null;
                this.cell.style.padding = "1px";
                this.cell.style.paddingLeft = "5px";
                this.cell.style.paddingRight = "5px";
                that.editando = false;
            };
            input.onkeydown = function (evt)
            {
                evt = evt || window.event;
                if (evt.keyCode == 27)//esc
                {
                    this.value = this.valueOrig;
                    this.onblur();
                    this.cell.focus();
                }
            };
            cell.appendChild(input);
            cell.style.padding = "0";
            input.focus();
            input.setSelectionRange(isNumb ? 0 : input.value.length, input.value.length);
        }
    };

    _ChagasTable.getCheckedRows = function ()
    {
        var ret = new Array();
        if (this.selectionCheckColumn)
        {
            for (var c = 0; c < this.columns.length; c++)
            {
                if (this.columns[c].type &&
                    this.columns[c].type.toLowerCase() == ChagasTableColumn.type.checkbox)
                {
                    for (var r = 0; r < this.grade.rows.length; r++)
                    {
                        if (this.grade.rows[r].cells[c].checkbox.checked)
                        {
                            ret.push(this.grade.rows[r]);
                        }
                    }
                    break;
                }
            }
        }
        return ret;
    };

    _ChagasTable.removeColumn = function (column)
    {
        this.headerInner.rows[0].removeChild(column.divHeader);
        var i = this.columns.indexOf(column);
        if (i > -1)
        {
            this.columns.splice(i, 1);
        }
    };

    _ChagasTable.removeRow = function (row)
    {
        if (this.grade.rows.length > 0 && this.grade.rows[0] == row)
        {
            if (this.grade.rows.length > 1)
            {
                for (var c = 0; c < this.columns.length; c++)
                {
                    for (var cell = 0; cell < this.grade.rows[1].cells.length; cell++)
                    {
                        if (this.grade.rows[1].cells[cell].column == this.columns[c])
                        {
                            this.columns[c].firstCell = this.grade.rows[1].cells[cell];
                            break;
                        }
                    }
                }
            }
        }
        var selects = new Array();
        var index = 0;
        for (var r = 0; r < this.grade.rows.length; r++)
        {
            if (this.grade.rows[r] == row)
            {
                index = r;
                break;
            }
        }
        for (var cell = 0; cell < row.cells.length; cell++)
        {
            if (this.selectedCells.indexOf(row.cells[cell]) >= 0)
            {
                if (index + 1 < this.grade.rows.length)
                {
                    selects.push(this.grade.rows[index + 1].cells[cell]);
                }
                else if (index > 0)
                {
                    selects.push(this.grade.rows[index - 1].cells[cell]);
                }
            }
        }
        this.selectCells(selects);

        if (this.persistenciaGrade)
        {
            var ir = this.persistenciaGrade.valores.indexOf(row.persistencia);
            if (ir > -1)
            {
                this.persistenciaGrade.valores.splice(ir, 1);
            }
        }
        this.grade.removeChild(row);
        this.atualizarAllCheckColumn();
        this.atualizarOrdemRows();
        this.atualizarHeaders();
        this.atualizarTotalizador();
    };

    _ChagasTable.removeRowAt = function (index)
    {
        if (this.grade.rows.length > index)
        {
            this.removeRow(this.grade.rows[index]);
        }
    };

    _ChagasTable.onModalClose = function (row, ok, retorno)
    {
        if (retorno)
        {
            retorno(false);
        }
    };

    _ChagasTable.onModalOpen = function (row)
    {
    };

    _ChagasTable.onRowDeleted = function (row)
    {

    };

    _ChagasTable.onOrderChanged = function ()
    {
    };


    _ChagasTable.openModal = function (row)
    {
        var that = this;
        this.modalEdit = document.createElement("div");
        this.modalEdit.className = "chagas-tablemodal";
        this.modalEdit.row = row;
        this.modalEdit.tituloDiv = document.createElement("div");
        this.modalEdit.tituloDiv.style.cssText = "width:100%; height:40px; line-height:40px; text-align:left; padding-left:30px; font-size:18px; color:#428bca;";
        this.modalEdit.tituloDiv.classList.add('colortitleedit');

        if (this.modalDataPropertyName &&
            this.modalDataPropertyName.length > 0 &&
            row &&
            row.persistencia &&
            row.persistencia[this.modalDataPropertyName])
        {
            var ok = false;
            var cps = this.modalContent.getAllCampos();
            var valTit = "";
            for (var i = 0; i < cps.length; i++)
            {
                if (cps[i].tagName.toLowerCase() == "select" && cps[i].attributes["chagasfield"] && cps[i].attributes["chagasfield"].value == this.modalDataPropertyName)
                {
                    for (var o = 0; o < cps[i].options.length; o++)
                    {
                        if (cps[i].options[o].value == row.persistencia[this.modalDataPropertyName].valor)
                        {
                            if (row.persistencia[this.modalDataPropertyName].valor.length > 0)
                            {
                                valTit = cps[i].options[o].text;
                            }
                            else
                            {
                            }
                            ok = true;
                            break;
                        }
                    }
                    break;
                }
            }
            if (!ok)
            {
                valTit = row.persistencia[this.modalDataPropertyName].valor;
            }
            this.modalEdit.tituloDiv.innerHTML = this.modalTitulo + (this.modalTitulo.length > 0 && valTit.length > 0 ? ": " : "") + valTit;
        }
        else
        {
            this.modalEdit.tituloDiv.innerHTML = this.modalTitulo;
        }
        this.modalContent.classList.add('panel');
        this.modalEdit.divConteudo = this.modalContent;


        var okbutton = document.createElement("button");
        okbutton.type = "button";
        okbutton.style.cssText = 'margin-right:10px; margin-top:10px;';
        okbutton.className = "btn btn-success pull-right";
        okbutton.innerHTML = row && row.persistencia ? "Confirmar" : "Adicionar";
        okbutton.onclick = function ()
        {
            var valid = that.modalEdit.divConteudo.validarCampos();
            if (!valid.ok)
            {
                alert(valid.mensagem, null, null, function ()
                {
                    valid.campo.focus();
                });
                return;
            }

            if (that.modalEdit.rowPersistencia)
            {
                that.modalEdit.divConteudo.carregarCampos(that.modalEdit.rowPersistencia);
            }
            if (that.modalEdit.row)
            {
                if (that.modalEdit.rowPersistencia)
                {
                    var indexP = that.persistenciaGrade.valores.indexOf(that.modalEdit.row.persistencia);
                    if (indexP > -1)
                    {
                        that.persistenciaGrade.valores[indexP] = that.modalEdit.rowPersistencia;
                        that.modalEdit.row.persistencia = that.modalEdit.rowPersistencia;
                    }
                }
                that.atualizarRow(that.modalEdit.row);
                that.selectRow(that.modalEdit.row);
            }
            else
            {
                that.modalEdit.row = that.addRow();
                that.modalEdit.row.persistencia = that.modalEdit.rowPersistencia;
                that.persistenciaGrade.valores.push(that.modalEdit.row.persistencia);
                that.atualizarRow(that.modalEdit.row);
                that.atualizarOrdemRows();
                if (that.modalEdit.row)
                {
                    that.selectRow(that.modalEdit.row);
                }
            }
            that.modalEdit.row.persistencia.Alterado = true;
            that.closeModal(true);
        };

        var cancelarbutton = document.createElement("button");
        cancelarbutton.type = "button";
        cancelarbutton.style.cssText = 'margin-right:10px; margin-top:10px;';
        cancelarbutton.className = "btn btn-danger pull-right";
        cancelarbutton.innerHTML = "Cancelar";
        cancelarbutton.onclick = function ()
        {
            that.closeModal(false);
        };
        if (row && row.persistencia)
        {
            this.modalEdit.rowPersistencia = row.persistencia.copy();
        }
        else if (this.persistenciaDefault)
        {
            this.modalEdit.rowPersistencia = this.persistenciaDefault.clone();
        }
        if (this.modalEdit.rowPersistencia)
        {
            this.modalEdit.divConteudo.popular(this.modalEdit.rowPersistencia);
        }
        this.modalEdit.appendChild(this.modalEdit.tituloDiv);
        this.modalEdit.appendChild(this.modalEdit.divConteudo);
        this.modalEdit.appendChild(cancelarbutton);
        this.modalEdit.appendChild(okbutton);
        this.modalEdit.divConteudo.style.display = "";

        Chagas.openModal(this.modalEdit);
        if (this.onModalOpen)
        {
            this.onModalOpen(row);
        }
        this.modalEdit.onkeyup = function (evt)
        {
            evt = evt || window.event;
            if (evt.keyCode == 27)
            {
                cancelarbutton.click();
            }
        };
    };

    _ChagasTable.selectCell = function (cell)
    {
        var l = new Array();
        if (cell)
        {
            l.push(cell);
        }
        this.selectCells(l);
    };

    _ChagasTable.selectCells = function (cells)
    {
        for (var x = 0; x < this.selectedCells.length; x++)
        {
            removeClass(this.selectedCells[x], "focus");
        }
        this.selectedCells = new Array();
        var foc = false;
        for (var x = 0; x < cells.length; x++)
        {
            if (this.selectionMode.toLowerCase() == ChagasTable.selectionMode.fullRow)
            {
                for (var c = 0; c < cells[x].row.cells.length; c++)
                {
                    if (!foc && document.activeElement != cells[x].row.cells[c])
                    {
                        cells[x].row.cells[c].focus();
                        return;
                    }
                    foc = true;
                    this.selectedCells.push(cells[x].row.cells[c]);
                    if (document.activeElement != cells[x].row.cells[c])
                    {
                        addClass(cells[x].row.cells[c], "focus");
                    }
                }
                this.selectedCells.push(cells[x]);
                addClass(cells[x], "focus");
            }
            else
            {
                this.selectedCells.push(cells[x]);
                addClass(cells[x], "focus");
                if (!foc && document.activeElement != cells[x])
                {
                    cells[x].focus();
                }
                foc = true;
            }
        }
        if (this.selectedCells.length > 0)
        {
            var cell = this.selectedCells[0];
            if (cell.row.offsetTop + cell.row.clientHeight > this.divTable.scrollTop + this.divTable.clientHeight)
            {
                this.divTable.scrollTop = cell.row.offsetTop - this.divTable.clientHeight + cell.row.clientHeight;
            }
            else if (cell.row.offsetTop - 20 < this.divTable.scrollTop)
            {
                this.divTable.scrollTop = cell.row.offsetTop - 20;
            }
            if (cell.offsetLeft + cell.clientWidth > this.divTable.scrollLeft + this.divTable.clientWidth)
            {
                this.divTable.scrollLeft = cell.offsetLeft - this.divTable.clientWidth + cell.clientWidth;
            }
            if (cell.offsetLeft < this.divTable.scrollLeft + this.divTable.offsetLeft)
            {
                this.divTable.scrollLeft = cell.offsetLeft - cell.clientWidth;
            }
        }
        else
        {
        }
    };

    _ChagasTable.selectRow = function (row)
    {
        var l = new Array();
        if (row)
        {
            l.push(row);
        }
        this.selectRows(l);
    };

    _ChagasTable.selectRows = function (rows)
    {
        var cells = new Array();
        for (var x = 0; x < rows.length; x++)
        {
            for (var c = 0; c < rows[x].cells.length; c++)
            {
                cells.push(rows[x].cells[c]);
                if (this.selectionMode.toLowerCase() != ChagasTable.selectionMode.fullRow)
                {
                    break;
                }
            }
        }
        this.selectCells(cells);
    };

    _ChagasTable.setDataSource = function (source, naoLimpar)
    {
        var l = PersistenciaJS.listFromDT(source);
        this.setDataSourcePersistencia(l, naoLimpar);
    };

    _ChagasTable.setDataSourcePersistencia = function (source, naoLimpar)
    {
        var that = this;
        if (!naoLimpar)
        {
            that.selectCells(new Array());
            this.grade.innerHTML = "";
            this.atualizarAllCheckColumn();
            this.atualizarTotalizador();
        }
        this.atualizarOrdem(source);
        for (var x = 0; x < source.length; x++)
        {
            this.carregando = x < source.length - 1;
            var linha = this.addRow();
            linha.persistencia = source[x];
            this.atualizarRow(linha, true);
        }
        this.atualizarHeaders();
    };

    var _ChagasTableColumn = Object.create(HTMLElement.prototype);
    _ChagasTableColumn.checkHeader = false;
    _ChagasTableColumn.dataPropertyName = "";
    _ChagasTableColumn.editavel = false;
    _ChagasTableColumn.header = "";
    _ChagasTableColumn.image = "";
    _ChagasTableColumn.type = "";
    _ChagasTableColumn.width = "";

    _ChagasTableColumn.createdCallback = function ()
    {
        var that = this;
        Object.defineProperty(this, 'header', {
            get: function ()
            {
                return that.attributes["header"] ? that.attributes["header"].value : "";
            },
            set: function (value)
            {
                that.setAttribute("header", value);
            }
        });
        Object.defineProperty(this, 'dataPropertyName', {
            get: function ()
            {
                return that.attributes["dataPropertyName"] ? that.attributes["dataPropertyName"].value : "";
            },
            set: function (value)
            {
                that.setAttribute("dataPropertyName", value);
            }
        });
        Object.defineProperty(this, 'image', {
            get: function ()
            {
                return that.attributes["image"] ? that.attributes["image"].value : "";
            },
            set: function (value)
            {
                that.setAttribute("image", value);
            }
        });
        Object.defineProperty(this, 'type', {
            get: function ()
            {
                return that.attributes["type"] ? that.attributes["type"].value : ChagasTableColumn.type.text;
            },
            set: function (value)
            {
                that.setAttribute("type", value);
            }
        });
        Object.defineProperty(this, 'editavel', {
            get: function ()
            {
                return that.attributes["editavel"] && that.attributes["editavel"].value.toLowerCase() == "true";
            },
            set: function (value)
            {
                that.setAttribute("editavel", value);
            }
        });
    };

    ChagasTable = document.registerElement('chagas-table',
    {
        prototype: _ChagasTable,
        extends: 'div'
    });

    ChagasTable.selectionMode =
    {
        cell: "cell",
        fullRow: "fullrow"
    };

    ChagasTableColumn = document.registerElement('chagas-tablecolumn',
    {
        prototype: _ChagasTableColumn,
        extends: 'div'
    });

    ChagasTableColumn.type =
    {
        text: "text",
        checkbox: "checkbox",
        image: "image",
        textbox: "textbox",
        combobox: "combobox",
        inteiro: "inteiro",
        decimal: "decimal"
    };
})();
// CHAGAS TABLE //

// CHAGAS TEXT-E
(function ()
{
    var _ChagasTextE = Object.create(HTMLElement.prototype);
    _ChagasTextE.chagasfield = '';
    _ChagasTextE.chagasfieldTexto = '';
    _ChagasTextE.inputId;
    _ChagasTextE.input;
    _ChagasTextE.nomeConsulta = "";
    _ChagasTextE.obrigatorio = false;
    _ChagasTextE.persistencia;
    _ChagasTextE.value = '';

    _ChagasTextE.createdCallback = function ()
    {
        var that = this;
        this.className = "chagas-texte form-group " + this.className;
        this.style.overflow = "hidden";

        this.inputId = document.createElement("input");
        this.inputId.style.cssText = "float:left; width:40px; margin:0;";
        this.inputId.className = "form-control";
        this.inputId.textE = this;
        this.inputId.onkeypress = function (e)
        {
            var tecla = (window.event) ? event.keyCode : e.which;
            if ((tecla > 47 && tecla < 58))
            {
                return true;
            }
            else
            {
                if (tecla == 8 || tecla == 0)
                {
                    return true;
                }
                return false;
            }
        };
        this.inputId.onchange = function (evt)
        {
            evt = evt || window.event;
            evt.stopPropagation();
            that.consultar();
        };
        var btn = document.createElement("button");
        btn.className = "btn btn-info";
        btn.type = "button";
        btn.style.cssText = "float:left; width:30px; height:34px; margin-left:1px; margin-right:1px; background-size:70%; background-position:center; background-repeat:no-repeat;";
        btn.style.backgroundImage = "url(" + pesquisarButtonImage + ")";
        btn.onclick = function ()
        {
            var cr = new ChagasConsultaRapida();
            Chagas.openModal(cr);
            cr.nomeConsulta = that.nomeConsulta;
            var retOK = false;
            cr.bloqueio.onclick = function ()
            {
                if (!retOK)
                {
                    Chagas.closeModal(cr);
                }
            };
            cr.eventoRetorno = function (pers)
            {
                retOK = true;
                that.inputId.value = pers.Identidade.valor;
                that.consultar();
                Chagas.closeModal(cr);
                var x = 0;
            };
            cr.consultar();
        };

        this.input = document.createElement("input");
        this.input.disabled = true;
        this.input.style.cssText = "float:left; width:calc(100% - 75px); margin:0;";
        this.input.className = "form-control";
        this.appendChild(this.inputId);
        this.appendChild(btn);
        this.appendChild(this.input);

        if (this.attributes["chagasfield"])
        {
            this.inputId.setAttribute("chagasfield", this.attributes["chagasfield"].value);
        }
        if (this.attributes["obrigatorio"])
        {
            this.inputId.setAttribute("obrigatorio", this.attributes["obrigatorio"].value);
            this.obrigatorio = (this.attributes["obrigatorio"].value == "true" ? true : false);
        }
        Object.defineProperty(this, 'value', {
            get: function ()
            {
                return that.inputId.value;
            },
            set: function (value)
            {
                that.inputId.value = value;
                that.input.value = "";
            }
        });
        Object.defineProperty(this, 'chagasfield', {
            get: function ()
            {
                return that.inputId.attributes["chagasfield"] ? that.inputId.attributes["chagasfield"].value : "";
            },
            set: function (value)
            {
                that.inputId.setAttribute("chagasfield", value);
            }
        });
        Object.defineProperty(this, 'chagasfieldTexto', {
            get: function ()
            {
                return that.attributes["chagasfieldtexto"] ? that.attributes["chagasfieldtexto"].value : "";
            },
            set: function (value)
            {
                that.setAttribute("chagasfieldtexto", value);
            }
        });
        Object.defineProperty(this, 'obrigatorio', {
            get: function ()
            {
                return that.inputId.attributes["obrigatorio"] ? that.inputId.attributes["obrigatorio"].value : "";
            },
            set: function (value)
            {
                that.inputId.setAttribute("obrigatorio", value);
            }
        });
        Object.defineProperty(this, 'nomeConsulta', {
            get: function ()
            {
                return that.attributes["nomeconsulta"] ? that.attributes["nomeconsulta"].value : "";
            },
            set: function (value)
            {
                that.setAttribute("nomeconsulta", value);
            }
        });
        this.nomeConsulta = this.nomeConsulta;
    };

    _ChagasTextE.consultar = function (retorno)
    {
        var that = this;
        if (this.request)
        {
            var executor = this.request.get_executor();
            if (executor.get_started())
            {
                try
                {
                    executor.abort();
                }
                catch (e) { }
            }
            this.request = null;
        }
        if (this.inputId.value.length > 0)
        {
            var showBloq = false;
            if (!document.divBloqueio || !containsClass(document.divBloqueio, "show"))
            {
                document.showLoading(true);
                showBloq = true;
            }

            this.request = PageMethods._staticInstance.ConsultarTextE({
                id: this.inputId.value,
                nomeConsulta: this.nomeConsulta
            },
            function (result)
            {
                if (showBloq)
                {
                    document.showLoading(false);
                }
                this.request = null;
                if (result)
                {
                    PersistenciaJS.create(result);
                    that.setarPersistencia(result);
                }
                else
                {
                    that.setarPersistencia(null);
                }
                if (retorno)
                {
                    retorno();
                }
            });
        }
        else
        {
            that.setarPersistencia(null);
            if (retorno)
            {
                retorno();
            }
        }
    };

    _ChagasTextE.focus = function ()
    {
        this.inputId.focus();
    };

    _ChagasTextE.setValue = function (value, retorno)
    {
        this.inputId.value = value;
        this.consultar(retorno);
    };

    _ChagasTextE.setarPersistencia = function (pers)
    {
        this.persistencia = pers;
        if (pers)
        {
            this.value = pers.Identidade.valor;
            if (pers[this.chagasfieldTexto])
            {
                this.input.value = pers[this.chagasfieldTexto].valor;
            }
            else
            {
                for (var x = 0; x < pers.Campos.length; x++)
                {
                    if (pers.Campos[x].identidade)
                    {
                        if (pers.Campos.length > x + 1)
                        {
                            this.input.value = pers.Campos[x + 1].valor;
                        }
                        break;
                    }
                }
            }
        }
        else
        {
            this.value = "";
            this.input.value = "";
        }
        if (this.onchange)
        {
            this.onchange();
        }
    };

    ChagasTextE = document.registerElement('chagas-texte',
    {
        prototype: _ChagasTextE,
        extends: 'div'
    });
})();
// CHAGAS TEXT-E //

// CHAGAS TREE
(function ()
{
    var _ChagasTree = Object.create(HTMLElement.prototype);
    _ChagasTree.nodes = new Array();
    _ChagasTree.showCheckBox = true;

    _ChagasTree.createdCallback = function ()
    {
        var that = this;
        this.className = "chagas-font chagas-borderbox " + this.className;
        this.style.cssText = "overflow-y:auto; font-size:12px; " + this.style.cssText;
        Object.defineProperty(this, 'nodes', {
            get: function ()
            {
                return that.children;
            }
        });
        Object.defineProperty(this, 'showCheckBox', {
            get: function ()
            {
                return that.attributes["showcheckbox"] && that.attributes["showcheckbox"].value.toLowerCase() == "true" ? true : false;
            },
            set: function (value)
            {
                that.setAttribute("showcheckbox", value ? "true" : "false");
                for (var x = 0; x < this.childElementCount; x++)
                {
                    this.children[x].setShowCheckBox(value);
                }
            }
        });
        if (!this.attributes["showcheckbox"])
        {
            this.showCheckBox = true;
        }
    };

    _ChagasTree.addNode = function (node)
    {
        node.setShowCheckBox(this.showCheckBox);
        this.appendChild(node);
        node.atualizarSeta();
    };

    _ChagasTree.removeNode = function (node)
    {
        this.removeChild(node);
        node.nodePai = null;
    };

    _ChagasTree.expandAll = function ()
    {
        for (var x = 0; x < this.childElementCount; x++)
        {
            this.children[x].expandAll();
        }
    };

    _ChagasTree.collapseAll = function ()
    {
        for (var x = 0; x < this.childElementCount; x++)
        {
            this.children[x].collapseAll();
        }
    };

    var _ChagasTreeNode = Object.create(HTMLElement.prototype);
    _ChagasTreeNode.nodes = new Array();
    _ChagasTreeNode.text = "";
    _ChagasTreeNode.checked;
    _ChagasTreeNode.showCheckBox = true;

    _ChagasTreeNode.createdCallback = function ()
    {
        var that = this;
        this.nodes.node = this;
        this.className = "chagas-treenode";

        var divLabel = document.createElement("div");
        divLabel.className = "labelbox";

        this.divNodes = document.createElement("div");
        this.divNodes.className = "nodesbox";

        this.checkBox = document.createElement("input");
        this.checkBox.type = "checkbox";
        this.checkBox.style.cssText = "float:left; margin-top:5px;";
        this.checkBox.node = this;
        this.checkBox.onmousedown = function ()
        {
            if (this.indeterminate)
            {
                this.indeterminate = false;
            }
        };
        this.checkBox.onclick = function ()
        {
            that.checkChild();
            if (that.nodePai)
            {
                that.nodePai.atualizarCheck();
            }
        };
        this.title = this.text;

        this.divSeta = document.createElement("div");
        this.divSeta.className = "seta";
        this.divSeta.node = this;
        this.divSeta.onclick = function ()
        {
            if (containsClass(this, "open"))
            {
                this.node.collapse();
            }
            else
            {
                this.node.expand();
            }
        };
        divLabel.appendChild(this.divSeta);
        divLabel.appendChild(this.checkBox);
        this.label = document.createElement("label");
        this.label.style.marginLeft = "2px";
        this.label.innerHTML = this.text;
        this.label.node = this;

        divLabel.appendChild(this.label);
        this.label.onclick = function ()
        {
            if (!this.node.checkBox.parentElement)
            {
                return;
            }
            if (this.node.checkBox.indeterminate)
            {
                this.node.checkBox.indeterminate = false;
            }
            this.node.checkBox.checked = !this.node.checkBox.checked;
            this.node.checkBox.onclick();
        };
        this.appendChild(divLabel);

        Object.defineProperty(this, 'text', {
            get: function ()
            {
                return that.label.innerHTML;
            },
            set: function (value)
            {
                that.label.innerHTML = value;
            }
        });
        Object.defineProperty(this, 'nodes', {
            get: function ()
            {
                return that.divNodes.children;
            }
        });
        Object.defineProperty(this, 'checked', {
            get: function ()
            {
                return that.checkBox.checked;
            },
            set: function (value)
            {
                if (that.checkBox.indeterminate)
                {
                    that.checkBox.indeterminate = false;
                }
                that.checkBox.checked = value;
                that.checkBox.onclick();
            }
        });
        Object.defineProperty(this, 'showCheckBox', {
            get: function ()
            {
                return that.checkBox && that.checkBox.parentElement ? true : false;
            },
            set: function (value)
            {
                if (value && !that.checkBox.parentElement)
                {
                    divLabel.insertBefore(that.checkBox, that.label);
                    that.atualizarSeta();
                }
                else if (!value && that.checkBox.parentElement)
                {
                    divLabel.removeChild(that.checkBox);
                    that.atualizarSeta();
                }
            }
        });

        this.atualizarSeta = function ()
        {
            that.divSeta.style.display = that.divNodes.childElementCount == 0 ? "none" : "";
            if (that.checkBox.parentElement)
            {
                that.checkBox.style.marginLeft = that.divNodes.childElementCount == 0 ? "12px" : "";
                that.label.style.marginLeft = "2px";
            }
            else
            {
                that.label.style.marginLeft = that.divNodes.childElementCount == 0 ? "12px" : "4px";
            }
        };
        this.atualizarNivel = function ()
        {
            this.style.paddingLeft = this.nodePai ? "15px" : "0";
        };
        this.checkChild = function ()
        {
            for (var x = 0; x < this.divNodes.childElementCount; x++)
            {
                that.divNodes.children[x].checkBox.indeterminate = false;
                that.divNodes.children[x].checkBox.checked = this.checkBox.checked;
                that.divNodes.children[x].checkChild();
            }
        };
        this.atualizarCheck = function ()
        {
            var checkeds = 0;
            var uncheck = 0;
            var indet = 0;
            for (var x = 0; x < this.divNodes.childElementCount; x++)
            {
                if (this.divNodes.children[x].checkBox.indeterminate)
                {
                    indet++;
                    break;
                }
                if (this.divNodes.children[x].checkBox.checked)
                {
                    checkeds++;
                }
                else
                {
                    uncheck++;
                }
                if (checkeds > 0 && uncheck > 0)
                {
                    break;
                }
            }
            if (indet > 0 || (checkeds > 0 && uncheck > 0))
            {
                this.checkBox.indeterminate = true;
                this.checkBox.checked = true;
            }
            else if (checkeds > 0)
            {
                this.checkBox.indeterminate = false;
                this.checkBox.checked = true;
            }
            else
            {
                this.checkBox.indeterminate = false;
                this.checkBox.checked = false;
            }
            if (this.nodePai)
            {
                this.nodePai.atualizarCheck();
            }
        };
    };

    _ChagasTreeNode.addNode = function (node)
    {
        node.setShowCheckBox(this.showCheckBox);
        this.divNodes.appendChild(node);
        node.nodePai = this;
        node.atualizarSeta();
        node.atualizarNivel();
        this.atualizarSeta();
    };

    _ChagasTreeNode.removeNode = function (node)
    {
        this.divNodes.removeChild(node);
        node.nodePai = null;
    };

    _ChagasTreeNode.expand = function ()
    {
        addClass(this.divSeta, "open");
        if (!this.divNodes.parentElement)
        {
            this.appendChild(this.divNodes);
        }
    };

    _ChagasTreeNode.expandAll = function ()
    {
        for (var x = 0; x < this.divNodes.childElementCount; x++)
        {
            this.divNodes.children[x].expandAll();
        }
        this.expand();
    };

    _ChagasTreeNode.collapse = function ()
    {
        removeClass(this.divSeta, "open");
        if (this.divNodes.parentElement)
        {
            this.removeChild(this.divNodes);
        }
    };

    _ChagasTreeNode.collapseAll = function ()
    {
        for (var x = 0; x < this.divNodes.childElementCount; x++)
        {
            this.divNodes.children[x].collapseAll();
        }
        this.collapse();
    };

    _ChagasTreeNode.setShowCheckBox = function (show)
    {
        for (var x = 0; x < this.divNodes.childElementCount; x++)
        {
            this.divNodes.children[x].setShowCheckBox(show);
        }
        this.showCheckBox = show;
    };

    ChagasTree = document.registerElement('chagas-tree',
    {
        prototype: _ChagasTree,
        extends: 'div'
    });

    ChagasTreeNode = document.registerElement('chagas-treenode',
    {
        prototype: _ChagasTreeNode,
        extends: 'div'
    });
})();
// CHAGAS TREE //

// CHAGAS SPLIT
(function ()
{
    var _ChagasSplit = Object.create(HTMLElement.prototype);
    _ChagasSplit.horizontal = false;

    _ChagasSplit.createdCallback = function ()
    {
        var that = this;
        this.className = "chagas-font chagas-borderbox " + this.className;
        this.panel1 = this.children[0];
        this.panel1.style.float = "left";
        this.panel2 = this.children[1];
        this.panel2.style.float = "left";
        this.spliter = document.createElement("div");
        this.spliter.style.cssText = " position:relative; float:left;";
        this.spliterRisco = document.createElement("div");
        this.spliterRisco.style.cssText = "position:absolute; background-color:gray;";
        this.spliter.appendChild(this.spliterRisco);
        this.insertBefore(this.spliter, this.panel2);
        this.spliter.onmousedown = function (evt)
        {
            that.start = true;
        };
        document.addEventListener("mousemove", function (evt)
        {
            if (that.start)
            {
                var rect = that.getBoundingClientRect();
                if (that.horizontal)
                {
                    var w = evt.clientX - rect.left;
                    var w2 = rect.width - w;
                    if (w > 10 && w2 > 10)
                    {
                        that.panel1.style.width = "calc(" + w * 100 / rect.width + "% - " + (3) + "px)";
                        that.panel2.style.width = "calc(" + w2 * 100 / rect.width + "% - " + (3) + "px)";
                        var tables = that.panel1.getElementsByClassName("chagas-table");
                        for (var x = 0; x < tables.length; x++)
                        {
                            if (tables[x].atualizarHeaders)
                            {
                                tables[x].atualizarHeaders();
                            }
                        }
                        tables = that.panel2.getElementsByClassName("chagas-table");
                        for (var x = 0; x < tables.length; x++)
                        {
                            if (tables[x].atualizarHeaders)
                            {
                                tables[x].atualizarHeaders();
                            }
                        }
                    }
                }
                else
                {
                    var h = evt.clientY - rect.top;
                    var h2 = rect.height - h;
                    if (h > 10 && h2 > 10)
                    {
                        that.panel1.style.height = "calc(" + h * 100 / rect.height + "% - " + (3) + "px)";
                        that.panel2.style.height = "calc(" + h2 * 100 / rect.height + "% - " + (3) + "px)";
                    }
                    var tables = that.panel1.getElementsByClassName("chagas-table");
                    for (var x = 0; x < tables.length; x++)
                    {
                        if (tables[x].atualizarHeaders)
                        {
                            tables[x].atualizarHeaders();
                        }
                    }
                    tables = that.panel2.getElementsByClassName("chagas-table");
                    for (var x = 0; x < tables.length; x++)
                    {
                        if (tables[x].atualizarHeaders)
                        {
                            tables[x].atualizarHeaders();
                        }
                    }
                }
            }
        });
        document.addEventListener("mouseup", function ()
        {
            that.start = false;
        });
        function montar()
        {
            if (that.horizontal)
            {
                that.panel1.style.height = "100%";
                that.panel2.style.height = "100%";
                that.spliter.style.height = "100%";
                that.panel1.style.width = "calc(50% - 3px)";
                that.panel2.style.width = "calc(50% - 3px)";
                that.spliter.style.width = "6px";
                that.spliterRisco.style.width = "2px";
                that.spliterRisco.style.height = "25px";
                that.spliterRisco.style.marginLeft = "2px";
                that.spliterRisco.style.top = "50%";
                that.spliterRisco.style.marginTop = "-12.5px";
                that.spliter.style.cursor = "col-resize";
            }
            else
            {
                that.panel1.style.width = "100%";
                that.panel2.style.width = "100%";
                that.spliter.style.width = "100%";
                that.spliter.style.height = "6px";
                that.panel1.style.height = "calc(50% - 3px)";
                that.panel2.style.height = "calc(50% - 3px)";

                that.spliterRisco.style.width = "25px";
                that.spliterRisco.style.height = "2px";
                that.spliterRisco.style.marginTop = "2px";
                that.spliterRisco.style.left = "50%";
                that.spliterRisco.style.marginLeft = "-12.5px";
                that.spliter.style.cursor = "row-resize";
            }
        }
        Object.defineProperty(this, 'horizontal', {
            get: function ()
            {
                return that.attributes["horizontal"] && that.attributes["horizontal"].value.toLowerCase() == "true" ? true : false;
            },
            set: function (value)
            {
                that.setAttribute("horizontal", value ? 'true' : 'false');
                montar();
            }
        });
        this.horizontal = this.horizontal;
    };

    ChagasSplit = document.registerElement('chagas-split',
    {
        prototype: _ChagasSplit,
        extends: 'div'
    });
})();
// CHAGAS SPLIT //

// CHAGAS DATEPICKER
(function ()
{
    var _ChagasDatePicker = Object.create(HTMLElement.prototype);
    _ChagasDatePicker.value = new Date();

    _ChagasDatePicker.createdCallback = function ()
    {
        var that = this;
        var nivel = 0;
        var fechar = true;
        this.className = "chagas-datepicker chagas-borderbox " + this.className;
        this.style.cssText = "width:105px; position:relative; " + this.style.cssText;
        this.input = document.createElement("input");
        this.input.className = "chagas-input chagas-borderbox";
        this.input.placeholder = "__/__/____";
        this.input.style.cssText = "width:100%; height:100%; float:left;";
        this.input.tabIndex = "0";
        this.input.onfocus = function (evt)
        {
            this.onclick(evt);
            document.addEventListener("click", docClick);
            if (!that.picker.parentElement)
            {
                setMesAtual(that.value);
            }
            that.appendChild(that.picker);
            var r = that.input.getBoundingClientRect();
            that.picker.style.left = "0";
            that.picker.style.top = r.height + "px";
            if (that.divBodyMeses.parentElement)
            {
                that.divBodyMeses.parentElement.removeChild(that.divBodyMeses);
                that.picker.appendChild(that.divBody);
            }
            nivel = 0;
            atualizarTitulo();
        };
        this.input.onchange = function ()
        {
            that.value = toDate(this.value);
            atualizarInput();
        };
        this.input.onclick = function (evt)
        {
            var s = this.selectionStart - 1;
            var e = this.selectionStart;
            while (!isNaN(parseInt(this.value.charAt(s))))
            {
                s--;
            }
            while (!isNaN(parseInt(this.value.charAt(e))))
            {
                e++;
            }
            s++;
            this.setSelectionRange(s, e);
            evt.stopPropagation();
        };
        this.input.onkeydown = function (evt)
        {
            evt = evt || window.event;
            function keyLeft()
            {
                var e = that.input.selectionStart;
                if (that.input.selectionStart == that.input.selectionEnd && e > 0)
                {
                    e--;
                }
                else if (e == 0)
                {
                    e = that.input.selectionEnd;
                }
                while (!isNaN(parseInt(that.input.value.charAt(e))))
                {
                    e--;
                }
                var s = e - 1;
                while (!isNaN(parseInt(that.input.value.charAt(s))))
                {
                    s--;
                }
                if (isNaN(parseInt(that.input.value.charAt(s))))
                {
                    s++;
                }
                that.input.setSelectionRange(s, e);
            }
            function keyRight()
            {
                var s = that.input.selectionStart;
                if (that.input.selectionEnd < that.input.value.length)
                {
                    if (s < 0)
                    {
                        s = 0;
                    }
                    while (!isNaN(parseInt(that.input.value.charAt(s))))
                    {
                        s++;
                    }
                    var e = s + 1;
                    while (!isNaN(parseInt(that.input.value.charAt(e))))
                    {
                        e++;
                    }
                    s++;
                    that.input.setSelectionRange(s, e);
                }
            }
            if (evt.keyCode == 37)//Left
            {
                keyLeft();
                evt.stopPropagation();
                return false;
            }
            else if (evt.keyCode == 39)//Right
            {
                keyRight();
                evt.stopPropagation();
                return false;
            }
            else if (evt.keyCode == 40)//Down
            {
                if (this.selectionStart != this.selectionEnd)
                {
                    var val = this.value.substring(this.selectionStart, this.selectionEnd);
                    if (!isNaN(parseInt(val)))
                    {
                        var p1 = this.value.substring(0, this.selectionStart);
                        var p2 = this.value.substring(this.selectionEnd, this.value.length);
                        var s = this.selectionStart;
                        val = parseInt(val) - 1;
                        if (val == 0)
                        {
                            val = 1;
                        }
                        that.value = toDate(p1 + val + p2);
                        if (s == 0)
                        {
                            this.setSelectionRange(0, 2);
                        }
                        else
                        {
                            this.setSelectionRange(s - 1, s - 1);
                            keyRight();
                        }
                    }
                }
                evt.stopPropagation();
                return false;
            }
            else if (evt.keyCode == 38)//Up
            {
                if (this.selectionStart != this.selectionEnd)
                {
                    var val = this.value.substring(this.selectionStart, this.selectionEnd);

                    if (!isNaN(parseInt(val)))
                    {
                        var p1 = this.value.substring(0, this.selectionStart);
                        var p2 = this.value.substring(this.selectionEnd, this.value.length);
                        var s = this.selectionStart;
                        val = parseInt(val) + 1;
                        that.value = toDate(p1 + val + p2);
                        if (s == 0)
                        {
                            this.setSelectionRange(0, 2);
                        }
                        else
                        {
                            this.setSelectionRange(s - 1, s - 1);
                            keyRight();
                        }
                    }
                }
                evt.stopPropagation();
                return false;
            }
        };
        function docClick()
        {
            document.removeEventListener("click", docClick);
            if (that.picker.parentElement)
            {
                that.picker.parentElement.removeChild(that.picker);
            }
        }
        this.input.onblur = function ()
        {
            if (fechar)
            {
                var p = document.activeElement;
                var ok = false;
                while (p)
                {
                    if (p == that.picker)
                    {
                        ok = true;
                        break;
                    }
                    p = p.parentElement;
                }
                if (!ok)
                {
                    docClick();
                }
            }
            fechar = true;
        };
        this.appendChild(this.input);
        var days = ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'];
        var months = ['Janeiro', 'Fevereiro', 'Março', 'Abrir', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
        this.divDate;
        this.picker = document.createElement("div");
        this.picker.className = "chagas-borderbox chagas-noselect chagas-font chagas-datepickerdiv";
        this.picker.onclick = function (evt)
        {
            evt = evt || window.event;
            evt.stopPropagation();
        };
        this.picker.onmousedown = function (evt)
        {
            fechar = false;
            evt = evt || window.event;
            evt.stopPropagation();
        };
        this.divHeader = document.createElement("div");
        this.divHeader.className = "header";
        this.buttonBack = document.createElement("button");
        this.buttonBack.type = "button";
        this.buttonBack.className = "chagas-button";
        this.buttonBack.onclick = function (evt)
        {
            if (nivel == 0)
            {
                var m = that.mesAtual.date.getMonth();
                var y = that.mesAtual.date.getFullYear();
                if (m <= 0)
                {
                    m = 11;
                    y--;
                }
                else
                {
                    m--;
                }
                var old = that.mesAtual;
                var nm = createMonth(new Date(y, m, 1));
                that.divBody.appendChild(nm);
                nm.style.left = "-" + that.divBody.clientWidth + "px";
                that.mesAtual = nm;
                setTimeout(function ()
                {
                    addClass(old, "anim");
                    addClass(nm, "anim");
                    that.mesAtual.style.left = "0";
                    old.style.left = that.divBody.clientWidth + "px";
                    atualizarTitulo();
                    setTimeout(function ()
                    {
                        that.divBody.removeChild(old);
                    }, 300);
                }, 10);
            }
            else if (nivel == 1)
            {
                var y = that.divBodyMeses.anoAtual.year;
                if (y > 1)
                {
                    y--;
                    var old = that.divBodyMeses.anoAtual;
                    old.style.left = "0";
                    var nm = createSelectMonth(y);
                    that.divBodyMeses.appendChild(nm);
                    nm.style.left = "-" + that.divBodyMeses.clientWidth + "px";
                    that.divBodyMeses.anoAtual = nm;
                    setTimeout(function ()
                    {
                        addClass(old, "anim");
                        addClass(nm, "anim");
                        nm.style.left = "0";
                        old.style.left = that.divBodyMeses.clientWidth + "px";
                        atualizarTitulo();
                        setTimeout(function ()
                        {
                            that.divBodyMeses.removeChild(old);
                        }, 300);
                    }, 10);
                }
            }
        };
        var divSetaBack = document.createElement("div");
        divSetaBack.style.cssText = "height:0; width:0; border-top:5px solid transparent; border-bottom:5px solid transparent; border-right:5px solid black; margin-left:6px;";
        this.buttonBack.appendChild(divSetaBack);
        this.divHeader.appendChild(this.buttonBack);
        this.divTitle = document.createElement("div");
        this.divTitle.innerHTML = "";
        this.divTitle.className = "titulo";
        this.divTitle.onclick = function ()
        {
            if (nivel == 0)
            {
                if (that.divBodyMeses.anoAtual)
                {
                    that.divBodyMeses.removeChild(that.divBodyMeses.anoAtual);
                }
                that.divBodyMeses.anoAtual = createSelectMonth(that.mesAtual.date.getFullYear());
                that.divBodyMeses.appendChild(that.divBodyMeses.anoAtual);
                that.picker.appendChild(that.divBodyMeses);
                that.divBody.parentElement.removeChild(that.divBody);
                nivel = 1;
                atualizarTitulo();
            }
        };
        this.divHeader.appendChild(this.divTitle);
        this.buttonNext = document.createElement("button");
        this.buttonNext.type = "button";
        this.buttonNext.className = "chagas-button";
        this.buttonNext.onclick = function (evt)
        {
            if (nivel == 0)
            {
                var m = that.mesAtual.date.getMonth();
                var y = that.mesAtual.date.getFullYear();
                if (m >= 11)
                {
                    m = 0;
                    y++;
                }
                else
                {
                    m++;
                }
                var old = that.mesAtual;
                var nm = createMonth(new Date(y, m, 1));
                that.divBody.appendChild(nm);
                nm.style.left = that.divBody.clientWidth + "px";
                that.mesAtual = nm;
                setTimeout(function ()
                {
                    addClass(old, "anim");
                    addClass(nm, "anim");
                    nm.style.left = "0";
                    old.style.left = "-" + that.divBody.clientWidth + "px";
                    atualizarTitulo();
                    setTimeout(function ()
                    {
                        that.divBody.removeChild(old);
                    }, 300);
                }, 10);
            }
            else if (nivel == 1)
            {
                var y = that.divBodyMeses.anoAtual.year + 1;
                var old = that.divBodyMeses.anoAtual;
                old.style.left = "0";
                var nm = createSelectMonth(y);
                that.divBodyMeses.appendChild(nm);
                nm.style.left = that.divBodyMeses.clientWidth + "px";
                that.divBodyMeses.anoAtual = nm;
                setTimeout(function ()
                {
                    addClass(old, "anim");
                    addClass(nm, "anim");
                    nm.style.left = "0";
                    old.style.left = "-" + that.divBodyMeses.clientWidth + "px";
                    atualizarTitulo();
                    setTimeout(function ()
                    {
                        that.divBodyMeses.removeChild(old);
                    }, 300);
                }, 10);
            }
        };
        var divSetaNext = document.createElement("div");
        divSetaNext.style.cssText = "height:0; width:0; border-top:5px solid transparent; border-bottom:5px solid transparent; border-left:5px solid black; margin-left:8px;";
        this.buttonNext.appendChild(divSetaNext);
        this.divHeader.appendChild(this.buttonNext);
        this.divBody = document.createElement("div");
        this.divBody.className = "body";
        this.picker.appendChild(this.divHeader);
        this.picker.appendChild(this.divBody);

        this.divBodyMeses = document.createElement("div");
        this.divBodyMeses.className = "body";

        function createMonth(date)
        {
            var div = document.createElement("div");
            div.style.left = "0";
            div.className = "divmes";
            var table = document.createElement("table");
            div.appendChild(table);
            div.date = date;
            var r0 = document.createElement("tr");
            for (var x = 0; x < days.length; x++)
            {
                var td = document.createElement("td");
                td.innerHTML = days[x];
                r0.appendChild(td);
            }
            table.appendChild(r0);
            var daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();// mes 1 = janeiro neste caso
            var diaSemana = new Date(date.getFullYear(), date.getMonth(), 1).getDay();//0 = domingo // mes 0 = janeiro
            var daysMonth = new Array();
            if (diaSemana == 0)
            {
                diaSemana += 7;
            }
            while (diaSemana > 0)
            {
                var m = date.getMonth();
                var y = date.getFullYear();
                var daysInMonthAnt = new Date(y, m, 0).getDate();// mes 1 = janeiro neste caso
                daysMonth.push(new Date(y, m - 1, daysInMonthAnt - diaSemana + 1));
                diaSemana--;
            }
            for (var x = 1; x < daysInMonth + 1; x++)
            {
                daysMonth.push(new Date(date.getFullYear(), date.getMonth(), x));
            }
            for (var x = 1; daysMonth.length < 42; x++)
            {
                var m = date.getMonth() + 1;
                var y = date.getFullYear() + (m >= 12 ? 1 : 0);
                if (m >= 12)
                {
                    m = 0;
                }
                daysMonth.push(new Date(y, m, x));
            }
            var r = null;
            for (var x = 0; x < daysMonth.length; x++)
            {
                if (x % 7 == 0)
                {
                    r = document.createElement("tr");
                    table.appendChild(r);
                }
                var td = document.createElement("td");
                td.className = "chagas-borderbox day";
                td.innerHTML = daysMonth[x].getDate();
                td.date = daysMonth[x];
                td.title = td.date.getDate() + " de " + months[td.date.getMonth()].toLowerCase() + " de " + td.date.getFullYear();
                if (td.date.getMonth() != date.getMonth())
                {
                    addClass(td, "othermonth");
                }
                if (that.value &&
                    that.value.getFullYear() == td.date.getFullYear() &&
                    that.value.getMonth() == td.date.getMonth() &&
                    that.value.getDate() == td.date.getDate())
                {
                    addClass(td, "select");
                    that.divDate = td;
                }
                td.onclick = function ()
                {
                    if (that.divDate)
                    {
                        removeClass(that.divDate, "select");
                    }
                    that._value = this.date;
                    atualizarInput();
                    docClick();
                    if ((this.date.getMonth() > that.mesAtual.date.getMonth() && this.date.getFullYear() == that.mesAtual.date.getFullYear()) ||
                        this.date.getFullYear() > that.mesAtual.date.getFullYear())
                    {
                        that.buttonNext.click();
                    }
                    else if ((this.date.getMonth() < that.mesAtual.date.getMonth() && this.date.getFullYear() == that.mesAtual.date.getFullYear()) ||
                             this.date.getFullYear() < that.mesAtual.date.getFullYear())
                    {
                        that.buttonBack.click();
                    }
                    else
                    {
                        addClass(this, "select");
                        that.divDate = this;
                    }
                };
                r.appendChild(td);
            }
            return div;
        }
        function atualizarTitulo()
        {
            if (nivel == 0)
            {
                that.divTitle.innerHTML = months[that.mesAtual.date.getMonth()].toLowerCase() + " " + that.mesAtual.date.getFullYear();
            }
            else if (nivel == 1)
            {
                that.divTitle.innerHTML = that.divBodyMeses.anoAtual.year
            }
        }
        function atualizarInput()
        {
            that.input.value = formatarData(that.value, "dd/MM/yyyy");
        }
        function setMesAtual(date)
        {
            if (that.mesAtual)
            {
                that.divBody.removeChild(that.mesAtual);
            }
            that.mesAtual = createMonth(date);
            that.divBody.appendChild(that.mesAtual);
            atualizarTitulo();
        }
        function createSelectMonth(year)
        {
            var div = document.createElement("div");
            div.className = "divmes";
            div.year = year;
            var table = document.createElement("table");
            div.appendChild(table);
            var r;
            for (var x = 0; x < months.length; x++)
            {
                if (x % 4 == 0)
                {
                    r = document.createElement("tr");
                    table.appendChild(r);
                }
                var td = document.createElement("td");
                td.innerHTML = months[x].toLowerCase().substring(0, 3);
                td.month = x;
                td.className = "chagas-borderbox month";
                td.onclick = function ()
                {
                    setMesAtual(new Date(that.divBodyMeses.anoAtual.year, this.month, that.mesAtual.date.getDate()));
                    that.divBodyMeses.parentElement.removeChild(that.divBodyMeses);
                    that.picker.appendChild(that.divBody);
                    nivel = 0;
                    atualizarTitulo();
                };
                r.appendChild(td);
            }
            return div;
        }



        Object.defineProperty(this, 'value', {
            get: function ()
            {
                return that._value;
            },
            set: function (value)
            {
                if (!value)
                {
                    value = new Date();
                }
                that._value = value;
                setMesAtual(value);
                atualizarInput();
            }
        });
        this.value = new Date();
    };

    _ChagasDatePicker.focus = function ()
    {
        this.input.focus();
    };

    ChagasDatePicker = document.registerElement('chagas-datepicker',
    {
        prototype: _ChagasDatePicker,
        extends: 'div'
    });
})();
// CHAGAS DATEPICKER //

// PERSISTENCIA JS
(function ()
{
    PersistenciaJS =
    {
        create: function (persistencia)
        {
            persistencia.Identidade = { valor: "" };
            persistencia.Cabecalho = { valor: "" };
            for (var x = 0; x < persistencia.Campos.length; x++)
            {
                if (persistencia.Campos[x].identidade)
                {
                    persistencia.Identidade = persistencia.Campos[x];
                }
                if (persistencia.Campos[x].cabecalho)
                {
                    persistencia.Cabecalho = persistencia.Campos[x];
                }
                var c = {
                    valor: '',
                    campo: persistencia.Campos[x]
                };
                Object.defineProperty(c, 'valor', {
                    get: function ()
                    {
                        return this.campo.valor;
                    },
                    set: function (value)
                    {
                        this.campo.valor = value;
                    }
                });
                persistencia[persistencia.Campos[x].nome.toLowerCase()] = c;
            }
            if (persistencia.Grades)
            {
                for (var x = 0; x < persistencia.Grades.length; x++)
                {
                    PersistenciaJS.create(persistencia.Grades[x].persistencia);
                    if (persistencia.Grades[x].valores)
                    {
                        for (var v = 0; v < persistencia.Grades[x].valores.length; v++)
                        {
                            PersistenciaJS.create(persistencia.Grades[x].valores[v]);
                        }
                    }
                }
                persistencia.Grades.getGrade = function (nomeGrade)
                {
                    for (var x = 0; x < this.length; x++)
                    {
                        if (this[x].grade == nomeGrade)
                        {
                            return this[x];
                        }
                    }
                    return null;
                }
            }
            persistencia.copy = function (semValores)
            {
                var ret = {};
                ret.Campos = new Array();
                ret.Grades = new Array();
                ret.Arquivos = new Array();
                ret.Alterado = (semValores ? false : this.Alterado);
                for (var x = 0; x < this.Campos.length; x++)
                {
                    ret.Campos.push({
                        nome: this.Campos[x].nome,
                        valor: (semValores ? "" : this.Campos[x].valor),
                        identidade: this.Campos[x].identidade,
                        cabecalho: this.Campos[x].cabecalho
                    });
                }
                if (this.Grades)
                {
                    for (var x = 0; x < this.Grades.length; x++)
                    {
                        var g = {
                            grade: this.Grades[x].grade,
                            persistencia: this.Grades[x].persistencia.copy(semValores),
                            valores: new Array()
                        };
                        for (var v = 0; v < this.Grades[x].valores.length; v++)
                        {
                            g.valores.push(this.Grades[x].valores[v].copy(semValores));
                        }
                        ret.Grades.push(g);
                    }
                }
                if (this.Arquivos)
                {
                    for (var x = 0; x < this.Arquivos.length; x++)
                    {
                        ret.Arquivos.push(this.Arquivos[x]);
                    }
                }
                PersistenciaJS.create(ret);
                return ret;
            };
            persistencia.clone = function ()
            {
                return this.copy(true);
            };
            persistencia.limparIds = function (limparArquivos)
            {
                if (this.Identidade)
                {
                    this.Identidade.valor = "";
                }
                if (this.Cabecalho)
                {
                    this.Cabecalho.valor = "";
                }
                if (limparArquivos)
                {
                    this.Arquivos = new Array();
                }
                if (this.Grades)
                {
                    for (var x = 0; x < this.Grades.length; x++)
                    {
                        this.Grades[x].persistencia.limparIds(limparArquivos);
                        for (var v = 0; v < this.Grades[x].valores.length; v++)
                        {
                            this.Grades[x].valores[v].limparIds(limparArquivos);
                        }
                    }
                }
            };
            persistencia.todosArquivos = function (validos)
            {
                var ret = new Array();
                for (var x = 0; x < this.Arquivos.length; x++)
                {
                    if (!validos || this.Arquivos[x].name || (this.Arquivos[x].base64 && this.Arquivos[x].base64.length > 0))
                    {
                        ret.push(this.Arquivos[x]);
                    }
                }
                for (var x = 0; x < this.Grades.length; x++)
                {
                    if (this.Grades[x].valores)
                    {
                        for (var v = 0; v < this.Grades[x].valores.length; v++)
                        {
                            var arg = this.Grades[x].valores[v].todosArquivos(validos);
                            for (var a = 0; a < arg.length; a++)
                            {
                                ret.push(arg[a]);
                            }
                        }
                    }
                }
                return ret;
            };
        },
        listFromDT: function (dt)
        {
            var ret = new Array();
            for (var x = 0; x < dt.rows; x++)
            {
                var p = {};
                p.Campos = new Array();
                p.Grades = new Array();
                for (key in dt.table)
                {
                    var c = {
                        nome: key,
                        valor: dt.table[key][x],
                        identidade: key == dt.identidade
                    };
                    p.Campos.push(c);
                }
                PersistenciaJS.create(p);
                ret.push(p);
            }
            return ret;
        }
    };
})();
// PERSISTENCIA JS //

// CSS
(function ()
{
    var style = document.createElement("style");
    style.innerHTML =
        ".chagas-button { background-color:rgb(209, 210, 212); height:30px; border:none; font-family:" + fontFamily + "; cursor:pointer; padding:0; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; } " +
        ".chagas-button:hover { background-color: " + corOver2 + "; } " +
        ".chagas-button:disabled  { opacity:0.4; cursor:default; } " +
        ".chagas-button:disabled:hover  { background-color:transparent; } " +

        ".chagas-button.header { background-color:#25B89A; border:none; } " +
        ".chagas-button.header:hover { background-color:" + corOver + "; } " +
        ".chagas-button.header:disabled:hover  { background-color:#25B89A; } " +

        ".chagas-input { padding-left:3px; padding-right:3px; border:1px solid gray; font-family:" + fontFamily + "; font-size:12px; } " +
        "select.chagas-input { padding-left:0; padding-right:0; border:1px solid gray; } " +
        ".chagas-input:disabled { background-color:rgb(245, 245, 245); color:black; } " +

        ".chagas-borderbox { box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box;} " +

        ".chagas-logoemp { position:absolute; margin-top:3px; margin-left:3px; height:40px; width: 120px; background-position:center; background-size:contain; background-repeat:no-repeat; cursor:pointer; } " +
        ".chagas-homebutton { background-color:transparent; position:absolute; margin-top:2px; right:3px; height:20px; width:20px; background-position:center; background-size:contain; background-repeat:no-repeat; } " +
        ".chagas-homebutton:hover { background-color:" + corOver2 + "; } " +

        ".chagas-border { border:1px solid " + corBorder + "; } " +

        ".chagas-tabhead { height:18px; line-height:18px; border-top-left-radius: 5px; border-top-right-radius: 5px; margin-top:2px; font-family:" + fontFamily + "; font-size:13px; float:left; padding:0; padding-left:7px; padding-right:7px; cursor:pointer; border:none; border-top:solid 1px " + corBorder + "; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; background-color:" + corFundoGrade + " }" +
        ".chagas-tabhead.litem { border-left:1px solid " + corBorder + "; }" +
        ".chagas-tabhead.ritem { border-right:1px solid " + corBorder + "; }" +
        ".chagas-tabhead:hover { background-color: " + corOver2 + " }" +
        ".chagas-tabhead.active { background-color: white; height:21px; margin-top:0; border-right:1px solid " + corBorder + "; border-left:1px solid " + corBorder + "; border-bottom:1px solid white; }" +

        ".chagas-alert { opacity:0; min-width:200px; max-width:600px; margin-left:10px; margin-right:10px; text-align:center; left:20%; top:7%; position:fixed; background-color:white; box-shadow:0px 5px 25px 0px rgba(50, 50, 50, 1); font-family:" + fontFamily + "; -moz-transition-duration: 0.2s; -o-transition-duration: 0.2s; -webkit-transition-duration: 0.2s; transition-duration: 0.2s; -moz-transition-property: opacity; -o-transition-property: opacity; -webkit-transition-property: opacity; transition-property: opacity; -moz-transition-timing-function: linear; -o-transition-timing-function: linear; -webkit-transition-timing-function: linear; transition-timing-function: linear; }" +
        ".chagas-alert.show { opacity:1; }" +
        ".chagas-alert .chagas-button { margin:5px; margin-top:25px; margin-bottom:25px; min-width:100px; }" +
        ".chagas-alert .chagas-button:hover { background-color: " + corOver2 + "; } " +
        ".chagas-alert .texto { margin-top:25px; margin-left:25px; margin-right:25px; }" +

        ".chagas-cell { overflow:hidden; text-overflow:ellipsis; } " +
        ".chagas-cell:focus { background-color:" + corOver2 + "; outline:none; }" +
        ".chagas-cell.focus { background-color:" + corOver2 + "; outline:none; }" +

        ".chagas-bloqueio { opacity:0; position:fixed; width:100%; height:100%; background-color:rgba(0,0,0,0.5); top:0; left:0; text-align:center; -moz-transition-duration: 0.2s; -o-transition-duration: 0.2s; -webkit-transition-duration: 0.2s; transition-duration: 0.2s; -moz-transition-property: opacity; -o-transition-property: opacity; -webkit-transition-property: opacity; transition-property: opacity; -moz-transition-timing-function: linear; -o-transition-timing-function: linear; -webkit-transition-timing-function: linear; transition-timing-function: linear; outline:none; }" +
        ".chagas-bloqueio.show { opacity:1; }" +

        ".chagas-tablerow: { height:30px; } " +
        //".chagas-tablerow:last-child td { border-bottom:solid 1px white; } " +
        ".chagas-tablerow:first-child td { border-top:none; } " +
        ".chagas-tablerow td { border-left:solid 1px white; border-bottom:solid 1px white; } " +
        ".chagas-tablerow td:first-child { border-left:none; } " +
        //".chagas-tablerow:nth-child(even) { background-color:#898C8E; } " +

        ".chagas-tablerowheader td { padding-right:10px; font-weight:bold; border-left:solid 1px white; white-space:nowrap; text-overflow:ellipsis; height:20px; font-size:12px; line-height:20px; cursor:default; overflow:hidden; padding-left:10px; min-width:30px; } " +
        ".chagas-tablerowheader td:first-child { border-left:none; } " +
        ".chagas-tablerowheader td.checkbox { padding-right:0; padding-left:0; min-width:0; } " +

        ".chagas-modal { opacity:0; position:fixed; box-shadow:0px 20px 50px 0px rgba(50, 50, 50, 1); } " +
        ".chagas-modal.anim { -moz-transition-duration: 0.2s; -o-transition-duration: 0.2s; -webkit-transition-duration: 0.2s; transition-duration: 0.2s; -moz-transition-property: opacity; -o-transition-property: opacity; -webkit-transition-property: opacity; transition-property: opacity; -moz-transition-timing-function: ease; -o-transition-timing-function: ease; -webkit-transition-timing-function: ease; transition-timing-function: ease; } " +
        ".chagas-modal.animated { -moz-transition-duration: 0.2s !important; -o-transition-duration: 0.2s !important; -webkit-transition-duration: 0.2s !important; transition-duration: 0.2s !important; } " +

        ".chagas-modal.show { opacity:1; } " +

        ".chagas-tablemodal { width:65%; height:90%; left:20%; background-color:white; border-radius:5px; font-family:" + fontFamily + "; } " +
        ".chagas-tablemodal .conteudo { -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; margin:0 auto; border:1px solid " + corBorder + "; width:calc(100% - 6px); height:calc(100% - 100px); overflow-y:auto; border-radius:5px; } " +
        //".chagas-tablebutton { height:20px; width:23px; background-position:center; background-size:17px; background-repeat:no-repeat; float:left; }" +
        //".chagas-tablemodal .chagas-button { background-color:rgb(209, 210, 212); }" +
        //".chagas-tablemodal .chagas-button:hover { background-color: " + corOver2 + "; }" +

        ".chagas-login .chagas-button { background-color:rgb(209, 210, 212); }" +
        ".chagas-login .chagas-button:hover { background-color: " + corOver2 + "; }" +
        ".chagas-default .chagas-button { background-color:rgb(209, 210, 212); }" +
        ".chagas-default .chagas-button:hover { background-color: " + corOver2 + "; }" +

        ".chagas-tablebutton.add { background-color:" + corOver2 + "; }" +
        ".chagas-tablebutton.remove { background-color:#EE3A39; }" +
        ".chagas-tablebutton.edit { background-color:#F6A31D; }" +
        ".chagas-tablebutton.add:hover { background-color:" + corOver + "; }" +
        ".chagas-tablebutton.remove:hover { background-color:" + corOver + "; }" +
        ".chagas-tablebutton.edit:hover { background-color:" + corOver + "; }" +

        ".chagas-noselect { -moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none; } " +
        ".chagas-textselect { -moz-user-select: text; -webkit-user-select: text; -ms-user-select:text; user-select:text; } " +
        ".chagas-font { font-family:" + fontFamily + " } " +

        ".chagas-hidescroll::-webkit-scrollbar { display:none } " +

        ".icon-images {}" +

        ".chagas-listafiles { width:100%; overflow-y:auto; border-radius:5px; border:1px solid #d1d1d1; padding-top:5px; padding-left:5px; padding-bottom:5px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; background-color: #f1f1f1; background-repeat:no-repeat; background-position:center center;-webkit-transition: all .2s ease-in-out; transition: all .2s ease-in-out;} " +
        ".chagas-listafiles .file { width:90px; height:95px; float:left; margin-right:5px; margin-bottom:5px; position:relative; border:1px solid white; } " +
        ".chagas-listafiles .thumb { width:100%; height:calc(100% - 18px); float:left; margin-bottom:2px; background-repeat:no-repeat; background-position:center; background-size:58px; } " +
        ".chagas-listafiles .thumb.image { background-size:contain; background-position:center; width:calc(100% - 4px); margin-bottom:0; margin-top:2px; margin-left:2px; } " +
        ".chagas-listafiles .title { width:100%; height:16px; line-height:16px; overflow:hidden; text-align:center; text-overflow:ellipsis; white-space:nowrap; font-size:10px; font-family:" + fontFamily + "; padding-left:3px; padding-right:3px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; cursor:default; outline:none; } " +
        ".chagas-listafiles .edittext { width:100%; height:20px; position:absolute; left:0; bottom:0; text-align:center; font-size:11px; font-family:" + fontFamily + "; padding-left:3px; padding-right:3px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; } " +

        ".chagas-listafiles .file .checkbox { position:absolute; right:0; top:0; display:none; } " +
        ".chagas-listafiles .file:hover > .checkbox { display:block; } " +
        ".chagas-listafiles .file .checkbox:checked { display:block; } " +

        ".chagas-listafilesunico { width:100%; overflow-y:auto; padding:0; -moz-box-sizing:border-box; -webkit-box-sizing:border-box; box-sizing: border-box; cursor:pointer; background-color:" + corFundoGrade + ";background-repeat:no-repeat; background-position:center center; } " +
        ".chagas-listafilesunico .file { width:100%; height:100%; float:left; position:relative; } " +
        ".chagas-listafilesunico .thumb { width:100%; height:calc(100% - 18px); float:left; margin-bottom:2px; background-repeat:no-repeat; background-position:center; background-size:50%; } " +
        ".chagas-listafilesunico .thumb.image { background-size:contain; background-position:center; width:calc(100% - 4px); margin-bottom:0; margin-top:2px; margin-left:2px; } " +
        ".chagas-listafilesunico .title { width:100%; height:16px; line-height:16px; overflow:hidden; text-align:center; text-overflow:ellipsis; white-space:nowrap; font-size:10px; font-family:" + fontFamily + "; padding-left:3px; padding-right:3px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; cursor:default; outline:none; } " +
        ".chagas-listafilesunico .edittext { width:100%; height:20px; position:absolute; left:0; bottom:0; text-align:center; font-size:11px; font-family:" + fontFamily + "; padding-left:3px; padding-right:3px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; } " +
        ".chagas-listafilesunico .file .checkbox { display:none; } " +

        ".chagas-consultarapida { position:fixed; max-width:500px; width:calc(100% - 16px); height:calc(100% - 8px); max-height: 550px; top:0; background-color:white; box-shadow:0px 5px 25px 0px rgba(50, 50, 50, 1); } " +
        ".chagas-campoborda { -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; height:26px; border:solid 2px transparent; } " +
        ".chagas-campoborda.invalido { border:solid 2px red; } " +

        ".chagas-treenode { width:100%; box-sizing:border-box; } " +
        ".chagas-treenode .labelbox { width:100%; height:23px; line-height:23px; box-sizing:border-box; overflow:hidden; } " +
        ".chagas-treenode .nodesbox { width:100%; box-sizing:border-box; } " +
        ".chagas-treenode .seta { float:left; margin-top:6px; margin-left:3px; border-left:solid 5px black; border-top:5px solid transparent; border-bottom:5px solid transparent; height:0; width:0; cursor:pointer; } " +
        ".chagas-treenode .seta.open { margin-left:0; margin-top:9px; border-left:solid 4px transparent; border-right:solid 4px transparent; border-top:5px solid black; border-bottom:5px solid transparent; } " +

        ".chagas-datepickerdiv { width:189.5px; height:166px; padding:2px; font-size:11px; text-align:center; background-color:rgb(230, 230, 231); position:absolute; z-index:9999; } " +
        ".chagas-datepickerdiv .divmes { width:185.5px; height:140px; position:absolute; } " +
        ".chagas-datepickerdiv .divmes.anim { -moz-transition:left linear 0.3s; -o-transition:left linear 0.3s; -webkit-transition:left linear 0.3s; transition:left linear 0.3s; } " +
        ".chagas-datepickerdiv table { border-spacing:0; width:100%; height:100%; } " +
        ".chagas-datepickerdiv table td { cursor:default; width:24.5px; max-width:24.5px; min-width:24.5px; } " +
        ".chagas-datepickerdiv table td.day { cursor:pointer; } " +
        ".chagas-datepickerdiv table td.day:hover { background-color:" + corOver + "; } " +
        ".chagas-datepickerdiv table td.day.othermonth { color:rgb(200, 200, 201); } " +
        ".chagas-datepickerdiv table td.select { background-color:" + corOver2 + "; } " +
        ".chagas-datepickerdiv table td.month { cursor:pointer; width:44.37px; max-width:44.37px; min-width:44.37px; } " +
        ".chagas-datepickerdiv table td.month:hover { background-color:" + corOver + "; } " +
        ".chagas-datepickerdiv .header { width:100%; height:22px; } " +
        ".chagas-datepickerdiv .body { width:185.5px; height:140px; background-color:" + corFundoGrade + "; overflow:hidden; position:relative; } " +
        ".chagas-datepickerdiv .chagas-button { width:20px; height:22px; float:left; background-color: transparent; } " +
        ".chagas-datepickerdiv .chagas-button:hover { background-color: " + corOver2 + " } " +
        ".chagas-datepickerdiv .titulo { cursor:default; float:left; width:145.5px; line-height:22px; height:22px; font-size:13px; } " +

        ".page-header {color:#159ddb; font-size:20px !important; font-family: " + fontFamily + "}" +


        "@font-face" +
        "{" +
        " font-family: 'open_sansregular';" +
        " src: url(data:application/x-font-woff;charset=utf-8;base64,d09GRgABAAAAAGEUABMAAAAAsRAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABGRlRNAAABqAAAABwAAAAcZgAcCUdERUYAAAHEAAAAHgAAACABFwAER1BPUwAAAeQAAASiAAAJmCwaFlhHU1VCAAAGiAAAAIEAAACooF6Ikk9TLzIAAAcMAAAAYAAAAGCg5ZmGY21hcAAAB2wAAAGGAAAB2s9AWKBjdnQgAAAI9AAAAEYAAABGE1sNN2ZwZ20AAAk8AAABsQAAAmVTtC+nZ2FzcAAACvAAAAAIAAAACAAAABBnbHlmAAAK+AAATOUAAJGs52V2UWhlYWQAAFfgAAAAMwAAADYHW/N2aGhlYQAAWBQAAAAfAAAAJA9zBj9obXR4AABYNAAAAjcAAAOm2kNYqmxvY2EAAFpsAAABzAAAAdZ55Fc6bWF4cAAAXDgAAAAgAAAAIAIHAZduYW1lAABcWAAAAegAAARORj+S23Bvc3QAAF5AAAAB7QAAAuUaeDKocHJlcAAAYDAAAADaAAABfLpWDR93ZWJmAABhDAAAAAYAAAAGly9UZwAAAAEAAAAAzD2izwAAAADJNTGLAAAAANCNR6542mNgZGBg4ANiCQYQYGJgBMKXQMwC5jEAAA5NARwAAHjarZZLbFRVGMf/M51hxoKWqtH4CBoyNrUGjQ1J27GwatpaDZZpi4MOig/iAkJCY0hMExaFgbgwIQYrOTxqCkyh0FmQUpryMkxXLNzhaW3jyuVJV8QFIY6/c9sp4EjVxHz55dw597vf43/OPXMVklSpbn2qSEvru916/rOvenep5oveHTtVv+uTL3droyL4qFiU9/0316GdO3p3K+6vAiIKB2NcoXhv4Lldt3QrdDg0ELoDd8PpcA6mw7+GcxWrw+mKfTAW+SlyL3IvnIvOr/gtdDj2TKw2loLPudoL/ndt7MdYKp6MJ2N740ni3b1vRMvBgqUfNOIGFp2v2BfEKVntwxbfDklPeDo6T6V3gqoHAt5UorhHDXpVTZCEZj2tlmJercWs2qAdOooFdcJmSPG7i7GbsYdxC6Shnzj74QBk4SAcgkHiDeF7ipin4QzkYBjOwjnujcB5uACjMAaXYBwuwwRMwhXyXIVrcB0KzE0RP6R1mtCzqika1UE9rIcG8jcWrZrwS0IzfM38EfgOBuB7OAoGjuF7HE7ASRjE/ybzU4whouXJESVmJdRwvw7WhSrJZ8lng3xNeCVhIZcllyWXJZcllyWXJVcptg1iT/PcLDylKp6shkQQrUA0RzSnt/jdzLgB+rneDwcgCwfhUBDREc1phufnYNVSjaX6lqvH1+J17OO5KmqqhrXsB1/JozLO4DMHL6BKBlUyZRkboJGamhiTQQ+FZas4hu9xOAEnYRD/BZUKqJRBpYweV9Ufv6saEqyNV2ZBFUONhhoNNRpqNNRoNIPfHLQGXa0O9C11tqe8XuZbyNHKvTZohw7oJNJmSHHdxdjN2MO4hVhpxq08+wF8CBnYRp5HvRHL9T/E/VPkPw1nIAfDcBbOcW8EzsMFGIUxuATjcBkmYBKuUNNVuAbX4cbiChe4d5vafobS6q1EGYsqFkW8jo6qHVU7qnZU7aja7yqLNyuIt8HbLGqYR0OHhg4NHRo69LPoZ9HPop9FP4d+Dv0c+ln0c+hn0c+hn0M/nzVP1jxZ82TNkzVP1jxaObRyaOXQyqGVQyuHVg6tLFpZtLJoZdHKopVFK4tWFq0sWlm0smhl0cqilUUri1YWrSxaWbRyaOXQyqGVQyeHTn53Gzo22vCX9yFbtmta8GqFNmiHDubun5dm8bw0i+dlPjgvtwXvVZaus3SdpessXWfpOvsPO8TQtaFrQ9eGrg1dG7o2dG3o2tC1oWtD14auDV0bujZ0beja0LUpO0sXdodZ2hWrytZ1ubXwmkU4BRyngONNdbypXC/NlL8fLtiTJU+eRdtKmIZZ8DN9zPQx08dMn558aFf5ndQd6PHfVtuv7Bpip4id+tu9+mDk/2N/+YzT5JqFx5Yyl076tUHPqeDU9h7+5PZKWnTz+lj0sXx5+JqjwVfOSs7pKlWrQgmeXaHX9QarVa/1eoLzayN3WjjtnlO73taLegdbo03YS+pUl15WD5bQ+9gr2irObH2E1alf3+g1fYs16IiOqlFGP7D7hzRMxBGNqkMXsU0a07je4x93gnonsS7d0E2+vqawtG7rF+LOYR//CRxXWnQAAHjaY2BkYGDgYohiyGBgcXHzCWGQSq4symFQSS9KzWbQy0ksyWOwYGABqmH4/x9IYGMJMDD5+vsoMAgE+fsCSbAoyFTGnMz0RAYOEAuMWcB6GIEijAx6YJoFaLMQgxSDAsNLBmYGTwZ/hhdg2ofhOQMTkPcMSPoAVTIyeAIAoLkaBQAAAAADBEUBkAAFAAQFmgUzAAABHwWaBTMAAAPRAGYB8QgCAgsGBgMFBAICBOAAAu9AACBbAAAAKAAAAAAxQVNDAEAADfsEBmb+ZgAACGICUyAAAZ8AAAAABEgFtgAAACAAA3jaY2BgYGaAYBkGRgYQuALkMYL5LAw7gLQWgwKQxcXAy1DH8J8xmLGC6RjTHQUuBREFKQU5BSUFNQV9BSuFeIU1ikqqf36z/P8P1MML1LOAMQiqlkFBQEFCQQaq1hKulvH///9f/z/+f+h/wX+fv///vnpw/MGhB/sf7Huw+8GOBxseLH/Q/MD8/qFbL1mfQt1GJGBkY4BrYGQCEkzoCoBeZmFlY+fg5OLm4eXjFxAUEhYRFROXkJSSlpGVk1dQVFJWUVVT19DU0tbR1dM3MDQyNjE1M7ewtLK2sbWzd3B0cnZxdXP38PTy9vH18w8IDAoOCQ0Lj4iMio6JjYtPSGRoa+/snjxj3uJFS5YtXb5y9ao1a9ev27Bx89Yt23Zs37N77z6GopTUzLsVCwuyn5RlMXTMYihmYEgvB7sup4Zhxa7G5DwQO7f2XlJT6/RDh69eu3X7+o2dDAePMDx+8PDZc4bKm3cYWnqae7v6J0zsmzqNYcqcubMZjh4rBGqqAmIAN0SKoAAAAAAESAW2AJgASwBlAHUAeQCBAIcAiwCRAJMA3QCqAGAAdwB7AIMAhwCUAJ0ApgCqALAAtADEAJoArgCoAJYAoQCfAEQFEQAAeNpdUbtOW0EQ3Q0PA4HE2CA52hSzmZDGe6EFCcTVjWJkO4XlCGk3cpGLcQEfQIFEDdqvGaChpEibBiEXSHxCPiESM2uIojQ7O7NzzpkzS8qRqnfpa89T5ySQwt0GzTb9Tki1swD3pOvrjYy0gwdabGb0ynX7/gsGm9GUO2oA5T1vKQ8ZTTuBWrSn/tH8Cob7/B/zOxi0NNP01DoJ6SEE5ptxS4PvGc26yw/6gtXhYjAwpJim4i4/plL+tzTnasuwtZHRvIMzEfnJNEBTa20Emv7UIdXzcRRLkMumsTaYmLL+JBPBhcl0VVO1zPjawV2ys+hggyrNgQfYw1Z5DB4ODyYU0rckyiwNEfZiq8QIEZMcCjnl3Mn+pED5SBLGvElKO+OGtQbGkdfAoDZPs/88m01tbx3C+FkcwXe/GUs6+MiG2hgRYjtiKYAJREJGVfmGGs+9LAbkUvvPQJSA5fGPf50ItO7YRDyXtXUOMVYIen7b3PLLirtWuc6LQndvqmqo0inN+17OvscDnh4Lw0FjwZvP+/5Kgfo8LK40aA4EQ3o3ev+iteqIq7wXPrIn07+xWgAAAAABAAH//wAPeNq1fQlgFOUV8Hwzs2f2mj2zm3Oz2SwhJJvsJoRwi1wiKiIiIEUuEfBALhEpIlBABEUOuRQPxIiR4sxmQaSI4I1WKUWj1Fp+q9VupVStrQrJ8L/3zeyRS2j7/+Luzh6Zee9973v3e8OwzECGYadqrmc4RsdUSIQJ947peP/fI5JW88feMY6FQ0bi8GMNfhzTaYuae8cIfh4V/ELQL/gHsoVyMdkqT9dcf+75gfx7DJyS2Xrhc7Jas58xMhZmIhPLYpkyUR+Os2bGxpcR0RoWmaa4xscIfJn60mjSMPoyKcuXELPCksmXaDSbsuADizUhWsKS2ZqQbKRMMlsEu6Rn6+oYKYsV7KK5rrKqtjoacbuc2kBRiSPKBbbe3bNf/7rqgfbj0Rkz7x88oP+QfppN5z+lcK3g6lkR4EJ8ezIxBuHio3HOzOj5MlEbIQClyDVJLFyVtUk6uKIWrmyAVx1cTiI8XLmyCi9D4LHiYJcZZOjB0uma/S3fsLaWb/AaUYbh/w3XyGEKyLVMzMcwZTGX2xuNRmM6uF5Mn2WC4zhDfDpzWSMr5OYVe6ISo080Oj3ZOcWeSFzD0684W34BfqWBr7QGoxm+ImJhWPQ1SV5HQvQq8OkdiZhObyxr7K/jDUBlm+SGT13wqcuNn7oc8KnLJmXBpyZHQvKTMrG772DfV/85l3GVGQ/2/fifX+CB6LM1sj6dA65Ln7X4DBdpNHj1cOC2NRrdWQ48VaPZZYIf2OizQJ+d+Iy/8dDfwF9l07+Cc+Ykz5ObPE8e/qYxP/nLAvyc629jOUTSJiAVcvPyCyra/Cf29yHpa/wOPzyiHH24/PQRcOCjFr6KkoKB8tekbOSakaRq1MpRRC+fHkBy5PdGrR4lnxh5/4idJDxAPkH2LiWjlpC4PBwfS+Q9S+WRZC8+4HNgFeCQ5RdW8yatnSlkQkw5M4kR88NiTlTijQmxSySWzyNx8/MMwM4VYVHfJPktCdFvk/JIWYzPKo5EIlKuPREzO7rAoZhrk7rCAmRbElIYX7sCMwk2ZGM+H9iYQTauySdRoYLUVHevrYm63B5dSUjIZ4Gxda5ADfC20+0RLIR0r6kuCS2/7tTY955599mlB56vfmT7jseG/fbFpXf+fuG4WVOmkWGnxt7X8FgwTA5ctuf+5bvt++OaQct7ZclXR25aNOZ+yfPX0wFu8/DxpWS57RfNm/O3Dx3bjWE0zPQLZ7TlmneZLMbFeJkgU8VsZ2Ju5N4APEmlukTMg/zLwZPk0CbiBmuAM5dJBl0inhemh3m6BBEjuLmR1USTTRJwB8Gh1ib54LAEDktsUjc4LARejMKrYBLsjQbO7S321EndSuCNJy+QDW8YyVAK73yFJd3wK0cevNGaBAbeABtUd09t+lqnOxoRbIEirYNEDaTtN0gv+G76IxvWP7Zt09pHHxh23a5d1w1bxJVtbP6QnHpkw0NPbNv00LbVw0aNGjFi1KhhPPv5l2c/+SJx9nRDAxlFRu4+P0qz/9wwcuDzL7/+0+eJs396/rlnf/38M88gj8y+cEZzUvMeU8CUMjXMEibmRXrlIb0CpkTMiKSKGoAo3SlRCi2JRl0hyrmu7oRYaJMqUaTBodkmOXHXAnvUwmslsMc+I5cXKLEBtqJZEIN1otMeEzy+uro60SWIXiBPNCDY9zM6s8dXUp4kSm0FW5MkgI70JbVRVkcCIQtJUqOWWFjkpL5EIcvsNfHr+xS//sLOg/fvIFt6XO7ZPXA1KfvLS3d9v/mjv+95dOm3T8hXzhrfbenQG5befMvosbPIomVvT7tp8sy6zbuffeTWfb+QF/Z9dor8503yp7GZ4z94Zf7q7WT3wLHT2JODFo+5csl1V02ayDAE5S7pR+VugSJ1VZFLRD4pbyUNKVNFK4pVlKi4DwkzUj7E+uFvzUw2Q5mPgEJAauqyE5JV+SObvTaqha1i9wRK2JGPbtj58PpNa57cuJ2tIgby/t6jcuT7b+TuLzeQN5Vz9oFzmpLnZJLnzGqS+PQ5o267YGN1ge72mmq2z86N2x/d8OSaTes1+1+Qq+Sf4F/P3YfIu998T95XzjmaXcJbtE7QeFZG5MKo5VBl4alqNVyUC3o0Dl0WCTlG55FVZa+WkYd88vIf94hPit/xA/bPImvk+bP2F8gHJ5CZ8uYJZDCeczrzOV/KvwZ7ciQjMmFRF5UIyB9NJMYQlD+M0VAWIwweEg5FkSksGptENiIZYKfxkZjBiN8ZdPAzowEPjYwBWE4Bq8YvgBZ3+YWAMJ1sX0Mek6euYR98gOyRRz0gjyYNCl795B/J7cxZRg8yAWDAlTPgyhnoyml8CckILKvhQIzpqXLsDovh0bK6fjmXmfdbfZNr5B+nk8h4z1T5hzlwvlHkFNuPnQ28UITnkwiXwAeygsQQ4Awfnj/JDjV+1yjyNTm1dSvCQu0K5jugRwUDACAsYFCoLxR7sCu0il2hvKi41mZIha196npeNqAuevnMywcNuvyywf0UPJ2gvD+hPOoGnsB9TChjAFgKLFESJU7WX99yGuUBtSemXTjDl8P+z2I8YEvFDLjxrVkJRUa6sgCpbEUaWqg0tKM0hD3uhVc7yD3JwKEKcFnhUMvU1VFOjkaQ7wJFbObxtLPfn/3hH/8+8+/dm3fVP/JI/a7N7KfkXjJHvlfeIK+QHyBL4Pio/CkJkV7wLyifpjgdAiDfpXZYiInxaZyywiLfJHHuhGQCUDgeTRudYtqADgoI1bUWojtElq/dqXdVf8CvIWXnhvH2ZfNcFXvoeaeCjRMEXeFlrlZoJXm4RMyKWBuMgLUvLGqbJLslEbNrkevsXmBArR0PtciAOUBQycMAy5A60SCAXDfbqQiL1pC+rCKZdCFlyUCAufyuqWSpeGePh+698empY945+/7fHmuSj7DfrCPLY1sfvm7+6t7XzN59MrZG/uZ38lv67RTGCbA2OQBjiJnGxIIII6juWDbCKBgTcaMhmA0qy4iL1IWCWwSLZClsEqQcgNuSg8BaDABsKQLLB6maBmAlUgQy2GiX8gvgVRBiTldOXV1SgRcKNn+gJqV1dKG+RJXGrnzicvL+opIJN300kSySJzy84fm3N947seH2UeO+XvbhmR0Piw3yn+R/zTva6/FwFSklxnVbVt56T/XgO4dc+1rD2li+3h3fcOKzAPJdGdB/puYwrKsd7WvkuxiLYpXJMrBmtGklhktQg9YRFg1Noiki6V0JkYvE9FQM6LWwIAZqNhpwQVD76A2Aog0ta3VhWEG0Ik/WAE9EXQHgCzD4q2sDWl0Zu188deqJltOs36iv7EpGrOc+bS7dIotkxBby5br4RHVPrYA1yAP+8zG/VHSjxMPuMOMa2LhE3GHwmmENHMgyOSDdmiQ37I5cxULt9+a5J6lhaq6wiKYjGsmW85NFFI4wkkmoqCCNJrAVVbuQSG4dsLDFSg0qb3KlYiTLnV4ZWBKbv0gXcqDBCKzFu5xMoGjF4N/dLL4mr77pidG17EctLwbnzvmKGOTT8g+9niyP1u8gkbxads82+QrPX976XJaB9vMBpzDwlZspZmYwMSdilWNU97zekIhp8ECblYhb/E60iix6QC9I5YAHWCwrInpsUj6aPrD/SuA13wOmjZ6zOFHZCwI4X4iHPwc+ZUxClqLbbYw/4nEBT7FcVN0YgSKm1qlghsxmIfPJNWTo7MuumvL1jybTHWff+PynDz6X/90wYuOEh3ZsWD9u81h2NnmBPO9Y55U/kd/cc/a3X8jnyfUv3RKb3vjc+vphy5R9A7xVBmumBfkc0yRlBvITEXVhSY+7gSCHcHWKfCYBMoF7r6XhADtBk7dtxbnjmjy0jcCe5EspnYpAWs9UKeUDAunxfN3QLAorVAELyGMTA7j3NHCsCUsB+hEa00glsRS/ygJyocFUCnbPPj3r9BVaqJnYzQfvGU2WUFiimkFBakWr+w63XStjEKyhTAsIdNMDd3/9xtt/W7kxtln+9G/Nu57d8kj9Y68+sjJ81xPrFqxfsvghMv/82qv33fnEywd33Ra74vrfLNp/6r0Dd69ce8+kLUP6P8quGf+rAb0fGH/zgrtxb84EvFE2ekBfzlB1ggWwzsIDU1I5BEAEuXIMyB8upEQJpUQ24JptkwpgBwgRSQebIYTeAlBbyjKBtCkQGg0WzkXRDiB/6Bi7oCLNAL52F7B4qAYQtTsCaaMPUa2Ad9qZzIXfPXL67pbp8waOmPbNv7JMtfvnvPrFroc33rhtzMiNN617jDv9BdFvkz95q7neuc4HQih63Q1//f3Dzwy7b/CM2LQDKd+en0r9hNoMLQySJm5RFbEbsQGhoyph0ZY8kjzt1LEA7nsbxXzXokz1zP1m1SqqpVlqi6yAa+tB7nVnRFs4nqVekYq5uFmJJXBmA9jYaJo4VdMEvG7wtlqZJ1zquilDpXQBXLhnnwx7hf8LXlu1xb4DW+wU7AsGJKLLQFzTuaPN33EWdutkcvoR+UH5wBaEcQE5ynu5z2mMwadYOIYEtSJwnfVhjCaolg2BxwJuZ/MEbic5uno12bZ6tSI3M65VW2MgNXgta/O33NHvtpDBZP4jsn8ytT9yLnzO1QGv5TAlzB0MVQFStikhFofj+SphQmHR0iSZ7YlGwZJrKYv7lRUBj8OFtHElpC5qXCWWnV+M7oVfkFg7uhn2mNFhQKpJ2eCkxjRm1HOoFdBddSgrCHaCheSRAHqtyR2lc6QjMjkvv//KB6Hh99582dKhk+8fsnzRNZsnbVcjNJrpUw49f8WsKXeMnnOTv/v8LaPmzhs5bVaw6vxqJWzDUBwXXhisPaCJg3/Vj1nGiNGwVKFP4HqbolIPHbhREbFPWPLCUSgs8Shr+9O9VAYWcJkSDenuTIjdbVJACZdIl8FroLtg728w8Q5vqKIy2ovuqIooSLWqOtEhiJV1Up8e4F7pGZvbGihDwewVxFzFQiv2R3g7og74hZRtVlMNtprbw7mcVMawxYEinnWhzK51aQOFDIHPcwgSZ2ETWUuYj8g1L455cvrou7L0XR6Ztvm5M0cG7hnkXXHjnE3yP6TT8v69ZAAJ//7PR76XH5FnsTWvHbdbhl6/bCPbi/Bk8+l9cuOph84sm37tDZPfE3/LXPC65a7u2Ed79hHbxpfk5z6Tj8sHRq8YRdaRpTKpIxpnHOgI/2lsmkPAlRamm2K1iVyUCve4Vs8QkERalPGq2yIRPVDDBBhXgd0b4Pycw8+VhLQ6dsA6tp+4v2V//BtyqsEfcJdqDp0bSE7IYXYmeWP04olzlfjJMdAjh0GPWEASFjK3qHYiWsdUnxSaEvFsD71sNtpgfrpoVlCQ2RHRapMcsEhZ3oSYS7UCuGVF8EEuWsoGPbKkBw7FrDoxWwClC2xZaBc1aEAK/kKemlu84OQDQb+iHP01ykEZOUZ2gMLi168hV8k/nJX3kCqp8cXfgOWcHX9cPHJOs3/voWXPeY118iev/3H96g333/fw7JWLboc9uRjk+nGqz/qouswKNi9PbV6km0ex9L0JKRvJZ3UCfHZqhjA6ATcObxe1VFLbi6MRjw6YgRFsILUVDln8HOn38Zd7htbXfysniPmnba9vPS2/LD/FfvpnMurAqA1Xy6/ICfkz+a3aR+rI/bCeQF/NGKCvnhGYHip1DSaVuoIJILJTiAxAUoMN/SBJA8A5EDgBTb0kuRAKfyiqvAaOkSPkenKPvEBe9/VxUkkicM2//KDZL6+Ufy1vlpduJGUkSPJJEcopgIH7EWDIYoYkIeBUCHiw7zQKW2mQPKYUMOiWUpfVkGUoA19V8U9VR01xSJXHMS7csoid0LKTXaHZv0Uu3dzy5VZFPiava2D6Z/hq9Jp6Db2mHq9p7OCa4CarF8xqc8HU5eBiLYktLeuUa8G6a/pQGXu3asNaTel1jzucXh5tWLxebpIFkqEvMGdjAg3DCg64Yp4a74rxBi9yhFuQdFpkZivarO46yekA/jaBJwhsI+o6YBllkSK1AjC0AJIH+OZFMuarv70z4M0X5X/JHxA/yd66Xn6JfDvvm8fluPwQ+8kX5IZ9ozePkl+Vv5Q/lt8PkFe3ttQFS8gahY6aArp+/VSJoFMkgqiJxjkjpSSXXr0soCQbEbNsaAUCTamFklw3TAig7wiEbODYhoZmWbO/ZQN7x7lhrNgyIrVuZC71sf1tfGw8PQdnw4cmdcZjDaqrzZALE+UlZDqN2ZQzMS3CmBWWWATOEhY1TZLeooRuJDaLJgZEraKodNTsg5OhPgo9OeSqKbc0HIlP6P6h8+7ZcPYxh0/kJGHjE5QWlW1owUdbE4CiTG1giTPU1SmgYugxQHQAMrvlWEucnfx2y9fbgAJl7IctK5rfYN+4v+W1VryrSUphpLBKB22SDjGOciunAa7RpQnsgrPDXjj3xfYUPbWABONgxqrn0pmjqRUkopOezwGen4OuGV0w1PsOBN4KJpGoFxpZjRktaVGnWJkKajEuy1qnIOcHBaBEBQKAYsDtEo4RhhzXk5Nyg0Uvm1bLNr1Fs//8MB4Wi9u78/C57zS27R80T0jCqOlHYRyp0tWsQpjVEYRmV3sIJdYCr2YgN58ET+KsKuVJtCQE+hdXWAXuCJenZ3WaJ15t/kxvA7h65m6Y089u4YacG8a/+3G8+Si1K3BfH24buzElYzdcOnbjTcduvBmxG1x6NXbDaOtUF03Zn4VMZuxmMZlIBpJeZLK8Q34dRXq85dt//vjDd/9sYU+Tm8lq+U75Sfkp+Q6yhkyTP5LfIxHSlYRIlazk0pAvp1NZZ2cGZEo7O2hQvbJH9ahBHSlpZ4ygxLcApBjPdCK32o2wKXhV4oNm9Ae8quOIKnERWyyfkL/cs+3P7xw+BkpbHv2nf7QcZU9seGr9Q5RW8i5KKytIweuYmBlp5UjSypsWfTaglU2xuZBWKO/cNnBTuCyzATkMtqQRiOYwAzAGNKnakA6D1UTXCfn+/D4Z/4P8eW1nJPxKXjtA3kyGsR0RUqHjcaCjCfyWG1VO1CucKLmAlOBJICmzkJTuVNTOHEnKcrRBPKoAl4w6YEiNAEgwkhnljY6G5dMWiE0DmPgzabyemAmR/0AePCY/Lh9PxHc///Knmv3vH5f/NK1lFjux5Un2u3Xr1t9H9wz6kCzonGKM7tD0Cw/UFhBUN6dEE8Co91pSWcBCixJK0IEi2cebBHd+AOldKEgOJw2KBJSgiFtoJBZnIQ002EVHRmwEHMSQ6jZT20TrcuYTD41ZBQpnjvtgesOePus2/PYF+cQfXqzZ9/yqrT1WrP7y1/Jfv5Gbw7tKui2dO3zSyOor3n7qubdHbBo+75bhk66tGnlo89GPKT52oP04oD3NumozYwoipyRdtU2SBsSehoYLNRyGCzWpcGHaY8JYtZ0fKlc1aAq2bDn3maaAnv8g7GUvnF8AjzRmRXppVUtINEaThhCIVZGz0WQurCw1MA0YtbZStxAVREpTIDcebHj71ZfebpB/L/8I/06zem5v8+AXX3/jAHeg+epz8p9JoSKD4T/+NRpjBZ1mRNxoWlkXJTSTjZuPSW4+kFZuT/daNKjjZETPouKe5OoXW77drdnffPXT23c+xe1BbUdglzG64XBOL/OaYnOIQpSeNkb0lmg0SiOsgBKB/UaUnW6F/ZajRM6OXvimmEbOGJvoPWKBX4jskYOv/u7vffFTjShUWETnEcng+kkj6uGLp/9+Ar7IEp22RrtTcJQ1OvA5Bs+FDxQ+ENCCb1gHpiwTZw12h5MmY8mLLIYLverbZBjOQtQQIuNFKyfLRTWII+pQscYXByBPgNm0sNdz4l9VunNqiDZOHJV+R82XcXnhbvmUq5q4K+U/I1kWHt7+4gvcwuYlj7720G+5FaBfTl72pvNXO5qjSCc90H4z1dkl6R3NJrWfKaxoahb1BW+k+sJAlP8DBqInX8q9DpFbyR375F7kr0fB5L2TbWYPtrzMXt4yvFlml7csVtd3EbWzwS7QpdaXi9IUiK6JRvQxBaLVAfIsIq8c4HLDtQiw7EGynKw8IGfvAYMgyH7SvLTlGBvG3Bacux+V7RVJ/Z30KTjFgKVWqqRTIrJg4AqKRRitIX4MIPhdIzl9i417pflHLn8Fv3XbivMzVNugXj7EzqJ7DvaEYh4bExiO0BhpHAJTJTqaa0GTBjZf8h0XSW441TSuJ8fk08QvH9Kee/Cc/yE4twcE6qfJXAmXpElGrgQX2fM0G6hP2W9MVD5Emik8YLMrKAI8ujDsDgUeXRNcGuNECITWJhHYrwQkgy0JmC4ZO/H4qf3rjwJQpwG42v2a0w/+pFWuU8WG+IDmKKMFaaCaVWrYkmY3cOWrSGwK+XSrvFaOsSFue/M0NtHiQRuzWX6D235hKODlwRweVmXgIwMxHZCE5x5vnnxgOUPIav4UZ9f6Yf2CDFwFC1zMGHQBx4M0SawvoSwfq6PLV1lFaqIgVF2BCvjTdwvndLvjspjW5Bq0r8ZbfQR11IQLZ7j3+HGw74uZpUzMjpT1IFvkGRIxM0H+MCTibJEHw+asNhVX9mUnRJ9NKiR09WxuqoXtQgIkH1ULPtwBBiO6yUJMZ/ag82G3i05g1qI82KmM0wEf0RoX1MtmVNOolYO1NKKTzGIIgVBGVLF7H4z9TGAdB2fds/rX0WuPTnr95UV+/cin73n6pb23T35Uqn/zBRImw23agUsXjVrSLbL3SIvzsVuu2L1t3ISGbVN0utupfSOCrpuvdYJ9WMBMUu0bC00pGRMxPeKcZ0RGoLUvgKvTTW1why0BAotWFYBVjmUt1HsCDUKdbZ8ADgFqvjyPYI9rQUVrlAQTincdRtAV86IkFNA5MsoExHq9ft4XH//j26Zbpd6mQHh7w7bNm3c0bNY65ZVLbt4ln5K/g38fXjPyQTb41VunTzT9/jXgubmwbmv48ZmxAQz0IvBGUzo2YGkTGzCmYgOc6uhhPMDt0VWAZ0fVEcYGipi5xPCvax4vj9Yui8ixp59c/fAdz56Vz7N5xEG6FXkedOfJo9/5uNfGOhIEegIsfC3Q0w70nMbETEhPG4LkNqogIT0NKXo6LNT2BrtW1EcwyI7+HS3fCFOqeoGqjRrOQksRTALACUR1A1EZrcZiVpMTYK55omGCNngooMXkpcohlKhzv2k6vcCs4+tXuQ1z//LRP/Zsf6Rh26MNGx5l/cRKuu285ipy+KczG58lJcT8+6aX3g8k3vqc8gbiYge6Ohgf4iIgGllJymYbE3GnXsAQutOgZJAY6gaKzgjWQaEXoc2m+STJpaceDaW1VpDMlDWyBFgGCzoY6C46lTIcugT2POJHs6jWhezOOPwKGkTzx/e/aTFrXtot/XrMjln/lL8S2T5rl/1qB5tDDKSH/O2fZhx9e9imEj/pQhbueFbJlWIyxKItYJzI3Q7K3Qi9oAdhHBEFkHN8IsYSNFXUGLErLDpoEt+OpSGRmN1Bc6g2MIocNIfqQKPIjZxktNC0O5qiOpqOqa2x+VUDDnCg9lLe62TSDfPLJg6cMI545EQ999aVvfuQTYEVBYsfGLK0uY57i8brQrKTrwJadwV9cRlzFKiNYPbTAHdEYlZ6rEuIXZTwaw3whglkTljqhVJoQFg0NUk93InGsh4mPRh2LqppyuClh03MxxCe1pkA91bKd+JHUh9YEw/8vMrTB34eBJ/vcvikRxlNK4n5aFiLfewvCj5rUZeKXv2Q9aoEKq9qKkCUBRkap8bf+uySNh/e9RIkQy689rNLJg+qeg9Ww9BqqqRZ63clk0EhsG7dvQgGqTEjm0oNlZEiLeXcmmogYOi+eV3r+g+6/taP3xw9iCx6N7fbH49Ulc0cNu7V2CvyH+W/fpR4cvPaE8fu2PrW7HvHLZv3z3/Pv/fg1A0+x4ia3uO6BnbfFn/NOSM7MHvIU0f0Pa4vL9u8bv8rT24aO37xrWMH3cb1nnvXmR/uBf4QwQYYCPvVzVyT9o8NSG+7KvcU6eGmck90K+ae2U1lieRmaCRPtACFqUNqR1Gv0dalZV1SdguUicFLqdcbq/bNO3as/p5lv34CBFvp9T2uufGV37XUsG+s/NWhJhqnYJmdwLzTNKfBtrIydaocQRUEokPizLDoNjXJI2lBz1kBHnxFT8majIwwrQu10Mbe2aNrl549u3TtYanXzK6r6d6jR23tubf4oecxt3Rhveyk1zQx2cwg8BTUXcECIdxhyYZX9WKwB3aFpIcrOuCKqAH0GriizY34s0Yatk5Gf7ikTY+JsczasFmDLr/6inr5w5t6ESEJkxx3Xn8D33zeJh/y6gaq0DHqGuXBGtlo1SiukUlZI7o+guL7KnoJ4wS25BowkoEeWjKWAw1hJVQk7tLry8aQyW/IA8knx+TFi7XO5rdq5/aZQhbI5S1rWO1t8jTFpoHrk9FwfaV+ilGvq0bNgA74SEfNxHqt86cz6t9p34U9HWDuUu1LwYfxIYA8piOU1aQCJC44YcX0bAE4EwvCP2BDb0LyualhYVez1UHMnLDKJs0RJB69YNiARgzA21GKMlIBg/4+bzAm1a0HGbDQ044N0/y4Kkcffn4mIdfoqw4uOPhS/Z13Pbqp/s4Fj63nh24eMe7g6Kmv/B5489jy5bGXWh7D19982HI0SRd+IODnTMWWsjKwk2xGRZoqVgPFC0wGLHnBXDJGKZwqMiYFGUZZOgwzpWFvvXkA2OiB2W++DZtnz5MA4LVjKXSwcw42AVSKzpoMMCEPJ+NKtqTqdRsVFgaIzBZa9+dQa4KQjTEuotYEYdgkVRMEGgnjSsC1bGZJ41xiOvslscrfnd34xS/37H766eeee+bpejZIAEP5ffm8/L188gHC/vr3f/zk5IlTTWhrgYyfT+nlR21E4zigRjNIlk99BCIWpWwtVyRJOGQII4AaUAkX421UoQIn6MwIdH42mAW80SZwilmQJGDK1HK7vCQDAZ24yqPv33DHH/6umFum7Q0PPfXUht2bWdmprd48boTcJP9TMbcmjpb78MJXb335znufHHyX2gaASy3ggnbOLUzaxEE00naOsa2dA56a6AUPx02zIjo7dUHQzjGi9QhmDmKkEyQNxUg1dMwWTaahU6tGQ9OGTm1N0tD5bKEhe3l9lnnuVx+c3bNtc/1WzWPPUUPHTCoaNv/0W/Lh1MHPkq7EcPxU7GTwy3c+V/mYCwEuNqZvMsKXWhETOoIpEYNrocgYlsYUGMlEg7Xo3mbyrBrDEcTVDn2fPbdc0dMzsHrFy/zQd2+dZ9pu/fiplrgaZ+LmwnWLmZuZmItWshhUTtUbk96F6AGz0EXNQqxasbvUqhWwqfdxeqsrpwjVs12IaW0ami3OcQFoHhpaZrR2rOVF+1aTLNttE15CdZyMQZWEKtiZfzgsPdfrmZV33V0+9aHXVn/x/h/uuWbfqGVrZz21bWk/rtfKR4euGjjssm59e9QMefj2bfWDtnepuO76/jf2rbv+NioTCi6cYXdrBoM+vUextyQTFuVQnKjNFdPQuicND/YU2l1UwbqaKEtgnsiVLJkUrZGYi5ZLucDgQnbhrAl0C1Hxci7V2zc5aMwDrUgrGGKKo+eo6UeiLrS/FPZAO7LQSwp2DJ5I+sqvThjZbWR2zoyu8qvcWyMG/U1e2rJoykyzbolZINewaxWZvwvkSB0/FPbqGGWnUndQ0qrblQa2XO0CW24arDbDhjWHqYTDEJdkxXw+eHUWjJ5r1VKpNkGvpG+3azdItifqV2cbovvmvf0WWcS+11IHdsGHbP/zBzZeO/qwGhNhAbYsjHllUf+f0OBLOujCZKViXg63J9rdjiGB2JkRFn3W/N/G5Imv8kNb7vpiYA25iq0E5U9jCtoAnDOX6JlYLo0w54CuwhM3EoNgL/bA2fOUsFc2DXuhrWEHeZSvhL1eZb9xJ8NeuUrYiztysA9zZqcS9sqpsIi+I2At/oRRrz7f/ONz/NwCJrhoPaIR7TbRceTgq9w3LhoMI7ZGlnCOsoN9Vp2dTD/R2hp1WuxhsNoabVaMkMEP0hGyGHyZ8Q5+AS/Mi3ofy2l1VlsyTkb6Z+kdvpzc1p+qATQgWy7KVeqhYxTFR3A719IIGhwgDTmQo1odF/DsnTlLr8+qsh+Lv97DqecDr+yRT7xxzFGt75J//Ag/VJ4ij7iybl8NO79lzZ75xZvZP54/wC7vdeKFm1qWIH+FYA31dA0zY2fk0mJnIbJeXv4y8ZPC38jLyfpD8nvyO2wV65HHk10tiZbj5JA8EK4BcpzXwjVcTBmTZBFkTLuRBtlpNZBkN9MgrMiADral4oUKtsiRtJ6pH2Fj31zj0Juv+zImlw7+zf3Dh9UOfP6KPsBF65puiv7A/vJ84UuPCitMRx5T4mrc7XBdQyrHptMnYjzKAE7fPq4mckLbqBo7uuU4+1VLjP3FbG700qXNB5cm68aPavYzucwURiljMyimqUsJrRHEC3jU24TZUTut9ovZvdR9c2N3TiTmpR6cFz24fLpPXNgK4oX9SQS6LUUDGIwmSoeavqRGAJHpAkHu1LnADnG6Pa4K/LCkz7J5H+//8vTp+bMP/+5e8t2D7PiJJPLonjXao/Kpj0Km0EfyR9PGs2vZhu0kPH0sQ8hZ2cYuojakj8k0H0Gf4EMxH9GDJ2cflG16/odmRQ4NBZyxhjgPcbYgzmZANo/G7VTUCfbGcE2iMSL5AGchEvNxiKUPcI5xPprJzAP0wWovQJzdlKl8tJVB0uuU3J5RoF6bgnJtTQm8hGoiKs6oypxa3dBZ8+6YffrzAyfvWnbPve8dPsCOn0ZK59ztBHRJ6Kh2zd5t8vuTbmQfHDtdPrHteVappWJH8KXcKJAvrzBY3MLYolG6bPpIo8tg1ZeJziiGIiXeFIlg0o+GQkCenHnp1Z+oPHFWiPoKNIB4+08YWLDZfzr4deK13lQq8LZGDQ9SoVGHzxhDF5w2GkOH5xh8lRFD19XF4GM8EuqYRo1OcCixdF6j09uE1rF0JbUIJjV4UkJSqaD74qG6kgaGQF1iVd/0qQ8PnD12wORw9/u6T3to6L1DrxxXWbOEHbHl9tyS3Jz+dZvv8BcWZl+OtXvyGrKan0D78gYx2JLHtWrJa1U632jSYquK2ZpotJuyLGWSxac24WEGTM+18u+w1ItEHVv71UUHL+zd2xadEeU+6je0/5BVQ859z/vPK/Xoh8He+ITWUoD1mc2o3URxt5CNURy3KQGmWTKXaMFybBvqWnSrPHaqnD3ZyEwep1pM4cnGykgBjQ4BLDXRiVtZYhyK2RysibrAyFDNDSWahr1UJexhsnbVlQ9VVa24fc/OJ5598KEZG1e8e5T9C2nuu2cX5/esjZ489cG7vTZ21779fiER1D4I9DU0x2AfzFZlSg4HaihCuxvRwo9rtEyWmbKTRq/4VMq20EWw8QvDKNl0W2TnYQ1KNt0cFB8OpUEBVnpifD8H3RCtEprKEmiLY7Smex8SqIkKGfYTBuex0Mnv6vfem2/+n8OHG/psX7doMamXx102jMt5MPz96SWkd+y9USuu+uVa+dSST6fe32078MAiouU9/JOwBiFmDoMR4gKANhgGB0gphfc2YecatqxhyVWWkMAKeCnXC36di3hy0JwrEhpZrY3WK2fZG3UGs5UW0BXY4TcWm9ONXwSFRoNZ+Tlvb2RYrV4xoWs9iEOtRwdco/PoQmj/6UK1JTXVtZ4M32DRhFXTly2ZtvqmlcOGLR+/etqSlVNXjV8xbFjtjnnzHn9yzrwd/MDxK664YsX4B2bftfwW+GoIHN9/6y/vufUvdz75xOw5O3ehLQhrhvURbmYeQ4PKydxN3CZYGFgrQ1Sy6VFKx50u+oEmKjnhA12EWoWWJtEeoR1aILnNFlwrMxYtaSMxi5k2CNjgnTNCLUKsoVTyPy41/+Og+R9cJyUSB8oF/xXEyAKyaK88kWjl10gf+bVd8uukNzwYzf6WUvajZnlJwxL5e2KCF1oDmSG7dUwhI2rCaqMqahl0zrT0Rc2OYK2Gl1A5/uCDxNv8A9/M6Zt/RJ9p0YUfuc3aPKaK6cP8iolV0swyoFsANi1GHvuGRWsTenwRZ0IqAps/YpN6AGpdnAmxPCJ2AYfJTR2mfvBhUUSJYuuEF3lTdkGwsjuWUYo97FJJGYj0LoKUm0Nzz5VK7rlA2EdcueXKr7KRz9tkn9mMYJ0SBKf7VvESdErcBtytAqJWcS+6+tWRKx+svG3uQysHjL562Kjjb0x4YlLN5FXD581btP3g5pHDtv3tgz8U/erZqtUbXxp4fd2mYPnwIRU9e5ZVj5tz9ejVwS6zrl+4Ji+0PJTTq3/vUbUDt9w67sHSazes3cYNumpGH8eIWdfdWeeYSulfo7Fw0zUnaa1IDdY8iy7aNRzl0UlQj5IFI1ov7XfEMINdKRjBRckIPQUzjklNTbCktrYkWEMW1wSDtbXBYI1mdnVFRXWkqiqivtL47NQLZ7QDQXZagau7M/cqETnJqVHzhbmaRLy6woS1aNX6RDzQhR4G0NqpVZxHK80KFQJYZU5aGotMq7cm6Apnlwn2F01OPjcQrKxWqmGrlWrYLsJ+vS27kCmpxHUL2MVgqiINq2BZPlBUzNY67Xw0UmxPlr5qMh09dzKQXjL1bTLhHXy8Ku/83XF552sz9pLyPXtJ1xf2yh+Je+WmF4jz01fuu6vnbSMWr1qyfPTiypsHv/o79hP6Z/LOd96Q6393nIx789fyyV/vJaVi8i9P/Lv3XdG923fvLP+lM/dbpW54MOdkZ2vegr1fgJ2ZJiSSDqiVR7D9kGd+AWvnBGWvSSQjBB6BNh3kKLVtVlsC61AFIJugRAg8qSxajhDTmZwYKaBbgJG8TgxAopeVh3FHSccpmTSTmkmr7t6PYExZbWoCZ0tpSYhGaBJt8PZn5m+aOr1szoplq3+RPX7tTbMW3Xls8p23cJ9Om6HlQyuiE3vMWCj3HjVpyrSp8Soe3H4WbNz5XAN3mNEyZlqR7jEQnfoykoxegBnbBTVkzHz5M1IwnzXlkC2D5J3yU0PIltShYh8RPdhHHkbDVDKqDZvsLuQNSk0aT83EGE+1F8+katKwgzAgTOeOrmYffKDlW7KC+Z/6+/hWPF4OXP5YR1zeTeHybrpErFsFQtStEIRwaSRe1J1+UdQhz5cDz1dExHKbFIF3QXgXbL0DIuWCfZ/JmcuXdVHUl9iViqlS3AvdBHscdgFDv+ouXNIeIIJf6UBxBLiMaqtL2ANziKb+0euvOrv7+KrbRkwa/YuZF90BLRu4RctWjFiUL88nk+XHyafX9b/yMqVu8pwWfDDtRiWnjeuhT+AjI6fNwSJoeZPWuWQJ8NUSbjx7FtYA45cDlI5bya3FSBpYJenopS0dvbRlRC+N7aKX1clGgTYN2Uv2LVu2f+SGUcv2zZ1/7Yi5s0deN5ffuGz/vuWj1o2KLx85Z/Z1o2bPAXimM4T2wHLAFWp+mDbBgvuSFcViBtEQifG0WIi3GNDkptEdHfKujapw0I4YEEmrcA74xZhS4Rjbgf0uKJ0pSltGujGWO9rcL9kcSx54gF3btj+2ih3GPgh2YTYznFFbILXUGXSr1MJOSEeqEzK7dSekL+kPte2EdNTUtu2E1FrBlqg6M3akf0S/6smDBizf+quNYzfuJQfYYdO/HD1pePeBg0oiY2Ytu3PElgeeWYCw+dk6CluAuY2h3fu4hGJRWOLgBVtRUPgV0/LZAqBQgU30Ya5ODR/5AGoz9d7MegA1iKByRYoy1wsSKaDOqphXJ9oEyYHunAdNcISdptDKSIeNkIrm829cdPKr+QtumTLi8gF39hjUb+2E+3eMHCge2L2Vnb1o4M71t14zsM/AssC48u4Lbh1xiye88z6lHjuj5qIL7Ufm0zUXigeTxdPabuA/mhpD5VuDnfCggnRkdeWswne67e3Pn/L2FS9zXXa4msZfZSc/kB8KuuK2zIy5JhlrM0RpULxtIk6vZBIyEubJlBysOK1cRRveZKeJEY0p+TFREtBK/M2WDr+hJYwFrGK9/PwT9ff7jLP+snmM0Yix8JYa+dzLH9AI3LPjRz0pH1F4b7bczE8Eu9DKDEZvTuJATuqplNab0cfQG4G3NRG6EYxNksGGQe8442N0oPyYMIY0tVjaaEuo7O+wdY8i2+M/1FCz//BXtnzOh3P7jvr4Xd5OePnHv2rFn0ZwUfs52o/NDieruXH/eT92Zlpwa98eStsXe3LAoEEDlIZswgRlJ4nDkZ0JK1kUQEJiNUqhKzAsMClt7zJrYJmxqtSQHAjjSDb61UZVZgsFy3tdc+01t4+5oza+bXO0XF6l79K9rEaIzQ2tmjTTQK83nB1GPqN1fNlM6zZpO22TRqiDqebo4US7boXW3X2VhtnfvIm9/MZx7vKbqN0xAOyOw2B3oK2YlJ8uDVZcSFZNuqpYSFcVCz/bEd7JPIsBu7dsfeb5zRueu+fG6TPGj59xy3h+1tP79u/c9eLBp+6B/xbddZdiB40AO2F30k6oNZBa4iJB5WUEKZA/m0/GyLuJO3V4iNxEJgySZ+TIM4akD2nIl7mSYTQn1Lk+XpAqDytePM26sTQrGrf6sgj4UY6oZNVi8ijms9JQkIDyuDhJUIxXo2efA549qGJ7yrN3gXjM9lAv2arIHNqejvsoW4iZfP46KmYkgaZnfFYl9g47isun1EqvENZ6BdXWZXD6g35wMC2c7srkwt3BrVrXvJDUk61rFi9eL58jWq0mOz8/K3NBz0/E+rV3b2Yvb3lv/jvvzH9t5ZWmUHm5gPZ4BdDiE5UWIWalUneKKimoTaSokZ+N1JDytYprnUYei1P8IF/9NNvgDwHaLj9NNViVlvMU2i4hbjLn5gepUR5UEc4W9vEuLifXj6NRSD5wi87YIf4dK5GKJBGGUCKwPTpQKu3pQDrUMsgXPobR+qmeCTERpjdzlolVITWKo7EgVq0mVU+8S7TYA9zRLSp10aI9Fot2QaSjXQ1l8SIOv5OKcLSMnh5S9dTn0tQTfCtVAUdVR6RKdG8iscoq/K6yAkhbVYmHVV2AtH1R3mHEOVQnVRUI9lgw2hN5qlKQupYgT0W7CGj0UU0XY3r2qqOCPEYKauva6bvgpeu7TtjR16kafEfhz+1t+bNT9djyVifMyjJReQE3ip/A5IHOXMiIOWEcTiGWhON+VVKXUqFkB4/frqo1O6q1RiGr2FIWz1d6RvPD8WJFiOMso3w70Edr1Sj0kSzg64tZduDiEpoEdOUAGb11YokQYwzZdUpFTl+2ti/pQ6JIFgvrUnJjHuoHp+OI0UD/sd1vvmz5lZMW9vH+6p4Rmyf3euPNwx+TcQNqo4MX9uobHDxj7MjwmHm/KIjes2nUzCuv6jF1dknlqokH+fr+Q/oPWTVI6T+k/Y66uYyOEUCi92nb8WhPdzx6sXdYMqq1JEYDyl47bTSx07asjBbIVP1mu2bIJ9R6ztI2TZG6CWp95/krk92RmbBZALaf68b0hhUTsbNuTC5l2rTvy+yp2jpt+zNJc9r2yYTFDfzRDhZPGpb8sBK+Jx5Bid6kyYJAUMpQmNrSZvmSaZ569+2LF4xZcf912jYE0pZ57r+vtOuKZZ7a5SurwdjO6CFVYAsDbAVMCXNnW9gKk7CJuWEpy4Ah1kZbVq4eXHUD2vu0+1kPwsOKwoM6K06r0vBcoKeV1KJD2MdmEU8hClIsAdTgshfCd8WZ+OFezmeRWaPox1SwtMu5LZoTzXNvrBh99dDCqkrrFPPsCWUjr7miKBK1VLTDeOHqYHmwV1/60rtPJs68inMQcMaqxxKse2yNtTOFtRAGCYuNKxiHyk+ha/HSyDmWa/i9CroWvRIdzxFiLMmmXd520YW4YilqdiaumQVbCoaZH6WxPZIMXFWq6D2Vil8l8XxNjV+dP6UgyG5KBbSSeGrpHkA8p7bGU8yPStng7ac3A1gPkh01QgijlFIOLGqOjQZmXDYFSxyZEWeJKdtOlWW2smekYoIpcFddB7unE+Oq/V56cuHUm3+5aNrNd0/vXVd92YBodEDbXcWyk+cuGD95wfzJPfr06QEPhr0gg15cCjaCFdbyRsWzEJlouknWQRtg4habCXG00JZZU6pl1olVlTgIItnBaqKVCSz1XtUmVhc1TAMk3TULD46ADc+yB7ljLYfYvJbP2YEt9hb5cxIih+9KddIqsLP7lP4up9rbXMrMUSqY44U8U8qXpRpdsbuexsq6ZnbFYW1tMRjiZZghAPt1n9NTUFgUws1ULDQarLyfroQT20EKinC8gGQoRL2qs9OWaOHnWqJJezu+sy5p0tDawO+4a7rF0dbsV/rCuB9B/uE8i2sv1sXsvkgXc6PZgDkzzJR52vUzp6v/MzqbW4aqiiO1MCmN0R6+If8rfB1CldQgmVD9Lqk7klBpy1JKIxMm38VplnMxmJCJaJ2kyUxziu2hS6mWTBCtmVolA85MdaLMIQBYg9QfS9dBdgItVtT4wYlRRGpOqizyZ1bcRFfc7qX1ZOj/53uVQkm7QRU7bVHqJBWQids1HWQFUhi+1j49AGuiASf2IVgTA/h53dVOtKzkaDsjrTdsZM1GgJX4sK6YtqdY0YFmaf8ijcamzBshKmhUxnysIcWRP36W7OdhmUZ46q1eL9Tx9ZTTGzHZr1YDp3iNgws0qjw2tiHNXD99qFyBvfAWPM2A81sYO57fSqMy9PwOKv+x8gf9fzv6ylgwQSOZrbgFsajL5JJ1Da3ZI3k1tDWAfmwPyifY5Xp5suKZpRXPojeV73GkMtQZiR6cW2G10ey4W2l87aD2OfO9JrnArzdYkyucWtifZiFYqbcKvcOgL4fS/lUP1jvScULmZA+uw5SIMySLU9LP2FfpprUMYhZtJDAr6WdzFvWWMPGcRWOfWRj7NCtw69RcldJerVMqcWqSAyJKieBQhpshF4e3kxi5lvwSp0TIixoOs5Nb3vqKLJJXsKs3s1vk1XIDjopgV8tGdgTbv6V5szqvoh/YcjbwQdarut6o6np9FBPQTjDaciONXmcQmLTQgIUkSc/E5qKBfKz46gJ6v4uiefLgME9pgDW6EtQrsSXliCgIIlsn5YI3JzkLlcRMVp1k1GG1pIavSzeSm9KN5Gxtqou8Y5tP6S23sEpveU0nBh/2nOeQYL90x3lbkw/4jfafUxnqYfzMqA470IsydS3GiHy+VD02NlhjJa4dx18ZrJwrr4Dq2kvoSE/v885700+ou/9iPerc+JS2aovTkIvj1AoRnATLGbLzLhWNtObqHA0yWRUyF8ODLU3pt9Z4lHSCR6gjPLpk4FF46cvRSmp1jsy6TFl2cYRaKUImvT5hilcYdWE7vMRuYawBFgORxsKcbrAPQwacyEPEykx0Uc2FYfOFlaFKxXBYnCZCFXYJhLE62OrKMYQumQwd7rjOqTG7k913ccK02Yu8SpekjRBmbu+IMv6wWB6N5yhaINQRSUDzi/k28DfKpK5w2LU1SYrzcdKx1cVdMkE6MRQ6J8mp9mbDRalxtJ01QZgVxMab+K1AD8ZRa6A5Zaz5XDGVTCKTJ8pPkgkT5cflXTeTCXz0ZnkHmTIF3j45hdwkPzWFTEzy2muaw5p/g+eTAzS9Xe02yG81NUIxrpxe2l5Ai/BV28mJklmw0y6v/VrObNN7ctGpMFJLRcrHVKNVqMMREvv0jN3oDihV5pJGV5ealog1RkhVpb7IYfPgAC4WswNA4BCl4okdk+6eeQTp+PWmm+++LRa/md0xc/c/W96dK/7zxzsU2k1YNWLTO/IjSL6pD16z/U1yq3xuO/fCFfJ7zSdGUkLasK6KziAAmSEAvkM6mkKQm1msTWMRPop3ciABFrQZnDjl2urBadgdTiVI+xBt5hMQV1Jgtx9UoB2Z0WOcCWevi8LZCjgcpGe1e9RBeh3AlpTHbWF7PCmE28OmKcjwK9KwFXYMm78j2IrSsPk6g62NjG0D4PFWgrUjINvLUgXWIMCaB17zTe2hRccnFI27FIHhjyRdZxV6FA3ZXjrvEdsqir1UhiZxQoca22kkrbUzanciHdrS3tGBTOgAwxfbOxWsMvsA1gPnLVa3nX7gSE0/cKrTD2Ks2U4Nq/YTENJxytQshPKUi5meicBlhCbVmS0L1ZktV2bMvskY1hLnXMqYKlPm3BaJNUdSk1t0yuQWkLlm6ukoXXJRnBPXeg5Ow0fES9zyCfmzA7v//M7hd36r2X+BOf7R52flo+TEul2PPEBzxRfO8I9pC5hKZoMKEQ7Jl3JBT5aHccYCEasoGJXuhFipaAOTi+oIXGeHO9Goc+TjDHjgg65hOpIJS1KKK2G1vdl1WDTUyBeFcKa72NUeN7pz/bTsJF+QDGCVSw4krgBUzmVo7E4sF2I6h1ft/1E5QWch7TtsaIMNSY9qLHQJ4h//9P5n68b0yBly9dpprzYeurXfY0M/uu6OJRMHDbmy//1L5DN8LPabHQ+vmjv4cn9h17roTTtufvL5QdtD4f1X3jp45KJR/WbW1I2rGXH96fPDYU/Q+QVaP51fUMFsuaQJBuHMCQbYWBQCsRjqZJhBZeYwAylUAkd5HjrWoFFn7tqNNiL9v5lrkLaRf37CQZYqdn9+0gF/W0ZMpy2d7v/P6XRx4gBJ9unMnmBphUKUmLNrGd2b/zthkjv55wmzW93eP08Yri4zF5GmSxcmitnlNF26taJLVZou1ZQupUCXUhveXKM9XWpwaDC2pRqMPlqPHBb2A2nyC4q7pBgGU3pFAaRPVbf/gj5tMyAXm4+xMlPhXPlzwzL4Qa3SI6MyZmeo9NJ8CvSqYnozr2fSq6YVvXom6SVWhJHFxGCksSivAqRQKbzxRWiaFcgYycaaX7EK06w+K23wr7Imy4BVyjaW2Hpg076VVo2XWDNJjdnViC+5P3tUCfb9Zk9eUamuRqmpU8dR9az5b2jcoX9wUVLf0ombkPPzVG+TosmkO6/S/STdv5VA+VcuYQfjbWJ6RuNdFHOgOkVydUej1VvmpXW56c3dWGsrBFJXwedVYanW25bUaVFYFlR6LquEmDlPV0cHokrVPeG1Nkn0/3njd2JwXEQ+tjc/LiIRTrS1RFR6a1ek6P2bS6O3VK1JiD3DUhe1jqA1tZGxq2z0hitlNoXwWR1Ij9Z0rlLpXCvEgc4VKEAke5lwEZ0j9ayGE5QF6/4bWmcmpn6e1uPaJakuopVsbVNWikzRYk9wKVh8vRiRiYUwUlcUlcpBUuRE6PQSDDLq4W1dJM5FQhY0v4DgNcDTvSmVu2ZT77dSqaT2ZuN3OKquO+3ZSdDxH12RpsYsWnoRs4T0yLNecPqK67DCNubMKVJLffA+C4xUHgJ6FtSJEaGRyc4vVpuKWVp7gK1jogXHAadKrdP09dAO9Dzit/uTjbihtuQOKgNeJvwoH5i34OGnq4e9NXnZM6U1z99x5K8tl+tJzxt3jBr9yM3y6cXXvrESyH3rmPW7Hz+4i93L6u67e+EaEn6qUa8QubTmhpvGTJT/9adb5fmB0KYSf2LJzIYtN419bvsUnf72s4/trmfU3nV1XlApdhNl5tuSQ4Pa5tssHebb+hs4ozUz5yb626TbjBnpNrXNsZMpQx2UzXU8eOjHNrm2DuYQtdzVJtPGKbN8QLdjVrsY+3F+fppPsM00Hxrv9dF4b3KwDxqLrjyMMwlZ2YVFyoDJ/3K2T9rc63zKjznpYXc87Yf9qpWN1xrfmf8pvq2Q1Av2OJcl5BQqOMbMFn/df4dl0nbrHMv6pK/eMZYknhEnTeNYBJw8vy2OIcAxoOAYMKRYGXEMUByLVRyRm/UoDpyuOmTi/YBrtjdfWVF7o9mSq8SEW6Mb+Bl02xakdDa/6eFMO+y6zoY5cW9k2mAtcWW0E+glij/YAUqlRoTZ8fOrjPGuyiiO5kcToCuIy2ibVVdrNvBGcioDNJZrXRiG9VKJUO5NSNXoI/nBnAUiYSwRm2i7VsJW15YDCf9L9u9Et3fOJpYOwgqd7IvhHeUrL/wBnOkptI7bjJMVlfwhUfOHyvQ8vAejUR12lEwlKplKrlWmskTdmHvrU9P0zkXVmULMc/AUUa/Tpe11jE0iE2l9fkP7ROVz6paYXp8uGz8fohdgL2Ae9Ho4f6s8JcnIU7ovKU95VSYnPgAXymQ59WIoVy6cgesV0F60VnlKcml5SncHeUruZ/KUzlSest7Rrj/t3FEEK/VWyVPOxvk3QA+8J8UN6sR0nDEEvgKnDp5TUie5llSTKd72B1MnRbko6IyCA+OfolugvVXBAiCcm6H6S51sRe924qktCWlC8FTrpqFeuwtH5DBpc6lwNtH98c9T9XXvyd9ekRg/YdBj084OP771Gfn8D3+SE7tf2PnCU/HH5BfIJ18S7UK+6ZVHb1vRt2r+FVc+cOuCdfKcv8hfyb8n5j8+++KxIxsbFNzovCHwt5xMN2ZtJxOHxFLaTCkWRhrzskth6xYbsA6aiOVtBhGJ3dDLclmx6EDqZk3OJoI/pOOJ/NaEVJExoqibIPLYTNCoy7YVU3F4sVlFXIcOU+sJRv078Y7aTTbiT7XJmiR1HK419goVpXKYracdBTKnHWGBZY6PFm4lBx+hEnDkYA7TxrnzlaTZpUw/ytTYHc1B6pvU1p3NQ+Ic7fR1GpchF8elFQJmehdLb/5/AH5aFXcE/pGkGu4MfPJ/MkKlreDHzu6xKvwFSfiDRqUEvfXGQ/hpdzejqg6xSNhnEOyeHE5RvpKLTlWje7ADjDqMe3QwnerZTOl2R6ejqrhJrRTt8OTkqhR+sPdM1AdcpOLXJYlfhREzdJKNpisbXTa/XonJmiPJeGzQkmg0B7FYB/OVQRs1O4AAjV5tGXyYDR/i8AerEo7NLlZYMrdLhdIO26UTCvxMVKIDQqzsZL/1/hmatN54aarwKk1OUp5FqtzeAddiwr0iGs9VtEKXFDlULsaqggIvrY/NCEEkeZv2ShYod2i1XTpvd2pNdMTpVR1YEp0xPXt3B7YEx0xnmvlSfjftEcxhqhhsD9CZkyNZsIjaYVaG1PNNeDMqo4/JBWK4I8lKnZrWZVlCxvF07mjL3l7dq3v2ilb3Sb6yDz7wgPzX3n369azr34/9Xj1Q+fQQP5lfpeY9ZybznskV8RrTeU9LOu9pSec9sb/DL+zTmK1cKumpbZf0NNqZ9knP5GAFXBIl6emkSU+t6tuFcAW2zxs1f9ISWITtd46aM2Hpnhpy4so5T9dvHDL76eeiCuWHLxi+YutPSPtB869evpUwz09jV9Y0tUjRT07umqroQjqfCuSNk8nHeqROJlQVZKa83Fng/vtok3X7YVXYpGugw3qtdl+uet/bSxlYlVYEbUZXPZcMzbcbYaWZnBT9rfG48dLw6AR4DvaJ1e7y/Qewp1OmbcdujU6Gz9sBz4db5cjS8Key7B3AX/zz8NM+K9QBKO5Fg7DPavf6Cih/makjYRKkvPy6S1qNVhqhDVr3ZOqBjlBrn2el+IGMczKFYHWt7ARDHLXSNRrPVqRcccrcUjFGZyrHmyoYp95UR3SoUKvIJbsWS5VDymSqS2XETsReGxrEOoiItqfEWx3lY+k8LVhrF1PQ0UStQjqn4JInamX0jnQ4W8uf7JVtP2OLP92KB+lcSTrDtoj5hZoJ98Hi0DG2BXQ2Fh05FUg20EqsJ0JbaHPVfHdxsnGWFn3kCjGtwUpDHAU+FAmm5PxuR3qmrVIlEwp41KmShYI6VLJeqx/QMPOjb75t+tM9Jl6/vYGEN+3YvRWIp2fkiupxY6+RP5D/hdLtqaKr+sk1OFeSiB+dOIClwgqNgd9cjJ8pp/fVaUVllARl0bhXYbRghN7+3Q1GlTdlVHXxKnd5R28mpuFdFI9LXZNOeKjjFfp9e1bqYLE0CzripcUXzmi/0rwH+6qAWazOzzRzCWVnZZvU+5jqObVeJ49LxA0mO73jO5cameECrF2KWY/2BLbY+5JTcuhO87mABjY7R2lgQhr46E10GYz06JU2IqVaJ6ODAYv/qTarFbAlYDEZT/pPG8dZWhrZUMsp9qoWZtp4+SX5iTghP9x5ww13/CRfYE+T6WSlRy2RdpLVZDrWOfn5+fnycUa9V7uugB8HUr4rWAjbGJo8EHOjki0LbKQI1c44DwgsSjEciZdZPIhrmSF1S/uC7JSVhLOpyiLYZIO3ssd7vkVVQ0nicAg1TmPGkW7d7DGDjU7bNyWzMDaP0okDP8GcvVhmjzEFwTo6MCczOttJ1JrJHFavUYI3s4n26+Xv9e155N5jZ1tCejJswlM3jNk6Xf5kcd8Tq0/L3+/d+uzT2x59pv4Rrowt/NV9S9aDkjeQbgvm3LlQ/uHPM+QFgdCmUOHX8xaQKqI/+cmHJz798OOZj+/alerB4hfpwG6mEnh06yoQvLVKfjRuUnYD1gn6aTOd1UvjDDrFjMTogyqBFa+j8ztldMb/qaqRox1VxqcLSPhN7XmdEBN/ittLYzRl9D7qhmhqbIAy+MxAW+KVGx/yjNpq16b3gJgylVfrAA1cQw/X+Ab8Ew/Tj8HBMpwB50o16jknOBdWA04eTTZ/e6zUBsIhBSar0vyd5VHvaYXjCjp0KYi+M1e9rWeeOYOFaTVhhfkfvhtJgIb0O0+7uS7pYS4jufH0T+jvdZfwe13y917uKBlI7w8SUrrm4zr1TvN6JYrlowOulLEOXOpWrlpdwOue3GPRgOAB7qNxxAQsrcDb6nzKzbHT52ObJF49H59uf1DvDOsdXEhvBwsnyPvYTW9bj/Pf5B/J7cxZ2AdBOneCU89mwEFc9GazRvVms6K+1Y1mU7eXzbivLGwfgI+NU/isOCeaU4rGVKRFS0QdsI544/7BplyLOmBdJQFGd3ma+DJmkiPY3RGFy6bIQib3uGdRfi9zij7uj4h+rnx6kmLHp+GwpeDIisb1ChzItEKSXiKvMK3Zp8wgVkmHU+r01FC0ZZIx2r0NPb/3DTDvt2RP7JEm7FRS/Qv3jfK/71FhkX9i40BjhGUAQxc+qhIa9tL/B0jknzqAhDC7yPfsBO7zzLkWBmWuhaHVXAujMtfC2OFci12zb5sxb+7MW+ewZxbe+8tFC5Yup/J064XPNXuZ79Ra46vw/JLLH42qUzMka04kkpqgUZwxQUON4cbtyjs7rTlWe7PpcJbW91UOdnKcussya08dOZJHyXsuD23zSmeqzmcL6BwJP4O31EHANKk7BiF7ahhlJDW9ZXyrOVQZ46fo3hwB58prfS6RjainS5+L4Llq286qOJQ5lQL2JfgHqzXvMiUYkQ0ka4yzkveYZ0gAhzJmR/FGPqIvEtdpA+qURp0eJ0/QcDRH780e89DZKR6fQR0yGeM8yfmMOEUIQ9QenKesp5MC8vGman406cTCOsmmazWyEbzSUE1toCZqZ9Rbd3Jg2ehcaOco+RfMuXL9trLs1l0hUvz3Y88MqV8qH72GyC3fsyw5Ll8WfLfhsW0bnv+7fLqCnFvatduIpURDiietGS3fvrSuf/nS+BsDNl+Gd5WjeyfI/kWzgu7jbGaWescngykKeGuwyTQ500sLWOktjgjOOlWGE2mETu67JmoUsZNto1WqmCGxCEqTvDUbfF2jye5S65RBkuPIGCJEc+itImsDHPzzCEG24OQisv43r78bX6LVn5x+TK/VDJ85s2Ubews8TrScYMMtYbJrdctJNrxUHpm8n9oQfgjekSRjFro6rhanh+n4MvVF1SNVgl/APzn/Eu6vPPj7XHq/lUKmGDNyOANc9EeVwoWiaOd3XgF8xWyMyeeblYTsxe/CgrsQzFwcmVgMXk2OT0nY+u2CEpURiwUpm/rNRryFtt6Ft9CWNNlqHWqH92shbdIxbe/fwi5I30Oj/b1cSEObu2ogbyBNTJQm2WjrU4q4ohe9EY33UkmAxl22W2ENVxL3zLvTxHRudaR1hxh3hOWKDpBrrlPubIG5V8BJe5LiVMrUMHXMGXXae6h7VMUsUBulqx1jXEHgdxVF0R2hWMY8CqpiQZQuvi+C61+O6x81J0SjTdTDsZgXxboiMRe2S89LJQfICRyaH3JTxyJER5yEgvCzLiE87OKCn4VoNYzYNSLVAeEqgXF6oeuYJJ7UPQRHPnQm6gSpPIrk1EfhI8z0dkJG/3/DTeRMW8Lf+3PsdTF2o/tP+5q6LuXMb5Or0lVdFTHwn61J20Wo+H+8CNg0WO5WHPY09bsmqc/Aeyzd6vYfUv2SKL32EgissPz/BVVRAVkAAAB42mNgZGBgYJSc5Zv3aVI8v81XBnkOBhC40Ou+Dkb/T/gnwL6OvRjI5WBgAokCAHVxDQUAeNpjYGRg4Ej6uxZIMvxP+L+CfR0DUAQFvAQAkd4GwwB42m2TT2gTQRTGv515++fQg0ih1BJKKKWHgiWEHEoIASlV0EMJ0pMEKUFioIQQikhYPJQceiyF0lMPpYjeFgqiUnqREnIQCSUIoicPRShFRDyE4vrNmEgsXfjxzbzZNzvvfbPqDAvgI6OAMkxhR2fQdGeRli2se9soux9Rc47RVEUUSE4qWOZa2fmFvNrCfZXEjvqOUcYekUNSIkUyS5pkrT8vk4p9P4l8f/7EqK5iwk/hqXsNcOfQdkcQul20pU6SnJ9wfoq2ypKp+KF8Y3wGbX8ebS8gWYTS6esPrpVQkVVcZ95beQf4ZUzILgJpsNZN1rGH5zzzGDUty0jp7fhCdp0Nfq8op4j0B9SpdQlRV6+QkBXM8JuR8rCnvHhT0nYc+TVEJi5d+35kcvQC8zus8wSTXNsXBXjzGJMU9wig9BEKOmAfy8459Y6pf9B7jo+I6U2DTJp3WH+DZ8t4L1BSXSzqHgo2h703MUHc06t4ZmMtpEjS1vITkZtDzfTb6WCa8XsauMX8JS+Hu+QmucHep23fr8C7iH8bL6wPQ9AHlxyobNwyY7eFuYEPlzF3wKjxYhjrxVfu12PfTN+vwPuCovUi/B968In9f0k9IGdyjNo/Hy5j7plR48Uw9MJ6RvVXEPob3MOc6dAZIYF+w3tTBwaquOZ8Jrm/4JwaUh9zzfwHfQQoBMR5gHGL+U/eY9ygc0Rh31ukJ8xVVd7FKpbMvvIaeS+DabltzsA7R/w1JJD4A5Gu2UsAeNpjYGDQgcIKhlWMfUxsTAuYTZiDmGuYlzFfYBFisWKJYGljWcDygVWGNY/1DpsB2yx2LnY99lUcUhwRHNM4VnGc4XjAGcT5jUuPaxrXPW4F7hjuSTwMPBY8YTxtPLt4HvAa8dbxXuIT4SvjO8Nvwp/Cv4j/jECCwCSBAwLvBGUEzQSzBDsEFwieEHISWiL0SjhAeJMIm8ghUT5RD9EpoldEP4gliK0S+yEeJ35EgkEiS+KcpIykm+Q/KQOpHKkZUt+khaSLpH9I/5CxkJkm80u2QPaK7A+5KXLX5PPk7ykwKWgpeCh8UuRSDFJcpPhBKUqpRemBsoKyn3Kb8irlTyoSKvNUVVS/qOWp7VFnUg9QP6Vhp7FBk0WzSfOSloxWkdYzbTltP+027Ws6Zjr7dDV0e3R/6LXo8+jr6S/T/2XgYTDPUMAwwHCWkYbRHmMP4wcmc0xDzLjM9pibma+zULBYYGlnWWS5yPKWlZbVDGsF6zbrZzZhNg9sU2xX2cnZNdk9sg+yv+Gg4tDl8M+xzEnIqcRpmdMbHPCXM4ezmLOes5tznvM0530uTC4eLj0uu1zFgNDGNQkIn7l5uWW5fXGP89DyuAYAhxOWagABAAAA6gBCAAUAAAAAAAIAAQACABYAAAEAAVEAAAAAeNqdU00vA1EUPdMpKmjEQkQsJlZd6BhFIo1IfJRIhESFjc2YVpV+MJ2i1n6B32DjP1gLGysbez/AT3DenddSrY1M7uu5951z3733vQIYxhNMGNF+AD4txAZG6IU4gjhuNTaxiDuNo5jEq8Y9GMOnxr0YNmIa9+HeGNM4hoTxqPEAFowPjQdxGJnQeIj4WuM49iJvGj9j1Exo/ALHXArxu4lxs4I1FFGgBbQb5JGDRXPpu0QeqjhHg/0o1gmjFh5oKTiYoSU1msEUoxtkV8krMY+FVWKfarW6kr+KCmzsMJYnspBlvIIadukXUKfOJXeZEU8YOa4+eUlap8rCCjVFqlTNqhqnK6s9+77krOlqlM4WbVPZ1HXLVJRVzSWQnlR9Zcl6xlgVxx0zcKULS1gN/h5J1JeKVLZAqgmnXpTTPImo6Yf+KSv3hZvj6rXmWGPdnZPqPnN1bwGjaUzzu5LP5n672tNaW1CZzP/qAvZ6Ll3lZdIFcsOp25KzzOlsSTd56STsv/6jj4A8Nall5nHJC712jXpxv28zxROcP+v+zmVLzQXultpy1hjZwibnmME2bz4jL1zlPODuEW9YnRPod+PI21DeNuOXEp2XdZZnq1eVpj/X+r/MYb2lz+JCXq8v91/6AoaLrkd42m3QR0xUcRDH8e/AsgtL7x3svex7u49i3wXW3nsXhS2KgIurYkNjr9GYeNPYLmrsNRr1oMbeYol68GyPB/WqC+/vzbl8MpPMZPIjirb648PH/+ozSJREE42FGKzYiCUOO/EkkEgSyaSQShrpZJBJFtnkkEse+RRQSBHFtKM9HehIJzrTha50ozs96EkvetOHvjjQ0HHiwqCEUsoopx/9GcBABjGYIbjxUEElVXgZyjCGM4KRjGI0YxjLOMYzgYlMYjJTmMo0pjODmcxiNnOYyzyqxcJRNrKJG+znI5vZzQ4OcJxjEsN23rOBfWIVG7sklq3c5oPEcZAT/OInvznCKR5wj9PMZwF7qOERtdznIc94zBOe8imS3kue84Iz+PnBXt7witcE+MI3trGQIItYTB31HKKBJTQSookwS1nG8kjKK1hJM6tYw2qucpgW1rKO9XzlO9c4yzmu85Z3Ypd4SZBESZJkSZFUSZN0yZBMyZJsznOBy1zhDhe5xF22cFJyuMktyZU8dkq+FEihFEmx1V/X3BjQbOH6oMPhqDR1O5Sq9+hKp9JQlreqRxaVmlJXOpUupaEsUZYqy5T/7rlNNXVX0+y+oD8cqq2pbgqYI91rangtVeFQQ1tjeCta9XrMPyLqSqfS9RcJap7iAAAAeNpFzjtuwlAQBVA/HhjzCTH+8JOiGMq8ho4qHaaAAkRlC1ZBQZs0FBSwgWxiTIVYAD0VyyE3MBm6OVdXunNUtw2pnTUhZ5ZkSu3TbGSbpEtuOqFgjuM7fSPbLBOLdBSTNkOqRvFBX3LmjgpQ/WGUgUrCKAHlMcMBSp+MIuD0GTZQ7DEKgD1l5IFCyHj5G/UfUFTjVxpRfLJqSls5k+nRGkmIXmMhDMBwIPTBwAg90H8X1kHvKnTB+lb4CrqesHmf/ro9p1soNM/CNthaCTtg++OfKQXmF/+9a+wAAAABVGeXLgAA);" +
        " font-weight: normal;" +
        " font-style: normal;" +
        "}" +

        ".chagas-loading-anim { position: absolute; left: 50%; top: 50%; margin: -25px 0 -25px 0; border-bottom: 6px solid rgba(0, 0, 0, .1); border-left: 6px solid rgba(0, 0, 0, .1); border-right: 6px solid rgba(0, 0, 0, .1); border-top: 6px solid rgba(0, 0, 0, .4); border-radius: 100%; height: 50px; width: 50px; animation: rot .6s infinite linear; } " +

        "@keyframes rot" +
        "{" +
        "	from {transform: rotate(0deg);} " +
        "	to {transform: rotate(359deg);} " +
        "}" +

        "";

    document.head.appendChild(style);

})();
// CSS //

// queryString
(function ()
{
    'use strict';
    var queryString = {};

    queryString.parse = function (str)
    {
        if (typeof str !== 'string')
        {
            return {};
        }

        str = str.trim().replace(/^\?/, '');

        if (!str)
        {
            return {};
        }

        return str.trim().split('&').reduce(function (ret, param)
        {
            var parts = param.replace(/\+/g, ' ').split('=');
            var key = parts[0];
            var val = parts[1];

            key = decodeURIComponent(key);
            // missing `=` should be `null`:
            // http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
            val = val === undefined ? null : decodeURIComponent(val);

            if (!ret.hasOwnProperty(key))
            {
                ret[key] = val;
            } else if (Array.isArray(ret[key]))
            {
                ret[key].push(val);
            } else
            {
                ret[key] = [ret[key], val];
            }

            return ret;
        }, {});
    };

    queryString.stringify = function (obj)
    {
        var ret = "";
        for (var x = 0; x < Object.keys(obj).length; x++)
        {
            var key = Object.keys(obj)[x];
            var val = obj[key];
            if (key && key.length > 0 &&
               val && (val > 0 || val.length > 0))
            {
                ret += encodeURIComponent(key) + '=' + encodeURIComponent(val) + '&';
            }
        }
        if (ret.length > 0)
        {
            ret = ret.substring(0, ret.length - 1);
        }
        return ret;
    };

    queryString.set = function (key, new_value)
    {
        var params = queryString.parse(location.search);
        params[key] = new_value;
        var new_params_string = queryString.stringify(params);
        try
        {
            history.replaceState({}, "", window.location.pathname + (new_params_string.length > 0 ? '?' + new_params_string : ""));
        }
        catch (e)
        {
            //location.search = new_params_string;
        }
    };

    queryString.push = function (key, new_value)
    {
        var params = queryString.parse(location.search);
        params[key] = new_value;
        var new_params_string = queryString.stringify(params);
        try
        {
            history.pushState({}, "", window.location.pathname + (new_params_string.length > 0 ? '?' + new_params_string : ""));
        }
        catch (e)
        {

        }
    };

    queryString.value = function (key)
    {
        var params = queryString.parse(location.search);
        return params[key];
    };

    if (typeof module !== 'undefined' && module.exports)
    {
        module.exports = queryString;
    }
    else
    {
        window.queryString = queryString;
    }
})();
// queryString //

// MiD
(function ()
{
    var MiD = {};
    MiD.port = "5000";
    MiD.version = "1.4.0.0";
    MiD.formato =
    {
        jpg: 'jpg',
        pdf: 'pdf'
    };

    MiD.scan = function (formato, retorno)
    {
        MiD.callScan("scan", { formato: formato }, retorno);
    };

    MiD.listaScanners = function (retorno)
    {
        MiD.callScan("list", null, retorno);
    };

    MiD.gerarPDF = function (files, retorno)
    {
        var imgs = new Array();
        for (var x = 0; x < files.length; x++)
        {
            var ext = ChagasFile.fileExtension(files[x]);
            if (ext == ".jpg" ||
               ext == ".png")
            {
                if (files[x].nomeTemp && files[x].nomeTemp.length > 0)
                {
                    imgs.push(files[x].nomeTemp);
                }
            }
        }
        MiD.callScan("gerarpdf", { files: imgs }, retorno);
    };

    MiD.deleteArquivosTemp = function (files, retorno)
    {
        var imgs = new Array();
        for (var x = 0; x < files.length; x++)
        {
            var ext = ChagasFile.fileExtension(files[x]);
            if (ext == ".jpg" ||
                ext == ".png")
            {
                if (files[x].nomeTemp && files[x].nomeTemp.length > 0)
                {
                    imgs.push(files[x].nomeTemp);
                }
            }
        }
        MiD.callScan("deletearquivostemp", { files: imgs }, retorno);
    };

    MiD.getArquivosTemp = function (retorno)
    {
        MiD.callScan("arquivostemp", null, retorno);
    };

    MiD.callScan = function (func, data, retorno)
    {
        var that = this;
        this.test(function (ok)
        {
            if (ok)
            {
                that.versionOK(function (ok)
                {
                    if (ok)
                    {
                        $.ajax({
                            type: "GET,POST",
                            dataType: "jsonp",
                            url: "http://localhost:" + MiD.port + "/" + func,
                            data: data,
                            cache: false,
                            success: function (result)
                            {
                                if (retorno)
                                {
                                    retorno(result);
                                }
                            },
                            error: function (erro, a, b)
                            {
                                if (retorno)
                                {
                                    var msg = "MiD.exe não esta em execução";
                                    retorno({ erro: true, mensagem: msg, naoInstalado: true });
                                }
                            }
                        });
                    }
                    else
                    {
                        if (retorno)
                        {
                            var msg = "Atualize o MiD para a versão " + MiD.version;
                            retorno({ erro: true, mensagem: msg, atualizar: true });
                        }
                    }
                });
            }
            else
            {
                if (retorno)
                {
                    var msg = "MiD.exe não esta em execução";
                    retorno({ erro: true, mensagem: msg, naoInstalado: true });
                }
            }
        });
    };

    MiD.test = function (retorno)
    {
        var ok = false;
        $.ajax({
            type: "GET",
            dataType: "jsonp",
            url: "http://localhost:" + MiD.port + "/test",
            cache: false,
            success: function (result)
            {
                ok = true;
            },
            error: function (erro, a, b)
            {
                ok = false;
            }
        });
        setTimeout(function ()
        {
            if (retorno)
            {
                retorno(ok);
            }
        }, 1500);
    };

    MiD.versionOK = function (retorno)
    {
        var ok = false;
        $.ajax({
            type: "GET",
            dataType: "jsonp",
            url: "http://localhost:" + MiD.port + "/version",
            cache: false,
            success: function (result)
            {
                ok = !result.erro && result.mensagem == MiD.version;
            },
            error: function (erro, a, b)
            {
                ok = false;
            }
        });
        setTimeout(function ()
        {
            if (retorno)
            {
                retorno(ok);
            }
        }, 1500);
    };

    window.MiD = MiD;

})();
// MiD //

// RESOURCES
(function ()
{
    logoImpalce = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVwAAAB2CAMAAACziNv6AAAAM1BMVEUAAAAQMGAQOGgQOmoUPGwTPWkTOmoUO2sUPGwUPWsVO2sUPGoUPGsUPGsVPGsUPGsVPWw/9w4DAAAAEHRSTlMAECAwQFBgcICPn6+/z9/vIxqCigAACLFJREFUeF7tndmCsjgQRisLIWT93v9pZ9qgBa0oxKC2P+dOIFWp02kMq/SH0bYgqTkHFgVNTTk45HZ2hn6HXGML9GWojCnDW0ZuQIG+CzFzmw2145AbwSAqasch14GBF9SOQ24PBj015JDbgcmaDrk7TRSioIYcckXabQZ2yA27zcAOuW63Gdgh1+CME9SWQ66+cJy4Oc6KHRxylznkSmN9uMJbo2gBFVYw9LfrN6FAazjHupYbwxxDE4Z7ObR14Q6zrq7gjiftEpbxvbzZCOvIvlseerQGFMK0+W3sutmCdhn3IcZiHcnd8msSHuF1hVwmmU+SKwOAhnKZoLekYryskMtE9TFyTUZbucsnWHXGOnJXL5ebv1+uBXaUiyi5pcJ6TI1cpvsIuQb7ykUWfLELG9BPyc3qA+Qq7C0XkUZ8xR+lVi7iB8gN+8uFvfITrRZ0jeyGNG22FdHxrAemkdz6EzdcMEKvqB3SeFyQ8xOEUdMyJk+G7nbEgJH0drl+UnBrZMSIO5W98nKMiM9dbjQY6d4rd15wexwKmS8lrhiQMnN9NXgU3JvlGi54D0QCpxxW70ott6pBopDeK5cL3vtaAWeHpEcIjOjnhq54r9zA3diHidzM4+kR4amdLst4r1wU4u6XZzmX32DHUhUaBfMRcj3tBZclNggzKAzPybUfIdfuL5cr3mAnUBVqP7mHXHpC7iFXanuXb5GrOnuP0F6udgkP+Aq5xmc8oLVcHcB8sVyTALxWrnDAvyBXReDVckXEPyFXZbxULrv9frkq4/VyI/4JuTLj9XIt/g25Ea+XK8F4q+W3HkQYXMiu1+IlBxEOZ6z45iO0dPUkwv5yxfSi+RfL7SYXh14m17Dbr5br2O3r5Hpu/9VyMyd7ndyEgvhuuQqFSK+Uy4u+WS5reqFcbv7dcjnXn5R7yD3kiv3kHnLpb8g95B5yD7mH3EPuIfeQe8g95KrXyVVLcvU75A5VcuWGbTmX21+ufkauai037H/4y7mekzvU3J9reMHqxr6V3Fwlt9smN6Igtsituk+7/y1Db/ivGVCwT8sN6z3VDSLPKR0K5rmT5arimQjC+j9sYiVPyeWCHW1AbhjvmVP2bKxGrlvdXFwP8oiCXz3qIZo9KgVdc/HXbnqaR2Kkr5LbYW1Wj8JwQ5lZ+XWGSE/LFVWPgg4YUVueQ6PIeirkUlr5hjZzo3cSZwaxatzCUMvHU+X2/QJyf9+tQyHPq0bq5Va58+bLhiRXdPuOkmyXcguTeCvRQK6eJBUV76cM5o6i30M1gUnBWtvp29zePUcwcbD6CmOHuPA6CwkGyVtr9JTOWhfBwFADuRTARLdcr1i8Dy/Y/oYe666HgcZG5nJVrm9Ltj5xvVyZa97b0dfdbTg8JZfM9ldhML66cb1cMjVyydUpchVyq+xmRb8QsbpxvVwyK+XW2o2Cau0GqrTLeipyR0nN5JLJFXLJbXHLdHmL3MonMQLr2VwoBkEN5ZIKFXKpq+uqsLlCLmPS1jfiMcI9zO1V85cTm7RG7nZNQdM1wri4XS6jhjvN46DoHt0Q8mJbZ+Qub35WdjnpcuDOJSyRw71DBaHv8Pg0hNQ3kbQKpW9AD5OJNTE3JX0YWG1s0P7B6oPvl3vIPehRcNScA37+oTUHIu8m98BixFBjDhTOaGrLgc44Q005kA4XIq3jQOjH9EMEg2HpeJfq0OoFVcrbB7G0KxqbUUuXaagOBNobDXutNrXaw6mUnQxDC7nh6pf6HYL9YS+5xsv2ckVGtJZaMBQvDeRmudj1veQm2PZyOwRqRIANQPe83KTo5XJtUO3lWvS0C/VyB3G36yZMfv5HuHSZF2uf8YPvSmWxbGdHubxx7KaB4iBPHjT3MzlJjLCRl45BvOoSgKDnPeROhO7ybUHa44fsFZ0DENkxTBw7NM1+EqDGSCAKADjqpI6NcpOV98fFACCFMO48AhDDD4o6AOF/xptCXFmTADfKFbEsykBfAuUQQgSyLHJVGJnvmAJO7QAkQXQKAmQghQzoeQ/12Il86oQ95euAzFFLABpO7UuYBBiaZRflRED5zL8JloB+Vsd6uTnwtZZFuRK51CMCelKIkh30dKJHJFJIZY3KkEVuDy/GWJmILoE8hiKXcTwX5EgywlCHKE7JoYjIwM976M9jwCCdgyaYkugU1ZwCyBLU4bROI9Ese0/Uw4lrCzlP6jAAkbQP6LTWat0eTV/KVvCznRoyjSQQWRgqWJgi11/8BSjScFSQCL/lCgQ6019ydLBk0U2/qlKe9zCARiLOQRHniboSqiw5tedmvF2ApGsc9Kn3xM0awHLtWGlAmClhHwFTWRa2rAzgDTQHImK5orMFDsaRTIQ9f9IIY6QludwJhKtEpVO8LpTshrP/EqdsIZzk0q5yRZ+AZFrLVQkjV3KFTUDSe8pVGSNXch3O7C5XDrncZNlabkQw+sQvudKNvzK4p9wE33H2qTiD3Jc1bme53gNwiqi5XIk4XcTtA5CdJNpVrubtfssNXMy+cvl+4tZyedFcLt9zvbNcuyyXXiQ3GSq8TG7gjN8u1xJj0W2RK+rkWnqLXPEGuZEYA7cgt8fAU8Oy0qGjQoLkHSzXrGYzUs7hn5SbqKBX7nMVPJGDubHPDZDkuY62cqffqVqRyHDlk/wlVyJb/cOALEoR3fid23mEEqhMDbjmMM4WTOTKxhwFWSPXjz02CebRbIGza+ShJBU8W3BIRIbraC13Phc1fCl+LpfXZHMuYpidduNp5Y15riOGl9YdRIjIUdfNcx1Ne6t5nousZnVQG6TVfPJ8xEyOajRZQyOmHFr2ZSNBRGTNZFEniH4fEOlywqg7R2PK0oI+byet4VTcw8lnY8eg3F4RnZfpMcUljJx0SI0BB0Rr7WmVslzMtI6PRyLQRxKAqOhPI9ynyvUhI9FfRIVC/NjbeySRA/70vapI3tBHEnIAAu3CwZvvPDr4D1LzbsjLqRlXAAAAAElFTkSuQmCC";
    homeButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAACZklEQVRoge2YwUsUURzHP9u6pR08GEaiYNFB8OBFKEgPQnTykIh48dKtLl4C8V8o6tKlTnXpkHiogwcpiKU8mYcuoUGlhyikIIqKsszX4X3FQWZ232xvZnZiPvBgefN739/3O7M7+2ag4P+mQyOX9AAvgTWgO2MvsTkJbABGY0NzueAEsI41vgw80+c3wPHsbLkxAGxiDT8EDms80tymanapsneVokZqnAY+qekD4GDg2CHNGdWc0vwSTRJgCPishvNAJaSmomNGtWfqaKYW4BzwXc1uA+UatWXgjmq/aW0UqQQYA36q0Q2g5LCmpFqjtecj6hIPMAFsqck13MzvUgKua+2WtPaTaIALwLYazP6Dzqw0tqUZJLEA08COxowHvZmA3nRgPpEAl9XoD3DRo+4lae6oB9hb7BOPPbiCPSO/gSmfwmJK2ka9vHJVwr+ASd/iASbVw1uIA8AtCf4ARn2I1mFUvQxwUx4aogW4K6GvwIgHc66MqKeRh5a4AhVgTgJfgGGP5lwZVm8D3CN8exJKK7CghR+BwSTcOTIoD0aeWustaAMWteAd0J+kO0f6gfdYT4tYj6G0A09V+BboS8OdI31YTwb739C+v+AIsKKCV0Bvmu4c6QVeYz2uYD0D0Ak814EXwLEs3DnShfVosJ47Ye/Mr5KPtwfd2DcdBlguYb9TZWAc+BAorGL3JmfTdujAUeA+1l8kqT9UR1AFHjeysFkC1PTR8D6jWSgCZE3uA8TeptYg7g8+zluMSIorEEK9M+v11pz7K1AEyJoiQNYUAbKmCJA1uQ/gspVI6qnMi27ur8BfOfqtWFdCHWsAAAAASUVORK5CYII=";
    logoutButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAgVBMVEX///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACBrqoyAAAAKnRSTlMAAQgJCgsNExwhJyg0ODxBS2lsbnF4fIOZpq28v8HJ0tXa5+js8fP4+v0De0SVAAABaklEQVR4Xu2UX4+qMBTEi0oRXEX5u4igFEHs9/+Ad+IDzWVqb0xusi/7e5tJJzk9PT3ipzi07eGjQKt1+1FAg/8R+A2sT99f9sDX92ktCO+s9X1lC6zuWp89ChQa7GyBnQaFWHB8wr3YS7pAPI/iL/weptraA1sF1ftU0GP/rkv7x7KoYICTv29rDjkEwlDCUJv3gY2CLoXhBp26Hi6FvhkZQk7SFZATjHCWCVTtHo0aRjKrCipzBzIY1awaqNgdiGE0s+qgIvcSiHCkm9UIJd1rRuLISAGCAlQSQSXRpQm6tGkrQW2lhyPo4Wg0CBoNGj4DDR+NN0HjTR+I4Q/EX9TAX5SXgIGWAK+ZqzWxvc5rhhaZslS1VxoUtlUJHvmiV5sc9VtXpfCRACqVxpOpeplnX1jwCtwDTHUWR1JGcVZPL+NZeMLOsdcWenNfwi8GvWAofOEiKDFXhlsZiH8SJlXTjWPXVEkofoA/k6JIZaeujEEAAAAASUVORK5CYII=";
    iconeImpalce = "data:text/plain;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAgICD/ICAg/yAgIP8gICD/ICAg/yAgIP8gICD/ICAg/yAgIP8gICD/ICAg/yAgIP8gICD/ICAg/yAgIP8gICD/ICAg/yAgIOYgICCqICAgqiAgIKogICCqICAgqiAgIKogICCqICAgqiAgIKogICCqICAgqiAgIKogICDmICAg/yAgIP8gICC0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAICAgtCAgIP8gICD/ICAgtAAAAAAAAAAAAAAAAAAAAAAgICA5ICAghyAgIK8gICCdICAgWiAgIBUAAAAAAAAAACAgILQgICD/ICAg/yAgILQAAAAAAAAAACAgIAEgICBpICAg7iAgIP8gICDzICAg/yAgIP8gICB8AAAAAAAAAAAgICC0ICAg/yAgIP8gICC0AAAAAAAAAAAgICBKICAg+CAgIOwgICB2ICAgNCAgIFogICCjICAgPAAAAAAAAAAAICAgtCAgIP8gICD/ICAgtAAAAAAAAAAAICAgtSAgIP8gICBhAAAAAAAAAAAAAAAAICAgAwAAAAAAAAAAAAAAACAgILQgICD/ICAg/yAgILQAAAAAAAAAACAgIOUgICDUICAgGwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgICC0ICAg/yAgIP8gICC0AAAAAAAAAAAgICDgICAg2iAgICAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAICAgtCAgIP8gICD/ICAgtAAAAAAAAAAAICAgpCAgIP8gICB7ICAgAQAAAAAAAAAAICAgDiAgIAIAAAAAAAAAACAgILQgICD/ICAg/yAgILQAAAAAAAAAACAgIDggICDtICAg/iAgIJggICBXICAgeiAgIMMgICBSAAAAAAAAAAAgICC0ICAg/yAgIP8gICC0AAAAAAAAAAAAAAAAICAgSyAgINQgICD/ICAg/yAgIP8gICD3ICAgbwAAAAAAAAAAICAgtCAgIP8gICD/ICAgtAAAAAAAAAAAAAAAAAAAAAAgICAhICAgZSAgIIYgICB2ICAgOyAgIAkAAAAAAAAAACAgILQgICD/ICAg/yAgILQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgICC0ICAg/yAgIP8gICDmICAgqiAgIKogICCqICAgqiAgIKogICCqICAgqiAgIKogICCqICAgqiAgIKogICCqICAg5iAgIP8gICD/ICAg/yAgIP8gICD/ICAg/yAgIP8gICD/ICAg/yAgIP8gICD/ICAg/yAgIP8gICD/ICAg/yAgIP8gICD/AAAAAAAAAAA//AAAPjwAADwcAAA53AAAM/wAADP8AAAz/AAAM/wAADjcAAA8HAAAP3wAAD/8AAAAAAAAAAAAAA==";

    novoButtomImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAABAklEQVRYw+3YK0tEQRgG4GcRDIKo3bg/wN/grXgLNgWrVdBi0y4aDSYFbRbBoKxNq9j8BXZFMMjKsYywRc4R91wWvjfNDDPfPAxTZlpZlmlyWgEM4IAAbzH3x9pdHGC3CuB/jnkP+1UBp/BeYP4yjnr62zisAjiOtwLz13COD4yk9Zs4aRrwGs/YwRc2cNE04BKO0wl2sYqrJgEXMYRTrOMTC+g0CSghL7GS7uY8HuoG3mOrZ3wYZ2inOtN4rBOYlxdM1gFs4wYTv+2bav20KwfmZQyvAQxgAAMYwAAGMIABHDhg0Yd7XkbxVAawlP+hfgA7mCkBd4fZfgBrSwADWHe+AcmL5rGDravcAAAAAElFTkSuQmCC";
    excluirButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAABNklEQVRYw+3YzQ6DIAwHcC/wfNO946ZRtufzxCBjiYfKR/vvZjKb9Ib1p0GKdN777sjZncB/Bg4hx5BWERBrTyH7VmDErf4dDyVkrPlM91j3kNSF1w3OKyFtqrkNEkldPHo6UEgK94lbDTBXQIrM1V6o2pxCXGQzrvQVI5FxrGvF1ayDCCQbV7tQS5AiXEsn4SDFuNZW14KE4Di9uAZpE0KM424WSkgYTrKbySGpcNwFXqtlQXCI/WAJ6aT9Wwo0mTkXY05jfgK0BRwEqY0TI9G4JWFgSDTOJsScGWO0gC3tyxQexKCBnN4KQapuNhFIlW06Y96ygAicGPkNnAhZ+uNH4djzmSoyKeFqkPca4EAcfaBwOWS856V2DvYbpPN6h0cuhyt9xX165drHb+Me7jxhPYFHyBfF4Ak6CuQUdAAAAABJRU5ErkJggg==";
    salvarButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAA4klEQVRYw+3YsQrCMBSF4byE4CbuOjm5SwV3UZx19TH6PtKHcHDrJkhBEMVBJ0UUIZ7CFUqo1VpSA54L/3ITyLe2SmutXE4RSKCDwJm2O3NULQIsY1aoXhToo45RlPHoxLi7l32U2E3RTfZb1CwCHKWchRlA87G17ENj30NnOTuitmtAJaiDnJ9Q1zVgXANt5M4VDV0DxtXQUu7d0dg1YFwFLeTuxSZwgFqJdh8Clbz1HGvAV0NgMg/1c+aVCbQVgQQSSCCBBBJI4B8C0z7cbeV/A/zVvAUGP8QF/D9IIIE5ewCILA3kAslwEwAAAABJRU5ErkJggg==";
    pesquisarButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAACPElEQVRYw+2YT0gUURzHl/5BGxEaiwdNqQwUChIPeQi0LnUIq2t4EKQsDfQobYimFnQpD1FkENLJixRBYocI8+DNS6dEIqE6RKWGG261Hy9fYVhcGPf9ns1hFt5p33zmM/Pem9+fBJCI8kjEgrFg+FEN9AATwEdgCVgGFoDXQC9w7H8I1ksq7G8KaNwKwZ3AXeCfbrwEjAAXgUNAEtgNVAHngAfAD83NAY80x4tgEpjUzbLAHaAkxHV7gDSwomtngP3Wgtu1pwC+Ag1FMI4C8wHJpKXgkMCfgSMOW6QMmBPrsZXgYS3pH+CkwQGrBTLak6csBMf0xPcMP0/9Yk67ClboxGaAlKHgXuCbJI+7CF4XZMxDxLgv9oCL4PrytnoQPCv2GxfBWUHqPQiWi/3FRfCTIJUeBHeI/dtF8INHwV1ir7gITgvS4EGwUuwFF8GngrR7EDwv9oSLYIsgLz0Ijoh9w0UwBawqzFUZyqWU2LJRbN8sbFSgUUPBYTFfWcTig/oU5IBmA7nTWpG/QJ1VutWtJ14ETjhmMt/Fum2ZD6YD9cUv4EIRjDPATzFeKAk2EbxZoBAaB2pChrSHgTrmueoWk5Q/KJcDOoA2FUtoH00CXUCTTmS1EttOvams5q5qJbZZFU29G8it/3dAFVomRMmZBZ5tplQoRu5agXn7gEuSfQu813gHPFGaVmpduPflyV2NUuujP0+unQj1Zm7lyV2JUvMoHRW5QoLLAbnLUWy/DSpxbIW4gRkLxoJrWVhm2wfvAN8AAAAASUVORK5CYII=";
    desfazerButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAACAElEQVRYw+3Yz4vNURjH8Ys7M7nJlR8ZKcJGDTULFpSFLKVs2CjZiCH5FTulWbCxmgV/gFLWrBj5ETUNoVhZ2MiPKGNsUNyXha86ne4d35nvj/mq+6mzus997rvz3Oc5n3NqqFV51bqAXcDprdNVBjzhjyoJeBytqgIeC+AqB3g0gqsU4FAbONiEdZg3m4CHOsCF+oZRHMbCMgEPpoCL9Rln0VM0YB2PzFyPsbroHWxibAqIfgziAG7iV/T5OwwU/R9sYrwDYBw7gAdRzBusKLqLm0nJpBgzdVyO4kYxp+g5uAhPpjEHr0Sx+8o4SRbjaUrAXrwIYl9hbhln8RI8S3mS7Ix2cUdZbmYpnqeMfRkAjpTpB5eljLsYzcbKOeq9AeBkFsC1wWrkCLgtbef/K9FkkGRXjoDb8wJ8HSQZyhFwf5D3QxbAG0GiqzkChgP7bhbAM0GiL5ifA1xvsmt/NZwFcE3k/U7mdIcJtSHrmAnLPIFVGeCW42Pa8qYFHMTPIOkYFswArhHZrha25GUWLkRlGcfKacD142GUYyRPP1jHregHJpJ7xlSN04dT+BR9907SLLka1kYbSPiK6ziC3diTvDRcSzo/1v3E7BZi+Xtwqc09I41aSVn7yrgXb+6wm510D1tn4+ljPc7hNt7iO37gfTJCzmNj9wGzC/g/A/4GJ/ifos4wLUQAAAAASUVORK5CYII=";
    primeiroButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAABKUlEQVRYw+3X3SpFQRgG4GfuD/fgJyFJEpJ2IUnakiRxHVvOnDh0Cw4cuYzlhKxW9s/ae681X9rf0bTW1DxNzXzvpKIoRK40A86Av/VeGr9gZYJ1L7A/bWB5Yg9zE+D2kCICL7H7s3Y0YBc75bWjABOusP3H9+zAfrgQwIRrbA34nw2YcIPNIXOyABNusTHCDrcOHBWXBZhwh7Uap7s1YMI9Vuu02baACQ9j9OZWgAmPWB4nqDQNXPjGLY2bpJoEPuMTi5NEvSaBT/jAelRgD/M17rysh2RYSwtxzQwKBWEu6n6xKlSrq4PMlmaq0T4csPo4CgksPy/DAochw7zqznEQGQhnOIwMhFMcRQZWkSGBcIxOE8C30vi1ZlurVgcn0wZmqRnw3wO/AGfU17HwFR0FAAAAAElFTkSuQmCC";
    anteriorButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAwElEQVRYw+3YQQ6DMAxE0Z9LF7FpoRvaU6crpAhRiENlj6r4Anmb2GOnnDPKlTqwA39bCzCqAl/AACRF4IpDEVji5IBbnBRwDycD/IaTAB7hwoFnuFBgDS4MWIsLAb6Bm+VNT6AV5wpswbkBW3EuwCs4F+ADmJSBV5Fun6QV6dpmWpDujdqKDBl1FmRYWKhFhsatGmR4YD1DSkT+OzArA4+QUmvnHlJucd8iJU8fJVISWCJlgSvyqQy0R58O/HfgB76Ml7FEDGXEAAAAAElFTkSuQmCC";
    proximoButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAsUlEQVRYw+3XyQ3DQAxD0e+usyDOBsTuelxAbhIpCMb8Ch7AE5cxBp1bJnAC/3sA387AAaxVyCgQ4Al8OgNLkFmgHakAWpEqoA2pBFqQaqAc6QBKkS6gDOkESpBuYBpZAQR4Ae/OwDCyEhhCVgPvwK8r8AZsXScO4aqAYVwFMIVzA9M4J1CCcwFlOAdQilMD5Tgl0IJTAa/A3vV2WnFZoB2XAZbgosBLFS4KLG0CTw88AH/ftbHIdvi4AAAAAElFTkSuQmCC";
    ultimoButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAx0lEQVRYw+3YSQrCUBAE0HhrEVFEJAQnRD2E59BreJbnRnDYOESTWuRDr/+DkHSqChTJU3TADnibUc3LDjjfzc+BMKkBPHo8fwHWQTYGhGk6EGbpQCjTgZ8gWwNClQ6EeToQlulAWKUDYY1eMhA2T8g4IGzvkJFA2F2RsUDY45QM3CQD18mPeJn8Fi+Sv4Pz5E1SJa+6Mvln4VUEaBX4Tj5pDfhuwmsF+ElGbhw4To6d3/Q0jQGHydXHILk86qfXb13D2gH/MReBKiVyMJaazAAAAABJRU5ErkJggg==";
    imprimirButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAA1ElEQVRYw+3XsW0CMRQG4O/KNIEyCRKDMAXUjAAThAHIAjBBujAH7MAEQQJBitRHw0koEnAS4Lug98on2f4sWfbvLM9zda4sgAH8Z8AGdteuGcAABjAB8BVj9PCMHzRvCDyeZ4svjLAuA3zDAu2j3j2BRS3RweYS8BP9P70UQJhgeA6Y4RdPFQG/D8frJPDUwAJ4qzq30ewhgUnzQQADGMA7AOMeLAus+qlb4aXOYWGKwSVgC/M6x60isH6gmyiwzvBeNrDGnySAAXxkYPrkEMAAVlx7nqfDscL2qycAAAAASUVORK5CYII=";
    digitalizarButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAArCAYAAADsQwGHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAmZJREFUeNrUmU1IFVEUx6+jqSiKIRhSiEgZStYiiFAJCcGFiIjLBMsCQRFaCUqLFm2TpCIEBVdigbYIF6IbFVd+RIqIC6lFEqGbMEh95vQ/ci4O13OxjXrmD7/33nzw5vfmnXfunXkmDEPjMBge5RXIEfY5V5LoIZKb4DMIePkvWAQNYMMoSeAsXwWJyHIyuA3mwR0t0u6pvwDuga3weLZBi4bykFYmgyLwRRD/DV7yPqqkLblgTBD/A8ZBtkZpK/5MEN8HK6BEozSRCRpDOd95mzppW+d3PT/Qn/xtqJO2XAczgvgvMMTdR500UcgdxE0CTIMCjdLERdDqqfNlLiV10kQKqONBx80aaNcobSkHi4L4JujRKk2UgQFPPx8GGRqliUuegYgywR9MnbSt88dgRxCfAw80SlvqwaogvgG6tEoTlWDEU+e9IFWjtOGBpt9T5zSCXtMoTWSBF2BXEJ8EtRqlLR3gqyBO655olTZ8Vmc985Zu7j7qpIlb4KOnznv/Z8J1Xtd5NOHqA3uC+Ah3HnXSlufcu91Qj6/RKm14lFwSxOkqqUm6sNByq4tmilOeOu8ExRqliTwwyp3EzXvQxvuokra85rl4NAv8/Ea6Aakl7aAZ5IMfYBY85W2lgdGZt6CaRe/TLcfItodaz7Rx7tx+A1d4eSJFuXAaeBQRpqST9A3F0hXgnbPugKSXTcwSmPglKXB+mXHILklvxkx6kqQ/xEh4D3yiPp2BF+sgN/r/ka+eTvM/qxOOeQB6QLcdXC7zsGmT6nnTxClKn3RMGmCGDj+FZ0SUVk6BqjM+0+Ix/wkwAEEXok/ByTbzAAAAAElFTkSuQmCC";
    menuButtonImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACMElEQVRYw+2Xy2oUQRSGv6rOZCZmCAiiEkFceM2ig+B6vL2CuM1O8g6z9h3cZisufAu3jkRdqCiByJBAzGSmL/R01XHR15kexcXcwBxourq7uurvw/m/qlZPX7wFQJOEElAKlAgK0AIKUMhIu3gmyTuATt/Jj/SasTEo9zkXsEwC2sDzOQp4reBlWUDnzgPt1hyHoTEYY5h2BMOIz0ddjs/6PP5164OC7REBzvUjt+44RMYwtHbqAk7CgBPPoxYZWoO7VQFf1j662gqIoESmLgABZQVlLa1gqyrgh9NxlRVEBGYwfzlaYVVAu093NyuwTHHujvSsS+0sSyN9RCb2LZ4n7c3hpVfjRQjwbI4ueLOUINqao4BPFRA1m+wgiQtEBCgG1PlA45OU7o183fjkSX3EJiaMh2xEF/YqILp5X9zG6irGWuIZcMALAzrdQ3oDnyent6s2jDd/uo2VFQSwM+DAse9x6vk4keGhNwFEXxv7rrKSp2s2ILIoK5NB9F13XOyMKPgvIAo529EiqQsKW+pSYTHijOSzyu6gVJy6lMlszAxMG6a5NwlEC7XhUoBooShuX76qdxHBWIsVW4BGFdmhDJnyIiXk4GIMStnZisGPInqhzzXvYmUx6tzYjt31+hpCImLa0fM93h0eIEHEo94EEHlXDtz12iqO1imKpxtdb0DfC3CimJZ/ryrgW33fVelakOd9mpHuhv4CoveusrPfDf0RRIvelp//Gf2fAn4DcSdKf/F95ugAAAAASUVORK5CYII=";
    loadingGifImage = "data:image/gif;base64,R0lGODlhSABIAPABAP///wAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFBAABACwdAB0AKwArAAACPIyPqcvtD6OctNqLs968+w+G4kiW5omm6sq27gvH8kzX9o3nCsD3aA8EmILBEhFoPPKSSubRSRw+T1FLAQAh+QQFBAABACw5ADoACgAKAAACDkyAaZfNDaOctNIHVzQFACH5BAUEAAEALD4ALgAKAAoAAAIOTIBpl80No5y00gdXNAUAIfkEBQQAAQAsOQAjAAoACgAAAg5MgGmXzQ2jnLTSB1c0BQAh+QQFBAABACwtAB0ACgAKAAACDkyAaZfNDaOctNIHVzQFACH5BAUEAAEALCIAIgAKAAoAAAIOTIBpl80No5y00gdXNAUAIfkEBQQAAQAsHQAuAAoACgAAAg5MgGmXzQ2jnLTSB1c0BQAh+QQJBAABACwiADoAFQAOAAACH0yAacudD5o88E1W7TVZbdp9YCZ2iDhqaIW27gvHXwEAIfkECQQAAQAsIgA6ACEACgAAAh9MgGnL7Z4ieLRKWTO7UXuueBmIiCNpWmhKgezZvU8BACH5BAkEAAEALCIALgAmABYAAAIojI+py+0Po5y02ouz3rz7D4YbQJZiUKZAqKpgm74wKc81fLcsLuphAQAh+QQJBAABACwiAB0AIQAnAAACPoyPqcvtD6OctNqLs968+w+G4kiWwCmeKKiuXguwrazSbgfbYV32aY6D3TDCmabIyyCHlmXsuNw4OcgXcFMAACH5BAkEAAEALCIAIgAKACIAAAIqTIBpl80No5y00gdXXLx7rnhOl4GjdholGrII2bLfTMPnhFn6zm+4WioAACH5BAkEAAEALB0ALgAPABYAAAIoTIBpu+nAzHtxumoRztv2GR1eKF7kiaYoSHIfxWRaLL9mU9OuDodsAQAh+QQJBAABACwiADoACgAKAAACDkyAaZfNDaOctNIHVzQFACH5BAUEAAAALB0AHQABAAEAAAICRAEAOw==";

    successImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjBAMAAADs965qAAAAJFBMVEUAAAD///////////////////////////////////////////+0CY3pAAAAC3RSTlMAMO+fEGDPUN+Pr1kBZ2wAAABzSURBVHherc6xCYAwFEVRQe1dwQ3Sp7ewsncEG3vncBFDQLjLCYI88n9pbvk4xWsqdbqlZbFTJDuEZRHIDsFuEdfg0Fgb9cGhNTkEwSJIDkEo0AFwF58mAIpP3QYIiQmJvUhMSExITEjsQ2JCYh9Ss9CPHtu+cSayxyCkAAAAAElFTkSuQmCC";
    alertImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAMAAAApB0NrAAAAM1BMVEUAAAD///////////////////////////////////////////////////////////////+3leKCAAAAEHRSTlMAECAwQFBgcICPn6+/z9/vIxqCigAAAOFJREFUeF61kkuOhDAMRPMxELrzqfufdjQlL2zR8Wx6aoNAD1LPOP1b5G9kYOQU5wLQYiQvAKuEzI3fvCKkAFgAot4dQF3A2CMHgMba55aZwMy8rJ1/0w/I1p/ePdUmrPXZ/0WhBZQC4P0JEXAwBNvGv3PAZHjsfCIniyrDu+tReOqbesqg/9P7MIwAuJ8/qifDpLf31yfVMYXTMjFfntrD+/uGWYq30FzG9GinnwbjJiZmMbrxv83JzbxcdW2dt+qI+ctq6zehtOK3hSVttc30J+JMioThqgzEob7Eyelb+QGqGBUBrWQHIgAAAABJRU5ErkJggg==";
    errorImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAMAAAApB0NrAAAAM1BMVEUAAAD///////////////////////////////////////////////////////////////+3leKCAAAAEHRSTlMAECAwQFBgcICPn6+/z9/vIxqCigAAAQdJREFUeF61lNkSgyAMRUUWUwzL/39tLxiKih360vsiZE42IC7/kdkIcvo74HNTpPWJ0JwvIjUgW4aSdwaikKFwz1jSRPfZrmWf9ID4S3Adb5AD4m6RFRJG1bcJJeI7Qv6caX/qFK6mFYjMOA67s20hiD0sBN/e9gtIhgRiLJOqNUhFyAunvTB8WLCqaV5opVni8syYVrUui8dcgMXLSOOWbzVXJnRmVAs4YSSO1POd4d6XaLUEWXlg0lc/HwAUsyhs6KqdTz9n5fNZiRY5535fW5IIzFFWWu6r3ftB7La6Kcc1VL932IQ4zYIJ1eSv7zDZ8YUn9dt7ns/FfL7mczqf9/l/4x96AyCgFl2kytGTAAAAAElFTkSuQmCC";

    editTableImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAABMUlEQVRIx+3WsStFURwH8N8hlIEUm81gkLLZDZJsVmV+i9W/YLaajXabMrEyKMnERKH06hV9LAcP13vveu9Ret86w+l2z+f+zrnn3JsQf5G++KP04B78+3BKqVSLiEpE1CKikvuNg8JWJpjHs/esfzfu2/jtwhhHwk4dXOsqjGU8YgX92M3wdtdgLKCaoWruD2Ajz0Dn4bymDz6miplm786PYczi3tfsY7ArMKZxU4AeYLiV3VIaxhSuCtBjjLa6TUvBmMRFAXqKsTLnQ8swJnBWgJ5jouzB1BKMEZwUoJeY/MmJ+Nr6GtyYIuIpIm4/XbqOiMWU0lVbX4kGT7WEzVz1ca70pn6vtlNxo4tbGatgFIeY60BBTeGjujVd6+BMNoXvsIdVDHUaTr3/6h787+AXgWr2bIlFRqUAAAAASUVORK5CYII=";
    deleteTableImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAA5UlEQVRIx2P4//8/w0BghlGLRy0ecRYrA/FZIHZiIBKA1EL1KFNi8Yn/EPANiP2IsNQPqvY/VC/ZFqsA8R2oQb+AOBqPpVFQNf+helTIthhqoAwQX4Ya+BeIM7FYmgGV+w9VKwMVJ99iqAGiQHzyPwJUIMmVI4mD1IgiyVFmMdQQfiDeh2RJBxC3I/FBcvxoeii3GGoQFxBv+o8JQGJcWNRTx2KoYdxA/AjJUhCbG4daqvmYE4+POWliMZDPR0Qc81HVYiBbhIRULUIViwckHw9YyTWQZfWA1U6jLZBRi0ctHvwWAwCf844oIDlPiAAAAABJRU5ErkJggg==";
    addTableImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAV0lEQVRIx+3WMQ4AEAyF4b4bcxJuXKtBogMh/J0MXr6QtKnc3U6UgP+DJU1D3oUUCIweBwwMvA82sxTo/9Kdc+B+jcA7Rpmuhs98Ne0EDPzErGa9BV5SDfVxz8WohAC2AAAAAElFTkSuQmCC";
    moveUpTableImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAuklEQVR42u2UvQnDMBBGDalSpU+fDbKCZ8gWWSJrZAI12cEjeAEXKgNqgkDoJ+f7wGkCIZEtqwj3QCBOd99DINQ0giAItUkpXbCqSmOMZ5rAvpb09GReYuxRW1UaQmjZ4+kN1HC2itR7f+SbPegDOENPUalz7sDBd/oCetBbRGqt3XPgQD+CXswskhpjdhzUUyaYwewsqdZ6ywEdzQSzyMiSKqU2/FJvtBBkICvnV7pSIZAl/7wgCP/BCMmW7zyFRqF+AAAAAElFTkSuQmCC";
    moveDownTableImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAu0lEQVR42mNgGAWjYBSMgmEB/v79O+c/lQDILKItXrVqFfO/f//WU2opyAyQWST5+vHjx5x//vw5RK6lIL0gM8gK8nfv3vEDDbhEhqWXQHopiu+vX79K/f79+z6xloLUgvRQJbH9+PFDFWjgayIsfQ1SS9WU/uvXLxOgwZ/xWPoZpIYm2QxouCswpf7Cknp/geRomseBCSccaNE/JEv/gcToUsAALcpDSsF59C7dmkF4tJwfBaNgFNAdAAAH3u8qYndYLwAAAABJRU5ErkJggg==";

    downloadFileImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAABdElEQVR42u2ay0oDMRSGM9EO6MoL7gSX+jw6WtsZhS71rdzpXnwVl4paKhYEFVyoRP/gWRSp1ubSXDg/fKvp5MzXnIR0qBCc/CLBGlg3ZDkmmWNwYshRTCJn4NOQUxZhERZhERZhERZhERZJW0TmItIGm//gwkLkfMLYW8SGjUgPXANl8aC26NpXYNdGZAHsg35AkT49Q8u2vQqwHWhWFNUuXK6XCtzNSEhRrcrXDtYFN55lFNXQteZ87WLztJO9exT5AHtUy3v0lD84nhlFY1Zixqkdbs2KxqpDHEtK0AHPDkReaE2UIc9Zes08Gc6MonvbsRwauwZtFrSd/mqzBgynEBnSPaWIMHrNvE2YGUWf6YjIo1vl9hcZRddqkUD04e4QDMaIDOhaSyQSSf3/U6SZ4sdaVDkA90QjEo7+9ncIKRKPzEGC4zpFLiJ6ka4EZnUE4zzSq5hYME7I91njYJFsRC7Ba0QYZ1F8/+klJEsjcJLJF7Pa4Ty9Y3QeAAAAAElFTkSuQmCC";

    fileFileImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAcG0lEQVR42u2dB3hU15XHsbFjbIfYiUnWjtcpJF6MTRdCIFADgQAhioljp2wMWccGTBWiCVXUhVBFCIEqRbYxOJh8G5OsY68xIDqiGdR7HY1GDWE75ew599038+bpzWjezBuR/Xbn+/7fjOrM3N+ccu89574hQ5x/e9g9aO2YV44fXBhwNHfp7JzUVxYX5f580bHCVxcdyXt1yfv5r+D9zwLey10acLRgKd3PL9q3dP57+5b6Hdyz1K9oz1JflHdB6lI//N6k9KiXn1n366UPLfXxG7X6te8P+f+bqtuDY157bfKclOj3f/b7Az3+B7NgXsFuWHh4Hywq2s/uA4r2wQKJ5h3eC3MPZYHfgUzwLciAWfnp4JWbDLMP7YGZOcnwUsQGeHbVr+CJ3wT0jly3LP9Hq5e98H9uVO/evfvMvXtfRfT29h3t6/vqxIDqvXeiu7PnxOmbN//wck76jcXv5cKcrCTw25MEc7N2wezdiTArIx580mLBJz0OvFFe6bHgifceadEwPSUKpiVHguvOUJiUsB1mZCXA2Kgg8IncDPF5++HwyT/Cf144B3+5dvXryqamy319905I1UvqtVV9J3pE9Qysbqm6ramXqbOr5+i9e/cidLq7z2gCo7f3q3H4Qq50dd39W2dn7z9QMJB0OgNc/aIUVh3OhSVH8sE7IRx8EiPBOz4cPOJCYVrkFpgSHgSu4ZtgcthGcAkLhElhG2Dclrdh/Pb1MAbvRwX+DkZtXQVjw/FnkUEweuW/Q25BIZw/fxnq65tBr++Gjo5uMBh6zNRB6hDVbSY9SW+udn2XoPb+0rV34nsxqU2qNpKBqVVUq6gOaDHpH/i9v+Fru0Jj6RCMxkZ4DCkfFAcaoVgU/ZwGpLVVDxdv3YH17xSC756d4EUwYsPAY8c2mBoaBJOD18OELWsEbV4D4ze9DeM2rYJxQSth7KaVMHo9glj3H/CTNctg1Ka3YEzIWnBdtRyy8vLh3LlLUFvbyAaHnkv6ITCgOgyoDnxMMnRzCZD0CEivx8d6Eyx9B8HA+3YuI6Ru0BEkfJ52nQinC9roHr/WIQwdwcCft3K1tZIIDEJAMTAtgppbCIwe/3fnAQB4zG4gPT0930WTbh0IBoneeGNTG3x8+QqsPJQHc8gt7YoBn5hQ8IjYClO2rYeJm1bD+CC0go0EALVRKgSCGhP4Foxe9waMCUYQ0VvBc91bkJ6TI8Coa2IwaDDlVmkJhsEaDJQlGDoJjDYFGOQFLMFoaZPAaCXpEQoC6ejU4Zh+zwF3BU+jn/27LdZBMP588RKseKcA5u5NBt/EKPCJRhihm8F1y1qYwAdd0Kp+moCWMj5wBUzcvBrcE0JhIlrSlHVvQmZeHsK4CHXopsiFiG7KDEanFRgGOYweI4x2GQy9CKO9U7AOiWUwIBZhdCrCaOEwWjgQfO1/p3hsNxA0LwJi0V2JbqoJYZy6eg3WHDkI87JTYN6uaPCLCQPvkE3gGrRaAkIKRHg8Hi1lIlrNJHRfU0IwnoQEYmwJAs/AtyE//wCzjDq0DBocq26KAbDgpjrsc1P9LENnzU0ZzN0Uxg8jjOYOaGrWs1jlNCDsk4lvsLlZB59duwbrjr0DC3LSYH5aHMzFmOFDlrEJY0Q/GOZgJiIwguEavAGmRG6CmclR4B8cBFksgFPMUHZTBme4qXYVMaPNcsyQWkZLC8EYBCD0pgnGf5dchzVHi8A/NwNhxINfXDh44SfdlcWLVRZhEKgJZBlb1sHk7RvAJzkapoRuhIDwYNiTX2C0DItuyqDGTXVbdFPmMaPTIgxb3VSzEowWEUinc4DQ15RNnb5+E1ajZczfnwrzU9AyCAZlUjjQEyzCEL5PMCZuXsvclHv0dnALC4I52zdBXiF3UxgzaHAGNWbobIgZbSIQyzGjmcMQXFUHix9Nze3aA5Gmtudu3IL1x4oQBrqplFjwiw1FywiCKeh+JnALsBTEyU25bMWgjfDc40LAa2ckvBwdDrmFB03ZVHun/TFDq9RWpyK1lcQMqWUQjGaE0dTUzt6TpkBEGKeuXYfVRw+BP8LwT0YYMSHMTRGMSWgZ5kBMICjdnYhxZTLGC7fwzTAjgSaKIfAKwtiLlsFiBsJow4EYlNTWgZhhKZsyWoYIo4Vg6DEL1RiI6KYoZqzAbMof3ZQ/WsZctAxPhOGGMFwoJpArojRWntbi98kqXIMDwQ3nJTPQKtyjt8FihJF94CBLbaXzjP8tqa1izGg2wWhGEI1aAunp6ROyKfznp65eh5XvFsICtIwFOOnzx5jhHboJYazFuLGayQU1UTLpY2ktBvjJWzGLot/dsRU8knaAR2wILIoJh5y8AiguvsBg/LOltqZ4YWtqK3dTegaDAjoDotMASFdXL9Q3tsKfLl2GFUUF4L8vFRamxkNAfATMDNsM7jgDJ1dFKe5kAoOPaV5BIFgWhd9zDd7IXJQ7WpPHrh1sbevn8VGYTRXC2bPnoaamUXVq2zEYbqpVZWrbz02hEAbFj8ZGneNAOjs7Rza16ODD4mL47cFcAUZ6AixM3AG+EVsYDDechU9hIExyQREI5qIwcE+N3ArTEYInThi94sLgl0lxkIkx4/Tpc1BDa1P2pLYKMDoIBIPRpVFqaxjYTTX3h2F0U83cOhBGgyNA0DIeMBgMT567eHH2yUuXYFlhNsxHGAFpCbA4KRrmRm2HGds3wrSt6xDIOnCluQS3DjeE5IruyXUbuqiwTTANAz65KM+UKPBJDIPXkuNhL076CIbopuyPGQIEufR6JXVyCV/rbEhtrQXw5oFiBofRxGHYDYRgXL169cmf/uKVaXPeWJ79VlE++O9NgYUIYwlmVPNjQsEbB9odB3wqWQDB2CLAmIJfT8fUdypaj1skWhBagyfOvL3TYsF3VxS8npYE+zBmnDlzjrkpW2KG+Mk33WsrgmTPcohyatvfTREIih9032YPkPz8/GG/WP3mC2NfWZwdgDPvBdkpsCg9EV7GiR/FDV90Px6YVU1FIG4IQIRBFjINrcYjcht4xIfDDLQKr4w48MlMgDmpsbB8dzJkHTwoxIzaRptWbZ0BQEnteskeh40xo9lazJC4qQZmMQ4A+dHixU+++Prrs2YlRdcE5O6GBWQZCGNRYhT4ReN8A4MzDbwIw4XBWANTMZ31QFhe8WHgnRoDM/fshFl7d8HsjHhYticNsg8fRhgXbEptnWUN1qRr0yC1lbspI4x2qG/QsfmVaiBPL1/+3amZ0QEBRfvq52cmwaKUeFi8MxrmxYbDTLIODNJuaB2T0VW5cMugvY4Z4VvAKzYMvNBF+SKIOThPoa3a32RnQg7GDEptbVm1vR8wGBBdZz83JViF7amt3E2ZYLQx2QXEpSh8xKwPE+f65aTW+WfshAAEMj8+EmZiIPck66AZOVqHy2bBOiiG0Pe8IoOF3cH0OJiTm4YwEuA3e1Jg/6HDwqSv1rbU9n7AEKUqZrRYjhlyy2ho5EDa7AAy6sOE4R456VPm5aVXLshOAz/MqmZGbwcPBiMQrWEDc1W0ZE5AyDqm0zJIDALDOcbs7F0wMzUOfpWeBPuL3jWt2upsS23vJ5BWDVJbOQwCQd+n+1Z7gAzJfvPhicfDv7/onZzbAfm7YSbOOWZg1jSVltS3rWeuatKWNQwIgXHbHgjT0JXRutSc7GTwxmzq1cwUyEHLoIKEOhtSWzZXaBfS0ftmHTjwYnra4mDMIJlgoOhxvb1AKLDnLxv2s+MHrvvnZYD3zkiYimnuFBx4F2YZq5noMW0q0c9mYHrLSnqSEEZSLOQXmBYKLcUMmkvIqzzuJxAa7Aaj/9fxGKEutZW7Kfb/8HFdPamNxSh7Z+oPLPmgsMT/wB7wwvTVDQfdBQdfsAy+w0ezdLSa6ejOfFJwnpESA79IjMGYUcQsw3pBQo9i2Y1gSYOv9naDMTUVxT75qlNbczclWAbCwK/r6lsdAjJkyfsFJfMPZIEnAnENCwIXnJVP5NYhAnFHVzUL5xh0//PYCBbAyTJYQYKV1NYSjPsBhCaGZBFSGEYoLepTW3M3JVhHfUOrAKTVESAfHCyZm5cOM3ZGgMv2DayOioBM5O6Kdvg8EyNgOsJYFB7M3dTlAXf6rMFwFEi73na3R4OjBEEuu2JGowlGHcLQBMjC93JK/DCoT4sLgQlb17BiNnEFdzLNO3AC6InuaklYMJtnDFzE1jMgDFtiiWldSnisZtW2SeaWbJWtqa3cTdVzGLV1bSgHgSxAIL5oIW4xwTAWYbwUuALGbFjB6qemIyS3yM2wOGQLW0KXby4pLYeImZRaSUGQBej0nXYXsTVJArc6IO1WYkabWcyok7gpEwwtgBzJKZm5bxe4RG+BF4PeglFrfsuqCqfFbocpYRvBd/N6yMovgGIbithoZdYeGKK1GFdoNdjpsxuITamt3E0JMMhd1da1sFpfu4HMO7y3ZGZOKkyIDIKfrl0Oo9a9AS8FrYBJoYHgs3E15JNlnCcYAy+H2GsdAhBti9jsAdJga2rbz00JMGoQRk2tg0DmFmWXeGYnwrjwjTByzTJ4PvANcE8IgdlBa5llnL9gObWVL4fYC4PJCUVsjgCxltrWK1lGPcFodRyIH1qIZ1YCvBiCM/PYrTB60wrwCFwJu/PyeWprWxGbI9bhrCI2S2nuwOrvpizFDBOMFkx2NADie3B3yYysRHgxMlBoC3jz15CTf8A4z7B1p88R63BWERvNLRob1WdcA6e2baYgTjDqBOuggE5A6LntBjLrYGbJtN2xMG1XBExd/ybCKDQuh9haxKbXO2YZzi5iszueWExtzd1ULYdRXdsM1TXN7INgN5A572aXTE+LBu/gDZCemwcXeMywtYiNfud+xQxbi9jscl2yGbhizKhrMcKgxwTDYSC+hRklk7ashv2Hi4yrtmqK2OwFMthFbPbCqLMSM2pkMGpqWoxAdPYCGZMYXDLmd7+EG9duGmOGrUVsrD5KpbvScQ12EZv6OGItte3vphgMhFSFj6ng2m4gjy8LuDR54yp8w+3qi9jssA5d+/0pYrMXiFJqW6tgGTUcRlV1k2NAnnx9YYZbWCD0dPepLmJrVw1EzKQGv4hNPYw2c8uQpLbMMuqazWGQEIbDQKa8/fZTrqEbyvp676kuYqMCZ1VAdOpTW62L2GwF0i+1VYgZDEgtd1UIoloLIHS7dufOj6nYWgRia+GzmsmgtA+c9YKz3u8OZeGAa13EphaIkpsSBr9Z0U0xGPi4Eu+FgmsNqt9FIGpqbW2FIkDoUCUti9jUACHrkKa2gnUoS4RRzWCgqppYmq0ZEDX9GawHA92Ms4CQhWhXxGY7EGsA5BJdV2WNAKOyqlEbIN3UH8Iyp4H7MwiEucsxCOtQGsIwWolmRWy2uys1QES3xWBUN0KFFkB6CQjLsnoG7M8QraL/4BmM8UEeN+yFIQBxvIitoaGdDZLZ6q0FV6UWBrMSdFVVCKICXRcD0ugokF4JEBtixsADabAetNW4LQeL2AgGDTRbseWLf5QpkUQQ4tf2wBDdlgADVakpEBt6+vSdmgy07UA67C5io6UPewdZrQhIZaUApEEbIHeNE0NrPX2DCYOktojNnsDsqFhQJxhVDVBe2aA9EGs9fYMNpEllauuI67FXVZTuomWUOwPIQMdVKAd0J1lHS4eqIrbBBiGdiwgwEEpFA9vY0gSIcBKb9T5wtjo7CDAou1ID434BYQGdYKAqKgQg9VoAoRMc2HF6AxxXoSUQCthyNatZDpFkU/amrI6AqK4xWQbBKKush7KKepZaawfElj7w9k5VGZIlNVuQ+sLnwYNRhaltFZuRN5jDqBBglJU7AYi14yqUBlDq87WR+v4MZ0KgDEouCt5SN0XWUV7eoD0Qi8dVtHdpOOADAxGOqbBlOcRUN6WNGxJWb5UgSFUhc1MMBoIpRSC07asREMtuiq0pDRoQ4SCwgQqf5UVsjqStQuraZLOMlsFhkErxcWl5HVsp1gBILz/XVvnskJbWjkEHMnCzjHkRm9o4YIwHlfZJdFMCjAYGo7RMIyCdXUIpqNIRR606w32BobY/Qyx2tj6Ba9JMIgzmqirqoKxMAFKrCRDeZKN0xFHb/XBXNsQMpSK26hpTEDbuVWgIQQ5DdFME4w7GjzvOACIvYqP97cEEYnNPn0IRW01Ni1MA9ANSIQZx0TIQBj6+U1bLlm80BaJUxDZYQV1NT1+dQrPMYMCggF5q5qbqmXWUIow7pRoDsdSfMRhAxNVa+XEVtvdnNDsVgphZ9XNTLJgTjDq4rSWQgYrYBgWIipghL2Krrm1xCgRLMEQ3RTBKEcTtMg4EX4c2QAYoYhuUYG4hZljrzxCL2JwBoR8Q0TIkborB4NZx+04Ney0aAlEuYhuseYianj5pEZt04LSEIJc0ZtwpF2AwV0WQEIbmQCz1ZwwGELIQNT19YhEbTe6UBk8rCEYLwTRXGjOkbopgfIG6dbsaqqupbVynDRBLhc+DAcQUxG3s6eP9GZaAKIGxB4SoUhkMMYiLlkEwrt+sQNdWrw2QNhGIhcJn57srneJxFdb6M2gi6Mggq1G/mCFxU2QdBONKyR18XI3zIQeBGDgQa/0Zzg/m/d2UNF4o9WdQkZqzQbAZuTS1VXBTDMbVO1B84Sbc/KLirzU1NRoAsaE/wxkgpDDU9PQRHFoqdyaMMr7HoRQzRDd141YlwiiFcwjj9NkSzMCqz9fW1n7bMSCGHl59aL0/Y6DdPsVFQsnAN1pZDrEltVXqz6isatLcIsrK6nnM6J/aSt0UwbhaIsA4dfoKXLh0s62sono5AAzVBIiq/owWhbopttMn2Vyy+SQ2K8dVDNCf4SgQtlBYYVq1LWXzDFHCcshtUTI3dfnKHThbfA0t4yr+vPLrW7fLt3/6afUwB69hKABp5UC06OlTfRKbHT190v4M1RAqGkzL58wK6NNfAxWVDex9sLoBnYG9T1YX3GSyZmrIIdXhB6Wisg5u3iqH85ev392VkplAh8JpcFFJeLqDA1F9zSWNTmKzdlyFpZ6+all/hhoYxm1XhEGf+pLrpfj/GuFu35fQ10fdZL3svdPrINdFVkEWQh+AxmYdu3DB9Ztl8PEnxfDe0T927NyVEY1D+Q2NrvLJgbDOJXXXXNLqJDZrx1VY6+kzNsvwlgBWhS6KAFQK1YTGIE2VIZKdPnJHFy/dgHPnLwNdQ+XuXbqOijAWtNlFvy/AqGMpNh39JMAoh//6SzG8/8HJnvjEtJglS5Y8pdk1cM2BqLvmknYnsVk5rsJCT1+VGQyhJYD1aJALYxPBxn5FbKwoodxkHTSwx0+cRChX4d69r1gHGcVS+lAIMGqNltGE75Ws48YtAcbxP3wK4RFxaSNGjBg+RMsbA9LRLdRRqb7mkl6jk9isH1eh1NMnbSMz9WeIs/JGxSI207ZrPQvY5y9eh/05hXDlSgm/ulCHAIO7KRKzDHyvVJFIMD7+5By8f+xPXRE74lJw+L6pSdwwB9LLgLCL7KpoPba1iE2L1HZAN8W6l0QYykVs8oIEcldnzl6GtIy97EgRiqHMTZWb3JR4EAC9VooZZBlHjn5kiE/MiBk5cuQTQ5xxkwKxftFERw8ZVp/aWofRbIJRLbMMhSI2Iwy+00dAPj9zCVLT9kBx8UXW0CMN4ELMoD2aNh4zzsIHxz/+Mjo2JcHb23vEEGfdjEBaOlRfc8nZqW21YuuxspuqkJd3yovYpJtLlF0RkNMXITV1D5w+c479juimqribMsYMzKY++PDjL8PCY3fTARiauylLQOy55pIzU1trMaPSzE2ZxwxLRWzSnT6adxCQFARy6vNiBoiCuJDatrNsSowZR459ZIiOT0lwOgwpEDbzVpHaqili0zy1NXNTCjFDoYhNvtNH849THMhnp86ymTlZJLkpcZ4hxIyT3QkJ6bHe3gtGDBmMGwHRMyD/fKlttQ2pbbm11FahOkTc6TMByYTP0UJEGMxN8ZhBqW3EjoQUpwVwy0CEg+rVXnPJ2altlQOpraXqELa5hN+jWHHq8wuQjEDOnL1gDOCCm8JJ37GTPeHhsemD4qYsAWnWajlE5UlslpZDrMWMCisxQ6mITb7T98XtaviMgKTshrOYZTWweYawHHLk6B8NcQlpsXzSN3gwjED0XcYrBWixHKL2JDZ1McPcTSnGDIUiNvlOHwNy6gIk7crA4F5snIEf+/2f+2LikuPnzZv33SH340ZA2jkQtddccnZqa4wXdqS2/YrYZDt9N7+ogk8+LYb4hBS8P82yqd9/+MnXktR2yD8BEG2WQ7RKba3FDFtSW2s7fSXXy+CjP30GO6ISMbhfwJjxkSEmJjnRKcshaoEYDD1f00RI7TWX7utyiA2prbWdvouXbqF7+ggSk9LRdZ3violLjR83zv17OCRDOZCB5JxbN8BThs6eiiYNl0PUnsRmKbWttCG1LbWS2t5RqA4Rt10/P30F8gveRXd15u9xCcmZzzzzzA9xOL7FLeRxiR7jehQ1jOsRvv/xMNdDqAe5VAN7gP8h/ZNvTJ8+ffjFi5ejW1s77onXBG+0cuKzKOGgL8EShGYZqWT9ezxmGPv4uJsSRSexiedNGfczKk37GUzcRYndrmynr7ze2Llky7YrK0hAGMXnb8DJP5+G944c70vfnXkIx2EiahTqedRPUT9BjUT9GEWgfoB6DvUs6vsoqip5GkUWRRNGKmqgucpwDm8YB/aQBJLi7UFuko9w+vRPRgQGBo4vPn8xo6m5rV6v7/6rtIuq3agu3vZmOu6VtjqZdPyypjrJNWfbJGo1GNXSKpSmijLvvtWbF0eI4h+UBvGUH6564weFrFS8DpREkpN+qIOqnK9XXSm5/eWHJ05WhIVF5OL7n0+HfKN8UN4oT5QHagZqOmoaairKDeWKcuEAx9NJu6jRHOSPUP+K+hfUU9zaHudjPZSPvaJlfINT/BanS7R/8Nxzz41JTk5bUlh46LcFBQfe3L8/b2VWVvbqzMy9azIyMtelpe1en5ycujEpKSWIlJiYtDkhYecWUnx84ta4uIRtsbHxwaTo6LjtUVGxIaQdO2JCpQqPjA4jhUVEhTOFkSIkCo+wRyH4t/0UEhEeGhoZFhoaHoaPQ4NDwkK3BoeFbtiwMdjX13fNo48++iq+90WoAK4FHNA8ungEXa8ANZvOnObQCJgXh+XOIblwOKO5dT3HrUeE8ih3a0OVLEV0VY9wKE9wKE/zf0Qm+m+oF1FjURNQk1CT+adjKv/EuPMX5cE/UV78EzaTaxZ/E7O55nD5cc2VaJ6C5svkL5P0Z0p/P1cm8XnnSCQdaPF1+/D3IrUSd4mVuPKxsNVCHuNjbTW2iJbyMLeWYfwPh3NA3+H/8Hsc1NPcbz7Lof2QP/lIrp/wT8bzXKO4XuAvdjQH/BLXGAWNlWicgsbLpPQ7Y2WSP4f4/C9KXtcLktf7vEIMGcnf6w8lMeRZhRhCY/YkH8PHZUFfdaB/QAJpKLcgMXt4hGuYBNxjkgzkmxIN55+Kb3GwT8r0ba7vyPSUFY0YQNb+Vv484vPLX9cTktc9XPae5JmWNMt6RJZlDbU3y9LqppSbPyjRUBV6SGOpee4HFVJWp843/gd2ZnaqXdc85gAAAABJRU5ErkJggg==";
    pdfFileImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAlu0lEQVR42t2deZhkdXnvP79zauttumeaGbp7FpgZlmEYhk0BE0QRUVwDMUp8TO410TxGSFTUuDzJjWIeNctVUBEcUETUa4yJ3HhvDFHcWRJFlhkGBpi1Z+2he3qtrqpzzu/33j/Ofrqquqq7J/d5Us9TT22nTtX5vef9ft/9KE7y7WLI//mrXn72K7ds2eBoyXP8BJ3Llwl5pXSwjeeB1hoNoMF/ZuPoYAsNjtYUevuY2D8sw8/sUdPDh2a6Np6249pnHz/Cf6Fb7iTv33rbJReev76v78Ni56/RlXJ3qWhjjAuejYhgDIgxiAACxhhEBG00YsR/7hlyPT1M1qqMT01imxr9HXZ5RWXmO9sHT/+brUf37/qvIhDVykblcnkwlyu8S2uzBSjMt73BUPM03vYnlLrjs6d3njK0pXz8GB2WJh/8rNaC1gaDwtMaYwQj4HgaI6ANuFqjtZBfuZLRsXFGu7rgNy9l3dAQq/qWUyoW3Pz6DTtyQ6sPg0n8PsmX89wEiZ+2sHWjF3NXAUCU5RRy6iljzLaurq6jixZIrVbb6jjmHmPMVhGxWvmO67qUDx2k567P09G7gqNP/Jo1UkUZQSwbXfMwCFoUruNgsPCMoVb1ENvCcTxcbRAsZEU/4729vDB8mOr7/oSztpzH4NAQXZ1dKAVKqbkL1mCFpc4iRuKQBqKSZsIQ6v9cLGQFAsqUSvntpVL+7cVicfuCBSIindPTs9uMkd+DuQef2RYRwfU8Zg8fouvrd5I/dJjK6HH6vQq2pfxtHA9PKUQbtOezhesaXGMwRqg5Bk8E1zPoZcuZXdbLoVoN5w/fzsYLL2JwcJDOzi5yOTv1fwQi2FMAykSHOPezcNEEEYWS9GpI8Kky4r+pwvfwtw12JkrFC2+CfSoJtlORgAyCUkK+kP/6ir6eP1ZKzS6IQ2Zm6LJt61XG6KbCCAXiuC7Vvbvp/vo28sfHqLk1eqRGPmchRvwDsS1sbTAIWCBGyCkBJXhA3hLEM9Dfj7tygCOzZeRtb/WFMTREZ0cntm3N1YwGwqCZMMIFriMMEsKI9paRbCNhJD804e8JKJHXzMzMdAMNBWI1VR+FDZwyL66K4Loe1QN76bn3Tjp7ljM9W8aeHOPZjZtwz78YFRyzytkoCyxLYeVsbAW5nEXOtrARcjmbjg0bGO1dxvgLx3CvfwvrLruModWr6eysL4xgPeoKI/1ZDCgSr1YKJ3yYkmh7aSB1E3wp/LmkMOL3/H8Q716tsCzLXrCV1dUF5XJjoaVg6sghuv7+XqyxCUYODNPnzrD/zLM556/+ltzjjyLbn0DlBFWtQT6HaI1yPCjkUa7rQ0cpj0eJMc/guFWGb/wTNm3ewlADzUidsEoSIJ6BKZVc7tZhao5mKEFoBFPBPiPN8F8oQIxCEIzPwSfH7A2F4bgulYMH6Pr7e8mPjjFrXPp1heGzt3DaO95Nx8O/oLbtc0x1dtGvPSwRlKdBWVDIY7kaLBtbDI5dYnrDmYxNTnDidW/hnHO3MDAwREdHJ5almnMG0himaBGmJM0ZWZhKCUOynBH/BQlgSgUvpI11tRYjEMf1qO7bQ8+9d1KcnaU2NUG/U2Z47Wmsfs+H6Dl+DPfeO9lZ6kZu+iicdyFYNsqyUZby77blL8Da9Yz0LGN6cpzx176B1Re9iMHBITq72ueMuTAlDWEqyRlKaEjgczhDaMgZEsKUqIiKWrWrrYUKw/M8KoeG6f7WPeSmyowdOcxyXPLAGdUynd0dcOGLeWzVGs645Q5W5mysfc+D1pAPhGIEVcjjlrrZV67iaYfnrrueoYsvZWj1Gjo7u7AtXxhJgczLGaYxZ6hmnBFCXTPOkPhfNOKM8IUI9fa6dALxPWuD47qUDx+m8x/uJe+4zE5PslzXONGzjLGBIZRRyN9+Epme4JLbv0K3bSPbPgdTE1AqBXhsIYUCNVXgxNr16I4CO9/2DjZs2crg0BAdHR3YtoVlxdohBMaPCTgjY9pGn6U0w/ickRGGhOaoEd9EVYrQUvV3HXCGUmkCF0GUwSjftA3/glESaYYKNMPfbQCFISIuJYeEBF7Zt4eub99LcWKCysQEXnmS4Ut/k5VvvI78ylVUR46R/8H34avb8N55A+SLyPqzyE1MYIkBO4c2Ht4pQ4yVKxypzHL81W9k45atDAwGwrDmgalmnKEWxhnSBmckYWoOZ0S7DfYjKhkPWBoN8U1bl8rePXR/bRsl12N6epLqzDhjl7+CM951IwOHDtD9ra9R6usj/+73oHr64DOfxmBQ7/pT5IKLwQhGWTh9K9njGsYt4djVr+XUCy9mYNC3pnK2/Z9j2mY4IyvZZpwRa8ZczpAkTBmWntRDYcweOUznt+6mWOxi4snt5KfGmfyNK9j8/g9S/MH3ka/dhf3rf8e+6d3UhveT+70/IHfiBOrmj0KljF0oIsUStVIPJ3JFxK3wxHVvY3DrRQwNraYr8MAXBFNqYTBlaAxT8c/5MOWbtmmYCtlFTEzgIUyJCfajWo+VzSsQwRdGeXg/XV+/i+KJcSb37WbVsgL9PV2cPTiIMoJz9ib0KSsRbbA6Snjf+Cqzlo06+2wKx46R/4sPoh9/lFq+g9HuXoZtxa43vIVN52wOOKM901b+E0xb2jVt58BU8PvSIoHMJ5Ay4Hqayp7n6dl2K4XpKWYnTtBTmWR/1eFYoQPzw39Ff/l27C0XoP/bH6F7l4NSHJ+ewbgOuC5YFrpWw12+iv3FLo5XK7xwxdWsOe8CH6Y6lzYcsmSmbZNwSARTSc3IwpQK5SEtw1ZTUp8tl/EO7KP7q7eTd4Xy4QN0mho7e1ew5dY7KHZ14T34Uwr/6x7cchnrfR/Cu+F9PPWeG1j2BzfQPXEC9u5Fcjkcq8gLYxPoAuy+5rdZv+X8KDYVakZbpq2cfNO2WTgkfJEJYYIJ9hMIA2Pa4pBmArEfuvHG1a9YvQobmxeO7aPfnWX/mtM45+Ofomu2jDu8j/zVr4Hdz5F/+Ge492wj9/vvYON3/4WeWgVuvxVTq1I1Nsf7ljOam2HXK1/PWZs3+1Hb/w/hkDmascBwSNLpU4HAkzClgkiGBFHyVrWkIWT9/mmDZ53vTn9WRo5TPrCf1ZVpjq9dx6kf+ChdRw7BR9/L2O2fY2rPbqyzz8Gq1cj/9AH0t+6lp1qGr9yBfm4XTncfR4qdjDlVDr/8VZy1+VwGBgbrckZbpi0n37RtyhnSmDNSMKVUO55FYw25+eUv+8CqVYNXzDy3i15vlpwxrCqVKJ06gCmVGFMWo5vO5eyVq+CrXwIBy3XI/+hf4bFfYV4YoZbv5GjVZdJWHLr8Kga3+jDVUepYgqitzG/amnTUdl7TFpqE0DPhEBpzRgRTSmU4XRYukFNXDr3ohBhWiEtOa0zOpuvpp3C/dCv2+z5Cz5e+wSldHVh33wE7t0NHCbRgVaoYRqkVujjUuQyrOs3Oq36LTZvPbZjPWAhMGd8xWHTUNv65uTDle+7ZqG19mFLByidhikgYZvEaUh6fJF+dIm9b/p+oOai8Tf4nP8QpdlB415/C0cN4T+/ws3c1B3I5tFI4qsAxUUx4DsNXvd6P2g4GMFVHMxL5l4aJzLlfmUveqXMw84X0rlXdvKmE+8xsEr2r0pqRNW2zMOULQ1AqPqkWLBDl1Ojr6kBPT4A2WEHlgVKK4v3fw5kto/74vfDOG+ELfwcjR9GuR63YzcGOHia1y+gVV9HjOth7nmPm0EEqVuAkZbSjaeWANChKkHkKEJrk2GOXQCXZZk5GShI8A2AVCvRfellz0zYFUxFrpYspFiKQUm8XpjaLMQbLGIxSKMeP1ErOJv+zB/AQrBveD13LMOYo7vJTOOoJucokw1dfy2kXXIT56HvZqFwsLNqjt+aVAC36WTF2S/o9kbRwJCGs8DvZ55yyktzLXoYzW/WF1IAzksJUscwXafbmbPSMRgwYI1iOA/k8ltZgDFYpT/4XP0Y98WuM51KVPEdrHpM2PHXN77Bp82YGhoYY81w6xFkSORgjTLke45Knt6eD5cZFeW5dS8aYBKMEp3K8uKGWqYQW+Ozth12CfUgIRcGuqlXfhG1g2qZhKvZATRt1Rg0FYjztx3SMoLwgjqM1WBbKBlPTqJyCqSlqPSsYWTXIeGWWI6+4hk2bz404w146vWDc0Ry//FWc9qbrMJUqZvujWP/7H1AiKdxO0bsElBxpRCyMEPfrao3EZUASfE9JmnYacUYEU5HVvQShEx2ckVprPCNokaCgzaANiKfRnsYdWMuoaxidOsHhK1/FwPkXRtZULsgGLtXteK7Exve/n86HfkJh2y1MD66FFSsiS8wY36IxYuKqSFFRBaQxgYCi7QJilnBxJXpuohxGoAUJgdHQtE3DlKQiv4skdQBPG1/1RTA1D1XIobXxj7xUwOvo5aCnMMrj2de/hbM2b5kTDlnKm17eT2f/Crw9u8kfPUR5xw76coXYNI7gyReE/2gSmqMQMRHR+wtmIozPakR01iNpK0nEN6UbcUYAj5LypxbJIZ6nEQFXG3ANtlI4rkcuZyOWQordTCzvZ6Y8zb43vz3wwAcaOn2Lvolgrej364AxgKL7uZ2Y8RPB4iaiSj4hBDyStLRMiqiNxHwvJiMAQxrijL+N3cCaSnKGBLmTUDOMUS2zemMOERXAk6/qRhvEAlyNDAwx2dHBQcdh4g1v4vTztkbJpZMijBBfC0XEskBZuNpjdNVaeqbGEakQVtjMpxFJLQoJu65GRN+RzPeTHJI+EZIEnhbGEiSoBPCM4GnBNQbH0Xha0KtPY3++xPHJSV543W8xeNGLGRxqXMS2lDclBvJ5sC3KVYfi+tORQiEBTzIPPEkEb3PuiYhBfJfU8ywxh7+X5IxkHCb8X2H0YVF+iKcNiPKF4hpyQC3fyVTNo2wq7H/Hu6PkUqNwSFtx51YEUqv6EaVCB1MzNboHBgLyTZuuPhyZBJ9IvEBJ8k5AlQm1x2QhLIaspECMqWPazoEpXxhGZAn8EAHXGF9DPAOd3egzzuDQbJnZ697qm7YDg3SUGmf61NJQRyyQ6Sn/uPr6GCtXOXXNGvB0sMhWItwYxqok5dlL1syN4EtSwot9yVhwUeVI0tnMmLYKmaMZEgbFFs8hoMXgak1x4xkcLZepjY1Ruf53WXfRxQyGfkaT6pClEIZInBCyZ6bR1QrWin4qff3kO0top9bUekp53Y2sp9T7sUBMIAETRroiDy82bRtxRigMMXGAZnFWVpCkp9jJkZkKU9PTzN70Xs7cutXnjFYyfYuQSuhpRzgNdCqoHB+la9UQbN6K1GrIbDV9hqcWuZkA5goo+T6S1qy5h6Mackb6f6hIIrIYT90y4CkLZ/UQE0eOUnn/eznrvETdVEshdGuBWiGJs1ZFC9dpCbMHDyIrB+i/8uXI5BSiPUTZ8faJhY7/k6TIO2VdmUTbjSROBJOALJmLn2LqmbZJzYij0UZMy/GsxivW0clMocjhmTL6d9/M2VvPX0ARW/vwFMWWQmFIHNq2nBrmyGHsDRsZfOVVmCOHohB4KACTsI4wcy2lpNedeq79NjrfS/c13Ui8P52MaWU88CxnSCKRlu3CWrBAKsCBkWOoN13L6ZdcysDA4IKK2NoThkkskgpCHWGIA6hUkYPD5JevoDQ4iDl0OErTpsIfzLP4891NnUeThMX4OI3xT5okZ/g1pKGDmcqyNO7UmQ+ypp/dDUePsP7SS1jRt2JBmb5mZ0aaTLMhD4lJObSWBJQ2WIeH8WbLWF3dyP69GFEpnE+ar1GIQ9KlOMnFNaF5m4Ko4HtGIlgD0ElSr2vazoUphQrjmOlYWLsaMvz4M96aQoFTTx2ko9SxoCI2aRmeQpJUMVxJpr1MBNuyMMMHqL0w6mvFweFom/qaUMfB0xLuLoIhI35KOHw+12mUhMnc2LTNwpQKiyuWolDOgkf6cjYdiy1iq2M9+QeXicgGkBVGZH0o8FXex3NBG8Ps1AyeU/M/W7kqhhTagKRm0BTuo8FnyRNKqUCQiYp7g4lhKszniyyeQ3rPuujjtjHPL6qIra4pm7CiIvNRYj8iERxM5TKCHnZn7ToKoSCuuBLRukGogyaaInO2NZF2SOT9G0mQfDJZlapsIVFmJImKijCeadpykBtyyCt/ed/YsSd3vVppvXcx1SFEZzANsncmcL5U6rUEzl7SH3CVhWw6l3xPD8Zx4KJLYMU/ImOj/veDRU1yQsQZ4d8xySiIRGZuFPmVufxiAvNNpcIGpq5pG2mGxNWSEhbKyfys3tRROHXr2ZXFFLGRLWiTWCP8RUhUbkRcEewr8Txch1ErR+9LfgMzcpSxn/4Ete405PwLA8iT+tqgg10Zn5RNgg9Eh/CUMJFNzDV+MkvmkDsNTFuVLOKOOnlVInKxhC1tCyliMxHeKr+mSVQim5fJyBlJ+BCSINdYy15YOUjv5nMxv/olldu/gHR0wpVXIaWOOr5GM+4gQeAy57Heeybku+jkUw05Q0KYCtod2okltdCOEOV2Wu7PMIlaW0mZtiaV2oxTq2FaNST5wPIx8WdHxsY55c2/C7aFfvghVh0fYeyHP0Rd/jL0mWdGuRsJEkkmevSf64CUtUnsO/g9E72X/t34MXwem44qhCkTm7YmyCOHMBVCHar16vd52xFYQH9GqqIwE2WNrCdJaBBpjZCMo6eNZjhfYs3rX4/s348cOICxLdT/vQ9vchLrAx9Bunui0LoO9qkl5AuJYSghNEJICuCL6LNkGVD8f1KhkwambRqmQi9KtZxTb64hZVIk1pAzGvX0RfJUkecdWU8mUfUh8bfj9Eb8fGyqzCl/+EeofA6962nk+HHEGArP7KL60INYa9bAu25EW3Zi0euYriH8BXedhCORFIzprBmcdCyTHnjCtM3ClEIwbdZltcghMg9nNOnpy1gv6TBJ7IMQxp5Im6eIMNrVzdC11yEzZeTJJ5ByGRGFXS7jfOcf0JOT2FdeBa95XQArEpmpSc0zQmPnr44pHPNJpsQnsRwmadqGwjA+hkiwQKr1opNWBCLzckajnr7Q5zDJGJVJ2/YmcAaTOB0+F4GpSpXiK64m19ODOTGO9+ijfomS9iGpuHMHU9/+NhSLWNe/Fdl0jr/fwEiI9qsDYWiJiv98rz0oD8pwRcwdyfcSY5eUasgZCr/SU4J+RL8/8aT0qdN+f0YmVGKSeGykaRTWGGGqs4euK67EKhQxj/0KMzycDr2oHNbX7qH8owew16xB3fgneAODfnVKyAuJiG3oAEbaEnCGliY8JnNDQfVNW4k1QxL9JktR5NDc7E0Lo2FPX4oPJBMXyhw4acjwQyXC9IYz6L3gfL/o4tt/n/DsY8zPiWBuu43KL39J/vzzyX/yU7jrNyBGzzFnG0d9G5vMyNxobz3TNuSMqKUB8buuotlZS+SHRGZvA85o1NMXpURFZYhaGgQEExaZwGilSunVryHX3YN+8GeY3bszOY94H9bxEdwvfpHa8EHyGzbS8em/xjnrTESbBjkRaRpKEUPdCpVUqLoBZ5BoiyOBHkvCIfWtqRb7wAPNMsbEjlUKm+vhtP/c1R57u5ex5jWvRWo13HvuwViW/7kOtgs8bf9RYT3zDNW//jTOkSPkVq+m8PFP4LzkJWjLxmgBHfsnoXkbW1JxUDH7f3Qd01c14QwVaEa4RibkkCUdrbGASWxxZlRlKj7SlR1k8thK4ODENKff+ilUPo/3s59g9uyJFyRV4BbmP3ySdx9+hKk/+zOKN97Iiksvw/rYX1L55+8x/k/fpTY1idXRiSnPBsXjHlKpIuVZbKUoWjYFZUfnqsxpSZAMhKdNWxVWTPosjxLfk1+adoS60NX+JDYhaeKmK0DiwGG6rLPiOJQvupgNL70CmZ5C//CHSM1JwbAxQtVxOFGZZVIMbNhIbuMZlNaupTQ0hN3bi9Eeue4uOq+/ntyVV2I8FywbPC/+fW38HvrxcSqHDzO6dy9m7z68J3fQN1WmYBd80k78tiRgyqRMW4mXJjzZFP4wnsWWAc0VhlnQJDZJhCTq1ctGpTaJM/2Yq1n9rht8mHvmafRjj2GMwRPfVz3qutTO2EDPy65k5SWXcvqW88DOgfbA07456nm+EI0BJRRWrSLsLTMiSLXmT3AOTp6O1atZtnUrA7kc5HJILseJp5/Cue0ucg/9hw+TCVYPYSrUDN+0TWiGCqK/JpExXEzVyVzdWMC4CtLWU73is2QVCEDF0+jLX0rnmWeC6zF9331MT01RXb8Bd9Mmui64kDPPOotSvoCZmkKmZ/B+/FNMpYycmPCLr2cr6JkZZLqMmZ2FnE3+da/FvuTFqOV9mNkqUz/9Gd6DD6GffoaC46E6OmF5H9YpK1Grh7BPX0fPWWei/uKjTP/5zZhfPprw1huYtlH8LgirhCWuZBzKxQmkhXEVCZiaM67CJGtjY+zN1j6FEhkt5Ol53Rsp9Cxj9qkdTCuL3BveSH++QIc2yC8exPyff6EyNeV77OWyzwOVShwuD4rFozwHgrt9J/Z5W8i9+mryL7uC3tdeQ+X883CefZ6Zn/wE9+cP0fn8bqzAzpF8HmvtWrz3/BHFwcEgHqVQiX7EhpwRwFRc2SpLxyFSJ0wyhzMajasgHQqZwxmZ5JHraUYv2MrFL30pRgzqti+wfNezQRDQ4HkmCEQSN8xESTrLb2tWibMyMe9QyhXcR36F8x+/gt7bKbzlTZSu/x1Kr7iS7pdejlcuM/LjHzPz3fuwn9hJZ8WlvOspnPvvZ/WJ2WAcemJMhmnMGWSnkkrrvJ5rzQlhQeMqwrRtGMbAkKgCCc5cT1O1c1iFArsmX+CcD33Y54tvfB3zxHbEtqPoaxSfCo422ccRchGGWGjJDGIYxPRAxk5QuW0blS99GfuSF1P4zZdgb97EwEUvQl3+UirTU4zveZ4OMQx19lH9zBeC6heV4JDY6ctyhq8Z8Uwuo1rvUm3TympvXIWKOESlKi8kiGEdny5TOXcLxU2bqD31FP2/99/pWrMWvXMn+nvfQyw7lVJNVqOQrEwk1kCT3TZr2YXfURbiGdwHH8H5+cNQKqJWrsQ6dSXWwAB9fb0Yrak9vh1v9745J6gkhDGHMwKYUhKNHFhKDqkDUy1OYgtngoRXPwiLAFxjOFCtkf/DdzBw9avxPvsZppcvZ+O112Emp/Duuw99bCTdjG/q+yxpgZASerpFLdvUGXxf2WAJ4riYQ0fQBw+nt1MqUe1ukqzamDMimPItLJUpjlgSgZiFjKuwEp5v4iAPGKHw4Y+w7lXX4Lz7jzl6cJgVn/wUdqmI98gjeP96fxSJjTjCpGt1UwVxpIsbwtfZYuko8ZQq1DOJ2txkZFtlQkDpEzD0wFOmLRmYQgJhLLEfIvHp0tYktri4IUyfGg44DvZ7bmLNpZfhfujPmHj2GZzf/h36LrgIMzKCc8stiKdTShef6QnzOVW1Xq+toP5wgHSvCCSnOYTjMjLJ61RVO5kIQ8q0zcCUCmFTpbV38Roi85i2DSaxRYoebDtcLpP74IdYd/U16C9+gdnHH+fA2nWc+/Y/QDk1nE99Gjl42O8jzPguqWaZTONNttYr3TWbLnBLQ53K9CQmm3GSAkvPJkh64EnTVklieFmk3SoO9yxtPmRhk9jCL50oz8K117HuzW9B//N3cf7tfp6q1dj8+dso9C7H/eo9eL9+DFFWothBMtm9xlm91HMTC9Jkt80II7Y1wlJWk9Ie/xoldczOxFTSJGegIsKLx2+0UbnYeuhELWwSWzh8YHpggBW//SZkxw5m77qT/Z2dnHnLrRT7T0H/6AG8f/k+RptEm3KGjJMVLJJuwEl5+2GNVp2ec38kX6InMVUnlm3UbBBhjc5DNde0TcBUaOoqI3GhhdHzVsrl2jF5FzSJDd+qss7ZTMfAEId//23MrFvHqhtuZPn5F6AffRT3S3chE5O+U5dZ6GY9f9KgSyo9vDjDFZLmhWwBdatNxPVM24gzAphSErdGaK39RtolI3VoexJb+BVLFMu0RmpVeOtbWf2S36BzzVq8n/8C9zOfQUZHEbHSFSh1BdIgdD9nek+6Xz3ZpxgWVqR5os1+FmKYqssZyp8BHIpaa43nebieTbm8FKETEkOG25jEplRcGFB8+N+pvfOdrPrK3ajubpxvfgv3y1+GmuvvM4pB1enXSJi9ae87KYy4lSGpEenPM/5EGxqRrhMQlKlj2oZak4AprTWe61FzPYrFvJeurVqwlQVtDRlOmpFBvMqIYB8fxXnDG5F8wQ8GWlZc+mMyFlHC78jm58m0KhNBmKT/W6KJRyUCjbHTLG123yXnotQzbdMwZYzBczWO66E9D9u2H1Oqr7rIWNbCrrmkgpVYftmldFVrc0MapCvNqdsnnh63JCa5/pIKfJpkVaHUO4S4XEaEBTXR26csj4OhdUzbSBhB2tpzPRzXxXEcSqXCCznL+tK6db1T8yZmG91mZmTAdWeOuq4XRVFjWKp//YzsYHpTnsFKpXPTKyVz5B+/k4ySqoyHnRwIoDKVLuGBZZtlJBEYTBekz72WocSdnYnqfAVdnamCOckcSwhTruNRq9UwYujvX+aCurlUUJ9Zv359dclIvb1rLvkHY3d3zTtkOOlZNxtXIU16+mTe/oxkDryFTF8YTQ4K/fK5HF2dJXL5XARH/oUxg0IHpbCCM8/zPBzHoVq1KVers/d///7bbrrpxk+1EoFvndSXaDB9u5PY1EJ6+totYqsTtRX8M73m1Ojt7WbVyn4UQq2mcRwXx/GoVB209i8pWCwWKBZzGGOYmJzi+PExTpwYHz94YN/tH/jAjZ9otczBasO8aOuaS+0NGW4+ia1e63Gznr52i9jCqK2oRPmSMczMTFOtzLCyfwWI4DgejuNSrdWo1kJhWJSKRUqlAsYYxsenGBkZY3xicmb//j13PPjgA7cALQ+dzLVjYbR9zaU6g+nbncTWbFxFo56+dovY5mT6EBzX4fCRw6weOhXbtqjVfK2oOQ41x8XzgpG5xTzFUg4Rw8TEFCMjo5Rnq+x86sm7b/v83/316OjodDuGQ8tWVnjNpcUOpm9/ElvzcRX1OIOwiK0OZzBPpk+Uzw+VSoUXjo+wbs0AtZpLreYLolp18ILLipeKJYpFf+zhxMQkIyNjTExMTT3zzBN3f/wvP/w/mM/paF8g5QSPNDJtVaIRdX7OaHcSW9h6TAamUsJooT+jWRFbBFOJggTPdZmtVPA87Qui5lCruXgBTBWL+UAYmvFAGOPjExP79+6+/Z67t/0tMMMCbi1DVt3LiTa4fkYzzkiatq1MYmvUepwSRoP+jFaL2OZGbcM6Mt+SqlUdqjU3IvBSsUCx5BP4eABT0zOztd27d935g/v/+Za9e/dOssBbG556Y9O2vcH07U5iazauYpGmbZ1MX9jCHGqKNpqq4wsDoFgsUiz6pu/4+CQjx31hPPnYo1+++eaPfIL5x5ksXfi9nWsunXTTtllPX7tFbImorZAuYQrTB6AolXzTVhsdEPgYE+MTE88/v+vOhDDkpAskim62aNo2G0zf7iS2iDNMG5zRZhFbNmqbDpH5Tl8kDK19zfBN2+l9u5+/4wf3/+izCyHwRTiG0pZpKy2YtizStFUtmLatF7GlM33Jo7cSTp+IMDEemrY1dj3z5FfuuXvb3yyGMxZUSuofVHvXXOIkm7bNOIN2i9hSUduYQwSwbDvywEPOmJicmnlqxxNfvfljH/mLxXLGwjmkzWsuNeKMdiexLSgc0mYRWzLTlxQGAVwZbRifmGBkZIwT4+MT+/buvuOLX/ifn14KzlhQKaks6JpL9cMhyUlsWc5IwtScPvAm4RCy4ZA2i9jqZfriegCP8YlJjh0bY2pqprJn93Pbfv7Tf7ulXQ98iTkkcTnRFq+5pBQn1bRFSTQBqF3Tdr5MnwkzfZ6H62pGRsaYmam427f/+u6bP/aRv1oqAl9kLKu9ay6dbNM2KYx2Tdv5Mn2e58esHMclly8wMjI28dxzz9x188cW72csWiC2bbvg5tu+5tJCwiFmicIhbRaxZTN9ruMyOztLqaOEiEzt3v3stm9/6x8/iz8btJWKCDkpAunq6nInp8vDCBtNm9dcms+0bXkSW9NxFc1N25aK2LLVIa5HtVpjanKSF118sfn3hx/5xudv/Zs7jh49WgU6qDM/gHTZr9R5bepsX18B6qR0reD93AMPPKAuu/TSvr6+FZcYkVw0Ia6eBUa9nLbUyXHXGZMn9eqIM2XCc9Ks6deSuYpNsohB0kW5ddOuxhhc16NWc5iemcFop3LgwJ7vvP+mP/3izMyMAF1Ad3DvCu6diXtH5l4CikAeKASPdnC3GmlZLpOsUsF7OSD30EMP5b/5zW985/rr39q7bt3p1xaKxQEk/o4wJxE9z3t1BhpkF79FwdYVTgNB160wSQnDJ3HbtgGpObXyoZ1Pb//ZJz7+l/8ErAD6Emd6UgNM5nnyroO7B7h1HsPPvKQWqYRmpISRkGp+7dq1yz74wQ+e2dfXv1zE5FzXsz3PtY0RZYy2jBGltWcbE/i/RlsSYJSIqPDufxY/l0xZuc6+bwj+dzqi1u7NpAZGxRU9SvnMIqLEBFfkqVXKeseOHdMPPfSL45VKpZqBm2YC0YlHk1lwJxBCLXhM3r2EEFMHaCXgyg6FEQgn+5hUvVCz7IRgrcRzlVHR5Ps0eWxEnmqe1zIPuUqD1zI3aV2XC9oRSDMN8YLP3eRv1Du45OImhWQl3rcT+Xgrs61KfJZd/Oz7NBBOowVvRUDtCKEVciajJdn3k3BV770khJkERCWFGP2H+SCg3lldb3HrvVZ1BJ19TpPvzPf/FmN6tqIlzQRU7zumyet62ragg1robSFn+n/Wf5QFbiuL3FdLt/8HofiKjwK1CV0AAAAASUVORK5CYII=";
    docFileImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAApHElEQVR42t2deZBsV33fP+fe2/tsb2bezJt5m96+SgIEAgulwhJbWJQxwTZ4IRW8kISynYTFBlO2C0Q5NnaBHYcl2IStSAgmThVJbBwHHIMl4xIISSAJoff09m3W7p5e73LOL3+c2923e7pnF3G5q+5709vt2+d3ft/v97ec04rn/HZX6tS7f+HEwbvvO+xEfmqp2iA/MiqucpWLBhfQgI7/DnT8gAs66JwmCBjKpbk+Ny+Xn3pSLVy9Wp2ZnvrO1U+88wb/gG7ec3x+Z+YnX3VndurwOz1lXlUPwiFclyAIcRyNEsF1wAEwBiLAaBABCcBoRASjNcOZFH61THlpibooouFdteXc+BfSv/Rf3h986Kef/odiELWRF9VqtRnH8f6l1vqs47jpdd9gDDoK+frNsnrn1+du2zM2cvbm8goNUUQajNYosYZwHXAUOAJKCUQaxNgjijCimRjOUV2YY8zUeOXeLDN79zI6vptMJh0e3z3ynf1juetdH9/+ZyM3QTp/buDVg+6sGgT7r0iQSXtPVKvmY7t3F25u2yDVqn+HiP6UMXKHiDgbeU8YhlxaLPMbj6+Qy+f5u6efxZc0Ya2O46UIleo+iRGUo1BBiPIcCEIIAyTlMp1x2aUazN+a5zfv3sXZkyeZnZ0lny+gFCilVg/YgBGWPoPYNocMMJWsZQyh/8dJ19sUyqTT3rdTqfSbhoYy394yZF2/Lnljar9iDM+H1V++6/pEEBGiMOTq8gofeKrC9VpIcX4Okx1mGBjOKK4t1mhEknhfbGIB5QAN3z6hNbtdg4p8KstzvOuuCc6cOMGePXvIZnM4juq6HmmdS+IZo0x7zq1+rjVoYodLuqenxM/aUyhQnbFWiZOJUp3HTXxOZY2hpP0mZRDXD4LnG6PfISL/SilV35JBRkaqBaXcHzLGrGmMlkHCMOCZhQoffKrCxaomhTC9e5KoeIujY1kqYYGadtClKrVGhGkZozUQosBxwRh2D+eYyglq4Qpvev5uXnD6GHtmZ8nn8rius9ozBhiDNYwhrQHuY4z2xakO+qkeyw4yRvJJ0/o8e7ofrlarQ8DWDKLUkAvNyXVxNfaMZxcr/OG5OqmhEYZqi8jICOra03zgNS9kz1iBmzXNz33lCoUoIvJrNEKxyio5Ip7H4fECXvkmSwt13n12nLtPH2Vm715yuRyus9oY8Xj0NUb3cx1Akc5odYGw9Rppv14GWN3Eb2p9XNIYncfsFcQAgMIZdxzH3TJkFQpQq1kRtCZMRRHXihU+fqHJXF3jLl8lM7EPs3iVD73+JRSGR/nk98rMNzSRkyY7OkLQ8AlXQiIjltVdB1Iuo57C1Is0m01+63aPM6eOMzPAM7omrJIEiPfAlEoOtyBrwtQanqEEYRBMxedse4a9owAxCkEwloOfG9nbMkYYhlxZrvKJC3VuNIWCY5Dpg4jAmelhZseGedcjyzy26KONkM9nmfGXaO4u8FSwQj00iONgUh5DGY9jYxDMV/n5M3nOnDrMnj2z5HL59TkDGQxTbBCmRNbmjKQxpJczOpcgMUyp+I5sYly3ZZAoDLmwVOGTl5vMBQ6+Xyc7OobjOIgYRvJ5XMfBU5B2FVqBI4qX7smwb2qSP/CvUqn5lDRMT4wQFeeplgLefCTDS04eYmZmllx8js1wxmqYkoEwleSMJEzJupzBGpxh2sYwCdGyEV3tbNkYUcSV5QqfvljnctmnvLyENzJOsxkSVpcJgpBn69DQwltODDGWUqRTLogiDJrcdWCCqeld7JreRTrroYu38CXibQd8XnryEDN795LPF9rGSBpkXc4wgzlDrcUZLahbizOkcxWDOKN1R4R+Z905D+lwRsj15SqfuVjjal2zuFInN7qL3aUrvPpAgRccmqHYjPjLOY//caHE64+P864zeT756DWq5Vu88Z88j0dKGj+VY75cZG/WgFL8+p4md5w4wczsrCXwrXJGr2dskDNWecaanNElbbs4I+kZqgWFLUTcTmAoIntqtebNKNIopTDGtNXUpy7WOVcJKdeauCPjTK1c5TdfdhwZ2sUTywG3j6c5XHD40rUai4HwQ/uGmErbL/jVuYDPPlPi0nyZ4q3rTEuNn9oL9xw/yJ4ZawzPdXGcbgc2a8HUWtJ2EEyJ2EEdAFNdcUb8+CA1JQmYEhvetD2jRerptEc+l5otFAZH7N5mvCMMQ84vrPAfn61R9DXL5Sp6eBxTq/GGE7tgeJw/eHyZpVD4iyvCm47m+aeHhvndx0v8zmNFhtIOzUCz6GtMGCHFOQpEvHHWcPexQ+yZGRxnPCfSVro5oxem1uKM1mN9OSMJU4ZNkbqzAUsgIjR9n/PzJT5yrsKVmuHxa8uU02OEuEQ64va9Uzy51OR6I6LuR5S14o+/PceKr/mR24aYb0RcWglYCKFWa7B08yY69HnHdJG7jx9idnaWQr6A51nPaBlEWrPfxDDVE4F3nkt6hrEw1cMZEiO/Mi3PUJiEZygTw5RSmFjGdmDKxNI2aQzpxBkmAVPKGlxMfB618VyZt749DNVanW/fKvGZ8w1uaIe54gpBYQIcF8cIuBnm6iGnJ0cZcWEFhR9pasUVqo0mSmXiETMEtSojukqqucA/mw45c+JUDFObk7byfZC2bFbaJmEq8ZjFMNm+h9RqNWoNn7+5WuQj312hrDyuLJZZUEM0iosMlW+QbxSJcPnCtZCjBZc3HyuQqpeo3rrOi0Y1u4dz/PWNBsYY8srA0g2C5TlePWl4wfEYpvI7mw5ZU9rKZqTt4HRImzNMD4EnYUq17CEbhq01PWRubi79dNnw2WfK3KoHLAcNys4Qtcvn+Pf3HeHHX3QSHIevXFjggUfKfPpciX9xeoJ79g6xUmuyezjHl643+MsbDcRvsrKygJiI1w6tcMfRo+0IvOUZm5K28txL27XSIa07PSlMMPF5YmNgzA4EhiKKgwfHXvO+j9539BWv43otYnaswOKKIrN8i9+7/zj333WK/36hxGTW5f5j0xyfHOWXH5znmRXNjx0aJZPy+NyTK3zlRh1TbzAuFaJmiR/Nlzh7/AQzMzP/X9Ihm5O2g9Mh/aRtEqZUzL0SZ8k36iXeANfIj7/0dT/rH3nJBx69WmJ4qMDNiiEvmre/cJpX3n6Yf/7nF/jarQaFtMMbDw/xnpfO8qbTu/iDx5f5yrU6aVfhKZjKKMLqAk5Y5v7RgLNHTrJnz56+nMH3IR0iO5UOkcGc0QVTSsG2PEREUW5OT7zwB3/BrxmKS03qdRiaSOGlFBOTk4zn0xwoKIzW+CF8+mKNvc6z3HfHIcZzDjerEVopMiYkqhQpNJYffOWEvvf5Rw7YoC+b2wFpK+tL2z7pkM1I2zXTIQzmjDZMxYW4DqdvLXWihq42D6h0YaReDak3obzis3BjiZsLVd72lUt899YKH77vCG88PkQkhqAe8M1nL5NKuejQoB1IhSEjwQopE/Cakcq9LzhygJnZveRz+R2RtqYFU2tJW7YnbUVtTtoqEcQYTAxTHWOYDadO+qqs05+VglS0F0at4p3g10P8lRqLSzVe+4WnePjyMu+79wCvP5DlVHSV99z/Iv782RJXKgG5ICDdWKZQX+C1+UXuOHHCStuNVPo2KG3XqvTtmLRFNiVte2HKGkO2GYd8AVXS5bQRUaukc2hQ4jNXdPjJLz7DR151lPfdu5fgxVN8swgffnyejDGM+kXGpMH9E4bnHzrEbOwZ369KX+v1O1Hpa0tbNihtuzyjzVq9dfZNeMjrj3kLd1zM65xx+iW+lAiYiJvFKv/2ry5zvhTyyBL84v+9SkoMamUJx9G8bqzKXYf3s2/fPrLZHBdqIV8vhjy8HDLX1J0v8FxU+lDrV/o2IG3VZqVtonmjZQylYBPJ3n4q6zzhv/v1pnr9W7VSblf20WnhsB+hUx5XFqv8+BfPU9Ew7hqGJCBLhTfnVzh17DT58d187lKFjy+u4DhZJtMuE65iKOVwW97h/uk0RwseKffvr7SVTUjbbpjqRKBmE31G/WSvadaXbmXzed8JQlTTdLxD2WprSoHnN5FcRBS67AJuG3HRpSV+Zr/izLEzqMIov/WdRf60OYT20sxmYFggdByMcrgRwKevBty9S/ODu1MMp5y/V9K22Ii4VG4QGsilXA4MpxlNewnoYiBntGGqfWkbT530M0gUXfneM7mhoZpTKaJUPMnicXAdRSoFqTR4WZd8VgiaDXS5yZv2KV507CAzs7N88rEr/K/lNKFnL2TRc8gaRU5DygHHUTgKHi5qPAWvmk7jOWpHKn3bkbbaCP/zYonPPXaNqysNIsclk/GYGs7xmiPj/PTRcTzlDJC23TAVl0FQyEYLhn0NIkDRyWRC1d2lYzsMPfAy4GXTpDIpomYNHQa8ZX+DO4+ftOmQfJ4/+c5lqlNnbZuuQDM0zDmKgmPwXHCMiygho+CbxYiTQy5HC96OpENUgoA2W+n786sVfvFL38M06uB6kHah6XG+FvGNos9SYHjrmcnYUwZwBqptjA43bif9/o53ZKKG7ypROI6Dq8BzwU2Dl1V4mRSO5xL6TQq6zq/u97nzlG1iy2VzlCo1zhcbEBk7TQIN2lAJDdcCw1IglEND3UBDoBgJF+vaDvIWUuitwl07zsC044z2mIiNM4xiVQq9lSgs+pq3PngNU63YD3QSnYsuhAKfv1TiseVme6KKgDGWM5RSlneM9UKl7HmNURtFrAEG+ehHh6NGzcXEHTqxMZwUiOeA6+LX60yEFd44q7j71FHbxJYv4LoOWgxNPwRtIDSW1SL795IvXAkMC6GmFBpqGgID56o6Jr/NN7HJDkhbheLjl2rUbi118gayuqZ6pRby6HIzwRmmizNMAqaIjbG91AkoslnXEaM8z8FLaUChXfCBiXQKXS2RNhE/d8zjxadvY3bvvq4mtqFcFjeooUVsN7txQTt2xkWasqvIKPCU4CpBOXC+qtEiuC3o+T42sSEKhfC/53xo1u0MTOahVDIvAhdqYawGpfOyJG/HHyISX4eSDScXnb4csrTUSKddUyikSWcUxhWaWpNWBl2rkIqaPHBc+IEzR9m7N06HuJ10SCGb4exwCqIAIrGeYoydzkahjWFFQ1ULDS34BoqhcLWh49bk7VX6uj2jf6VPEukQESgGmvlSI+6872nAbuVs4v9MQtq2YMpIf5hSSjAi24QsWCmM7AqHC1kcV9EIAgquMFtwmNJlfuVUlrNnTsfpkP6Vvh84NA0rRfsFWx0IrVmsFb6BhgFfhCBuCvhGKew4xPexiU0UXK5rwmqt2yud/iXt6YzTuciEtG1dQZdnxGme7dbUo1Q2K57roNAcmRplTKpIaZE3HR+2nDEzM7DSZwTuOjgN9XIn1ZnMr4khEkPTGAKESASN4rGS3plKn7B+pS9Rz3AQLjYNQWWlXWruGiFJpKOV4kAh1b7AfpzR8gxBELPBRTjrlXAd0biex1DKJSzP41ervP1snnvOHI2ztoOb2JQoDk+OMpuJF99o6a4rx+PYRBEai2oGuNE0LAdm05U+ULiA246X2oyx4Sa2K3VDWKklZ03f4LrgOkxm3Ljo1J8z2tJEurlHthipxx/i4EUBs8MepVKdX7t7F3eeOsH0zPpNbCiYGh3hcAZuSAjaBePYQ9tzY6BpwDcQOQotguconqho/tGE0+aMwRG4Yck3nK9rnq4abvoGFyi4imFPsSvlcCjvsi+rGEqpGNT6V/q0wMWVJmHgd7Rs7Ml2iDrkvjvjMppyO9K2R011PKPDg0bMhvNZAw2S8xwi3SBavMHPnx3jzjPHmd4zM3BJQG8KfWp8hCM5eNDEJGkSPBC/Jor5I4oHRSnFd1ci7p3w1kyHKISH5+v8h/M1vloRfM9jPOMy6bmMphyGXUXeMRRczfG84mWTHieHnL6VPlBUQs1SuWGvUfVW+bpHciLtMuw5iUa41ZwhiWxD7yqsLRvE8Wss3rjKL98xzh1HbmPPnhlym2hiy6Q8zkwNk73ZpKlyHcgSAXHAWCnYEIcQIcIqpGtNTS00FFw1MB3yrfkqb35oget4dhmDcVh0XBpAoBSilF0iJ4on68LNawE/MZvizJCXwPtW1haWQmFhpd4DqSoB/p3HpzIeI2mXVg54tWeQmEiCxIWyFlzVt8ohl24tc2NhmZecORanQzbfxHZ63zSFlXkbXUax2mp5TPzmhhFCA6EoNEJNw82mGShtG5HmNx++zvVv/BWc/5aNc8IIaURUfc21ULMQGcqRUDNCIIpl7fAXC5ql0PSt9BUjYaFS71wX0hOVqrYcnsi6DKXsUsukMYy0YMqe04jpGKM1sWQb3e/PPPaNyN01bT1jK5U+4PYj+9lVmoMUPbBFm1wDEYIYsiKBmhZu+qY9o1QPOf3NjQrnv/MEXLkCj/4t3LwAYRQfGt8XrgbCraRRgEsN4cmKjmOCRKUPsW2xK3U7WXrxRXWWE3hKMZVN4anO10hK216YUi1P2YlGOTeKvq5yw1uu9ImB0UKBu8azoJudB5OGMQYtiqYoQjFoFE2juN40RGa1tNXG8NR8meXlFcgP2wTTjYs2AA1CCK23hL7mZiDcioSyFqoGmiI8Uo7aMrUlfowR5mo+taafUILCqnXVCtIK9ubdtpMrFdf2E8sfDB3PaFctRbYve09OzbzHg3PbaWIThFeePQqLt+zotkajBQ1xLqgpEIlCixVhNxpCTcuqdEjZ13zz8qKdyOksZAtw7QoYH6LIJjEjK7ODQJgPheVIqBnwUXy3JiwF0l3pE7i84luDrqoMJQIiFBlXsb+Q7uls6eaMRHkxTgGZnYlDHv7wLy392c/cdZ/uWae9me4QEeHlLzhJev4auMpCgk5yiP3SvrGwFRnQCDd8TT00q9IhJT/isetF+14vDZk81KuwdL1VybFbdEQGtKYZGYqhUI0MTQ2BwF8vRTiJ7hANfK/sQ+jbe62wXlQyZw8CGWBvPtUitYGcoZSKOSVOz6hEJbS+jd7eO04cbMi2mthg99gwZwsOmLADVyYhBaUjfzU2Yl8OFYthTzoE+Pb8CnPL1fjKXVuYyQ3D049Bmk6axpj27KkYoS4WsiIUDy5rGrpT6ROBC6W6NYisTpO0CVGErKeYyroDpa1KpnbaSVDVQcGdXNK2tSY2wQCvOLkfVSt3OCR5GCESO3tDA1oUEXCu3nF1ZWwU/qVLy9CogePY4lEqZQ1y6wbUS/F2HLrr3KGxScymgdAoVozw6Ipu903VteZyuW49K5kL6QnYEdg/lCXrOVbaihrIGS3PaCnFhEW2b5D2qbbYxCYG7nveUZzKUotF40PaiS9jxMIWFrYE4buVyBo5tnZkNH92bgH8ZlxLdi1spXOW4L/7CFb+xLCoO95S0XESE8Hg8PWibs/+SysBYS1B6C3IUqbVZtM21MmxdOwZHZiy6xmttDWxUGnBlGkNkNp49/vayxESFtlOE9v+8VEOpqWPh3TKnb7YipyO03+X6oa6jgdIwZevFvEXi4nahAtuCtJpGB6D65cgatiZnox1tOCL0DCKQCwf3QzgWsMC4iPlEGrVHp27KsMJIpwezyUIu7+07YYpIZG02ZCTrO0hteR1br2JLZv2uH2yYOVpqy7SSinEuB+IELaIXRShwFMV3U6s/8mzRaiULXc4TqeUmcpApmDLmXOX7Ye3vSOOxo1QEyEwQoiVwk/XbGb50WJghQHtemwHG0zSIHB2LBtrkf7SthemFGKF2ib6sjbIIbKtJrZMOsXZyRyObnThexu24uAwECFE2fyjUjy+EqFQlP2Ihy8sQOAnsDHeF6UFW9kh1PxVEG3VViSdOowWGgYaWPHQMIrzdUNTC4/NrUCz2R2dy6pSJQAHh9OrpK1JStuWMUzcaRIPkJINO8hGDCLrcsZ6DQkp1+H4WJ5h8YmnbCcqFvutxNigMBJji4w4PFGxZd2/u1GmMjdvPcJRNlvsxttxeJ6FrWwBqlXwV2IeieLysQ1ugpjYg1haX2jCtXrExZuLYKLVywaSC/4FbhvJkHfVKmnbjzMUBqNUPCbEVUvZSQ9he01sArdNjrHHCe0X1zFsJYM/ERpic1pRXH+uRcLVhuHha0UaxVJnFjjxpSun05eUy6GCEKe2bOHVJGALQYtQ10IT+xmLEXz5Vh2KS91lW6RvLeT4aDa20VrSVjqeIYnK5U4EhmvL3k00scUzbf/UOLNhBVJuYgC6dX5TIBBDZKz0FaV4ZLHB01fm0GFgDaKc+LJjg7iu7dpL55F0Fnfuuu3EMzpWW9IukjXElo4DUUQCjzQ8KC/HdY+e+nnSNgZO7cq26yiDpG2LM9rtqYhtTW1vMLBDcUhb9m6jiW2kkONEzkE5yQAx9hRRcaFI8FsBoli19eCtGt+7fLOVPIoPOsZRro1JvDSSH8abvwlu1JG/bXIWfGN5I8RSjEpnmChkLO90F927eVAMt0/mEyGAaRuvH2eQaD3qauzekTikr5rafBMbwAtum8ZZvGE/tRVNt6SMUTH5Khskit2sZq5pKFbqtDN4KlZXTswnTkzuqQxk8pzYP0vu2rPxbqc6Piyxi4aKQNMIgVEExnDs+c+zYsHoxCxWnVltBEcpbhtKd6VD1BqcoWLPaI2RaXGI7CSHbHEntnavsRFedOIg7uVz4DmJNEdC/iI0Y/lrsJuUZrJp8rlUq1pvJW+r61u5MY+44KVw01ne8up7ubNZBKU7XtJuQTLUjdA0HS8Zn52lMFywIqDlgV3Ertid9xjOuutKWxVPsBZMKbES+TmQvZtcnzGgiW3f1DjHpG6TeCaRhm9tDiJCKMQxCWgRUrkchdHRbldVcRyiEofjsn9ynMNTu/jZl5zCKc5ZctfdwajWhlociEYCRjnM3HbQGqQNWaqrlDtbSJNuZYh7YMp0SVvpDI3Qbo1VO9AG1McY29+uQinFq84egvJiLH91Rw2JhS1jxMpfY4jEweAwNjWN205pJDzEcWKPseR+YmqUmeEcr37xGWYq8/bbaRPDlmk3ftvcliEwYByHidlZXEfF8jc5iaxX7SukSTlOX2lLQtq2PSMBU8okKoY7l1zcme0qjMBr7rkTbl22cURXNjXOa4lNdUSoOB4Rxmem460ZYw9xYjJPKCJXKU5ODTOW9VBK8SNHdkOjbAk76Ynael9TFGH8GfldY4yOjXSRdWdoHPYWUqTcDUjbGKbaaRVRidHbMQ7Zue0qMHDy4CxTYSWWvonyYiub0o7aDSYu7RbGx0nlMgl+Wn0Mpz1ePDPSBtaXH5tlpFGKUym6k04RQRuhZgy+gRDIFIYY2z0BEiUml7IKUgnTQ6l47Uo/abuaM0RJ2xhGJdL4O5Xt7U2TbKaJrXd9hue5vHB2F/jVHh7p5LdCsYlAm2xUOK7L1N5ZO9vbRnASJKwYSnu8cM9wO61yaHyE07l4y/IohsY4EBUNTYFmnEoxymH33v14Joo/o7NkLO+6TObStje4j7Ttxxn0NnTvaOqkD0ytuz6jX+NzvD4jMoaXHd8b84juUkCtICw0Bt90YCs0wr6jRyAM7RW05G6L3B3FP94/ZrtBYoebHB3iBQWFK35HbUnrszS+th0vQbxiYnx2hkw2HfNaa2gUI2mX8ZwHqr+07ccZChXLXmOTBkrtTPq9f21ke2v6BMWpmUkKUa1TEEp6iQhGFIGxlcQoVlvjs7O4rpMgdroyfK89Nt6V1U97LvccmGSsvmRjEpPwktg+dRMXxgRUKsX+I0dsTJJocBhNu0zlPZtCl+7vM5AzYpiyzyUSYvIcyN5N78Qmq9dn7B7Oc1sWW9Y1ic33TXsVDE1si1CrETuVyTA+NdnJEKtYYQEThRQv3jOU6Cixs/WuYwfYfesyZJ0EZOl2naQWd6MEKEIjHDp1GkzQCRKNNchkLpUQNXHQF9/vxxkSw5TEhSu1kxzS4eOd24ltYmSYE1kBCbtmbXsWxcFbYKwxLMoY9uzfBzqMvbPzuwA/cngCTzmr1mcM5fL88IFdUJq3o6ujuApmg8VAG+raemNoIFXIs2d2GqKwrbjymRRDKXegtLX2V51tOJIwpQQlptMHtlMGkbYH7MxObEOFLIeH07jG76wbafdrWTUTtfBdBC0OBtizbyaO1aQdiziO4tWHd6FZvT5DEF7/iheTfuqbtmm2zSPxBDBCydgscwAE2nDbyROosGGNJ4ZDo2k8pTYmbXtgquUZ0hvbbNtDZGd3YnMUnJ6dZKS+HKfje2KFOHKvGRUbRDAC+eEhhobyHZms4MBIlsNj2bbFu1YuYdg/Nc5Ld6WhsRJngHUiULTEXjZCQ9tu/MLkBEOFNARVVNhk37Bns3cbkLb2uW6YMqj2d5KdJfWd24lNUJw9tJ/RhWuQjvE9Ss5cO3trmLhnK5a/Xopd4yMQBjaq1pqXTA8lNh1Qq5rYtIa3vOoeuHLOrnEMtJXBUbwwxRcWw1aXI4RuhvE941BbwqmVeN70UGchdR9paxLSVjodeDH6qvaODzufOtmEtF1rTZ+Klw3vnZpgIqyAihKzVne8xRhCDUUNdWNoiGBcj7GJUVRQg2YdL/K5c9K25iQ9I9nEhoIzB/ZyR15DUI/bTiPLE5HtB458zS3fcMXXzIfC8OHjjE6OcfTAbu6cHl5T2vbjjI7UFVTckGczxXrdTjlnM5J3p3ZiEwWe6/KKEwfgxiX74rhZuj17Q4FQU4ybp+cCoRQJIzN7GcmnobzI/rTm9t2FNrkPWp+RTad4w5mDFOoLNgscBNAMoRnYv/0QaQYs1UMuNyKW3AKTL34Zv3r/ixJ1tPWlbZszYphSMUyJCFprIm12kNQ3IW3XWtPXWp9hRPOGH3wpPPWIjRMi3eliD1r/G3QQUWxqzvuai01DPTPMoXvu4eiLnscr7z7LyYmhVWv6epvYXMfh1c87zn35EOVGVm4HPvg+NBNHo0mj2uDWSoPTOeHecS/mgcHSth9ntO635qXWmiiKCKOIWm0nUic8Nzux7ds9zr++9zTcumij9qYPjRD8xNEMkUaAXw+4UvN5qhKwqHJEE/v4iaOTpD134PqMZBPb6FCe337ND/B6PYfjRRA1oVaDSgXKFSitQKmMKhW522nwzqN5W0dbR9qSkLYmVmNJmIqiiCiM8IMIY3TU3Vu1mTWGfRJaO70Tmxb4pde8nKf+61f5qq4SkrNEK57dbCBq9V+1ah5CUwlFF952dIgzw57tktzg+ox0yuP9P3YPP3ppgS/eqHNlJaARBTayd6CQcbhnepjXHR5nLJ2yxtiMtO2BKbtXviYII3QU4Xnut5RSzXXrgINu1arsCcPqzTCM7KxbtSh/nb3QB+zEltz8S7Tm8mKZj3/rCp+/oak5+XZx0OapWicQcIRDwyl+9vgu3nRkNIYlaXtCC6a676/um2rtc1UNIqqhRoCcYxOUnuPEq7hjz+iSth2YspdkOtIWWQ1TYYQfhARBQDabXhgbK7zz2JHbPqOU0ts2iKiEtJVWr9FqaSv9du9Ug3dia9FcM4g4v1DmT568xdcWGlxraAwwkkmxdyTNsfECL987wt1TBSYyKTxXda3pU4k1fbRq3gOb2Ex8rfG1xwtvWt9F4snW+lWF1vfq3lhfutNTPQQeBhG+72PEMDExEhpj3jt/y/vAy19+aPseEoS23ry531xi0zuxgdgGnxihkgonri21fznQSDdMtZYetz0j0WsrPX1TttIXxxWtidUK+hIwZT9XMGJIeR6FfBYv5bXhSOt4Qhm7xYbjqHiZSkQQBDSbPrVmo/7g1/7mQ29/6y++ayMZeG/DpL5DG9OvtxObUarTK92zXUVSTa3JGZttYuuTDhHsTPcDn9HRIaZ2T6AQfF8TBCFBENFoBmhtlzZkMmkyGQ9jDKXyCvPzSywtF4tXLj/7kV95+y8/sNE2B2fDpL7J7So2t8nw4J3YetMhsoE1fZttYuubDjGGarVCs1Fl98S47T8OIoIgpOn7NP2WMRyymQzZbBpjDMXiCnNzSxRL5erFC+c++rcP/tXvY1NlG7pt4iePtvCbS4M4YxM7sa21XcWgNX2bbWJbVelDCMKA6zeus3d2Gtd18H3rFX4Q4AchUWRiz0iRyXqIGEqlFebmFqnVmzzx7Uc/8eEPfeB3FhcXK5spcaxvkDaUbO43lwZxRq9nJHfv7IWpjWxX0Y8zaHWH9OGMVZ7R4ow2gVt+aDQaLMzPcWDfHnw/xPetIZrNgEhbkZTNZMlkPLQ2lEpl5uaWKJVWVp588lufeO97fu03WC/o2LxBagke2fgmw2txRvf2253zdu+n3m2MjrTtwFSXMTawPqPTkMCqJrZ+DQlRGFJvNIgibQ3hB/h+SBTDVCaTio2hKcbGKBZLpQsXzn/kM5/6498FqmzhtmHI2swmw2txRgemNrgT24Clx2vFGZtqYjOqT9aWdtuo1ga/GdD0wzaBZzNpMllL4MUYpiqVuv/MM0//0Zf/8ou/f+HChTJbvG0iUt+4tFU9e6Gvu8nwAJhaS9pKzzKyLUnbdhNbIgKPf+uj5SnaaJqBNQZAJpMhk7HSt1gsMze/SKVa9x979OGPP/Dedz/A+tuZ7IyHbPY3lzazyfBaO7Gtlw7ZjrTtlw6RRNVHsMvhrDEU2ayVttromMAtTD1z7uk/ShhDnnODxHN3w9J2rY3pWzC1Fmf0k7arYGotzuhqYuvhjF6YSlT6OgIkuYzeBn1tY2htPWNuiWKxXLn47LmPfvkvvvLBrRD4NgJD2ZS0lQ1IW7YpbdUGpO1Gm9h6K33Jb+8kgj4RoVRsSVufp556/D995lMfe/92OGMLHiLtJNtmfnOJ51jarsUZ60lbozow1U6Zt7O2HQ4RwHHddgTe4oxSaaX6xLe/9cn3vvfdv75dztg6h2zyN5cGcUYyAt/ITmxbSodssoktWelLGoMYrow2FEsl5uaWWF4ulp599txHP/zhD/72TnDGFgJD+sLUVtMhLZja6E5sA6VtX86IPSPRxDZY2vbpDlG2ENdqM7aEHlEslbl1a4mVlUrj3LmnP/bg1/7P7282At9hDklU+jYkbU2bL54raWtT5luTtkmYElEdrzHSrvS1y66hZm5uiWq1ET7+2Dc/8cB73/2+nSLwbeayNvubS8+ttDW9vbbbaWLrqfRFkc1ZBUGIl0ozN7dUeubp7/7xTsQZ2zaI6zghSGrTv7m0lXSI2aF0yAakbbKJLVnpM8YQBiH1ep1sLosYWXnmme997POf/28fBBobqSNth1fWNIgpFELK1SsIRzb7m0vrSdsN78S25nYVa0vbDTWxmdVl12bTZ6Vc5oV33WX+9usPffZDf/h7H71582YTyPUMdp/dBrrWhHVWcKx+fX8H6FNBdOLHva99+cvq7rvvHtu1a/xuI3itke63z1dXcbPnN5dEeqGvt6O+Xx9x94KhrhV6fe5Lz+4L0q/MukbZ1RhDGEb4fkClWsXooHHx0rkvvONt/+bD1WpVgAIwFB+F+MgnjlzPkcVuQpfCbq+WisfVbS8+WcdDWi/yWsdDDz2U+tzn/vMXfuINPzV68MDB12bS2T3J90jvRJH1HuuzoUHv4G/QsH2NM8DQ0vdCksawJO66LmB8v1m99uQTj331gQfe86fAODCWmOldqyJ7/k4eOj4i7Mq53v9bz0VJL0puDtVljIRVU/v37x9561vfcWxiYmKXiPHCMHKjKHSNEWWMdowRpXXkGhPHv0Y7EmOUiKjWYZ/r/C3SvV5Y9z5uiK+7O6O22Zvp2jyx/SBKWWYRUWIwGAN+o6a/853HKw899NB8o9Fo9sDNWgbRif9Nz4AHsRH8+P/kESWMKL2/iuck3CoVH16f/5Ou1/IsN2FYJ/G36nHR3p+2GvT/IPJU69yXdchVBtyX1UXrvlywGYOs5SFR/HyY/Ix+Xy45uEkjOYnHXZLrhrtfm1xTvOp3xXoeZ4BxBg34Rgy0GSNshJzp8ZLex5Nw1e+xJISZBEQljdi+hvUgoN+s7je4/e6rPobu/Zs13rPe9W1Hem7ES9YyUL/3mDXu9/O2LX2prd62MtO/X9coW3ytbPNcG7r9P2ihyTu2K4x0AAAAAElFTkSuQmCC";
    docxFileImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAApVElEQVR42t2deZRkV33fP/e9V3vv3TO9qWffZ5AACQnLhIAIETYxcRzbHOMkeMOEEDsHQ4ywYwzYDiGOIbHBGAcLHNsh2HFyiENsfGQfAwIRgUBIII00o9mX7p7uru7a33Z/+eO+V/Wquqq7ehEnx3XOm5p6VV316v7u7/v9/pZ7S/F83+68M3Xy/rcc33/3fYeswE0tVzwGh4bFdlCEgA2EoTlswAshfiL0Wu/jeQzk0lxfWJTLT31b3bp6tTI9uffJqw++8wZ/i27O8/z+1typf3BHdmrfO1NKXlPz/AE75eDrEK0tlIgxgAhoDaEYw4iAeKBDRAQdhgxmUriVNdaWl6mJIhgaq64WJv5k8O3/9QPl33jD2b8tBlH9vKharU47TvrNYajPAOlN/0BrwsDnkZtr6p2PLByYGhk6M79Swk1lEd/HzuSMAYLAvD4MQIeAgB+AaHMEAVpCxgdzVG4tMKKrvGo2y/TsLMNje8hk0v6xPUNPzo3krrd9fPOffm6CtP7bx6t7PVg3COYlyvLSjvqW1vpjhULh5o4N4rru7Z6nP6m1vl1ErH7+xvd9Li2t8UvfLDEwMMjXzp6nkpugfv05fvcfnuGnHi6TUmLeqVYD2zIGqtcgZYPrge8hKZvJjM2oqrM4v8i77x7lzIkTzMzMkM8XUAqUUusHrMcIS5dBbJpDephKNjKG0P3jWkZWIKB0Npt6IptN/Vgmk3liQ0jZcDaI5F03+NdhGL5IRGxlbnQ7otfjex5Xl9f4jafKLDY0j5y7ykp6nFoo/Mjts7z+rqMcGstQ8kLK1QZlLMoNn3LNpSwW5XKDshtQ9kNy2kcFNcoL8zxw5yinjx9namqKbDaHZSksy2p9vlIICkShUCglLYOte06hFKAEUKi289HrlXk6Ph+9mXksoJDoddF5UWamKjF/G70vopQWsesN90WVmvsOEclv2yCVCgXbtv4+rJ+JXYyH73s8c6vErz5Z5lxF8FwPPz8CgQdK8UxFEIF7Jgtm6tg2gkKU1fq/7SAoJgZz7B3KkK3c4k0v2sOLTx1nOvIM27bWe4aYGWoGRbcBQPtz8Qw2s1hJO1ZIPL+1MRbKgE/ztYk3a/6pjt5TxZ7RelJHhhcBJfI9lUplYNsGUQobmNgUV0UIfJ/nlsr85rkaqYEh3FqZanqA8MYF3nd7lgeOW2TrRQR4yWTOfEsnBaLAccB2zLmUw6GpUUalyvKtJX76zBgvO32E6dlZ8vl8V2NE49HVGO3PtQBFmqPVDsIGpqT5eulhdR39UfxxSWO0zpkraL29GrMsy942h4jIVLXauBkEYVcPERFjjCDg2kqJD5+rc67kUy2vUctP4IXC62ZTvPO795NWkLYVGUuxVmvwv775HF94bpkvLtVZrjuUyKItYdhvMGrV0eVV3nPK4vTJUz09o23CKkmAuOp4Ljncgojq6RmqwzNIeoYS48VdPSN6z6ZnmAcKEK0QhHTaIZd1ZjYi920bJDaG7/tcWanwiYtVLtShXq1RSg1h1dfYW8hQCm2u+w5zBZujw2l+Yp/iBfumeGq5wVDWZm/O4g+eWOQ9X7qONOocHxbchRv85OE0Lz15iKmpmaZnWJbV3RjdPGMjmOrwDEl6hiEApAtMiUoYIzofe4bhi5ZnSOwZosy19GmQbcchMUxdWC7zicsuK2Gaeq1EmB1g4OazvOPlxzk4PkiVFG96eIUr5YBnV10yK0UemNrDh75+i8tVj2FLeHqlzp5CGr9+i3LR5U2HM7z0xEGmp2fI5fPY1tY4Yz1MSU+YSnJGEqY25wx6c0anMVQPibdbBolh6mqxzO9faTBfh2ppkdToNN6VZ/n0G+7Fc3J85lKFhUaAFwqZlI1lWzxys8ZIxmYob/PkhRriWGQ9l+H6Leqhzy8ecLn9+HHDGbk8ltWu5PriDN2bM9RGnBFD3UacIa2r6MUZ8QORFoBKP0HOVg3S4gyf68UKf3CpQTGwcetrOOOzaIRX7htlenSYf/7wPE8uewS+MJBRDJYXsQojPEWOar3OaMomk3VI1+scKASUPc27ZhvGGDMz5HK57XNGp2f0yRnrPGNDzmiHqSRnJD1DRQaP73bdIDFMffJSg5u+Ra1awRkax3IsgiBkamzQBJSBkLEVjlJIEPDG48PcNncbb/4r+PA3V/ibeZe5vI3fqCPFJX5qBu44doyp6cgYm8EUsnNpKxsYgw5jSG9pK9HnqaRnqJa/iai+PcTaijF83+f8cpmPPFdnRaeolsuogVHchotXLSMCZ8uCpeAHD+TI2ZBOWSBCIwjZk0+TTjt85oaH6/o4xeuk/Cr/ZEb4rqP7mZqeIZ/L49j2d0baSjtndFq2zTOku7TVXQhckuCn6dMUWzBIbIxrK2V+53wV18rwzNUbuOk8XH+WN4yu8Wt3FPipWZ+yleVzF5a4f26QfzZnIQtXGLv1NK84Ms0Xr1UZyKXJ6oCCX0aL8LaJZV5y9AAzMzMU8gUcx25G4PFs1hLxgpI2Y7Q/l/QMbWCqgzMkQn6lYzWl2oI+pSOYUgodR+RNmNKRtE0aQ5oELjoBU8oYXHT0Pqr/XFkfkGVg6uJyiQ8/U+GKa+PWbpEdn2KqcZMH7j9NamiM5XrI3XsdXnYo4PfOV6lS5b4jk9x/aAytbL5yy+Nz11fJ+i6ZsIxTXeSfTnicPHIygqkWgfeUthtxBtuDKTaAKaSTM+gNU3SDqVhh9UkgmxqkCqHv89TCGh/4dgnPzrBWLqHzwziNGv/4yCiN7DC//tgyK75wdFDx5uODvPX0CL/6jSKPLLqM5RzKjYCnVl1Eh+TK8yivxuv2aF508ICBqW1I2404Y/ekrWwubbXxsqQxmjClYntI37BlbWyPKudulfjVJ0vUxObiwjIVZwBPLCT0uGNuLw9dqzHvhjSCgGfKmg999RrDGYd7J7M8verzxes1nlgNcBselcV5FCE/PFjirkNzzMzOUsgXmsbYkrSV51/abpQOiR+0A6gxho4soaJSxG5xiP0Dv/WZ2Q+ereFZaa7OL1JND+Irm1CBS5paGHJyLEXghVi2RaiFp28sU617FFIWGiHlKMJ6hYK7yrgu8yOpeV5w7DDT09Pkc60I/DvFGbFS7cYZLQLvzRkxgSvpzRkoUCKI1uhEYlZ2YpCBl/+jY4tjBz54qexxcWGZ5fQYQa1CtrqMVS/j2Rkeuunxqskc338gi66UqC0vcM+oIp9L8+Syh2jBCX2c8grpapHXDFY5deI4U1PTXTljS9KW51/absgZ0psz2mBKqa1EFr055MSPvevtsxPjL3/qygJrVo761XP8yisP88rjtyFOis9dLPI/LtU5M1njp0+N8fKpDLVanWN7hvjMxTKfv1kD36VRWmIgqHBfocIdB+dM0JfN7ULWVjaXtl04Y0Npu5V0CL05owlTSnVw+g5SJ7N79tyl62s0UnncS+f5/ded5HtefJy/vlrhRCHNL9y7n6NnV/j5R5b40eND/NDhEcYKeR48V+Jz8w2ynke2uojt2Hwv85w6GGVtc/ldydpqExjsOGvb+rj1WVsDb51Z2/YIPPYMFY187Bmtoh2I6J17SLm0xlojZDCo866/d5j7bj/MGz57kYeuVRlK27z/7nF+4MQEX1/x+cTZEg8+vUYmZZO2YIiQsLJEyi/zmoEyp06cYmp6mlxU6dsNabuRMXZN2iJbkradMGWMYSqXIjuMQ1xtkR4apJCyufvYHIGGWsMF0dQDxS98bYkJJ+BVc6P82ZUKIaCDAJuQjLtC1ivx6rGQ2/cfbCPw70SlL3697mKMrUlb2bq0bYOpJmu1N1Nsh9QLhUFAuFgTfvavrjBoKz7y6gPcM5EmQKjVPB45f5WhrE3gBfhaM2iBU16iruG1AxVetH+G2dlZstkcF6o+jxR9Hl3xWWiErS/wfFT6kh6wA2mrtiptI2PE2Q2QCMroO3/S00OslA0NF9GaRxdc7vnk4/z1j97Of3rVPn7ysxdZu3aON979aj51fo2qQK7ewC+vMhBW+fHcMiePniI/todPXSrz8aUSlpVlIm0zbisGUhYH8hbfO5nmSMEhZe9upW9rWdvelb51ako61FRPzpCIC83F6C30GfWO1APffOkwxAp8nl5W/OD/PMtH7z/M79w3SYo9fG0N/ujsMpkgYNapo2plXj/pc/rwaVRhmF97cok/bQwQOmlmMjAo4FsWWlnc8OD3r3rcPRry6j0pBlPW/1fStlgPuLRWx9eQS9nsG0wznHYS0EVPzmjCVPPSdiN1IhI1q4UQmIrPV66W+Zd/eYFPft9R/u/NOr/48DUKaRu3tEyFMj8xo7nr8H6mZ2b4xONX+N8raXzHXMiSY5HVilwIKQvTxqPg0WKIo+A1k2kcS+1KOmQn0jbUwp9dXOVTj1/jaqlOYNlkMg57B3O87vAYbzgyhqOsHtK2HaYkkfntU/VuYBBFq4tQaaj7hPksX7hS4u/8wVPcDIRJW0hX16j5DX5mrsbpoyeihoQ8f/zkZSp7z5g2XYGGr1mwFAVL49hgaRtRQkbB14oBJwZsjhScXUmHqAQBbbXS93+ulnnrnz+DrtdMJ0zahobD+WrAV4suy57mbacnIk/pwRmopjFa3LjT1EkQRJAVgOdBSkHD9NteL9fZo30m7AZuZY1fnKs3m9hy2Ryr5Srni3XjWXH/bqgp+5prnmbZE9Z8TU1DXaAYCBdroRnk3UiHoHumQ7SiZzqk6Ia87eFr6ErZfKCV6Fy0wRf49KVVHl9poBKJXK0laswzzXiijRcqZd5Xa9UvYm1gEBHTb6u1ufd8cx8EzOYthq069soN3nLQ5kXHj0RZW9OqE4qm4foQavC1YbXA/H/ZFa54mlt+yKqvqYbgaThXCZvdGd/RJraIMxSKj1+qUp1fbuUNZH1fzpWqzzdWGh1BXzuBS0Lnab1LqROUMvyho8PVkFUcGi2Qqt2iXHP5Vyds7jq+n+mZ2bay60Aui+1VCZtGtSG0zIwLQtZsRUaBowRbCcqC85WQUAQ7hp7vZBObKBTC5xZcaNTAtltj0LwG1byWC1U/UoPSelmSt6MPEYmuQ0nfKfieHiK+B7Yy0BVB1oh4hNUijUqFf3s84CUnDjfTIY7dqvQVshnODKZMC2kgxlO0NtNZK0KtKYVQCYV6KLgair5wtR6awdlhpa/dMzbO2koEJ0UvZHG1HnXedzRgx6nl6E4npG0MU1q6w5RSghbZBciyLPB9MxSiGcHn0ESG0doi7ziZ5tSpU0xOz5DLdq/0fdfBSSgVzReMu9biWRwqXA11Da4IXpQ3+uqq33KIHaRD1BYrfaLgci3Er1TbvdLqDuWTGauttz2WtvEVtHlGnBTbManraFaHAUcmRxhVNWrz13njsUHuPnmEqenp3r22Anfun4TaWivVqRMzTjSBaBpa4yEEIoQoHl8Ne0tb2UKlT3pLW+nSxGYhXGxovHIp8gbdPkKSSEcrxb5CqnmB3Tgj9gxBjEjdFZUV+qAUWRvc4jz1Uom3n8lz7+kjTM/Mks/1rvQpURyaGGYmExoPCaW9rhyNYwOFrw2qaeBGQ7Pi6S1X+sB0hdsSo0qTMfpqYgPhSk3jl6vJWdM1uC7YFhMZ0xXTizOa0kTauUd2EqmLsrADj5kBh9VijXfdPcodJ48bmNqkiQ0Fe4eHOJSBG+JDaIO2zBECygINDW20QmApQhEcS/GtcsjfGbeanNE7Atcsu5rztZCzFc1NV2MDBVsx6ChGUxYH8za3ZRUDKRWBWvcmtlDgYqmB77ktLRt5shmiFrnvydgMp+yWtO1QUy3PaPGgFt13PqunQXKORRDWCZZu8JNnRrjj9DEmp6b7bmLbOzbE4Rw8rCOS1AkeiLtZIv4IokFRSvF0KeBl486G6RCF8Ohijd86X+XzZcF1HMYyNhOOzXDKYtBW5C1NwQ45lle8YsLhxIDVtdIHirIfsrxWN9eoOqt87SM5nrYZdKxoUU53zpBEtqFzFdb2k4tulaUbV/mZ28e4/fCBZtm130pfJuVweu8g2ZsNGirXgiwREAu0kYJ1sfARAoxCutYIqfqagq16pkO+vljhTV+6xXWcaDmcxZJlUwe8aFWTsoyy+nZNuHnN44dmUpwecNY1sWlg2RdulWodkKoS4N86vzfjMJS2iXPA6z2DxEQSRAuqlTyhtl0OuTS/wo1bK7z09NHm+oytNrGdum2SQmkRLGUCQx2tto3zClqoa8HX4IsiRKiGcLOhe0rbehDy7kevc/2rfw3nv27iHD9A6gEVN+SaH3Ir0KwFQlULnihWQou/uBWy7OuuTWzFQLhVrrWuC+mISlVTDo9nbQZSZqll0hhaYpgy76lFt4wRT6w+tG9Pgzz7+GOBPTppPGM7lT7gBYfnGF1dgBQdsNUkKjwRvAiyAoFqKNx0dXNGqQ5y+uKNMuef/BZcuQLf+DLcvGBybn4AfojrClc9YT5pFOBSXfh2OYxigkSlD6HohqyUamaydOKLai0ncJRibzaFo1pfIyltO2FKxZ6yhUa5ngaxdfCIyg1uu9InGoYLBe4cy0LYaJ1MGkZrQlE0ROGLJkTR0IrrDW0SzB1WD7XmqcU1VlZKkB80CaYbF00A6vngG2/x3ZCbnjAfCGuhUNHQEOGxtaApU2Pxo7WwUHWpNtyEEhTWratWkFYwm7ebTq6Uqe1LYvmDpuUZzaqlyM5l74lDR97jKHVuW+szpFW0fNWZI7A0b0Y3Ho0YGqJcUEMgEGX2DQBu1IVqKOvSIWtuyNcuL5mJnM5CtgDXroB2o4xCVCoIBc8TFn1hJRCqGlwUT1eFZU/aK30Cl0uuMei6WCwREKHI2Iq5Qrqjs6WdMxLlxSgFpHcnDnn0/W9c/uxP3Ht/2LFOeytNbCLCK198gvTiNZOG0dqkUZocYr60qw1sBRpChBtuSM3X69Ihq27A49eL5m+dNGTyUKvA8vVWhjqMjRLSCDRFX6gEmkYInsDfLAdYiSa2EHhmzQXfNY+SLSeq/ctngNl8Kia1npyhlIo4JUrPqEQltLaDVtLb90/WZUdNbLBnZJAzBQu034IrnZCC0pK/ISZiX/EVS35HOgR4YrHEwkolunIbnAzkBuHs42Z/CUkKBzNDylqoiYGsAMXDKyH1sFXpE4ELq6aHbJ08TXgyImQdxd6s3VPaqmRqp5kEVYmOkx2Q+jrv3VYTm6CB+07MoaprLQ5JHloIxMxeX0MoigA4V2u5utImCv/zSytQr5o8m+1AKmUMMn8DaqutCmfivX1tkpgNDb5WlLTwjZJZxKqAWhhyea1mPCuZC+kI2BGYG8iSdSwjbUX15IzYM2Kl2HcPUD8Gab7VFnptddyQoE2x5v4XHsEqL8csGh3STHxpLQa2MLAlCE+Xg2hTiiiI1CGfPXcL3IZ5H9s2sJXOGYJ/+jGM/IlgMWx5SzmMkpgIGotHimFz9l8qefjVBKHHkKUipo4fa+HESDryjBZMmfWMRtrqSKjEMKXjAVK71v3esshOmtjmxobZn5YuHtIqd7piKnJhlP67VNPUQon3t+Chq0XcpWKiNmGDnYJ0GgZH4PolCOpmpidjnVBwRahrhSeGj256cK1uAPGxNR+qlQ6duy7DCSKcGsslCLu7tG2HKSGRtOnLSSw2tUhHh8A2mtiyaYcXTBSMPI3rInFKIcJ9TwQ/JnZR+AJPlcNmYv2PnytCec1wh2WZYNO2IZWBTAGsFCxcNh/e9I4oGtdCVQRPCz5GCp+tmszyN4qeEQbNKqluYYNOGgTOjGQjLdJd2nbClEKMUNtCX1afHCI7amLLpFOcmchhhfU2fG/CVhQceiL40b5mSim+WQpQKNbcgEcv3ALPTWCjiog9gq3sAGrxKogpM5sUclx/Eeoa6hjxUNeK8zVNIxQeXyhBo9Eencu6UiUA+wfT66StTkrb2Bg66jSJBkhJ3w7Sj0Fkx+szUrbFsZE8g+ISTdlWVCzmW4k2QWEg2hQZsfhW2ZR1v3JjjfLCovEIS5lssa1MHstxDGxlC1CpgFuKeCTag0ub4MaLiN2LpPWFBlyrBVy8uQQ6WL9sILngX+DAUIa8rdZJ226codBopaIxIapa7vIq3B01sQkcmBhhyjI1FgMptOok0evqYnJaQVR/rgbC1brm0WtF6sXV1iywoktXFlhOJH9zKM/Hqq4YeNUJ2EIIRaiFQgPzGUsBPDRfg+Jye9kW6VoLOTacjWy0kbSVlmdIonK5SyuoNpC9W2hii2ba3N4xZvyy2aCsOQDtOr8h4Ikm0Eb6ilI8tlTn7JUFQt+L9qyyosuODGLbkEpDOo+ks9gL100nng4jtSXNIlldTOnYE0Ug8FjdgbWVqO7RUT9P2kbDydFss47SS9rGnNFsT0VMa2pzg4FdikOasncHTWxDhRzHcxbKSgaIkaeIigpFghsHiGLU1sPzVZ65fDNOHkUHLeMo28QkThrJD+Is3gQ7aMnfJjkLrja84WMoRqUzjBcyhnfai+7tPCiaF0zkEyGAbhqvG2eQaD1qa+zelTikq5raehMbwIsPTGIt3TCfGkfTsZTRKiJfZYJEUYQKFhqaYrlGM4OnInVlRXxiReSeykAmz/G5GXLXnmvf7TQ0xC4hlAUaWvC0wtOaoy96oRELOkzMYtWa1VqwlOLAQLotHaI24AwVeUY8RjrmENlNDtnhTmyihZcc3499+Rw4ViLNkZC/CI1I/mogADLZNPlcKq7WG8mrIkMoO+IRG5wUdjrLW177Mu5oFEGFLS9ptiBpalpo6JaXjM3MUBgsGBEQe2AbsSv25B0Gs/am0lZFEyyGKSVGIj8PsneL6zN6NLHdtneMo1IzSTydSMOLNGHLF6KYBEIRUrkcheHhdldVURyS2AsRy2ZuYoxDe0f58ZeexCouGHIP24PRMNRUo0A0ENDKYvrAfmOQJmSptlLuTCFNOs4Qd8CUbpO20hoaodkaq3alDWidMXa+XYVSitecOQhrS5H8DVtqSAxsaS1G/mpNIBYai5G9k9jNlEbCQywr8hhD7sf3DjM9mOO195xmurxovl2oI9jSzcZvk9vSeBq0ZTE+M4NtqUj+JieR8arbCmlSltVV2pKQtk3PSMCU0omK4e4lF3dnuwot8Lp774D5yyaOaMumRnktMamOABXFI8LY9CQ2CZlnRWSeUES2UpzYO8hI1kEpxfcd3gP1NUPYSU8Mjfc1ROFHn5EfHWF4ZKiNrFtDYzFbSJGy+5C2EUw10yqiEqO3axzSv7TdrIkNDSf2z7DXL0fSN1FejLMpzahdo6PSbmFsjFQuk+Cn9cdg2uGe6aEmsL7y6AxD9dUolRK20ikihFqoao2rwQcyhQFG9oyDBInJpYyCVMLkQCpau9JN2q7nDFHSNIZWiTT+bmV7O9MkW2li61yf4Tg2d82Mglvp4JFWfssXkwg0yUaFZdvsnZ0xs71pBCtBwoqBtMNdU4PNtMrBsSFO5aIty4OwtZW5GLXVEGhEqRStLPbMzuHoIPoMmhySt20mculoX9710rYbZ9DZ0L2rqZMuMLWT7SoCrXnFsdmIR8I2BRQHYb7WuLoFW74WbjtyOOo1lpbcjcndUvzduRHTDRI53MTwAC8uKGxxW2pL4s8KcUPT8eJFKybGZqbJZNMRr8VDoxhK24zlHFDdpW03zlCoSPZqkzRQanfS791rIztb0ycoTk5PUAiqrYJQ0ktE0KLwtKkkBpHaGpuZwbatBLHTluH7/qNjbVn9tGNz774JRmrLJibRCS+J7FPTUWFMQKVSzB0+bGKSRIPDcNpmb96JdsZu/z49OSOCKfNcIiEmz4Ps3Y2d2PYM5jmQxZR1ddjafF83V8HQwLQIxY3YqUyGsb0TrQyxihQWMF5Icc/UQKKjxMzWO4/uY8/8ZchaCcgKm3WSatSN4qHwtXDw5CnQXitI1MYgE7lUQtREQV/0uBtnSARTQrxluex+HLKbO7GNDw1yPCsgftusbc6iKHjztDGGQRnN1NxtpgmcOKdlBuH7Do3jKGvd+oyBXJ7v2TcKq4tmdMMgqoKZYNELNbXQeKOvIVXIMzUzaVYgR4orn0kxkLJ7Sltj/6hiaCLgFkwpQYlu9YHtlkGk6QG7sxPbQCHLocE0tnZb60aa/VpGzQQxvosQioUGpm6bjmI1acYilqV47aFRQtavzxCEH77vHtJPfc00zTZ5JJoAWljVJsvsAV6oOXDiOMqvG+OJ5uBwGkep/qRtB0zFniGdsc2OPUS2uD5jgzV9gsZScGpmgqHaSpSO74gVosi9qlVkEEEL5AcHGBjIt2Sygn1DWQ6NZJsWb1u5hGZu7xjfPZqGeinKAIeJQNEQ+5oW6qHpxi9MjDNQSINXQfkNbht0TPauD2lrnmuHKY1qfifZXVLfvZ3YBMWZg3MM37oG6Qjfg+TMNbO3io56tiL566QYHRsC3zNRdRjy0smBxKYDal0TWxjCW15zL1w5Z9Y4emFr7X1gcihLftzlCL6dYWxqDKrLWNVVXjg50FpI3UXa6oS0lVYHXoS+rc3Mdj91sos7sYmG2b3jjPtlUEFi1oYtb9EaP4RiCDWtqYugbYeR8WGUV4VGDSdwuWPCtOYkPSPZxIaC0/tmuT0fgleL2k4DwxOB6QcO3JB5V3PFDVn0hcFDxxieGOHIvj3cMTm4obTtxhktqSuoqCHPZIrDTTvlrK1I3t3aiU0UOLbNfcf3wY1LNH/qyE/MXl/ADylGzdMLnrAaCEPTswzl07C2xFw65AV7Ck1y77U+I5tO8frT+ynUbpkssOdBwzfr7j0PXB9peCzXfC7XA5btAhP3vIKf/96XJOpom0vbJmdEMKUimBIRwjAkCPUukvoWpO1Ga/ri9RlaQl7/6u+Gpx4zcUIQtrrYvfheE3oBxUbIeTfkYkNTywxy8N57OfKSF/Kqu89wYnxg3Zq+ziY227J47QuPcX/eR9mBkdueC64LjcRRb1Cv1Jkv1TmVE1425kQ80FvaduOM+HE8L8MwJAgC/CCgWt2N1Albk7ZNCbjJJsO37RnjZ192CuYvmqi94ULdBzdxNHyk7uHWPK5UXZ4qeyypHMH4bfzQkQnSjt1zfUayiW14IM/7X/dd/HC4gOUEEDSgWoVyGdbKsFqC1TXUapG7rTrvPJI3dbRNpC0JaasjNZaEqSAICPwA1wsQ0UF7b1WPslOvW6UiU35Qvel7QZTH2YWN6aP1d3GnTbXW4F/8t8/zeTWJT84Um2zH1N7tuP8qrnmYSVGw4eeODPDmAwXTJdmxpi+5com4ozDKuyml+PKlW3zmRo0rJY96EJrI3oJCxuLeyUF+4NAYI+mUMUYCprRql7bGayLPUO0wpUXQWuN7AZ7vEwQBExMjj+YyI6/Zv3+kuH2D+JWbvh+YWbduUX73vdA322Q4ufmXhCGXl9b4+Nev8OkbIVUr3ywOmjxV/AYClnBwMMWPHxvlxw4PR7AkTU+IYar98fq+qXifq4oXUPGNQXKWSVA6lhWt4o48o03atmDKXJJuSVtkPUz5Aa7n43ke2Wz61tjI0DsPH577L0qpcMcGEZWQthL3Gq2XttJt907Veye2mOYaXsD5W2v88bfn+cKtOtfqIRoYyqSYHUpzdKzAK2eHuHtvgfFMCsdWbWv6VMIziGvePZvYdHSt0bVHC2/i7yLRZIt/iSf+Xu0b60t7eqqDwH0vwHVdtGjGx4d8UO/NptVvHDx4sLEzyPIrNz3f1Ju39ptLbHknNhDT4BMhVFLhRLWl5i8HamlfRhYvPe4GU9LRN2XgJYor4okVB30JmDKfK2jRpByHQj6Lk3LMOa0Jw2hCabPFhmWpaJlKgOd5NBou1Uaj9uUvfOHDb3vbWx/oJwPv9E3qu7Qx/WY7sWmlWr3SHdtVJNXUhmv6ttrE1iUdIpiZ7nouw8MD7N0zjkJw3RDP8/G8gHrDIwzN0oZMJk0m46C1ZnWtxOLiMisrxeLVyxd/++1vf+v7+m1zsPoOQra4XcXWNhnuvRNbZzpE+ljTt9Umtq7pEK2pVMo06hX2jI+Z/mMvwPN8Gq5Lw42NYZHNZMhm02itKRZLLCwsU1xdq1y69NxHH374oQ9hUmV93bbwCztb+82lDTljCzuxbbRdRa81fVttYltX6UPwfI/rN64zOzOJbVu4rvEK1/NwPZ8g0JFnpMhkHUQ0q6slFhaWqNYafPtb33zww7/56/9uaWmpvJUSx+YGaULJ1n5zqRdndHpGcvfOTpjqZ7uKbpxB3B3ShTPWeUbMGU0CN/xQr9e5tbjAvtumcF0f1zWGaDQ8gtCIpGwmSybjEIaa1dU1FhaWWV0tlZ5++vEH3/Pud/4SmwUdWzdINcEj/W8yvBFntIzRPrXb91NvN0ZL2rZgqs0YfazPaDUksK6JrVtDQuD71Op1giA0hnDNzzQFEUxlMqnIGCHFyBjF4urqpQvnf/uTD37s3wMVtnHrG7K2ssnwRpzRgqk+d2LrsfR4ozhjS01sWnXJ2tJsGw1DjdvwaLh+k8CzmTSZrCHwYgRT5UrNPX/+7O/+5V985kMXLlxYY5s3p29S34K03Xhj+i6bDPeAqY2krXQsI9uWtG02sSUicKVaOy8IhDqk4RljAGQyGTIZI32LxTUWFo0xvvn1r338ve994H1svp3J7njIVn9zaSubDG+0E9tm21XsRNp2q/RJouojmOVwxhiKbNZI21CHEYEvs1pcXT137uzvJowhz7tBornbt7TdaGP6GKY24oxu0nYdTG3EGW1NbB2c0QlTiUpfS4Akl9GboK9pjDA0nmGkbfni+XMf/cu/+KsPbofAdxAYypakrfQhbdmhtFV9SNt+m9g6K33Jb28lgj4RYbUYS1uXs09/8/c++eDHPrATztiGh0gzydavtN2IM3ZL2m7EGZtJ22TWtpkybzYktDhEAMu2mxF4zBmra6XKt558/BPv/eUH/s1OOWP7HLLF31zqxRnJCLyfndi2lQ7ZYhNbstKXNAYRXOlQU1xdZWFhmZVicfXihfMf/chv/Yf37wZnbCMwpCtMbTcdEsNUvzux9ZS2XTkj8oxEE1tvadulO0SZQlzcZmwIPaC4usb8/DKlUqX+3PlnP/aFv/nch7Yage8yhyQqfX3+5pJSPK/S1qTMtydtkzAlolpeo6VZ6WuWXf2QhYVlKpW6/8QTjz343l9+4Fd2i8B3mMva2m8uPd/SVnf22u6kia2jISEITM7K83ycVJqFheXVZ599+j+/95d3Hmfs2CC2bfvgp7b8m0vbSYfoXUqH9CFtk01syUqfKbv61Go1srksIlI6f/6Zj336U//9g0C9nzrSTnhlQ4MUCgV/rVy9gnB4q7+5tJm07Xsntg23q9hY2vbVxKbXl10bDZfS2hp33Xmn/sqXH/nD3/yPH/jozZs3G0CuY7C77DbQtiastYJj/eu7O0CXCqIVnXceeugh9dJ77hkZGRm7W4s48Uh32+errbjZ8ZtLIp3Q19lR362PuH3BUNsKvS6PpWP3BelWZt2g7Kq1xvcDXNejXKmgQ69++fJzf/Jzb/uZj1QqFQEKwEB0FKIjnzhyHUcWswldCrO9WioaV7u5+GQTD4lf5MTHl770pdQf/dEf/snrX/8jw/v2Hfj+dCYzhbT+Rjonimx2rsuGBp2D36dhuxqnh6Gl64UkjWFI3LZtQFzPrV779lNPfP5973n3nwJjwEhipretiuz4f/IIoyPArJzrvI+fC5JelNwcqs0YCaum5ubmht7xjnccHRkZHxXRju8HdhD4ttaitA4trUWFYWBrHcW/OrQkwigRUfFhnmv9X6R9vXDYeV4TXXd7Rm2rN922eWLzJEoZZhFRotFoDW69Gj755JPlL33pi4v1er3RATcbGSRM3OuOAfciI7jRffIIEkZs+4JWAq7s2BiRcTrvk64Xe5adMKyV+L/qcNHkeTa470WeapPHsgm5So/Hsr5o3ZULtmKQjTwkiJ73k5/R7cslBzdpJCtx3ia5brj9tck1xZ2D33meHsbpNeD9GGgrRuiHnOnwks7zSbjqdi4JYToBUUkjNq9hMwjoNqu7DW63x6qLoTv/zwZ/s9n17UR69uMlGxmo29/oDR5387Ztfant3rYz079T1yjbfK3s8L36uv0/5UjhdPaKxSwAAAAASUVORK5CYII=";
    txtFileImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAkdklEQVR42u2dCXhb1bXvaUMzkgES2iQQQsIQAiFAgdKSUtIWKFDaXm6gvfTS9r729fXdW6aQMGR0yGQ7gx1nHm1nnuc0M0k8xPMgWx4ka5Zs2ZI8yGMIpf2/tfc5Rzo6OpJlW07ae5++7/dZUWxLOj/ttdZee5/jW27p9duT35g8c8ekN495fv6ve81v/HBD+S9/vtv25puH3W+8dbR+2lsnGK5pbx11TnubeOuQY9rb+63T3j5E7DHR/Sr6WjXtrZ1l035/qHLaz1dn/+sjf94/bci0DT954p29o2/5/7cu3b4+5t/in3p2ZdbBXx5xtv50hxnPb7XgX/bX4t+P1+NXRxvwxiE33jhYhzcPOPHm/hq8udeGN/dY8MvdJvxyZxXe3FGJaSnl+O1ePX6dkofvLTyNu985hAH/cbBt+IwrKX1nFDz0P+6otre3j7p27fqCtraOQx0d1090Stu1E61e74nzZa6TT202aV/b58RTa6swKcmAx9easCmvCbk1X+CS9Qu8fdCF72104Nl1VsKMZ5P0mJJUgSmJZZiyvBhT4vPxs00lmLroAn628CBiU44h+XwxDhfW4KS2/kt9bUthR8e1E3LaGG2R0nGiVaK1c1rktISjjeNtbj107dq1BR5P+6ioyGhruz6ZXkhRc3P7V15v298JdIbHQwe83I4f77Dhpd1WjF1WiruWlePB5VrUt/0VlobrmH6iDpnWa2js+Apv73JiVIwBoz8uw+g5pRj9USHump6Nuz/KxhMxWSTjDB750y5sS92N3NxCOBy1aGhoQWNjC5qaWgNoZDRKtATQwGgIpL6hWaA+GE+9l96LH7ccN6OJ45JwSTSizs/f6bGv6LUVsWPZIxk1NRhIlndKB5qkhIT9PzsgHncDcspteHmHCePX0kHmMjR4YLkGazNqYXRfw8QlWoxZqMPt8w14bVs10oxt+HacAYOmazHonSIiH4P+Kxv3zsjEE3Mv4PE/7cDGbXuQk1MAm62GHxz2XPIPQRPR2EQ00n1GU4uIIKmBBDU00P0Gv6yGRiaDvtaL+CS1wMMk0fPUeyQ5zXCzr/RvD8nwMBn0/y4Rt4vBxJAEgoupE6itY2Ia6Hd7dwAY2G0hra2td9KQdnUmg8HeuJue9ILGjudIxt0Uou5JoAO/QoP7ScZEknLJ4MU7BywYH1OKEbPLMODjSgybrceeAi9+t8OB/u9p0f/dEvT/cyHGzirAM4uv4un3diNx60FBht3JZbCDqRyVoWQ0hZNBhJLhkclwq8hgUSCUjDq3TIaL0UBSSEij10PH9Js9CFcYSXH2b5GMDg+9kMtaO6bstWDSNgNGrizBXSs1uI+EnNM34Udry3GuwouUbA9GzirFbTPL0G9GBQbNrED8eQ/eTq1Gv/fosfe1eCRWh8mzz2Pcu6ewdtt+kpEPO4UpFkKkMBUgwxtGRpNSRqtPRr1CRoMko94rjA7ZyOBCQsrwqsqoE2XUiULotf+N5eNuC6HhxYSEDFe+MOVpRJ7OjhcPWXDv5ioMJwkjEzSYtLoUOgpRxY42TFxYit+lmtB67Sv8bKMZQz8ux8APy/HoUhNyLO14bpUVfadXYuScMjw0LxMPfnAaySk7+ciw08hgBydsmOICQoSpxu6FqaCR4QkXppoCwxTlD5+M2kY4axt4ruo1IfKckVFhx0tHzLh3O+WMtRSOSMZ3NpcjzdKM61/9HQ+RjPvnUrL+tBQzDlcjx9qOpMv1+OR4HXSu6/jjvloMmKnHqPl6yiV5eHLWaSSlHKYEznKGephq6o0wVd+FnOEOnTPkI6Oujsm4AUIkGZ+XUZg6asaE7UZ8i2QMJxljKVRl2VtwsKgBhbZWbE53455PSnHHDApVH5bi+UQzfrOjBr+l6urVLdXo/6kB4+KMuG9eFibPS0NC8hHfyAgZppq6EqZaQoapwJzhDSkj0jBVqyajThLi7R0hUmmbVWnHs0dMeGB7FUYkFWP4ymKMpQT++jY9zpZ78ciCUkyO0aLE3o7l5124bXopr6QGz6Bk/qkO/UhE37lmjFpsxP0xVOZ+mo1NKfsEGZQz2MG5oTnDE0HOcEtCQueMWlGGEKoaef5w1tZHX4g8Z+SSjJ8wGalVGLlKgztXFGMcVVI7CzyIP+fE/TSfuJdGxd0zSzFxXjmuVLVg+mEnhpKMEVRZDaN5R98YC0bGW/DUqlI8upBkpO73V1P13u7njGiVtp4ulLaynCEfGUxGLclwOuv5e4qqECFMUTVV5sDUg0aMT9bj7iSqpkjG/SRjwuISzD9ZjUJrG8Z+TDmDwtMIGhVDiWeWG3DF0IZFZ90YTjL6f2bBg+sdeJDmKU8tvoLE7SeEnEEy3HQgbkhp24OcEaqa8o0MSUYdk9GAmmgLkUrbK5QzntlvxqTkCty1ikYFyXgojuYZi0oxjkbFD1fo0EyV1EiSMPwDQcaQGWW4fWY5vrPCiBe32NB3kQX3rLFh0vIc3Lc0F6t3HguaZ/yzlLaqOaPWL6OWRNREW4gQppqQR2HqeZIxeVsllbbFGM8mfLFU2tZ18HLW0/pXHCtqwvW//h3PLNFh/JxyjJ7FqMCoeTrcuciAb8SaMXo1yVinxdj4AiRv3/UPW9r680Wkpa0yTDVwGSyhcyGeqAjp4CMjq5xK271GjNlQgTGJGjy4nEbD2gqa0BlxL030Js0tx8/XmLDwZC06vvwK5yta8LmuBflU4u4v9uKxNWb0jacZ/HobHt+kxZMrL2PNziO+MNXV0rbxRoQpVxdL26AwRZAMlj9qajxREEIz9eamZlwqcWDKdjPGbajEPQkleGhlKZ5YrkWaoRmrLrjwLaqchrxLJS0x+L1S6Ouu4bepNkxYUImXN1nw8z0O9F1mwIRUOx5fm4tHEjOQuPtEQALvcmnbeCNK26bOw1RtsAxfmKoVRwfJqI6GkLa2tpH5eie+n2rFxPU6jFpBFVNCKR6O1aK66TpOlXoxeoYWg0jEqE/KMJBkDH6/FPvyGpGa04A7Zpdh8MJK9KGEfvdWK8at0WAMCdm08wDycgv/4UvbcAm8trOcIcpwijKiIaTPTxceffLXR+rwxFYzVVKlmJBYhh+srURFbQcyja0YSgJGflSGlTRKTpU24w5K4MOIP+y24zSFq9tIxq3Lq3D7egv9Dg0mrcnAth17eJjqTs7whaqAnMFgMkQBQTJohMjClJQ3PFxGMxfhCaqm2FdRRJAMoXPLWuu1IXKGPEwxESx/sK/unggZ/t3XJz67oSJtSooD41ZWYEJCOaasrsD7h2w8JL23146xlLBXXHRjV24jxtJoGEYz8ZfXm6iU1eGPp5zou1yP4Rto0rdRgyfXpiNpz3E+MqQWemelbbNPiCDjSrEVF/IsCsw4z8gN5hzHpE6OwFkl2UbOGYksP6ezDIFkG6CtcgbnDFmYquYjJgpCfpis3/rWcQ8eSdLjgQQdnkisxFVzC+Ycq8Fz8XpY668j3dCKTen1uJcSOpPxiy0mVJCsYbEV6LdchxHrDBi/poDCXSZW7T4ujAzWtY2gtNXbXHh1fTocLmEUNXlbEbcvH3NSsjFXSXIwc5KzQrPNz+wgrvrZKjArBHPpd13MM1GYalAPUz4Z9XBUe/j8qttCfvOXBs2b+4w0Mix4NKEStS1fIsfUhuEUkliuuPPjMtgar+NPe+wY9lEpvpOgR03zl/jpXhv608gYsaEKD6zOxdjNxdiWuisgZ4RfXGqlMNCEqSsuYFhKC2ob28WVPvqZJj++UNYYDF8hpLwh0SCnnuHl1EtIyd7TJIawJiGviCuCLHxxXI18NVCChS6hpBVFKMKUX4ab0yMhr++xaX6w2YQXUqqRY2uHmUbEvydbKEdo0Z+E9Kfk/VyCAcWODvxxvx1275f44+kaDCB5ozfqMG5tEYWpNKzbdUgIUxGWtjUUp98/UIQha+24Y0cbOtraheXaLrRDolXahm2H1IXOGcqRUV0jCnH3QMhru+yaXxxw4UxVO1VMjXhxtQmntc34HZWurEnYn2bhAylMvbDJBAuNlN+crMHARB1Gb9Bj4qosfHt1GhL3nvIl8JBhSp4z6N8JZ7X45qpKDEpuwe3bSUiLKKRLK33eqK30dbW0VcpgItjj7KurJ0J+dbRB89LuOkxZZ8PIOZUY8IEWz9Bc4mJlC19g6kujZACFqsGfleMBStwDEqtw3xYzVWL5uHdtCdbvPBhQTXWWM7wk5nCWDkNiCzCQZAxKaceE/YFCeiNMeUKGqSbVMOUiQa46NmIE1HIGwy+DYPcdPRTy1qlGzY9Tnbh3qRGDZ5bTiNDy5dXnE43QUJh6ca0Jg2dXou9nBtwaZ8bINVY8mFCIicvTkZK60zcDj6S0ZQczXWvDsHmfY1BqO24jBqa04duHO9DW3MaFMBFxeyipByXv7Kgl73AJfNbWTMzakolPZbCfPZ9rDBAhD1NcEt23OxhuPhK7L+R4veaFHU7cF0dCPiYh7zMhpehL/GCViXduRyw0o0+MBWOS7Hh6XSmeXJaBpF3HwuYMZZhi/5+np8ptXS4Gbm3mMiQhTx+RhDTzUHXyqhEHL+txSMbBy7rQXKrkHFBh/6UKgc8D2SdxUUk59l4IhD1WUG4PkiGFKWFkkAz6t93h6pmQXx91a17e6cSDJGQICeknE9L3g3I8vNyGPrMsmLiuBvcu0eLRJSRjZ9dKW/apd1Fsf31rHt4+VYch6+w+IQNIyKun29HiFfdO1Sv3U0mPe2X7qGT4wpFyL5UwA1fuparjCBM+3kL3IeYMqaSVcPorqWAZbr8MB3vMJQhx9UTIcbfmldRqTFhmxNCPSAhVV1wGiek7XYdbP6WREWfHE/F5GLMgD8nbd3e5HdLa3Ip3d17FU6lmXDF5MWxZSYCQ1891UKJv4b+va+2Q5qit9IVth4TKGTV+GXaSER0hR5yaV7Y78FCsnuYZZbzM5UKmV+IbM00Ys8SKb6/U4uE56di4/YC4ISHyTWwuOlhLTxbhrsQynDJfg9HuxtCVFX4hyW144wIJaRKFKEeHoh3ikbdD3CojQ2yH+EaHyy+jTtEO8Y8OcWQ45SNDKG/D5QwpTDlEGTZ6bzZ7D4W8fciheSXFioeWVGLYhyUY8H4J+r1HYetDHcYvNeHRpcV4as45rNp+PGwLXa1ry0JNcpoOY5NKsVt/DS2UJwoojwxdVRUg5E9pHfA2NvuEsFbGsYwqgXQV0hh6ztEgdDgiccXPYTmX5VTi0CUlFZST/By+XIHCcodKAvcoZERDyBG75tWtBkxcpCUhRRjwn0XoR5XWw3FVeGxBOsbNoElf6mF/zujCJra0MjseTCrCFm2b2Cykx7RWDE0yBgj5v+l+Iay0jd2bj9m93A7h1ZRKRaUG+z3ncgyBIyMgTAkyWLiy2ev4COy+kH0WzaspJGRBEW5/J5dm54UYOYPK2vnZePDDc1TasnYIk9G1rq2TQsSTK9Pw/hUKFQ1tvjnG6QIThm6s9QnpT0I+zrqGJnEjtEc2x+jNdohynsGprefdXDk8dDk9gSMjKEwJMqwkw2rrqZD9JGRzJR6Jycftf87ku9G/vywH35lxBGtSDiMvr7DLK30GGravbcjA1H3VyLE0wWjz0GMeyh8epHxeiiGb3QFC4ouu8YPf65vYutEOUSttHWojw8FkuKIgZJ9B8+qmMjw8LwtPLy3EpI9O4vEPjmJV8iExTHVtpc9Bb/APO3PRN8mBCXua8KNdZkzdYfIxeUMZBiU3BwiJIyENDd5e38TWvXaIO2zO8Muoo2InCkJ+vUtHQkrx1GeZmDzzBMb9YTe2pOzx5YyurPSxJL7sdAnGbTRhTl4HZudew+ycQN6kimpwaluAkJ26a0IYEkvbyMOUt0ftECFMNaiGKTb/kGDC7NXBOYPDZNiF0cESOhPCPgzdF7Jbp3ltfSF+mpiG7763xyejO5vYjuaZeBI/bmiHlx7zshDXyBK1ALufeolCVkpLgJA9elFIvSAhdk8eT9K92Q5R45PNjIwg2M+fzTLwERM8Mlx8ZDAZFlstLNZaPhK7LeT3B3Sa19ek4eXZe7Fm2wFfzujKJja2bnG+yIQJK3MQn98MV0Or78wnede2kR5LPBMopB8JOVglCJHmGGwV8GSmAaeIk0FU4YScDD/HM/QC6Up0VCIHclQJL48r/Vz2c/RKBYrK7cE5w17nk8HuMxk9FvK7XYWa73+6B9t2H+Onkdm7sYktu9KOSSuv4oN0ktHY5p/cKTYkMCErz5WTkFa/kG1tSLN18NEYsCFB2Q6R5QyXfA1crR1Sq2iHOBXtkJrgdki1yqRPyCOuECNDFqZEGVZrnU+Ip7tCXlqWphn5X7ugLSmNbKUvIExRRUWfmlc2ZuOZg15cb+sIHBmKxSUmZM7xEgyWjxASkmlrF8/z691NbOFW+oImfWFL2+AwxWWQJDPdZxuuuy3krt9uK7hrzmV6w+4ulbYsTJnoxf9mZwFVVFYcNlyTnXSpvrjEDt4b24v4opRcyD5dG0/W/mrKPzKEx/ztEJ6wXQIusR3Ck7XL3wqp4wQ2CmslIWKz0CkTUkMyeJtEGh1stDBJ4uhgoUotZ8hHhlWUYbY4eyZkzNvJa8YsLUZbS1vEpa2lpgHbr5TjpU256Lfahr50UP/jYguuaO2opoOh3DfFSuHPiy1YfLwIo1dXoj99P5uhM1hSf+YI5ZHCapgdbj46WHtj94UK7OGUc3ZLnPezS845OWWcnWflaLFDwfYzjNKQpJ4W2EH3s0ssgSPDXhsog0EyeizkO38+MvzuJZoqtqYdac7IqKhBzGUnPrraio+plGV8QsTneaF3eoNGRiYlxPnnzZid3kCz8nbfz/jpQFxBKzT2Jn5uxVKqsmb1cjtErZqS8/EmP7O2ZODs1SrVBM5kWEQZlmgIYbcSnXVcW2uHT0hnG58Z3sZWsawVSlsGb3WorIGz+Ukjm8+wCaaINLdooBHBYK0P6VzwiNsh7u63Q5TzDAE3RwhXbl/YEnpUwTlDHqa4DLpvoq/ChusebrZulQn5ZzmnrzZcOySCDQkRtUNUSltJhnxkWLgMwuzkeSpqQrq217al8722nht8fkYnm9hCrfSFaodYO8kZPhlWQYbJXBMdIS0kRLhcRaTnZ7QKG9tULmPhX1xqDrHs2hx8GQu3/zIW6suujSrLrlJ5q1x2rQ+YZ0hbPOU4qj0+Qf5KSsAmC1HSyOAiZKVtYJgSZVhqYIyGkDYmpKVD2IITwfkZaUU2zE/NCbG18wbuDtna/XaIegJPD+Ij4pPN6TiVqQvKGfIwZSYRRnqMC6npqZA2mZAIcobB6sblIisuFwRyiWMJ4HOJfIbZx0WJPIELeSY/uX7Oq2IUyDHinIyz2YZAsgw4w6kSuBrMaUYmQ4+/BKHDqQwd/1pUbgsZpgQZbHSQEFNUhUR+foYyX0g5o14tgSvaIS7FGrhLeUqAS57AG0O00GUzcClnKMKU+hq4vx2itp4RctKnKqOWy+Chih43mQQh1dER0u6bGEbjnD5PVC9XEfn5GWE3sXWjHWINkzP8YUqUYa6GwVQdfSGRlLaNYm/KP9fwimsXfiE8oTNBikahxyO1Q5p6rR1SHUE7xMGTeB2XYBcR1jRqSU4dh5e3fBIYnDNMYphiMkw0Mgy9ISSS0raIZuqpZ8qQfLoM205rQ/MXRilnq5xTjBI/JwW2qLDZhwabT2iwqRM2HmcU+zkWyAbGUUaRKuuPFmL9ET/riA30WFq+MThnWJQy6Kuxmn8YoiJEOj+js3P6rlBCZ1XWjThZprfaIWrV1EcblaRxWJV1kpK7MmcYRRk8VBFGoyDEEQ0h7AoOkZ6fIbXYVXeH1PuXXevD7UJ3R7g7JOIlV1k7pCZ8O4TnEYcfO+UQOUK4qvXBQpVZJWfIRwaTUWVyoMro4CExekJu0pXYQp2fEVk7xB3QDgm3IcEq25DQlRm4vLQVZFQHyjAKMqoMvSDkZlyJrTvtEPXzM8JsYguz0tdZaauaM2Rhio0Og6E6+kIiKW3r6GA66EDZ5dQw6jk2iWoJD6wSDgELxy1AB8wcgIsOjgsmOXTQlBgtjFofBnMgVSYJJ/QMox8dHUidwU8lo6raRwVDz3CgnNDTQfeVtiphisug79GTEPZhiJKQyLq2l2nmPS8l+5+6HaKWvCVmbgiE/cyJtEreOJTL4CNDlMHQ0329wc5HYxSEtPnPz+jknD6TzYPsUjuySgK5KqERyNTY/BT7ySi2qmARKLIgXUZaoTmAKwUSJoF8Ey6rcCmPYQzi8zwDPs/1c5GRU+XjAiNbzzkvciFLj6Iya3DOMMllVHMZ+qooCfEyIV04P6Ozdog7bDtEfT2jy+2QmnDtEGkTm6vz9Qxx9c+XMyw1fKHJlzPM6jmD5QtfqDLaUVUlCLFFRYi3LeD8jBt/Jbaut0McYdohoTaxdbUdEqq0VYYpJkNHgnS9IaTTlb4I2iGuMO2QuhvRDnHUddoOsVqdRK0oh4TRyJBGhzDvYKOjmrfU1XJGlRimhJFBMui+rsrGny+qQsKXts3Iofyx/mgx1hxRUhTI4SKsDkshVh8qRJKCVQoSDzIK/ByQk48Exn6JvCBWMvYpyeWsYOxVIwfLRVbSv89n6YJzhlEug0YKydDpoywkkiuxpVGyXbgzFwu258jI9pMqEBOWLMSkZGF+J8xLvhqWudvkZAYwh7FVSYaP2YwtaqRjlsTmdP69J9MrZPlCEaZ4Mmcy7KiMphBPfWQbEniI8v1FgSYxl8h3njfJQlej4q8ISHmkQUzm/laIU44vbIlIoarG7UecoTtkJ1z6qeNnM0mwkCXH38FlOcTp21MlhKwansgFKGSZqlVzhhSmmAw9iaisEoVQSIyOkJt8JbZwu0NuWjtEpbTlI0MWprgMcXRU6qz8OaMo5CZdia0b7RDf+Rkq7RC1TWzhVvpCtUOk0lYtZ+gMggweqpgkkhF1IZGUtvbqeuhMdaiUMApUcGp9lBvkOFFe5URZADXQKijVM6r96CQcnJJKOXZoKvwUV9hQXO6HrYOzCZ1EIUMrUCBRyrAgX6LEgrwScxClFdagnCEPU0xGBVFeaeHVmsfjiY6QzjexefkmBXZRr3/sdkh62HaInyuYwVivzocE+55jl8p4n0ouQ0ri0shgMkrLjDS6HNER4paEdNK1ZQ3AospqFFUIFMopr0aBD4dAmQP5crR25MnIZZTaAsjxYRUosSKbYxHQ+MnSmJFV7Ocqo4hh4mQyCgUyJAoYRqRL5BuRlm8I4kp+Fa7kVaFIaw7OGbIwxUYHk1Gk0dF9C+WvHgppEoV05fwM6WKRvXl+hjxfRHp+RshNbMoNCbJl16CubbjSViVMcRnFOmTnlaGswvhXq9UaBSE38Ups3WmHhDo/I/Qmtq63Q0KVtvIwpS03kQw9ckhGZpaGKjBLrs1mu71nQtjflupCaaucZ7jqFHMNcYunb64hLbPK5xk10vVExHlGtUd2apk4z3D45xp8fmH3zzVs0nKrNNfgcmRzDd4OEecasnaIWZpnmATYPMNoFGGlL5cjIKwC2gNKW3mYYjKKNYKM9Mwi5BWUuauMlv8FoE9UhERS2rK2+LJ9+YjfmyewJw9xPnIFduciNgxLdzFyQrJkp0S2KosZO5RkcRZJbFdyFQslUoP5jJOJz1IEFogspMdOpZX75hnyMFVYpENWdgmNjGL6f9OX5ZWGOZcvW/r38G8YCkJcopBwm9hY3kgvFoTEBYjIi0hE12TkdCpiEccvYeF2BakS0gEnUpTQgU/ORExyOv3Oq0g8XIQ1x0ux+mgJVh3RIOlIMW8XSRNVaZSzboDRZEdZuQG5haXtKxPXxdPh/FoU/qgkRjaKQiI9P0OtHVJbF4hT0Q6pUWmHVCvaIQ5ZO8SuaIfYlO0QRUsk0naIUWyHSKFKr6cKSVvFz0D+4osvOGzBjoVv9lrY97CRwUYIy1M1tZTralw0Qqpw8VI29h863bh85ZrFdCj7RumvfIpCeLs8OperiNb5Gd1uh1hCJHBFO4R1Z/MLtMjJLQT7kx3t7ezvqAjHgkln31/BZdjpeeroPTeIMgy48Hk2Dh452xq3LGnJ66+/PvyWaN0ChdysK7GFuVxFiHaIOUw7JNQmtoCVPvrKDuyxE2dJSjGuXbvOzyBjuZR9KAQZNt/IcNJ7ZaNDWy7IOHbyMmIWxCaNGDFi8C3RvHEhjS1CGOqktGUyjFaXf+KnnPSV2YMQJoA2P74JoJXDJn1+Ipn0mVQnfYEYaQJo5JM/YQJoEMgX4JO+PD1OXszDslVbkZtfzGWwY8BlGISRweAjg0Iu25HIZFy8lIODh881L1gYm0iH77ao5A3ln6tgQng+iKBry87hmJN89ea3QzZ2vx3y4brLmL72Ev4Uexiv/e/PcJo+8Wxk8DBl8Icp6UIAbBSznMFGxoFDZ5rilq1ZMn78+KG39MZNLiSSTWwWCiNBjUJ9DbQK/I1BgRIfDo6mUsKOYkaFn6IKsTkoUsgoswkNQpECrYJwjUKNQC7HhNxiE7KLDDh1gaq1+NXIuJrLR4beJ8Mm5gzKf063mDOycOTYxS8WL02Mnzp16ohbeuvmE1LXGMXLVXiiermKiM/PCLeJTT4DpwPPDnpGZj5WrVqPzKs5/HukMGUWw5QvZ1A1deT4xS/mxyxdS4dsUNTDVCghN+tKbN1ph6ifnxF+E5u8HVJJ5S4TkkhC0jOyuSCWxIXStp5XU1LOOHD4TNPiuMT4XpchF8JPjrlJV2LrWWnbvZU+NrdIF4WkpWfxLTxsRLIwJc0zhJxxtiU+fvXSqVNfG3HLjbgxIQ1cyD9eaWuJoLSNfBNb4EqfX8g6ZNAIkWTwMCXmDFbaLlgYn9hrCTy0kGZ+8P9bb2KTLy7RYyxXpGfkIYGEXM3K8yVwIUzRpO/w2daYmKWrb0iYCiUkaperkIWpaFyuQi1nGMPkDLVNbMqVvopKC9KYkMS1yMrO5x8obbnQDjlw6HRTbHzSUnHSd+Nk+IQ0NIu7B6PTDrFHcLmKyM7PcHZ6foZqzlDZxKZc6eNC0vOwYuUaSu7Zvhn44aPnO5bEJsS98sord95yM25MSL0o5GZeia2rK32dlbadrfSVVZhx6XI24uIT6Wsmr6aOHr/0pay0veUfQEh0VvqiVdqGyxmRlLbhVvo0pVU4cy4NCxcto+SeRznjTNOSJQnLeqUd0lUhTU2tX7KJ0H/nTWzKlb78gnIKT2ewbMVqCl25zUtiV8VNnvzsN+mQ9BGFdEbv3FqA4U3eVqPzf8AmNvmya0ZmEVJS91G4uvq32PiEdaNGjRpLh2OIOEIGyRgoMoDoL9JPXP/4hsitxNdFuizsa+IPsl/Sd8qUKYPz8wsXu1yN16S/CV6juJyR/5JGbpVLGgVedU1AkiFVT7U+GRbfKQD+RSR2CoB0vSmjhLjebZAQQ5R0titDb3D4zlzS+RB2oVdKyJZd+YYEkpGdq8XZ85nYf+BYx+q163bRcXiCmEA8QNxP3EeMJ8YRTNQ97BKV7LqhxGiC7SoZSbARxSaMbFMDm6sMFuX1F4XdKpOkevu6OCT7ifbZLxnx4YcfPpadm7/GWet2NDS0/DX4zw6Jf3qoQXn9K//Ga7cMvjbvluEKcf0rl/LPDgVe+6pGQvygVPvOFRHDp+zaV8LlwAOvfeU/P6SOnwNiEPtVRZrKL46fOGucP3/BNnr/rxI/Jn5ITCV+QDxHfJ+YQnyP+C7xDPE08aQo8DFiEjFRFHkvcTfxLWK4ONoGice6j3jsVUdGX9HiENEus33PmDFjJiUkJL2+ffuu36em7vg/W7Yk/+eGDZveWbdu47tr1qx7Pylp7QcJCatmrFiROJOxbNmKj+Pjl3/CiItb9mlsbPyspUvjZjMWL46ds2jR0rmMhQuXzJMT89ni+Yz5CxbFcOYzFsiIWdAd5tLPBjF3Qcy8eZ/NnzcvZj7dnzd77vx5n86eP2/69BmzX3jhhXcHDBjwK3rvvyB+JvKaKOgV4mXiJ8SLxAuiNCbseVHWs6KkJ0U5E8XRNUYcPZKUAWJY66M2UqRQ1U+UMlSUMlL8RWyIPkg8TDxKPE58m3hK/HR8V/zEPCu+qOfET9Tz4ifsRyI/Ft/EiyIvifxE5GUZr6jwqoKfKpD/n9rPv6xAet6XZMgPtPS6fyi+F/koeVY2Sp4Wj0WkI2SgeKzD5hZppHxDHC39xR8cLAq6Q/yF3xRFjRTj5l2itLHik48XuU/8ZDwgMkHkIfHFThQFPyIySYVHZUxW4TEFat/zqALlc0jP/7DsdT0ke70PqOSQ8eJ7HSvLIXep5BB2zIaJx3CQIul3OdF/TSapjziCpOqhn0h/mbiBsgrkNhmDxU/FEFHsMAW3i9yhYHgYRnRCuJ9VPo/0/MrXNVT2ugcr3pOy0pJXWf0UVVaf7lZZ0bqp1eZfl9GnC9waZbry3F9XKVl7db7x/wBE5bSwiXD4fgAAAABJRU5ErkJggg==";
    zipFileImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAjbklEQVR42u2dB1iUV9r3TTeJpqwma5LXNWrsXSwIIWKJJd11902y2Xff7Kaa3dTNpthAYwHEiogKClKULtJ7772XoffeEWPa/7vPmWeGYRgGhnlwvuv6vrmu/zUPw8Azc37P3c595syECeN909O7b4GNzeLXSotfeyM/Z+fazOSdG8qL/ri5XLJzs6SIy1iSv9O4KIfLKD9zp1FOGonu6bkGTGmk5JidO+l+ZdCNPz5x/OjOez//eOt0E5OnJ/z/m0a3u6fv2bNqbZC/x5ulhb0vZ6fh+bx0GJfkY1NZETZKCmBclIv1hTlYn5+FF/IyYZRFz8lMhWF6EgxSE7AuJR5rEqKxjX5+PSoM853s8aSlGSaa7OmbZn3Kfor9hfn/z43qzZs3n7p9+2fT/v7bniTfEdV3y7ens8c3qrrGb31aYt6bJQXYQANrSEAM8jNhQAO/LicNazOSSUlYTb9blRgDvfgorIgOw/LIECwLD8KSIF8sDPDBJnpstaszNl66iMOurnCMjYNfYTFCKyp/qmjvyPjl9k++v5J+unXb91b/LXoNt3z7+karft9emXpHVo+ietSpj6u7t9/z1q1bpmwMRYFx+/btpb29tzK7u2/+0tXV9xsJI6m1tRMZZZV4OzMNn5SXYG2wP1akJWJ5WgL06EpfmhiNJTT4S+KiMD/UHwvCAjAv2A+zrrth9g1PzPS4ihnO9phxzQl6Hq7Y7OKIeZbmsLvihJSUDNTWNiI4Jx9HAgNhFhTMZWh7HiaJCWhq60JHR6+gnkFqZ2ofrLb2bqnahqqV/ldr64BaFNXC1MnVLFOzTB1oEkTHv9Fjv3R392WysdQKBoCHenpuOskGmqAMK/b7zs5etLZ0IItgfJidiW0ZKVgVFYrF5HqWphEIsoSFCZFYSDAWxIRhblggKQCzyQpmEohnfdwx3dUJz7g44CnHS1hA7mmtswNWHjPD+SvOSE5OR3V1PQ1kN06GhWMjubcXSVtIegT8vYJclLZ10utgr6VHEIEhtROg9nY6bh+A1d7BYNB9myA5pB60Mkg08G2tMjjdaGH39HMrwWhlMOj3zYJampkYGALR0iEF0yRVYxOD0472zh5HNqZjBtLbiydu3rzVPBIMJvbGW+ikCeUV+GtOJh8o4+Q4rKB4sCw7FUvJPS0lMIuTYwkKAYkNx/yoEMwl1/Qcs46A6xzKdM9reOaaIxbT/QZPNzx/0hKnnFykMGoa+JXKBrqwph6BVdX4IjYa+zPT8WlcDMxTU+CfncsHXAajUx0M0nAwWhVgtKiAwbzAcDCaWhRgNDO1E5R2Om93a29v75NjBtLXh2kE5NfRWAezjKTyKrxDV+mu6jIYkBXoUaBenpcBPYoZy7NSsYTc1TLSQnJZDMrC+EjMjQiWQgnxx7O+ZCW+XjAkyzF0csCyC2dhzWGkoYbcVCt3R9KBZucMK5Fgqa83VifE0PmiuVbFRuBmT7/8ee2dyjB65TDalGC0y2C0dUmtQ8EyOJBhYXSphNEkwGiSA+n5VatYQuY1jQLfsO5K7qboBWZVVOODonxso8E3pNiwkjKnVcW5WFOch2UEZWVuBlZQIF9EVrKM3AuzlPlxEVhEYOZREJ9DEJgW+nljHVnHalsbXHZw5JZRQ5bBBkcGgl8ANGgZ1XX4S1ICvszPxekyCawrymFbXQUK1IIVCDA6xuamhlhGqzo31TnYTVH8kMNo7EBDYzuPVeMGRAajhV5MKsH4sLgAr9Lgb85MxsqcdOygQf+wIAcvluRBrygHK8laVtDjTMx9LSEwS1LipO6LwMwj97UkJABvhAZh82U7WF9zpQDOYobUTbHBVEwcnBKT8G5YCD6ICMfHURFY5XYVnycnoaa1YzAMTdxUmwYxo2X4mKFoGU1NDMYdACIL4IkVVfibpAhvVxRjPaW1K9MT8RHVC/lNTWjq6UFKdye2U82xkqCsICgrBfe1lGVbBG1RUgwWkOt6MSUR6657YJOrC6yuucktQ9lNyeSSlIJPi/PxZUkhvqKLwZAA/4fu69mAdw4Po01tzOgaFsZo3VSjKhhNMiBd4wNEltqml1dzGG9QobeeMqiVuen4ODoC/T/dhn9PFyzratBz+za8O9qgR5aymlzYMllMIXiLWSpM1rKSLOR5grHG/Rpsna9KYVDMYIOjCkYHiQ1kJlnmDgK4i6zELCQUlmGhMA8M4lnYmGJG6yhiRosMyPAxo1GAIXVVHTx+NDS2iQ9kUGpLg/FxCbOMEhjTFb+mugT/oNS2rrsbYQRjbbUEm8kdNVBmENrbjZWlBdAjIAzKCioOVxC85ZR96ZGF7KCYs8XbA3ZOVweyqbauISA6GQxKaTs6+nhqG15ShuVB/tBPjocxnXt9aiIM6MK42dvPB1vr1LZVg9RWIWYoWgaD0UgwGhra+HsSFYgMRhK5qfeKC7GjtAjGNLArqNreUSnBB021kPTfRMOtfnwjKcRFCrYVdPxuQzVWk0tbU1aI1UJMWU5BflteFowjQ/GSuyus3T2lMYNgtNBAKMeMLkUYpLyqOnimZ+GL6CjsS0rEJ+GhsMjJRkRdA38uH/hxjBnDZVNyy5DBaGIw2lEvNhCmFgHGX8h3/w9ZxiaqMdZSfNhNV2dWTzc+aKzBO43VaP3xFjpJ5Tf78FlrA1ZXSaBfSRU7QVklyefZ10ZyW1soiG/0u47z7h6D6ozh3JQMRheBsSQXpR8VjufJRRqRloQG4n/SUlDCCrD2njuW2qqMGY0DMBoJRL3YQGSpbSbBeLcgD+9VlmIp5fxrJHlYR4P8Rl0V2sgSCvt68TZZyYu1Zajv70c9PbazphyGdRXQryrFmspiGBAc48JsvJQUi/XB/nC44jRsajvUTfXKK/Cw3HzYVJbjbHkpzlaUYWtKAg6TC22k/9HeLk5qOxAvRpvaKrupdg6DBXQOpFUMIOSPmWWksemQvGy8Ru7GmHz1qoJsvEUFWXBtNd5sqsFfyS0V9/eRpXSRldTgf0lFZCHMfb1TXwX92nJuJZsoAXidBu/VG9447+Yhd1OqUttOJTelOB3ClFxajheuOuMNclcfh4bgY0qD3w0M4L9jg621m2rWMLUd4qZIBIPFj/r6VhGAUKXeRWafUFqBtygr+nNRLowovVxJxy8FByCnsQG//fYbLDpaYUBW8D5BKKMYUkIgPqTj9wlU2q2b+EdzPQX5UrxOceZVci/bgvxwzst7UABX6abUwGBKqqjBhogQ7EhPxb+o5vkkPxsf5Gahl6yaA9E6te0c2U01DoUhd1ONgnUQjDoxgPS19E3Lqq7Fm5lp2EkBeHV8FJZT4bf5hifaaNBTKZ3N72jH1d4urKopg2F9Jd4ka+ij+FFClvFecx3WN1bCiOLKZoo168lNbaIaxY7qDDZrO1Jqqw5GeF4RjgUF43BwMCwo3d1gdxH74+NQSQMiG3RtU1t1AbxxpJghwGgQYIgB5J43HBz0dhXm4U1yT+vCg7E0Jw2bE2OQ29qKGEplt1KhF1Ffh+jeHqwlIOtIBuVF2FFbiVQCtqutEc83VMGoNB9/pnrj1ZAg2FNqy9yUpjFDcda2k2QWGIjVCdHQJ7fJtDg2Au9kpKG4uU3E1LZTw9R2qJtiIFj8YPct2gCZsmPHAoO4yJjXKD3Vj4ngk4Ir2HFBFvZQEDegQV5DdYQ/FX9FZA165I4MMlNwLDsTR9qaYEwg1tWV4xUK5iyAvxEWjPPe17llsCl0VTFjJDelOB2STUXp9fIKfB0bDVNyWe9FReBoSjJCcwuk/Yxxng5pVBczFNxUHbcYEYAYJsXb/ZXcDJu1XU7F1pL0RKwgAMtZQUeFHUtdl2elwK26Ep0//YzlVHUfSU1GO8H5sqUe+mQtG6nu2E4wXibrusBhSCvw0aS2Q2ComLUNKyzBErerWE3//wUCbkRaGxyIHqqZZM2lO5raKrspOYw21Na18vpqzED+VFaU/besdOgRiKVp8VhOxd/itAQ+D8WmPtgUCJu9PScpxs+//ILPKcjf+uVXfNPcAMPaCh4zdpIr2UZJgL3Q6ZPFjLG4qSGztnT1Z9fW44+xUfhnTgbOlZXCmnS6VIKenn71/YxRprZSqxh9aqvspgZgtHBpBWRLRkq2AV31qymjWpoSi4VJMQQmgU8KLiPLYJOEy+h3h4vy8OPPP6Prl59h1tIIAyoYN1PxtyWR3FRIIGzd3KVuagyp7XCdPqarCYl4398PnwQF4lOKTSsu2+GzmBhU08C1itXpUxczmoaPGcqWUVcvAGnRAohhVmq2saQAq5hl8GZSFO9hLEmNJyiJfB5qeU46XqfAntXXg4ONtdAnEC9QAvDHuCi8HugHGx9feQAf1k2NMmYoztqy3vilmDj8nYL4+2TFH5BWx4TjU4pf1XQVitnp0zS1VYbBQLDH2X2zNkA2lhZks+U6euRyFsaGYVF8JO9bMCjLCAhzXyymsMaTEUFYSzFlK9UpWygBeJGq+Iuu7oOyKTFihuJ0SD0NTlJpJXZedcGukBCYBATyGd+TQSHSgRchtR1rzGAagEFix7VaAtlcKck2Yv2L5DgsjA7DnPBAAhPBexcLEqM5FBbol1FmxWLKWlafRIViq/8N2Ds4yStw0WKGilnbwPwizPN0xRIqDlfQuZdHhWBRRDD6em5ytyROp2/0qa2ym+KQ6LimlqmFn2/MQDZJCjiQ5ZTrzwsP4itE5kUG89UirMu3SIgpSyjOGJJ20vNe8fXGeU8vtTFjrG5Keda2sLIOPunZ+Cg0FN9SQfhZeBjMMjPgV1nFoXIgYnT6NEptB7spqWUQDPq5prZZOyAbivOznyd3tDQ2HHOCfDEnxJ/3vOdHhmAh+WvmvthihW2Uda2j323xcsc5Dy+RU1vVs7YdpBNsLVaAL9YH+cE42A/zvVzx17gYFDW2DIKhbadP09R2sJuSWkdtXbMUSLOWQAxY75tcwZzAG3jWxwNzQwOka6nIYhZRkNcnbQv0hYG3By47umg1HaLp6hA/so7jxUU4IymBFaXeRlFhMM3PRQsrCFu6Rev0jSlm1A/AqCEY4gDJz8peR/FhEcWOWQRjpp83F1tDxaCsJn+9nXy38TUn2LpcExYk1Avrpnq1Tm1H6vSxOiS5uBRGlO6+4Xsdn/n64jN/X3xw3Zue3y1ip2/0qa2ym6oVYFTXtJC0BGKcn5GtT7FhQYgfnvW6hhnernxlIYOyLjIUG8lVbL1yGdbu6qfQxYoZAzB6UEdXrX96Fr7xD8BM+4uY5eqEWe4umOXmghkk15wc5JRXS5d8apHaji5mtAyKGTUKbmoAhghAXiALWUuBe36gD/7g7syXd/6B/LRRRBCMPa5h9RV7nBeW6qhaxDYeborBSJeU461rLtCjC2NxSADmU/yaT650fmigcB+AFeHBeDEyDI45+dz93LnUVtlNSWEwd1Vd08TX+o4ZiBFV6mtT4jH3hieecbyMZ+gqnOfmBH1KM9faXYSDkNrW1I5vaqvopmooYP/Lzw/P+ftgASUX6sSSD31KPnLqmoRFz2Pr9GmU2g5xU1IYVQSjqlpbIFSpr4qLxNzr7njKwQ5zXK5gm68XNthY4dxVsozUjDF3+sa6iK2sphFvB/pjHsv2qN4YSc/R81IbBSBadPpGm9rWqrKMWgajWXsgz5OF6FFBOMvjKgzJPejZnoe+9RlYCQGcWcZYOn3aLGKTAeE1EWV6I2k2ua/UhmY+8GNJbetHkdoOFzMGYDRRsiMCEMOU+Gw9qswXk4taaWuD+ZZmsHN0lseMO5HaKnf65EBooJmVjKTZFF9SG5r44N+Z1LZlIIgzGDVS62ABnQFh5x4zkHWpCdmsS/jyDS+sO3Ucto4D0yEjL2LrHZfPZ5TSud+i1Jan3jTYI2kWFbSp9U388xlj7fRpltoOdlPVAozK6kZUVjXy840ZyCvksrZS8DS2OYszzi5IFWLGaBaxiRUzlKfQGZA3fW9gNhWjcyjtHkkzA24IQLTp9A0TMxTc1JCYUdMkh8GOGQytgeyMj85eeu40bD08pRX4KBexieqmlDp9tTQg38XGYg4N9BwGRY2eo+dsoNS3oLlVCkSLTp+61FZVzKhSglFV1SQH0jpWIPN8PLJnH/4BeTl54nb6tPx8RjLVIeuD/DGbrFed5pLsikt4DaJNp0+z1Haom+IwCFIFHbMF12MGMslkd/p8CuYtTW2idfrEWGvLum7fR0Vhpq83ZvsNJy9sJ+vIZe6KX/1j7/RpktpWq7CMKgFGRWWDdkCm7PveapGbM/q6b4rW6RPj8xkMim1kDKa7OmMmJRwq5eOJd2KjqJBslVqAFp2+kVLbKoXUlltGTeNgGEwEQ2sga66cnbLA7aqkv7f/jqa2I3X62PN2Ojpi5nUPPvDDyTDYH9GV1RpNoWsyHSJPbVXEDA6kWnBVBKJSDCDsltPUNLNPAch4x4zRLGJro8dfcXHCs97uaqVPgT2yolr7Tp+Gqa2ym+Iw6Lic7qULrrVcbN2rAOROpLYjLWJjAF9xcsQML1e10qcsK7KiSutOnyaprQyGomVUchikCgakVTwgYnT6BrmpMS5iY4+/7HgFM9yvqZW+nw8iywmIEgxNO30jpbbDxQw5jCopjPKKenGA9BAQ5o40cVNtHeP3+Qz2nJcdHDDdzUWt1tzwRkRZ5QCIMaS2ivFitKntYDclwKisR5kYQPoYEP4h/F7ROn3afj6D/fySw2WeZanTah8vhJVWDIFRJ9J0yHCprbKbqiAQZfQYB1KvLZA+BSBixAwFNzXWRWwsU9lqfwnPXHVUqxVe7giUlHELuVOprbKbksJg1kFAykUFcmdTW3WL2CppcDZfIiAuV9RqqYcrfItKhsDQuNM3QmqrGkYjh8FdFT1eXi4FUicOkIHC8E6ltsMtYmNT6EUVNVh/2RZPO9mr1QJyW275hRyIdp0+TVJbZTclwKioQ2l5nfhAdL9dRQfSikqx7pIdnrpySa1mOV+BfXauNH5o0enTNLWVW4YAo5wso3Q8gIi1XYW2n8+IysqD3iVbTHOwU6unSVbpGRzIWFPbMceMSmUYdF9Wx88tChDpTmxixQwtFrHR7/2S0rD40kVMs7dVqycuX8SRpGTtO33DTIdUqIkZZQIM7qpIZWVSILViAGE7OEiXb+pgJzalRWxsGt0lNgHPXbDBk3YX1Gqq7Xl8Ex2NKhpwbTt9o09th1oGgyEpr4WkrJZfFOIB0dFObIqL2Fgr9nxENKafO4MnbW3UaurFc/goNASl5G7uVGorhVE3GEaZFIakdByA6GInNsVOHzs2DwrF1NMn+ICr05QL1viT3w0UVtWPObVVD6Nx+Jih4KaYdZSW1okPRHc7sQ00l+oaWvG9zw08fsoSU8+fVaspNlbYRLVIdkUtH3xtOn2jTm1VuCkOg8CUEBB2MYgERIc7sSn0MyqqG/Dh1Wt47NRxTDlnpVa/sz6DZY72yKC6hQHRptM3+tS2fsAyBBhMJXRcUlrDzykCkD5hX1vd7MSm2Fwqpqtu5+VLePzMCTx29pRaPU56miwltbyaD8T4pbYqYka5Iow6DqNEIhKQLgZEaSOXO7kTm2KnL0dSCaOzVniUgBhQNc6Ctky7wkLxaUQYdsdEwTQuFo9ancQjZ44jtFjCB2IsqW3lKFJbVTGDxQu5qyqrgUQiBVItCpCuPpUbudy5ndgG+hnpReVYaGmByRRD9kVFwio1Fd+GhdBxFIycHOg4FBklZWhr7MTE3d/y513KYEuYtOn0aZbaKrspBqOYABWPBxDd7MQ20FxKLJDgyYOmmHTiGI7ExyEsIwdzjh7GCqpLtlyywxZyZy9cOI9Oej0Tv/8Gk+l5JlSL8KCuRadPVcwoUxMzJIKbkloGwaDjYkk1P6eoQHSyE5tCg8k/LRMPmezFpOMWHEhcvgR6drZ42dsTn0eE49PwMHJfwXwj5fu/+5o/710/X/6R5LGltg0jprYqY0aZIgyyFIJRXCIyEF3txCZrLrEpEOvgUDx00ASTLM3xlrcHvrS/gt0uV7HfzRVzvv8Ou1zd+eAFJqZh4nf/4c97gVxZM/3PQQFck06fJqmtspviwZzBqEGRmEBa23S7ExurtNnxXg9PPHjkB0ymgZ507CgeMTuMR46ZYTLpEfMjWHj6JF62ssKD//kKky2O4mHSLEqB2UoVDmRMnT7NUltFN8VglBCIIokAhM4nDhAd78Qmay6FZebiJWcnDmESDTYXAZp06AAmme7DwyZ78DAdMxAPEaDFF87BKimFYLZKY4SY0yEqUltuGQpuisMQrKOouIqfU0QgOtqJTaHTx6Y+SirrcCImFlMszfDA0UN4mKxkEg0+EzueSI/dT5B2+fshu7pO/tkMmZvSJLUtH0VqqypmFJdKYXBXxSARDNGB6GonNlWdPvb7Chpgi4hIbHW4jLmnT2A2aZuLI44nxKOmtZ3iRrsQxJu06/RpktqqcFMMRiGpoKiSzsc+Nt4qDhDd7cQ2tNPHdOq6HzbanMP2ixfwKqW8G+0u4J9kFfkVtdxXyz9KpnWnb/SprQyGLIjLLIPByM0vo/9RKw6QFhkQHe3EptzpY49dDAjBewF++CgoEB8FB+LZ82fxfoA/JBQnxOz0jZTaqowZCm6KWQeDkZldTMeV9Fq0BNIpANHVTmzDL0hoQXx2AdYeMMXrNjbY6+iMfS4u+ML2EgciRmorjxejTW1VuCkOI6sYSan59HP5z1VVVSIA0eFObMN1+hiQ60npmEaB/FlKbZdcuojF5LbmUWbFttYQs9OnSWqr6KbyCsoJRgmSCUZ8YjZZU1VKdXXX49oB4V/aorud2FR1+tjvYjLzcc4/GH+3d8AXbu54zfosvrnhi4DcAt67Vr9uavSdPk1SW0U3xWBkZUthxMZnIjU9v6WsrObvAO4RBYjudmIb2uljUyHfEAiW+j553By/P2HBi8SNV+yRUlyu8SI2dZ0+xVnbEg5DJul0SJFMSm4qI7MYiUk5ZBlZ9Pvyn4pKKvZUVlZO1Opr82RAmgUgOtmJTWWnrwU+8UmwiIvD3rBQmERGYOVFG/w7KAC5pZV88EXp9HErYFd/FcrIgtj7YDMW7JPI7H2yxdO1DQPWzJa5MtXUNtHza5BfUIqUjLybJ09am9Nw3qX1l0oyIB0CEF3txDZcp49lUYHJGZi9fy+WnzqBl87Z4OXzNth45jTq6e+1TW0ZDHbVZ+eWUFJQj5v9P6K//xb/Ihn23tnrYG1ZZhXMQhj0+kZ63fXNZCEShEcmwd0zsOP4cetDNJT3i/Itn3IgfNW5bnZiG7bTR0BCM/KwkGC84GiPd2/44G8+1/G2twef5qnQMrVl7igtPQ/JKRlgX9lx8yb7HhXpWLCdfdjzpTBq6DxN9J7bBRilCItIgod3cK+FpdXhHTt2TJkg1m0wEF3txKa60+caFYuv7S5jj5MzTK9ew3NffYGPHZ14JiRbyKbJIrZBnT66ZwPr4xtMULJw69Zt4QttpJYhhVEtt4wGeq/MOvIKpDB8/KJgetD89NSpUydPEPPGgXT0SL/bVWc7sQ3t9NWQdey94oxZZ09jjrUV5pIeO2aGrc6OSJNUaJXasmMWsFPScmFrdwWZmdkcBhsDDkNwU0zcMui9sqyOwQiPTIaHV0j3wUPmJ9kny0WJG4OB9HEg/It2dbQTm6pFbMxKErML4ZSUgr/Y2eFjNzf8ydYW33p6wTksiqxDu9SWuauExAyctjrPtxRhMZS7qdIBNyXbCIC9VhYzmGW4ewZ1WlhYHZ41a9ajE8bjpghEZzuxDdfpo8dcYxLw6O5v8TtKeZ8yP0o6gicO/8Bfb7kGi9iGTIcQkLiEdJw6fQ5JSWncIhUDuDRm0IXV0CLEjER43wj/8YjZSXNjY+OpE8brJgci329KFzuxqV6QwJb3h6XnwuicNf7i5op9lP4eiAjH0ZhouqLb+UfIxtLpYwPPBj0uPg2nTp1DfEIyf47MTVUIbkoeMyibYjBMTMzO0pA9LLqbGg6IrnZiG25BwikvX2w9dYpX6G+cP4fff/M1/nz5MnJYzVBRr1Wnj9UdDMhJAhIbl8QBsSAuTW3beDY1EDOCOplljDsMRSBq95sa553YVM3aMlld98Of3d3w3x5STbM0xzueHiioqOUf0tem08fqj1gBSExsIq/M2QQlc1OyOoPHDK/gHnPzM0eMjV+ZOuFO3BiQdg5EhzuxqVjExmTnH4KDztdg4uiCC75BiM7MwzEK6iaOzrBw9UR6rkSDRWyDO30DQKwRRxYig8HdlBAzWGrLsqlxC+DDA+kevN+UDnZiU561raPfT/j733CfyR7ca7oPd+37Hl/aO+DuPd/hPvbz3u9hHRsnrTc07PQxa2CxIjYuFScISEJiqjyAS92UtOgzOWB25o64qeGA6GonNsVOHzuuIdXT3xiZm+HFK/ZcGx0uwyogiO7tsZl+3mB/CT45uagkt1VRXq9xp6+wqBIxDMjJs0ikLKuO1xnCdIhXYKf5sTNHhKLvzsGQA2nvlroiHe3EJpu1ZW3Z/z5mCT1TU+gdPIjp3/0Hz+75nmsGaf6+Pfxe9vPiAyZYecAUaw4dgm9yutpFbMqdPg4kNhWWx60ouCfJK3Cv62H9R81PmW3fvv2JCbq4MSBtAhBd7sTG3BSzirt2fYSJB0000j3kwo5ERvEYMtpOX35hBSKjkmBmfpLu43k2df1G5E/kpmSp7YT/C4Dobie2ShmQf36MiQf2a6R7TffCLCqKLzAYbacvmxKCoJAYHPzBgoJ7Kk9tDx89YTEu0yGaAuns6vuJFUK62olNNoVex4B8QhZCV7wmupcCvxmzEL4CfXSdvrT0AnJPQbCwPEOxJKWbuamlSw2epCG5RwAyksbnBmBKV09fWYMOd2KTTaHXVYsARM0iNsW2a1x8JuwdXMldJfxqbn7K+qmnnppBw/GIYCEPK+ghQQ+SJgp6QOh/3CfoXtLdgjQGdpfwh+yf3G9oaDg5IyPzUHNz5y3Zd4LXK3y3q0wyGDJJl/+3CB+UkQ76gAaKO+UVhYqdPvkUOhNV3bX03Lt2fcjTWk109/49OBoZKbRfh2+78gUJBCMpJQ/BofFwc/fpP2tt40zjsIJt0kqaQ3qONJs0izSTxED9gTSd9AzpaRJbVTKNxCyKFYxsUQOrVSYL8CYKwO5VgKTydrdgkg8I9Nk/mfrVV18tS0lJs2psbK2lIvFnxU9RtcnVLXzsrVtYmC1dnC37ts0WBfHefIuCmjvlauLqkIvPEAipNutWvn7hPNZftoOxhgqVSJQulBY+YShbZspmh0uF+arM7OIfff1CyvabHrxE7/8l0ibSBpIx+xYP9j0FbGt8ths72wCcpE9aS1pN0hMALiMtJi0QQD5L+i/S79n+ooK1PSyM9T3C2Ku0jPsFio8IdBntP0yfPn3x6dOnd1y54vwPBwfHD21tL++ysbnwL2vr859aWVl/fvr02S9OnDj1b0vLk18zWVhYfmNufuxbJjMzi++OHjX//sgRs91Mhw4d3fPDD0f2Mh08eHifokwOHNrPtN/0BxOu/Uymcr31r89Mt3z4oek2DbXf1NRk734l7TU12bfvwP59+0z20/G+3Xv37/tu9/59X375792bN2/59MEHH3yT3vvrpFcFvSIA2k7aRtpKepG0WYDGgK0XYBkIkPQEOAsE65ouWI8MyoOCW7tHlaXIXNUDApRHBSjThH/ETHQuaSFpCWk5aSVplXB16AtXjIHwooyEK2q9cIVtFLRJeBMvCtoiaKugbQrarkIvKellJSn+TtXfb1OS7LxbFKQ40LLXvUF4L4pWYqBgJauFsRithTwkjLXa2CKzlPsEa5ko/OFkAdDvhH/4pABqmuA3nxGgzRBOPkvQbOHKmCNonqD5wotdIABeJGixCi1R0FIVWqYkVc9ZoiTlc8jOv1Dhdc1XeL1zVMSQWcJ7naEQQ55REUPYmD0mjOHDSkFf40B/lwKkewQLkmUPDwiaqADuIYUMZJKCJgtXxSMC2MeU9Lig3ylpihpNHUHq/lb5PLLzK7+uRxVe92Sl96ScaSlmWQ8oZVn3jDXLEuumKje/W0H3aKB7RZYm575bRco6rvXG/wGz+Sb8Xkr+QwAAAABJRU5ErkJggg==";
    rarFileImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAjDElEQVR42t2deZAkeXXfP7/MrLunzzm6e5hjZ6Z3jj3YKwBhsyDAgAJjsVhhJIcsiPAfkiw7HBgcwhGyYHGEZXyALCPhxTbCNhK2ESYACWFYYCVYFuMF9p6FnV1mds6+q7qruo7M/D3/kZlVv8zKrK7q7uUPdUR3Z1VlZVb+Xr73fcf3vVK8xD93c3fuV+57x+nX3fLaE37OyW0ubTJWLIljoXzfBxt8H3x8bGx838fDB2x8/O5xOp7PvoNjLF6ryuL6imr7ur5/bvrJX/rY26/xV+jHeYmPb73hr/3My182dew3pWC/pdVojtk5C198RCsEhfZ9RECLQouPLzp4jI8WQUTQWihOFNno1Gm0ahQdxVhurDFjFz/7nfd+88Ov/nc/++xfFYGoYXZqNBpzjpP/Vd/XtwL57fbXaFzPZf3ZJXXpM88dnzowc+va8jqOp7BQgEKL4Pk+KAtf+/giIILnazQgBK9rhPGZfSzV1mnYm8zcMc5Nx49zYPoghWLeLR4oP5mfzF+Nnz/6M8yPIL3NIfbOetC/CgCirE7eUU9prR+oVCrXdy2Qdrt9e6ejP6W1vl1ErGHe47ou1etrrHzxKlMTEzz71HOMUUY0WI5Fp+2iEbAUW60Olm3hi9BstcnlcnQ8l47nkcvlKezL4RXbXL22wvxbpzhzy1nm5ueplCsoBUqp/gXLWGFJWcSuOCRDVDJIGEL66XpCViCgdLGYe6JYzL27UCg8sWOBiEh5c3PrAa3ll6H/4hP7IiK4rsvGYo31B6/jr3pU12rkXAtl2Xii6bg+WILvC57vIUrR8Ty0FrSCTsdFA75AYczBLimu1hc5/LoZTt92mrm5OcrlCo5jxz6PACLBhgJQunuJ/a9FiyaIKJTEV0PCV5WW4EkVPUewb3gwUaq38Do8ppJwP9UVkEZQSsjlc/99enLfrymltjJt/CCB1OtUbNt603bCiATScTtsXF6l9uVrsOLTabXIe7Dp1WhJEyVCzrawRGEpsC0bJeBYFpYVmDPHtlEo9k2WKE/lWWwvcuTe/YEw5ucplyvYttWvGRnCYJAwogVOEQaGMHSKMOitd58wzBe1Ct4oAkrk5+r1+tiOQV0pbGD/tnY11Iz61XWa31xlamyCF29cQW15rO6r8dpffyMKi0tfeI7NC3XabReUheWA39Y4ViCEZqeDYztMz1ZYbq6yvLLFodftZ+G2BeYPH6ZUKmFb/cII1yNVGPHXegZFuqsVtxOBmZLu/pIhdR2+KTqdKYzec8EnUF1rpqYty7IHekGDXqxUQCR7n8AD0nRcl/pSjfa315FNuPLCZcodReNQi7f8s7dRLpYplgvc8u470W2PfD6HY9mIpykV8wHMi6JSKmIXLDY7dRrtOlNvLHH6tpuZnz9MuVTGsW0sQyBCeCNrUEr6zFTvNVMzdGCmtEoIKTQtWgJzo1RMM5SW4E5XKiEMQZRGUCjpCUOr4FwKAo9SQAcYzI4Fsp1WBGbKpX6tivsXq1ibCtdtMi0ltg673PHOV9I+3+BHH3mUJ/7k+7jaRVse2nOxFORzDviCY1s4jgWOYnyuSE1qzNw7ydlz55ibm6dUKmNZajBmINlmiiHNlMQxgyRmoOLPxzCj9xEkFL4KH8gI67orgXRcl8bVdfRD6+Q7OTqbDaZVGUvBqb95G9PzMyw9dInLzevMvuplaK05/I6bOPDmeXILeQqTBSylsJTF5KFxOk6LxeoyE/fs4+StC8zNzVOulEfGjH4zJZlmysQMJWQCeB9mCJmYEWkGorpQNKxf7exUGJ7n0ViswrdqyJZiubbMvDOB0orc+DjyyCYcPcjcuxcYW5/j0NFDuJsdTr/pluD9TY+nPvZ9pOHTwuf65hItdwvnNQVOnj3F/OHATEWaYQpkW8zQ2ZihBmFG5JENwgzpfYoszIgeiPQMqAypJ85OzJTrudQXa+hvV3E8m3qjxkEq6Dw4voKWRl9usfZnl5h56zHGpsZ58dPnqT+zjlgK52AR1/dpLzbxlMaZsZGqR+4VRU6dvZm5+fkAwBOaEbthTRvxUrm2KmGmBrm24edJaoaKTGFkEffSZHWFcbWK9801rE2or20w6Re4fnAD9Tem4K4KnQOgfQ/vmU1Wv3QRv+Mx87rDFOcD8+MutegstShMlnALPjdqi9h35Fk4ZwjD2sZMDcIMtTPMkBEwwzRTfZhhCiO8IYbFEWckYYSubeebaxSsAhu1dfZbZWxlceDWeebvvgnf9+hUWzQu11j9yxcpPqtZaTzPzN85wdzPn+TyJ57Gtm1yZZsbzWW23C2cu4scO3uC2bl5yqV0zHhJXNsEZiQlG9eMdNdWG65tVzNM46cZCdSdUTBjc7FK86FVyrkS1y5c42hlEhsbq+TAkw38n3HZ+uI1nBNjTN01y4F7Xsa1Jy6x9tA1/M9ewPIsipUiDb9Dgxae26Fxj8/pMzcznxH0DW2mVMKbGtJM6aQ3ZZip3ukizVAxb8o0U0GyNG6mIs1Qqvc5d22yhEAzqtfW2PzGEqoBy1eWOVQcR6PwEfyOR6VdoPrCCvaJMj/4wiMsPXMF91KD/ZP7OfOLd1BUedzLTTpK2Mq7LHeWaN4pnDl7NjRTo7m28lNwbRnVtU0zUyo8tuwBqDcA13OpXl6l+tVFyqpArVaj0IEXuMLhO45R3oD8sqBa0Hm2RuWNR1l4111Mdspsfu4S2ApKFp0rDazJPIv16zQadeSWAsfOHgvMVLm8PWaMkA4Z6Noygms7KB0SmSkdBJCmMLpmSkXykL3BkK1Gg62r66x95ToFKbC0tELZt7hoLfLm++8jXyognqb6xxfxLtbxLzeRps/U/mnqn3gecTXa06ichZdTLK2tINJh4zafE2dOBLmpnbq28tK7toPSIdGDRAoTdHicUBhovWeBof2Ff/rfDq89eIOCynP9xnWcts/q2Cb3vvctyKrHlT8+z9ZanfKd02gbWPbZeG6V/FQJKg6iQeVtGp7Leq7Bpr3BldvqnDhzKsjahgD+00yHRJ7qwHSIbJ8OUaZrq0LM0KonDBFEB7Ud89p2rCFvXXjjzUdk9iPuqkt1tUbFK9CYbnPbL9+DWtNc/MzTLOsViq+axq1usZnfwhbwvnWZA3ccxipbeGuCl1cseRvUtzbZOC3cfPoMs7OzqZixnWvb/aeSONevFd136527tgMxQ7IxI2amlIK98LL+8Rt+/b3zNx299+rlRWzPRmuYGptm5tB+vLrL5c41bvr5M+w/eYjmVJ3xMzNIW+MttVj71HP46y5tW1iqr9HQG9TOaF525jhzhw9TKpZSXdvumkZPB6tA7cYaz331SfJbTuwCzQJSd42jRwKl/RUOv/4k5amx7sFVyolUdADVe1711f6GcG2TZkqpBKZvryOZ4nv8Q//3sZbTfnnjxU18D5xCDml5HHjlLAu/eJb8WIHWjS2Wv3YZS0Hl1CSl4/uofe0q3k8atNEsqio51+eZhWWOzx/HX20jW15mbSXp3SilOPXXz/Hs53/I0dp+HCfXLdRFtToRA0OkPxPtHC0x9Y6jnP/G4zQ3mjFzRuI9pIBvZXofR15+E5WJSr83lXBtCTMZYpopAZFApIVCjko5Pz+olJupIfWtJs1qnZyVx1Mu7WabfC7HyqNLWDmLE/fdTKfeYvNyDdY9Nn64EqTHbQspWax4G9R1jbWbXSqbOTb+9DLHCrNMlPejQujqW8RwpaOnxA5uz2mZwLIddLizFqNMamwHGYqeyogC90oTx7GwLvnMr032UkBmOiimWL3PpBF87XP1Bz9i5jXzTN95CCwr07VNmqlAGDJSHJIpENnymCiNsdFs42kNIri+xlaKpe/cQLc1x+87xfwvnOL6557HX2mDaLStqMo6m/4G7rkceRcqz/qcPnmW8pkpVNFG5Xq+hKiEz2+oriDIWJHy3Dj6uU6MuBBb1PBx8jXRmvGXz6AVHHjVyxiTkgFUxN5vbkTH8js+ftNj4/w6ta8vUZ6qUDqxb7BrGzNTYthC2R2oF3K5MJmocT2Nk7NwXRdyDpbjsPToDRA48bcXIG/hK1AVh+X2GrYWNk5rTpw5ypUvnuf0oTOM3bUf5/aJ3h21DSEBINR0Zl5/BH2kgWz6JkTEw9fEcQTI7S+QXxjD84SZc4eCOzW5+Ck2s2fOghNtSAP5TpPao6uUF8bRbuAMZGFGT6gSmjKGzp9kCiTn2DS2WoGZsKC15VIoOXhao9suhVKelR8usvnjKrYofPGpb23Q1HWqt3c4c/Ycs/PzVDs/wSo7qPF85KMYHlCk3YlEYSiMCGpy5Ty8PBdok2Rgj/QzTETA0+GioBJpksDH7XpoRpyRiNfxKoK2oL3SwrYtfFf3XNtUzJAwzRPcAXoEnlGmQDzXA8vC0xrX81EOdFyNYymUbeO2XRzbot3o4Izl8cc0G40N5LYcZ87ewuzcHOVSCdtyEKVQtuousO/5rL24TGtpC1vHM6GSyteRBJj3381Juo4kdpaka53BxzL31Fo49ppTiCVmHqBnUjMwo2umupmaPUid+KHK+iJ4WgesEPHwlYPSOkrxM35oHyu1NRruJvadRY6duynITRWDFHp3XVSPOfGjrz+J99QmlU4RS1RsoWKgnBBEv5srqdsMes04R9b5orqP+MLJ158OE46JeyTVtY2bqWiNFMKQXm+2QGxlseV1QpOlaDXblMsltPZpe5pyuQB5m8XmMo1OHf1qhxPn+tMhouIg+uJjF2g8tMjxg8eg0LvDJLxApVTXVZToLkOjFGiteoGbsa10mF8SCRdBBT6SgARvDBY62g7dHpEgTSzdbRAfchMOU7fMYBedwB+USBu6qB/mrzIwA9UVRi/TsEsMcX0fywponq7r4uQdWm2XvGNh2wptC6rss16rkn91mdPnFoII3Az6uosbXLClFD/+08e548C5rgsbAYbu3nja0Bgdy+pK5NqE22bAFTkAEoExCVc03Fkbd7eY2xLFIUJuokjprmmsik2r2QkZI4RcY1I1w8QMCevtkTGObqRdCUSEgPisQ7PledjKxvU1pakynuVxtX6D8VdMcOLcycziUtcFDQHTqQElIwgzTUiqeentK4ngLRlPJGOZ+LH6jyF9sYf0tCVY1fi5hASG9IK+JIDHhbEHGqJshd/W+OFn67gexYLF+MFxal6V+mad8dfu48QtJ5mbzyaxJR19Syzjru3HCdNhkkQknsSSvmhdGCysNPwwP0Pf88mscX/hrovpJm6HZkpCfFRKhk7BZ5usjouyLDzt0/Zccrkc2oGNTp31Zo3xn61w8uxCFzPSKn3Rxerw6lQ3A9uLts2FTUbhZv6ndwf3CyWpOV0NSAH24BwDBGScw1JGGkYkfgyd4tr2malAGFpk6DgkM/1uWRau74d3j4KcYvxQiWVvial7Jzlz7hyzc/OUigMqfcmyX59r2Csr6fCiulphCCx6XhvCENMspeBEdEfGtiMcMATQtU7mdkzlDM+rL1EYd21V9xoNzYhyOLs1WdGFeFo4eHia5c0lXlxd58Crp1m4NahnlEqDK31Jc2CG2X2malucSE8k7gwn0k1iD9iz0zNR6BqZqSzMiIQhGoaH9EGBoedjKYVdcFjaWGatvsHRt86wcMsCc/PDkdiCwk0C2CVjIQbhRN/iJp4bFSdSX090e/TVMyTRDaIyMSN+XaorEdlNpK4sGx+PyoEcN5abHH7rDDffciYwU0OT2IJOKS2m6xqYDZ3AhiRID8IJSQHWNJzIDARTj98js0U3kGVE4EkMEZ3m2pqa0SuaadFD57MyBZLfl6PaqXO5usShV09x+tYFDs3O7YjEZuKHXXL60hph8nRos9QXWceAPC7U9GNm7RvX5v7rE8MUk4kZYtT7BRmJmJUpkLbjcvXSFWbfPMuxMzcxOxtixogktiShYOrMQeSKxEyEbFNwSsUJc2F3IhBJO3+kWYKyFFigPelG6bF0fXidaZjRKxkLooP0ZfTurZ16WasvbjCWz3HzHTd3O5ccZ7T+jEj9teEFLbz9Vtp5v+ct6XA/LWhtmDRNtwNXhxqktYT/I7MXmo5wH9Hh4uleqiOwFoIWHT6vw2MH0X50TtEaCbME1eerfO23vsz//rX/Qb6QM1zfrqENkjMJ1zYwU0ESVYdBtaJHrBDZBYYsbax5N980z6HZORwrtzMSm0r49wiFcpH5v3uK+vfW8FfaiJ+481IDupRkYAwDVK9+geqmVbq88z6nIYFZKcnNU7OnEC3YdswDNkxYv2ubNFMq0pS9IMrlKuVHJosTd5dKZbTn74zE1geiAXezeKBC/k1FdN1D/PT0eUoFq59RmLKbJA4kKdWnmFAlq9AV9sC0vW6Oy7wxupoRAmAXwANiVlcYOqVWsyOB7D9S/KCyrDdbllqQPSCxJROfVs7GmrID3lSYN4pRdaKwPYyGtckOEXq8qeik3ffq9JYApMsy7F6K1miVYJgYEbiI4Hu95KNOodmSwAziqS3Qem/ikHf8zjtWb1y88WYlvLAr4nPX7e2RcLXWtOotvKZrNMD037HJGkdqwSlZqEq8N/tYku5JEXdtx+cmeumW2CXrVNe2qxnSY0tKN8W/PaoPpJIeOnaoWa83h3Jt04nPqj/rB1x+7CLXH7mEf8PFcq3YIve7pabrmJ0ElIxYJjUDEItdsoJFwdeat3387YZa9lIQaa6tMknc3U5eFU8F7YbbO6xrm9XTR0p9/8aFa7zw+WeYzR8iV8ohJYkVqCJTIfTMhiizzyKk3ygdrlG4rQOMitLhogKvK+YsKMLCVlCkEq1D0A3fFxWoxnJMnZrEciwsQ/t17EZR2ZgRmqmok3doDtAwApEYIWE4M6VDr8MK7Xg3J2Qp/t//epiz5YVutC5hgap3oYltISUHpUN/X4w6hcTzXjqpFUbVzyxcaYklNDWa8f0lZt9wGHvMYavR7hWozJp6wkyJ4U31zBS9npPdZnshaEdgB/0ZiIo3u0SrqgRWtJGpNevYZsZVYkEcpsky3mxSfyRuy2J+qll+7ZmyuLDM8/Q+h+rRSiWd1Jd0beNmSkLHWw1LOtlGQxpmcmtn4yokkSWxfKubfhfJsv3pNZBtscHM0MYSmikkh7QkZwrOKGWmaCTmXSVd26SZUgh6r3hZ6SW/0Xv6JHmH9i1cWip+QPIvUa5NS69IGjOlT9DEF1niBDmhZ6LFKOHGclNZmKHDkDQibsvwMnFGEcaOevokXjGLHuuBeaeMvFRmvSNNuFl5rgEaIvFSrBazImiYy5AdozMwI9AM1QuOlR4aQ0bsU2eknj5Ff1IuhhGpGpJRByHFBKWSHEjU2LM5WPSRtuNpnm46JMXqpLu20tMMMfpN9oLkMNjtjQsjs6dPEuDJAFOybWEq7Y4fTIZL1cCBr/eoPn0m2XQSMlzbLmZE3bpI0Kkrw9PfnaERZEfjKlR/fijmyu4BTmRogWQWqNIrkLECFUltNz3BWDIrFTMwGkYjLvGeYciuxlVYyYqhYOUctOi+iuHe4wTD1dATaRbTyOoe+87AQjEwJB0zTM1QAlrtQcWwv6a7PWYkJ7FJkl6jhamTM8iyjJTqSBXIwAJVNjlisDb2AkVlhbVw3S+ongmPu7YqNCXB41AY6JHc3tFmnYw4iS0ZqyFw9r7bcZUXW4yBwuij4sRgt8cUMSN6pC+uMc8TeUtpOCRKUX1xkwc/8nW+9IEvkcs7SF+Kv2em+jEDw0xJ2Lag9h7Uu63HI4yrsEJ3URscqYnZKU78vTNc//PLdNbaQdohxnNKRJW9bF1822wW6ds2KmRmkNdNCqoupUcS24RNSvtknDGBXM4OuF5Rqqfr3Oh+1xb6zJTSqovpsnfJxR2Oq1DxFGNUzxg/NkH5XRU6yy3E1WTUoxIjXAf3eCS7saSv4CV9nVepY2ATzBLX9WKMme4701xbUbGiXKQZQt8BdhcY7nhchZlzEsMeK8Eq2ZSOjhmjVIO/KgrEkjQJMYpU9NoVVCIdrwxN6iV7dEhJ6oW4XcekiwlGPYNeBN5pe6H2aKO33TRTKZihAs3oLtsIVNKhsr1Jn7wPMzLHVXRZS7GIJuoDMckSVvg3upEsTDOljOei41vxnm6JdZ0TduJhHs3qsqcDAVnGHWyJFbsWSzQoC+1rYv0eicAsDTNITiXd09TJLiaxRfWMbgo+tLOrl1c5/9UnWXl6GattJ/JcScqn9PemE+cOS5LAkDRpGU5DLAKXXgk4uAE0Wmv+/n99Vw8HxRR+umvb04zeTC6t1N5MA0qvjYw2iS0e0Cmqi2t87w+/w75GhaNjR5Gx7SqAwzAQzcWWofjCaRmDKFJ3ijbjc/uwwjksMWK4MZgmEzNCM6UMRsweYkiKmRp2Elsib6UseOSz32ayOUEuXwy4S7sivKVF79sFjQNyaCHJYezgOKfedoJcJUez2Y7RgMyO80zM6JqpwMNSInufXNQ7mcRm9VoNIsDceHaDg1MHuyAqKZnd7Kg6m3GYftdLJt8rLeDsMgFshVOyccZstJYuec8sIyit+l1bEmYqZNQEEx/20GSJ6dePNIlNIaJ6/jsQxoTpzPRRidFk8H7pn+yQXhOJVwx1aG96UKEMhoqkZotjrm3CTKkoAFUkp+PsUkNkZ5PYUjOxGG3HWQLZlv0+HE6ktSqkaV/8fBIrlClJXLgZgSdc28CE9eZlET02m4r2DkNGn8SmIlon/R7SIDM0XJY3nYKaVeBKUn3SaEdR7GJihHk9/W5vP2Z0mXvGY6X3aMRfTEnUKEOGA+9Dmf0W0aJqg3wd4QuDgTwVJ1JKwXHeVn9xi4RW6NSo3qgcqni21zyXQvW7toaZilxdpY0WO+1vy5RzRnF5R57ElhCG6TrrFOLzsDiRxBtStG8wTqTNy+o5H/H2A5Wepk9xbbuYEZopZSRFfd/H8/UegjoMPWTYzPMlic0m1Yfd4ERKO/Mg87dtS3W3wVRiZqHr4oskaKs6GzMMMyUQCMPzcD2bRmMvUicYAyOHGDLcm6gT2mVtaIllxfoskql3kh22kBlPkIITfa1yxrbuq6enzNoKj9FN66te+l6b/pdOcW0jrTHMlO/7eK5H2/UoFHJenFu1Yy8LRhoybGR3kxgwdWQK2cjqoMoA7T5CG/0tbX0DXrJxIpbQTPavm8MAVKJ2YhR30l3buJnSWuO5Ph3Xw/c8bNv+gVKTrd0VqHY0ZNic+GwshhZu/Vu34iu/B5K6N84i/ktsm8TzOmWf5Gs65Vja+E/ieNHlbi42ePTzj/HtT38Xx3FIuRe6Ebjp2irDtAbC8Oi4Lp1Oh3zeWXYs6z8ePTqxsUeR+vBDhmPjt8Wo4WmYP3sY6xccLv7ZRdobnf7pC1mpjgHEhUGxRRJXsqY/mOaxsd6ktuLha03hV3NGy5024jCdOG7cTLkdj3a7jRbNgQMTLqh/b9v+Z5RS/p6BepprO2gwvbIiexxoQqA9FnN3zrF/YYbNS5volu43KSleVF/xShLNbal9Jun9JZL8K+nniRp2QDMxP8bCK25iX6VCMZ9Ha40ffgkmOpigaoWzODzPo9Pp0GrZNFqtra98+Ssfe897fuNfDpOBHx7UdzSYHuy8g/YFt+n2hrRojVN2mDo3nbooyQWNL5SkVxYHVf6Swkk2/kj/cYTgTl9bW+f46SPc+6ZX4hRsWk0P0dDpeDRbHXw/aPcrFPIUCg5aa6q1DZaWVllbW1+/fOknf/De9/7Gh4alOYwA6mw/mD7RRiYizJyYwT3vceOpRexDOUoTJfLFPFFtzlys5JdoRVMSuw2WxAMzcxGVyc9PFM66dE/ihIRu2TUWnQfv9H2f6noVz21z1123A9BuuXQ6Hq12m3bHDYVhUSzkKRRz+L7H+voGi4urVGu1+sWLz3/8kYe/8VGgs+ckh1G/cylycW5/2+189+nvsfjDFZ575AUanU3abifVJKXSSVNiiH6cSAy+kPTWtUFtbUkvzvU9VhvL3PcPfw7rHot2OxBGu9Oh3XHxPB1qRo5C0UFEU61usLi4QmOrxdNPPf7Jj/3ev/lXKysrm6OUOJxhvSytRvvOpWjIcGVqH3f/gzu5+OcvUqwWEP9AbEJOr0LXmxhnjujrz0Glk+DSCG9ZcYZIfxduMvG51a7jLbWZPjBFu+PSbgeCaLU6wZcqA8VCkULBwfc11Wot0Izqxsb584998oO//Zv/nO2CjtEF0iBmUNJc25Tvz0gOpp88Osmtv7KP5soW4kk/wPYNNpZtp472ZU/FNHySWrHMxpdEGU5r1qpruE9uUpws0G67tNod2m0XLzRThUIuFIbPeiiM9fVq9eILF/7gU5984F8DdXbwM7TJynJt074/I20wvV1y2HdkPNAD3YfKKV206cC7ncBSu2wljVaUZMMbR9NCq9KieD0PDrRbHVpttwvgAWYEAL4emqnN+lb7woVnP/HVr3zhoy+88EKNHf6MBOqjfOdS9mB6RpzElt56PKinL7U/I43EFqY7xMjaBt8A3auL+NqnFQI4QKFQoFBwEBHW12ssLgXCePwHj/7n++9//4fYfpzJ3qXfR/nOpVGGDA+axLbduIqh+zMGkdiMrK0kJmdpLaEwFMVi4Nr62g8BfJXqerX63HPPfsIQhrzkAhGyXdtRB9OPOolt0LiK7fszhiOxJbO2yfybUoYwfD/QjMVV1qu1zZ9ceO7jX/3K1z+yEwDfRWAoA11bPcpg+hEnsQ0aV7Fdf8bwJLZ4pc+8essI+kSE6nrk2rZ59vzj/+VTn3zgw7vBjB1RSaOAKcu1HWUwffqQ4cGT2LLHVTBSf0YmiS2WtVUxxr1l290IPMKMam2j/tSTj/3h/R94/2/tFjN2jiEjfudSFmaMOolt8LiKDMwYkcRmVvqSU06VUmhfs16tsri4ytr6evUnL1z4+O//h3/7O3uBGTuiksqOvnMpfTD9qJPYBo2ryOzpG5HEllbpiwprvu+xXq1x48YqGxv15vMXfvzAXz70fz46agS+xxhifDXckN+5pAze0Evh2qLCQUzD9mcMIrElKn06qvR5Hq7rs7i4Sr3edJ944vufvP8D7/8XewXgu8xljfadSy+1a2sKY1TXdrtKn+cFOatOx8XJ5VlcXK3++Mfn/9P9H9h9nLFrgdi27YKbG/k7l7ZxbYedxDZoXMVuXNtBlT6347K1tUWxVERENi5c+NED//Mzf/IRoNnz94bjheypQCqVilvbbLyIcFKP+J1L27m2w05iGzSuYjvXdhQSW5cd4nq0Wm02ajXuuftu/d3vPPLp3/vdD3/8+vXrLcLvdUhZeEnZlhhvon//dAXooycGdXYbcB588EH1qle+cnJycvoVWsSJVnrbSWzJL+gaNNdwu+JSCgku7XF/niqeuiftcYKQ4Loe7XaHzXod7Xealy49/9l/8p5/9Pv1el2ACjAW/lbC37LxW0r8FoECkAPy4X87/LWytMxJEB5U+JwDOA8//HDuj/7o05995zt/aeLo0eNvzxcKs0jvPekDJgc9l/b1UpIehG4j2FThDJmA7BdGAOK2bQPS7rQbV55+5om/+NAHf/tzwDQwadzppgboxLb564e/HuCm/I9e80wtUoZmxIRhSDV35MiR8fe9730Lk5MzUyLacV3P9jzX1lqU1r6ltSjf92ytw/hX+5aENkpEVPQbvNbbFon3C/vJ5zXh545n1Eb90X2THUMTqAJkEVGiwxm+7WbDf/LJJzcffvhbS81ms5UwN4ME4hv/dWLBO6EQ2uF/89czhBi7QMswV3YkjFA4yf+m6kWaZRuCtYxtlVBR83kG/M8CT7XNY9kGXCXjcd84lgwsGEUggzTEC193zXOkXZy5uKaQLON52+B0WYl9lfFacvGTz5MhnKwFH0ZAowhhGHAmoSXJ501zlfacacK0YaJMIXY/w3YmIO2uTlvctMcqRdDJbQa8Z7vPtxvXcxgtGSSgtPfoAY/TtG1HF7XTn53c6T+tzyg73Fd2eayhfv4/7xXMV8nYoHEAAAAASUVORK5CYII=";
})();
// RESOURCES //

alert = function (msg, cancel, botoes, retorno)
{
    var div = document.createElement("div");
    div.className = "chagas-alert";
    div.bloqueio = document.createElement("div");
    div.bloqueio.className = "chagas-bloqueio";
    var texto = document.createElement("div");
    texto.className = "texto";
    texto.innerHTML = msg;
    div.appendChild(texto);

    if (botoes && !Array.isArray(botoes))
    {
        botoes = [botoes];
    }
    else if ((!botoes || botoes.length == 0) && (!cancel || cancel.length == 0))
    {
        botoes = ["OK"];
    }
    var botaoCancelar;
    var btFoco;

    if (botoes)
    {
        for (var x = 0; x < botoes.length; x++)
        {
            var btn = document.createElement("button");
            btn.className = "chagas-button";
            btn.type = "button";
            btn.innerHTML = botoes[x];
            if ((!cancel || cancel.length == 0) && x == 0)
            {
                btn.style.marginRight = "25px";
            }
            if (x == 0)
            {
                btn.style.marginLeft = "25px";
            }
            btn.index = x;
            btn.onclick = function (e)
            {
                if (retorno)
                {
                    retorno(this.index);
                }
                div.close();
            };
            div.appendChild(btn);
            if (x == botoes.length - 1)
            {
                btFoco = btn;
            }
        }
    }
    if (cancel && cancel.length > 0)
    {
        botaoCancelar = document.createElement("button");
        botaoCancelar.className = "chagas-button";
        botaoCancelar.type = "button";
        botaoCancelar.innerHTML = cancel;
        botaoCancelar.style.marginRight = "25px";
        if (!botoes || botoes.length == 0)
        {
            botaoCancelar.style.marginLeft = "25px";
        }
        botaoCancelar.onclick = function (e)
        {
            if (retorno)
            {
                retorno(-1);
            }
            div.close();
        };
        div.appendChild(botaoCancelar);
        if (!btFoco)
        {
            btFoco = botaoCancelar;
        }
    }

    document.body.appendChild(div.bloqueio);
    document.body.appendChild(div);

    if (btFoco)
    {
        btFoco.focus();
    }
    div.close = function ()
    {
        removeClass(div, "show");
        removeClass(div.bloqueio, "show");
        setTimeout(function ()
        {
            div.bloqueio.parentElement.removeChild(div.bloqueio);
            div.parentElement.removeChild(div);
        }, 200);
    };

    div.onkeydown = function (evt)
    {
        evt = evt || window.event;
        if (evt.keyCode == 9)
        {
            var ccs = this.querySelectorAll("button");
            if (!evt.shiftKey)
            {
                if (document.activeElement == ccs[ccs.length - 1])
                {
                    ccs[0].focus();
                    evt.preventDefault();
                }
            }
            else
            {
                if (document.activeElement == ccs[0])
                {
                    ccs[ccs.length - 1].focus();
                    evt.preventDefault();
                }
            }
        }
    }
    div.onkeyup = function (evt)
    {
        evt = evt || window.event;
        if (evt.keyCode == 27)
        {
            if (retorno)
            {
                retorno(-1);
            }
            this.close();
        }
    };

    addClass(div.bloqueio, "show");
    addClass(div, "show");

    window.addEventListener("resize", function ()
    {
        div.style.left = (window.innerWidth - div.clientWidth) / 2 + "px";
        div.style.top = (window.innerHeight - div.clientHeight) / 2 + "px";
    });
    div.style.left = (window.innerWidth - div.clientWidth) / 2 + "px";
    div.style.top = (window.innerHeight - div.clientHeight) / 2 + "px";
    return div;
};

document.showBloqueio = function (show, loading)
{
    if (!this.divBloqueio && show)
    {
        this.divBloqueio = document.createElement("div");
        this.divBloqueio.className = "chagas-bloqueio";
        this.divBloqueio.tabIndex = 1;

        document.addEventListener("keydown", function (evt)
        {
            evt = evt || window.event;
            if (evt.keyCode == 9 && this.divBloqueio && this.divBloqueio.parentElement && containsClass(this.divBloqueio, "show"))
            {
                this.divBloqueio.focus();
                evt.preventDefault();
            }
        });

        this.divBloqueio.onkeydown = function (evt)
        {
            evt = evt || window.event;
            if (evt.keyCode == 9)
            {
                this.focus();
                evt.preventDefault();
            }
        };

        var dvImg = document.createElement("div");
        dvImg.className = "chagas-noselect";

        //dvImg.style.cssText = "width:50px; height:50px; top:calc(50% - 65px); margin:0 auto; position:relative; background-repeat:no-repeat; background-position:-25px -25px;";
        //dvImg.style.backgroundImage = "url(" + loadingGifImage + ")";

        var dvLoadingAnima = document.createElement('chagas-loading-anim');
        dvLoadingAnima.className = 'chagas-loading-anim';


        dvImg.appendChild(dvLoadingAnima);

        this.divBloqueio.appendChild(dvImg);
        this.divBloqueio.loadingImg = dvImg;

        var divProgressOut = document.createElement("div");
        divProgressOut.style.cssText = "position:relative; height:41px; width:50%; margin:0 auto; top:calc(50% - 50px); display:none; ";
        var divTextoProgress = document.createElement("div");
        divTextoProgress.className = "chagas-font";
        divTextoProgress.style.cssText = "width:100%; height:20px; line-height:20px; text-align:center; color:white; font-weight:bold; font-size:15px; text-shadow:1px 1px " + corBorder + ";";
        divTextoProgress.innerHTML = "Enviando 2 de 30 arquivos";
        divProgressOut.appendChild(divTextoProgress);
        var divProgress = document.createElement("div");
        divProgress.style.cssText = "float:left; width:100%; height:20px; background-color:" + corLinha + "; padding:0; margin-top:1px; border:1px solid " + corBorder + ";";
        divProgress.className = "chagas-borderbox";
        divProgressOut.appendChild(divProgress);

        var divProgressInner = document.createElement("div");
        divProgressInner.style.cssText = "float:left; height:19px; margin:0; width:0; background-color:" + corOver + ";";
        divProgress.appendChild(divProgressInner);

        var divPercentual = document.createElement("div");
        divPercentual.className = "chagas-font";
        divPercentual.style.cssText = "position:absolute; left:0; top:21px; width:100%; height:20px; line-height:20px; color:gray; font-weight:bold; font-size:13px;";
        divPercentual.innerHTML = "0%";
        divProgress.appendChild(divPercentual);

        this.divBloqueio.appendChild(divProgressOut);
        divProgressOut.divProgressInner = divProgressInner;
        divProgressOut.divPercentual = divPercentual;
        divProgressOut.text = divTextoProgress;
        this.divBloqueio.divProgressoOut = divProgressOut;
    }
    if (show)
    {
        this.divBloqueio.loadingImg.style.display = loading ? "" : "none";

        document.body.appendChild(this.divBloqueio);
        var that = this;
        setTimeout(function ()
        {
            addClass(that.divBloqueio, "show");
            that.divBloqueio.focus();
        }, 1);
    }
    else if (this.divBloqueio && this.divBloqueio.parentElement)
    {
        var that = this;
        removeClass(that.divBloqueio, "show");
        setTimeout(function ()
        {
            that.divBloqueio.style.transitionDuration = "";
            that.divBloqueio.style.transitionProperty = "";
            that.divBloqueio.style.transitionTimingFunction = "";
            if (that.divBloqueio.parentElement)
            {
                that.divBloqueio.parentElement.removeChild(that.divBloqueio);
            }
        }, 200);
    }
};

document.showLoading = function (show)
{
    this.showBloqueio(show, true);
};

function addClass(element, classe)
{
    var clss = element.className.split(" ");
    for (var x = 0; x < clss.length; x++)
    {
        if (clss[x] == classe)
        {
            return;
        }
    }
    element.className = element.className + " " + classe;
}

function apenasNumeros(valor)
{
    var numeros = valor.split('');
    var retorno = "";
    for (var i = 0; i < numeros.length; i++)
    {
        if (numeros[i] != " " && numeros[i] != "-" && numeros[i] != "(" && numeros[i] != ")" &&
            !isNaN(numeros[i]))
        {
            retorno += numeros[i];
        }
    }
    return retorno;
}

function containsClass(element, classe)
{
    var clss = element.className.split(" ");
    for (var x = 0; x < clss.length; x++)
    {
        if (clss[x] == classe)
        {
            return true;
        }
    }
    return false;
}

function removeClass(element, classe)
{
    var clss = element.className.split(" ");
    var nclassName = "";
    var ok = false;
    for (var x = 0; x < clss.length; x++)
    {
        if (clss[x] == classe)
        {
            ok = true;
        }
        else
        {
            nclassName += clss[x] + " ";
        }
    }
    if (!ok)
    {
        return;
    }
    if (nclassName.length > 0)
    {
        nclassName = nclassName.substring(0, nclassName.length - 1);
    }
    element.className = nclassName;
}

function formatarData(date, formato)
{
    var d = date.getDate();
    d = d < 10 ? '0' + d : d;
    var M = date.getMonth() + 1;
    M = M < 10 ? '0' + M : M;
    var y = date.getFullYear();
    var h = date.getHours();
    h = h < 10 ? '0' + h : h;
    var h2 = h > 12 ? h - 12 : h;
    h2 = h2 < 10 ? '0' + h2 : h2;
    var m = date.getMinutes();
    m = m < 10 ? '0' + m : m;
    var s = date.getSeconds();
    s = s < 10 ? '0' + s : s;
    var ret = formato.replace("dd", d)
                     .replace("MM", M)
                     .replace("yyyy", y)
                     .replace("yy", (y + '').substring(2, 4))
                     .replace("HH", h)
                     .replace("hh", h2)
                     .replace("mm", m)
                     .replace("ss", s);
    return ret;
}

function formatarDecimal(value, decimais)
{
    var val = value;
    var p = val.split(',');
    p[0] = apenasNumeros(p[0]);
    while (p[0][0] == "0")
    {
        p[0] = p[0].substring(1, p[0].length);
    }
    if (p[0].length == 0)
    {
        p[0] = "0";
    }
    var d = decimais ? decimais : 0;
    if (d > 0)
    {
        var p1 = "0"
        if (p.length > 1)
        {
            p1 = apenasNumeros(p[1]);
            while (p1.length < d)
            {
                p1 = p1 + "0";
            }
            p1 = p1.substring(0, d);
        }
        else
        {
            while (p1.length < d)
            {
                p1 = p1 + "0";
            }
            p1 = p1.substring(0, d);
        }
        for (var i = p[0].length - 3; i > 0; i -= 3)
        {
            p[0] = p[0].substring(0, i) + "." + p[0].substring(i, p[0].length);
        }
        val = p[0] + ',' + p1;
    }
    else
    {
        val = p[0];
    }
    return val;
}

function toBool(valor)
{
    if (!valor)
    {
        return false;
    }
    if (valor == 1 || valor == "1" || (valor.toLowerCase && valor.toLowerCase() == "true"))
    {
        return true;
    }
    return false;
}

function toDate(valor)
{
    valor = valor.trim();
    for (var x = 0; x < valor.length; x++)
    {
        if (isNaN(parseInt(valor[x])))
        {
            if (valor[x] != ' ' && valor[x] != ':' && valor[x] != '.')
            {
                valor = valor.substring(0, x) + "/" + valor.substring(x + 1, valor.length);
            }
        }
    }
    if (valor.indexOf("/") == -1)
    {
        if (valor.length >= 8)
        {
            valor = valor.substring(0, 2) + "/" + valor.substring(2, 4) + "/" + valor.substring(4, valor.length);
        }
        else if (valor.length >= 6)
        {
            valor = valor.substring(0, 2) + "/" + valor.substring(2, 4) + "/" + valor.substring(4, valor.length);
        }
    }
    if (valor.indexOf("/") > -1)
    {
        var pathsH = valor.split(' ');
        if (pathsH.length > 0)
        {
            var paths = pathsH[0].split('/');
            if (paths.length > 2)
            {
                var y = paths[2];
                if (isNaN(parseInt(y)))
                {
                    y = new Date().getFullYear();
                }
                if (y.length <= 2)
                {

                    if (parseInt(y) > 30)
                    {
                        y = "19" + y;
                    }
                    else
                    {
                        y = "20" + y;
                    }
                }
                var m = isNaN(parseInt(paths[1])) ? new Date().getMonth() : parseInt(paths[1]) - 1;
                var d = isNaN(parseInt(paths[0])) || parseInt(paths[0]) == 0 ? new Date().getDate() : parseInt(paths[0]);
                var d2 = new Date(parseInt(y), m + 1, 0).getDate()
                if (d2 < d)
                {
                    d = d2;
                }
                var hr = 0;
                var min = 0;
                var seg = 0;
                var mseg = 0;
                if (pathsH.length > 1)
                {
                    var time = pathsH[1].split(':');
                    if (time.length > 0)
                    {
                        hr = isNaN(parseInt(time[0])) ? 0 : parseInt(time[0]);
                    }
                    if (time.length > 1)
                    {
                        min = isNaN(parseInt(time[1])) ? 0 : parseInt(time[1]);
                    }
                    if (time.length > 2)
                    {
                        if (time[2].indexOf(".") > -1)
                        {
                            var mss = time[2].split(".");
                            seg = isNaN(parseInt(mss[0])) ? 0 : parseInt(mss[0]);
                            mseg = isNaN(parseInt(mss[1])) ? 0 : parseInt(mss[1]);
                        }
                        else
                        {
                            seg = isNaN(parseInt(time[2])) ? 0 : parseInt(time[2]);
                        }
                    }
                }
                return new Date(parseInt(y), m, d, hr, min, seg, mseg);
            }
        }
        return new Date();
    }
}

function RedimensionarImagem(mapImage, qualidade, altura, largura, retorno, tamanhoOriginal)
{
    var img = new Image();
    img.addEventListener('load', function ()
    {
        var MAX_HEIGHT = altura ? altura : 1000;// parseInt(document.getElementById("alturaText").value);
        var MAX_WIDTH = largura ? largura : 1000;//parseInt(document.getElementById("larguraText").value);
        var QUALI = qualidade ? qualidade : 0.7;//parseFloat(document.getElementById("qualidadeText").value);

        if (img.height > img.width)
        {
            var x = MAX_WIDTH;
            MAX_WIDTH = MAX_HEIGHT;
            MAX_HEIGHT = x;
        }
        var width = img.width;
        var height = img.height;
        if (!tamanhoOriginal)
        {
            if (width > height)
            {
                if (height > MAX_HEIGHT)
                {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }
            else
            {
                if (width > MAX_WIDTH)
                {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            }
        }

        var canvas = document.createElement("canvas");
        canvas.width = width;
        canvas.height = height;
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, width, height);
        ctx.drawImage(img, 0, 0, width, height);
        img = null;
        retorno(canvas.toDataURL('image/jpeg', QUALI));
    });
    img.setAttribute("src", mapImage);
}

function base64toBlob(base64Data, contentType)
{
    contentType = contentType || '';
    var sliceSize = 1024;
    var byteCharacters = atob(base64Data);
    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);

    for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex)
    {
        var begin = sliceIndex * sliceSize;
        var end = Math.min(begin + sliceSize, bytesLength);

        var bytes = new Array(end - begin);
        for (var offset = begin, i = 0 ; offset < end; ++i, ++offset)
        {
            bytes[i] = byteCharacters[offset].charCodeAt(0);
        }
        byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, { type: contentType });
}
