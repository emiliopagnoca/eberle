﻿$(document).ready(function () {

    //$('.hover-historia').click(function () {
    //    $('.newsletter').addClass('invert');
    //    $('#historia').addClass('show');
    //});
    $('body').attr('nextpage', 'QueMolda.aspx');

    $('#home .box .titulo').css('opacity', '0');
    $('#home .box .detalhes').css('opacity', '0');
    $('#home .box .fundos > div').css('opacity', '0');


    //carrega barra loading
    var carregou = 5;
    setInterval(function () {
        if (carregou <= 100) {
            $('#home .loading span:eq(0)').css('width', carregou + '%');
            carregou = carregou + 5;
        }
    }, 1000);


    $(window).load(function () {
        carregou = 100;
        $('#home .loading span:eq(0)').css('width', '100%');
        setTimeout(function () {
            $('#home .loading').fadeOut();
            setTimeout(function () {
                $('#home .box').addClass('etapa');
                setTimeout(function () {
                    $('#home .box').removeClass('inicio');
                    $('#home .box').removeClass('etapa');
                    setTimeout(function () {
                        $('#home').removeClass('fundo');
                        $('#home .box .titulo').css('opacity', '1');
                        $('#home .box .a-historia').removeClass('inicio');
                        $('#home .box .o-futuro').removeClass('inicio');
                        $('header').removeClass('hidden');
                        setTimeout(function () {
                            $('#home .box .detalhes').css('opacity', '1');

                            
                            //$('header .menu .logo').css('opacity', '1');
                            //setTimeout(function () {
                            //    $('header .menu .left').css('opacity', '1');
                            //    $('header .menu .left').css('right', '0px');
                            //    $('header .menu .right').css('opacity', '1');
                            //    $('header .menu .right').css('left', '0px');
                                
                            //}, 500);

                            setTimeout(function () {
                                $('#home .social').css('opacity', '1');
                                $('#home .social').css('bottom', '45px');
                                $('#home .pimenta').css('opacity', '1');
                                $('.newsletter').css('right', '-330px');
                                $('#home .globo').css('opacity', '1');
                                $('#home .info').css('opacity', '1');
                            }, 200);

                        }, 1000);
                    }, 800);

                }, 500);
            }, 1000);
        }, 1000);
    });

    $("#home .fundos .a-historia video").prop('muted', true);
    $("#home .fundos .que-molda video").prop('muted', true);
    $("#home .fundos .o-futuro video").prop('muted', true);

    $('#home .hover-abre .hover-historia .som').click(function () {
        if ($("#home .fundos .a-historia video").prop('muted')) {
            $("#home .fundos .a-historia video").prop('muted', false);
            $("#home .hover-abre .hover-historia .som").removeClass('off');
            $("#home .hover-abre .hover-historia .som").addClass('on');
        } else {
            $("#home .fundos .a-historia video").prop('muted', true);
            $("#home .hover-abre .hover-historia .som").removeClass('on');
            $("#home .hover-abre .hover-historia .som").addClass('off');
        }
    });
    $('#home .hover-abre .hover-molda .som').click(function () {
        if ($("#home .fundos .que-molda video").prop('muted')) {
            $("#home .fundos .que-molda video").prop('muted', false);
            $("#home .hover-abre .hover-molda .som").removeClass('off');
            $("#home .hover-abre .hover-molda .som").addClass('on');
        } else {
            $("#home .fundos .que-molda video").prop('muted', true);
            $("#home .hover-abre .hover-molda .som").removeClass('on');
            $("#home .hover-abre .hover-molda .som").addClass('off');
        }
    });
    $('#home .hover-abre .hover-futuro .som').click(function () {
        if ($("#home .fundos .o-futuro video").prop('muted')) {
            $("#home .fundos .o-futuro video").prop('muted', false);
            $("#home .hover-abre .hover-futuro .som").removeClass('off');
            $("#home .hover-abre .hover-futuro .som").addClass('on');
        } else {
            $("#home .fundos .o-futuro video").prop('muted', true);
            $("#home .hover-abre .hover-futuro .som").removeClass('on');
            $("#home .hover-abre .hover-futuro .som").addClass('off');
        }
    });

    $('.hover-abre .hover-historia').hover(function (e) {
        e.preventDefault();
        setTimeout(function () {
            $('#home .box .fundos .a-historia').css('opacity', '1');
            $('#home .box div .imagem').css('opacity', '0');
            $('.a-historia').addClass('efeito');
            $('.detalhes img:eq(0)').addClass('efeito');
            setTimeout(function () {
                $('#home .box div .mascara').addClass('borda');
                $('#home .box > .a-historia').css('opacity', '0.8');
                $('#home .box > .que-molda').css('opacity', '0.2');
                $('#home .box > .o-futuro').css('opacity', '0.2');
                setTimeout(function () {
                    if ($('.hover-abre .hover-historia:hover').length != 0) {
                        $('#home .fundos .a-historia video').css('opacity', '1');
                        $('#home .fundos .a-historia video').trigger('play');
                    }
                }, 1500);
            }, 50);
        }, 50);
    }, function () {
        
    });

    $('.hover-abre .hover-molda').hover(function (e) {
        e.preventDefault();
        setTimeout(function () {
            $('#home .box .fundos .que-molda').css('opacity', '1');
            $('#home .box div .imagem').css('opacity', '0');
            $('.que-molda').addClass('efeito');
            $('.detalhes img:eq(0)').addClass('efeito');
            setTimeout(function () {
                $('#home .box div .mascara').addClass('borda');
                $('#home .box > .que-molda').css('opacity', '0.8');
                $('#home .box > .a-historia').css('opacity', '0.2');
                $('#home .box > .o-futuro').css('opacity', '0.2');
                setTimeout(function () {
                    if ($('.hover-abre .hover-molda:hover').length != 0) {
                        $('#home .fundos .que-molda video').css('opacity', '1');
                        $('#home .fundos .que-molda video').trigger('play');
                    }
                }, 1500);
            }, 50);
        }, 50);
    }, function () {
       
    });

    $('.hover-abre .hover-futuro').hover(function (e) {
        e.preventDefault();
        setTimeout(function () {
            $('#home .box .fundos .o-futuro').css('opacity', '1');
            $('#home .box div .imagem').css('opacity', '0');
            $('.o-futuro').addClass('efeito');
            $('.detalhes img:eq(0)').addClass('efeito');
            setTimeout(function () {
                $('#home .box div .mascara').addClass('borda');
                $('#home .box > .o-futuro').css('opacity', '0.8');
                $('#home .box > .a-historia').css('opacity', '0.2');
                $('#home .box > .que-molda').css('opacity', '0.2');
                setTimeout(function () {
                    if ($('.hover-abre .hover-futuro:hover').length != 0) {
                        $('#home .fundos .o-futuro video').css('opacity', '1');
                        $('#home .fundos .o-futuro video').trigger('play');
                    }
                }, 1500);
            }, 50);
        }, 50);
    }, function () {
        
    });


    $('.hover-abre div').on('mouseleave', function () {
        $('.a-historia').removeClass('efeito');
        $('.detalhes img:eq(0)').removeClass('efeito');
        $('#home .box .fundos .a-historia').css('opacity', '0');
        $('#home .fundos .a-historia video').css('opacity', '0');
        $('#home .fundos .a-historia video').trigger('pause');
        $('#home .box div .mascara').removeClass('borda');
        $('#home .box div .imagem').css('opacity', '1');
        $('#home .box > .a-historia').css('opacity', '1');
        $('#home .box > .que-molda').css('opacity', '1');
        $('#home .box > .o-futuro').css('opacity', '1');

        $('.que-molda').removeClass('efeito');
        $('.detalhes img:eq(0)').removeClass('efeito');
        $('#home .box .fundos .que-molda').css('opacity', '0');
        $('#home .fundos .que-molda video').css('opacity', '0');
        $('#home .fundos .que-molda video').trigger('pause');

        $('.o-futuro').removeClass('efeito');
        $('.detalhes img:eq(0)').removeClass('efeito');
        $('#home .box .fundos .o-futuro').css('opacity', '0');
        $('#home .fundos .o-futuro video').css('opacity', '0');
        $('#home .fundos .o-futuro video').trigger('pause');
    });


    //$('.hover-efeito .hover-historia').hover(function (e) {
    //    e.preventDefault();
    //    $('.a-historia').addClass('efeito');
    //    $('.detalhes img:eq(0)').addClass('efeito');
    //}, function (e) {
    //    e.preventDefault();
    //    $('.a-historia').removeClass('efeito');
    //    $('.detalhes img:eq(0)').removeClass('efeito');
    //});
    //$('.hover-efeito .hover-molda').hover(function (e) {
    //    e.preventDefault();
    //    $('.que-molda').addClass('efeito');
    //    $('.detalhes img:eq(1)').addClass('efeito');
    //}, function (e) {
    //    e.preventDefault();
    //    $('.que-molda').removeClass('efeito');
    //    $('.detalhes img:eq(1)').removeClass('efeito');
    //});
    //$('.hover-efeito .hover-futuro').hover(function (e) {
    //    e.preventDefault();
    //    $('.o-futuro').addClass('efeito');
    //}, function (e) {
    //    e.preventDefault();
    //    $('.o-futuro').removeClass('efeito');
    //});

    $(document).mousemove(function (event) {
        var x = event.pageX / 20;

    });

});