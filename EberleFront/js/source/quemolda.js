﻿var galeriaSlider;
$(document).ready(function () {

    $('body').attr('nextpage', '');
    $('header').addClass('collapsed');
    setTimeout(function () {
        $('header').removeClass('hidden');
    }, 200);
    
    $('.tratamentodiv').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });
    
    $('.hiddetbtn').click(function (e) {
        e.preventDefault();

        $('#' + $(this).attr('hiddet')).parent().find('.hiddeninner').removeClass('active');
        $('#' + $(this).attr('hiddet')).parent().addClass('active');
        $('#' + $(this).attr('hiddet')).addClass('active');
        $('html, body').animate({
            scrollTop: $('#' + $(this).attr('hiddet')).parent().offset().top - 100
        }, 500);
    });

    $('.iconitem.card.hiddetbtn').click(function () {
        $('.iconitem.card.hiddetbtn').removeClass('active');
        $(this).addClass('active');
    });

    $('.hiddendets.gbg .closebtn').click(function (e) {
        $('.iconitem.card.hiddetbtn').removeClass('active');
    });

    $('.hiddendets .closebtn').click(function (e) {
        e.preventDefault();

        $(this).parent().removeClass('active');
        $(this).parent().find('.hiddeninner').removeClass('active');
    });

    $('.galeriabtn').click(function (e) {
        e.preventDefault();
        
        //$('.linhaout .linhainner').removeClass('active');
        //$(this).parent().parent().addClass('active');
        $('#galeriaModal').modal('show');
        setTimeout(function () {
            if (!galeriaSlider) {
                galeriaSlider = $('.galeriadiv').slick({
                    dots: true,
                    infinite: true,
                    speed: 500,
                    fade: true,
                    cssEase: 'linear'
                });
            }
        }, 200);
    });

    $('html').mousemove(function (e) {

        var vx = parseInt((e.pageX - (window.innerWidth / 2)) / 100);
        console.log(vx);
        //window.

        $('.moveelem').each(function () {
            //$(this)
            //$(this).css('right', vx * $(this).attr('move'));
            $(this).css({
                "-webkit-transform": "translate(" + (vx * parseInt($(this).attr('move'))) + "px, 0)",
                "-ms-transform": "translate(" + (vx * parseInt($(this).attr('move'))) + "px, 0)",
                "transform": "translate(" + (vx * parseInt($(this).attr('move'))) + "px, 0)"
            });

            //({ "transform": "translate(" + (vx * parseInt($(this).attr('move'))) + "px)" });​
        });
    });
});