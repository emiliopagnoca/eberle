window.onload = function(){
    create_select();
    create_select2();
    init();
    getScreenWidth();
    $(".setorSelect").html("<option value=''>Setor</option>");
};

var objEstado = {
    "ac": [{
        "nome": "Acre",
        "capital": "Rio Branco",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
        }],
    "al": [{
        "nome": "Alagoas",
        "capital": "Maceió",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "ap": [{
        "nome": "Amapá",
        "capital": "Macapá",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "am": [{
        "nome": "Amazonas",
        "capital": "Manaus",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
        }],
    "ba": [{
        "nome": "Bahia",
        "capital": "Salvador",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "ce": [{
        "nome": "Ceará",
        "capital": "Fortaleza",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "df": [{
        "nome": "Distrito Federal",
        "capital": "Brasília",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "es": [{
        "nome": "Espírito Santo",
        "capital": "Vitória",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "go": [{
        "nome": "Goiás",
        "capital": "Goiânia",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "ma": [{
        "nome": "Maranhão",
        "capital": "São Luís",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "mt": [{
        "nome": "Mato Grosso",
        "capital": "Cuiabá",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "ms": [{
        "nome": "Mato Grosso do Sul",
        "capital": "Campo Grande",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "mg": [{
        "nome": "Minas Gerais",
        "capital": "Belo Horizonte",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "pr": [{
        "nome": "Paraná",
        "capital": "Curitiba",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "pb": [{
        "nome": "Paraíba",
        "capital": "João Pessoa",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "pa": [{
        "nome": "Pará",
        "capital": "Belém",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "pe": [{
        "nome": "Pernambuco",
        "capital": "Recife",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "pi": [{
        "nome": "Piauí",
        "capital": "Terezina",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "rj": [{
        "nome": "Rio de Janeiro",
        "capital": "Rio de Janeiro",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "rn": [{
        "nome": "Rio Grande do Norte",
        "capital": "Natal",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "rs": [{
        "nome": "Rio Grande do Sul",
        "capital": "Porto Alegre",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "ro": [{
        "nome": "Rondônia",
        "capital": "Porto Velho",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "rr": [{
        "nome": "Roraima",
        "capital": "Boa Vista",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "sc": [{
        "nome": "Santa Catarina",
        "capital": "Florianópolis",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "se": [{
        "nome": "Sergipe",
        "capital": "Aracaju",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "sp": [{
        "nome": "São Paulo",
        "capital": "São Paulo",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }],
    "to": [{
        "nome": "Tocantins",
        "capital": "Palmas",
        "text1": "Unidade Eberle",
        "text2": "Mundial S/A - Produtos de Consumo",
        "loc": "BR 116 KM. 144,3 nº 5.000 Bairro São Ciro",
        "uf": "Caxias do Sul | RS"
    }]
};

//carrega os dados dos países - substituir pelo response do ajax;
var objPais = [
    {
        name: 'Brasil',
        value: 'bra'
    },
    {
        name: 'Argentina',
        value: 'arg'
    },
    {
        name: 'África do Sul',
        value: 'afr'
    },
    {
        name: 'Bolívia',
        value: 'bol'
    },
    {
        name: 'Chile',
        value: 'chi'
    },
    {
        name: 'Colômbia',
        value: 'col'
    },
    {
        name: 'Costa Rica',
        value: 'cos'
    },
    {
        name: 'Equador',
        value: 'equ'
    },
    {
        name: 'Estados Unidos',
        value: 'eua'
    },
    {
        name: 'Guatemala',
        value: 'gua'
    },
    {
        name: 'Honduras',
        value: 'hon'
    },
    {
        name: 'México',
        value: 'mex'
    },
    {
        name: 'Panamá',
        value: 'pan'
    },
    {
        name: 'Paraguai',
        value: 'par'
    },
    {
        name: 'Peru',
        value: 'per'
    },
    {
        name: 'Venezuela',
        value: 'ven'
    }
];

var objEstadoUF =
    [
        { key: "AC", value: "Acre" },
        { key: "AL", value: "Alagoas" },
        { key: "AP", value: "Amapá" },
        { key: "AM", value: "Amazonas" },
        { key: "BA", value: "Bahia" },
        { key: "CE", value: "Ceará" },
        { key: "DF", value: "Distrito Federal" },
        { key: "ES", value: "Espírito Santo" },
        { key: "GO", value: "Goiás" },
        { key: "MA", value: "Maranhão" },
        { key: "MT", value: "Mato Grosso" },
        { key: "MS", value: "Mato Grosso do Sul" },
        { key: "MG", value: "Minas Gerais" },
        { key: "PA", value: "Pará" },
        { key: "PB", value: "Paraíba" },
        { key: "PR", value: "Paraná" },
        { key: "PE", value: "Pernambuco" },
        { key: "PI", value: "Piauí" },
        { key: "RJ", value: "Rio de Janeiro" },
        { key: "RN", value: "Rio Grande do Norte" },
        { key: "RS", value: "Rio Grande do Sul" },
        { key: "RO", value: "Rondônia" },
        { key: "RR", value: "Roraima" },
        { key: "SC", value: "Santa Catarina" },
        { key: "SP", value: "São Paulo" },
        { key: "SE", value: "Sergipe" },
        { key: "TO", value: "Tocantins" }
    ];

//region popula o select de países com os dados recebidos
$("#country").html("<option value=''>selecione o país</option>");
for(i = 0; objPais.length > i; i++) {
    $(".countrySelect").append("<option value='" + objPais[i].value + "'>" + objPais[i].name + "</option>");
}
//endregion

//region popula o select de países com os dados recebidos
$("#state").html("<option value=''>selecione o estado</option>");
for(i = 0; objEstadoUF.length > i; i++) {
    $(".stateSelect").append("<option value='"+objEstadoUF[i].key+"'>" + objEstadoUF[i].value + "</option>");
}
//endregion

//region pega o tamanho da tela do usuário para responsividade do js
function getScreenWidth() {
    var x = screen.availWidth;
    if(x <= 768){
        $("#map_brasil").remove();
        return true;
    } else {
        return false;
    }
}
//endregion

//region select customizado - cria o select
function create_select(){
    var li = new Array();
    var div_cont_select = document.querySelectorAll("[data-mate-select='active']");
    var select_ = '';
    for (var e = 0; e < div_cont_select.length; e++) {
        div_cont_select[e].setAttribute('data-indx-select',e);
        div_cont_select[e].setAttribute('data-selec-open','false');
        var ul_cont = document.querySelectorAll("[data-indx-select='"+e+"'] > .cont_list_select_mate > ul");
        select_ = document.querySelectorAll("[data-indx-select='"+e+"'] >select")[0];
        var select_optiones = select_.options;
        document.querySelectorAll("[data-indx-select='"+e+"']  > .selected ")[0].setAttribute('data-n-select',e);
        document.querySelectorAll("[data-indx-select='"+e+"']  > .icon_select_mate ")[0].setAttribute('data-n-select',e);
        for (var i = 0; i < select_optiones.length; i++) {
            li[i] = document.createElement('li');
            if (select_optiones[i].selected == true || select_.value == select_optiones[i].innerHTML ) {
                li[i].className = 'active';
                document.querySelector("[data-indx-select='"+e+"']  > .selected ").innerHTML = select_optiones[i].innerHTML;
            };
            li[i].setAttribute('data-index',i);
            li[i].setAttribute('data-selec-index',e);
            li[i].addEventListener( 'click', function(){  _select_option(this.getAttribute('data-index'),this.getAttribute('data-selec-index')); });
            li[i].innerHTML = select_optiones[i].innerHTML;
            ul_cont[0].appendChild(li[i]);

        };
    };
};

function create_select2(){
    var li = new Array();
    var div_cont_select = document.querySelectorAll("[data-mate-select-state='active']");
    var select_2 = '';
    for (var e = 0; e < div_cont_select.length; e++) {
        div_cont_select[e].setAttribute('data-indx-select-state',e);
        div_cont_select[e].setAttribute('data-select-open-state','false');
        var ul_cont = document.querySelectorAll("[data-indx-select-state='"+e+"'] > .cont_list_select_mate > ul");
        select_2 = document.querySelectorAll("[data-indx-select-state='"+e+"'] >select")[0];
        var select_optiones = select_2.options;
        document.querySelectorAll("[data-indx-select-state='"+e+"']  > .selected ")[0].setAttribute('data-n-select-state',e);
        document.querySelectorAll("[data-indx-select-state='"+e+"']  > .icon_select_mate ")[0].setAttribute('data-n-select-state',e);
        for (var i = 0; i < select_optiones.length; i++) {
            li[i] = document.createElement('li');
            if (select_optiones[i].selected == true || select_2.value == select_optiones[i].innerHTML ) {
                li[i].className = 'active';
                document.querySelector("[data-indx-select-state='"+e+"']  > .selected ").innerHTML = select_optiones[i].innerHTML;
            };
            li[i].setAttribute('data-index-state',i);
            li[i].setAttribute('data-selec-index-state',e);
            li[i].addEventListener( 'click', function(){  _select_option2(this.getAttribute('data-index-state'),this.getAttribute('data-selec-index-state')); });
            li[i].innerHTML = select_optiones[i].innerHTML;
            ul_cont[0].appendChild(li[i]);

        };
    };
};
//endregion

//region select customizado - abrir o select
function open_select(idx){

    var idx1 =  idx.getAttribute('data-n-select');
    var ul_cont_li = document.querySelectorAll("[data-indx-select='"+idx1+"'] .cont_select_int > li");
    var hg = 0;
    var slect_open = document.querySelectorAll("[data-indx-select='"+idx1+"']")[0].getAttribute('data-selec-open');
    var slect_element_open = document.querySelectorAll("[data-indx-select='"+idx1+"'] select")[0];

    for (var i = 0; i < ul_cont_li.length; i++) {
        hg += ul_cont_li[i].offsetHeight;
    };

    if (slect_open == 'false') {
        document.querySelectorAll("[data-indx-select='"+idx1+"']")[0].setAttribute('data-selec-open','true');
        document.querySelectorAll("[data-indx-select='"+idx1+"'] > .cont_list_select_mate > ul")[0].style.height = hg+"px";
        document.querySelectorAll("[data-indx-select='"+idx1+"'] > .icon_select_mate")[0].style.transform = 'rotate(180deg)';
    }else{
        document.querySelectorAll("[data-indx-select='"+idx1+"']")[0].setAttribute('data-selec-open','false');
        document.querySelectorAll("[data-indx-select='"+idx1+"'] > .icon_select_mate")[0].style.transform = 'rotate(0deg)';
        document.querySelectorAll("[data-indx-select='"+idx1+"'] > .cont_list_select_mate > ul")[0].style.height = "0px";
    }

}

function open_select2(idx){

    var idx1 =  idx.getAttribute('data-n-select-state');
    var ul_cont_li = document.querySelectorAll("[data-indx-select-state='"+idx1+"'] .cont_select_int > li");
    var hg = 0;
    var slect_open = document.querySelectorAll("[data-indx-select-state='"+idx1+"']")[0].getAttribute('data-selec-open-state');
    var slect_element_open = document.querySelectorAll("[data-indx-select-state='"+idx1+"'] select")[0];

    for (var i = 0; i < ul_cont_li.length; i++) {
        hg += ul_cont_li[i].offsetHeight;
    };

    if (slect_open == 'false' || slect_open == null) {
        document.querySelectorAll("[data-indx-select-state='"+idx1+"']")[0].setAttribute('data-selec-open-state','true');
        document.querySelectorAll("[data-indx-select-state='"+idx1+"'] > .cont_list_select_mate > ul")[0].style.height = hg+"px";
        document.querySelectorAll("[data-indx-select-state='"+idx1+"'] > .icon_select_mate")[0].style.transform = 'rotate(180deg)';
    }else{
        document.querySelectorAll("[data-indx-select-state='"+idx1+"']")[0].setAttribute('data-selec-open-state','false');
        document.querySelectorAll("[data-indx-select-state='"+idx1+"'] > .icon_select_mate")[0].style.transform = 'rotate(0deg)';
        document.querySelectorAll("[data-indx-select-state='"+idx1+"'] > .cont_list_select_mate > ul")[0].style.height = "0px";
    }

}
//endregion

//region select customizado - sair do select
function exit_select(indx){
    var select_ = document.querySelectorAll("[data-indx-select='"+indx+"'] > select")[0];
    document.querySelectorAll("[data-indx-select='"+indx+"'] > .cont_list_select_mate > ul")[0].style.height = "0px";
    document.querySelector("[data-indx-select='"+indx+"'] > .icon_select_mate").style.transform = 'rotate(0deg)';
    document.querySelectorAll("[data-indx-select='"+indx+"']")[0].setAttribute('data-selec-open','false');
}

function exit_select2(indx){
    var select_2 = document.querySelectorAll("[data-indx-select-state='"+indx+"'] > select")[0];
    document.querySelectorAll("[data-indx-select-state='"+indx+"'] > .cont_list_select_mate > ul")[0].style.height = "0px";
    document.querySelector("[data-indx-select-state='"+indx+"'] > .icon_select_mate").style.transform = 'rotate(0deg)';
    document.querySelectorAll("[data-indx-select-state='"+indx+"']")[0].setAttribute('data-selec-open-state','false');
}
//endregion

//region select customizado - selecionar opção
function _select_option(indx,selc){
    var select_ = document.querySelectorAll("[data-indx-select='"+selc+"'] > select")[0];

    var li_s = document.querySelectorAll("[data-indx-select='"+selc+"'] .cont_select_int > li");
    var p_act = document.querySelectorAll("[data-indx-select='"+selc+"'] > .selected")[0].innerHTML = li_s[indx].innerHTML;
    var select_optiones = document.querySelectorAll("[data-indx-select='"+selc+"'] > select > option");
    for (var i = 0; i < li_s.length; i++) {
        if (li_s[i].className == 'active') {
            li_s[i].className = '';
        };
        li_s[indx].className = 'active';

    };
    select_optiones[indx].selected = true;
    select_.selectedIndex = indx;
    select_.onchange();
    exit_select(selc);
    setMap(indx, 'country');
}

function _select_option2(indx,selc){
    var select_2 = document.querySelectorAll("[data-indx-select-state='"+selc+"'] > select")[0];

    var li_s2 = document.querySelectorAll("[data-indx-select-state='"+selc+"'] .cont_select_int > li");
    var p_act2 = document.querySelectorAll("[data-indx-select-state='"+selc+"'] > .selected")[0].innerHTML = li_s2[indx].innerHTML;
    var select_optiones2 = document.querySelectorAll("[data-indx-select-state='"+selc+"'] > select > option");
    for (var i = 0; i < li_s2.length; i++) {
        if (li_s2[i].className == 'active') {
            li_s2[i].className = '';
        };
        li_s2[indx].className = 'active';

    };
    select_optiones2[indx].selected = true;
    select_2.selectedIndex = indx;
    select_2.onchange();
    exit_select2(selc);
    setMap(indx, 'state');
}
//endregion

//region seta o mapa do país de acordo com a escolha do selectbox.
function setMap(indx, tp) {

    console.log('selecionado',indx);
    console.log('tipo',tp);

    if(indx !== "0"){
        if(tp == 'state'){
            $(".title_map_inter").attr("style","display: block");
            var title_state = $("#state option:selected").text();
            $("#title_state").text(title_state);
            $('html, body').animate({ scrollTop: $('#map_brasil_mobile').offset().top }, 'slow');
        } else {
            var pais = $("#country option:selected").val();
            var pais_titulo = $("#country option:selected").text();
            var pais_img = "img/" + pais + ".png";


            //remove o plano de fundo mapa mundi.
            $(".bg-contact").attr("style","background-image: none");

            if(pais == "bra"){
                //se país for brasil, carrega o svg.

                //tratativa mobile para pais = brasil.
                if(screen.availWidth <= 768){
                    $("#map_brasil_mobile").attr("style","display: block");
                    $("#select_mate_state").attr("style","display: block");
                    $(".box_select").attr("style","display: none");
                    //$(".representantes_inter").attr("style","display: block");
                } else {
                    $("#map_brasil").attr("style","display: block");
                    $(".box_select").attr("style","display: block");
                    //$(".representantes_inter").attr("style","display: none");
                }

                $("#map_inter").attr("style","display: none");
                $("#select_mate").attr("style","display: none");
                $('html, body').animate({ scrollTop: $('.bg-contact').offset().top }, 'slow');
            } else {
                //caso seja outro país, remove o svg e carrega as imagens padroes dos paises.

                //tratativa mobile para pais inter
                if(screen.availWidth <= 768){
                    $("#map_brasil_mobile").attr("style","display: none");
                    $("#select_mate_state").attr("style","display: none");
                } else {
                    $("#map_brasil").attr("style","position: absolute; z-index: -1;");
                }

                $("#map_inter").attr("style","");
                $("#img_inter").attr("src", pais_img);
                $("#title_inter").html(pais_titulo);
                $("#select_mate").attr("style","display: block");
                $('html, body').animate({ scrollTop: $('#map_inter').offset().top }, 'slow');
            }
        }
    }
}
//endregion

//region envia o formulário de contato
function sendData() {
    var formData = new Array();

    formData.push($("textarea")[0].value);
    for(i = 0; $("input").length > i; i++){
        formData.push($("input")[i].value);
    }
    window.alert("Formulário: " + JSON.stringify(formData));
}
//endregion

//region função para tratar o hover de estados do svg
function init() {

    var i = j = k = l = m = 0;
    var D = document.getElementById('E');
    SVGDoc = D.getSVGDocument();
    SVGRoot = SVGDoc.documentElement;
    svgns = 'http://www.w3.org/2000/svg';
    var estados = SVGRoot.getElementsByTagName('path');
    var qdeEstados = estados.length;

    for ( ; i<qdeEstados; i++ ) {
        estados[i].onmouseover = function(evt) {
            var x = evt.pageX;
            var y = evt.pageY - 25;
            this.style.fill = '#c99792';
            var siglaEstado = this.parentNode.id;

            var html = '<div class="title_map_inter">\n' +
            '<h1>'+objEstado[siglaEstado][0].nome+'</h1>\n' +
            '<p>'+objEstado[siglaEstado][0].text1+'</p>\n' +
            '<p>'+objEstado[siglaEstado][0].text2+'</p>\n' +
            '<p>'+objEstado[siglaEstado][0].loc+'</p>\n' +
            '<p>'+objEstado[siglaEstado][0].uf+'</p></div>';

            $('#nome_estado').html(html).css({
                top: '300px',
                right: '10px',
                display: 'block',
                padding: '0 0.6em',
                border: '2px solid white',
                'border-radius': '10px',
                boxShadow: '4px 4px 6px #444',
                background: '#ffffff',
                position: 'absolute',
                'width': '250px',
                'min-height': '150px',
            });
        };

        estados[i].onmouseout = function() {
            var fillColor = this.getAttribute('fill');
            this.style.fill = fillColor;
            $('#nome_estado').html('').css({
                border: 'none',
                display: 'none'
            });
            $('#capital_estado').html('').css({
                border: 'none',
            });
        }
    }

    var carregando = $('#carregando')
    var conteudoAjax = $('#conteudoAjax')

    for ( ; m<qdeEstados; m++ ) {
        estados[m].onclick = function() {
            var estadoClicado = this.parentNode.id;
            console.log(estadoClicado);
            window.alert('Estado clicado: ' + estadoClicado.toUpperCase());
        }
    }

}
//endregion