﻿var lastScrollTop = 0;

$(document).ready(function () {
    $('.mobilemenubtn').click(function () {
        $('.mobilemenu').toggleClass('open');
    });

    $('.mobilemenu .closebtn').click(function (e) {
        e.preventDefault();
        $('.mobilemenu').removeClass('open');
    });

    $(".animsition").animsition({
        inClass: 'fade-in-up-lg',
        outClass: 'fade-out-up-lg',
        inDuration: 1000,
        outDuration: 800,
        linkElement: '.animsition-link',
        // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
        loading: false,
        loadingParentElement: 'body', //animsition wrapper element
        loadingClass: 'animsition-loading',
        loadingInner: '', // e.g '<img src="loading.svg" />'
        timeout: false,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: ['animation-duration', '-webkit-animation-duration'],
        // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
        // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
        overlay: false,
        overlayClass: 'animsition-overlay-slide',
        overlayParentElement: 'body',
        transition: function (url) { window.location.href = url; }
    });

    $('.scrolltop').click(function () {
        $("html, body").animate({ scrollTop: 0 }, 600);
    });



    $(window).on('mousewheel', function (event) {
        if ($('body').attr('nextpage')) {
            if (event.deltaY < 0) {
                if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                    //window.location.href = $('body').attr('nextpage');
                    $('.animsition').animsition('out', $('.animsition'), $('body').attr('nextpage'));
                }
            }
        }
    });


    //$(window).scroll(function (event) {
    //    var st = $(this).scrollTop();
    //    if (st > lastScrollTop) {
    //        // downscroll code
    //        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
    //            window.location.href = $('body').attr('nextpage');
    //        }
    //    } else {
    //        // upscroll code
    //    }
    //    lastScrollTop = st;
    //});
});
